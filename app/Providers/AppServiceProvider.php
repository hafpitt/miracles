<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use App\Models\BarangKategori;
use App\Models\Cart;
use App\Models\CmsContact as Contact;
use App\Models\CmsPageImage;
use Auth;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
      View::share('front_barang_kategori', BarangKategori::get());
      View::share('front_cms_contact', Contact::first());
      View::share('front_all_top_parallax', CmsPageImage::where('nama','=','all_top_parallax')->first());
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
