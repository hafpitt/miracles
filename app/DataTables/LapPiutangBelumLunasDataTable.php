<?php

namespace App\DataTables;

use App\Models\HTransaksi;
use Yajra\Datatables\Services\DataTable;

class LapPiutangBelumLunasDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables->eloquent($this->query());
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        // Post::with('user')->select('posts.*');
        // $query = HTransaki::query()->select($this->getColumns());
        $query = HTransaksi::select($this->getColumns())->where('sisa_bayar','<>',0);
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */

    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    // ->minifiedAjax('')
                    ->ajax('')
                    ->parameters($this->getBuilderParameters());
    }

    // public function ajax()
    // {
    //   return $this->datatables
    //       ->eloquent($this->query())
    //       ->editColumn('subtotal', '{{ $subtotal->asCurrency() }}')
    //       ->editColumn('tax', '{{ $tax->asCurrency() }}')
    //       ->editColumn('total', '{{ $total->asCurrency() }}')
    //       ->editColumn('created_at', function($row) { return $row->created_at->diffForHumans(); })
    //       ->addColumn('action', function($order) {
    //           return $this->renderActions($order->id, 'admin.orders.show');
    //       })
    //       ->make(true);
    // }

    protected function getBuilderParameters()
    {
      return [
        'paging' => true,
        'searching' => true,
        'dom' => "<'dt_colVis_buttons'B><'dt-uikit-header'<'uk-grid'<'uk-width-medium-2-3'l><'uk-width-medium-1-3'f>>><'uk-overflow-container'tr><'dt-uikit-footer'<'uk-grid'<'uk-width-medium-3-10'i><'uk-width-medium-7-10'p>>>",
        'order'   => [[0, 'desc']],
        'buttons' => [
            'colvis',
            'copy',
            'csv',
            'excel',
            'pdf',
            'print'
        ]
      ];
    }
    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
      		'no_invoice',
      		'tgl',
      		'grand_total',
      		'terbayar',
      		'sisa_bayar',
        ];
        // return [
        //     'id'   => ['data' => 'usersid', 'name' => 'users.id'],
        //     'name' => ['data' => 'usersname', 'name' => 'users.name']
        // ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'lappiutangbelumlunas_' . time();
    }
}
