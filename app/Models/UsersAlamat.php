<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 15 Sep 2017 19:34:40 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UsersAlamat
 * 
 * @property int $id
 * @property string $nama
 * @property string $alamat
 * @property string $kodepos
 * @property string $nama_penerima
 * @property string $telepon
 * @property int $id_users
 * @property int $utama
 * @property string $deleted_at
 * @property int $kota_id
 * 
 * @property \App\Models\User $user
 * @property \App\Models\Kotum $kotum
 * @property \Illuminate\Database\Eloquent\Collection $h_transaksis
 *
 * @package App\Models
 */
class UsersAlamat extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'users_alamat';
	public $timestamps = false;

	protected $casts = [
		'id_users' => 'int',
		'utama' => 'int',
		'kota_id' => 'int'
	];

	protected $fillable = [
		'nama',
		'alamat',
		'kodepos',
		'nama_penerima',
		'telepon',
		'id_users',
		'utama',
		'kota_id'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'id_users');
	}

	public function kotum()
	{
		return $this->belongsTo(\App\Models\Kotum::class, 'kota_id');
	}

	public function h_transaksis()
	{
		return $this->hasMany(\App\Models\HTransaksi::class);
	}
}
