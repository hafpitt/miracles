<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 26 Aug 2017 15:52:59 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Modul
 * 
 * @property int $id
 * @property string $modul
 * @property string $url
 * 
 * @property \Illuminate\Database\Eloquent\Collection $departments
 *
 * @package App\Models
 */
class Modul extends Eloquent
{
	protected $table = 'modul';
	public $timestamps = false;

	protected $fillable = [
		'modul',
		'url'
	];

	public function departments()
	{
		return $this->belongsToMany(\App\Models\Department::class, 'department_has_modul');
	}
}
