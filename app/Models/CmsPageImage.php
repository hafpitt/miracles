<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 17 Nov 2017 02:18:20 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class CmsPageImage
 * 
 * @property int $id
 * @property string $nama
 * @property string $url_photo
 *
 * @package App\Models
 */
class CmsPageImage extends Eloquent
{
	protected $table = 'cms_page_image';
	public $timestamps = false;

	protected $fillable = [
		'nama',
		'url_photo'
	];
}
