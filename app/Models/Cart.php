<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 26 Aug 2017 15:52:59 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Cart
 *
 * @property int $id
 * @property float $qty
 * @property int $id_cust
 * @property int $id_barang
 * @property int $id_barang_warna
 * @property int $check_out
 *
 * @property \App\Models\Barang $barang
 * @property \App\Models\BarangWarna $barang_warna
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $d_transaksis
 *
 * @package App\Models
 */
class Cart extends Eloquent
{
	protected $table = 'cart';
	public $timestamps = false;

	protected $casts = [
		'qty' => 'float',
		'id_cust' => 'int',
		'id_barang' => 'int',
		'id_barang_warna' => 'int',
		'check_out' => 'int'
	];

	protected $fillable = [
		'qty',
		'id_cust',
		'id_barang',
		'id_barang_warna',
		'check_out'
	];

	public function barang()
	{
		return $this->belongsTo(\App\Models\Barang::class, 'id_barang');
	}

	public function barang_warna()
	{
		return $this->belongsTo(\App\Models\BarangWarna::class, 'id_barang_warna');
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'id_cust');
	}

	public function d_transaksis()
	{
		return $this->hasMany(\App\Models\DTransaksi::class, 'id_cart');
	}
}
