<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 14 Mar 2018 05:15:10 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class User
 *
 * @property int $id
 * @property string $email
 * @property string $password
 * @property string $nama
 * @property string $notelp
 * @property string $gender
 * @property string $remember_token
 * @property string $verified
 * @property string $url_photo
 * @property int $department_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 *
 * @property \App\Models\Department $department
 * @property \Illuminate\Database\Eloquent\Collection $banks
 * @property \Illuminate\Database\Eloquent\Collection $carts
 * @property \Illuminate\Database\Eloquent\Collection $h_piutangs
 * @property \Illuminate\Database\Eloquent\Collection $h_transaksis
 * @property \Illuminate\Database\Eloquent\Collection $users_alamats
 * @property \Illuminate\Database\Eloquent\Collection $barangs
 *
 * @package App\Models
 */
class User extends Authenticatable
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'department_id' => 'int',
		'current_unique' => 'float'
	];

	protected $hidden = [
		'password',
		'remember_token'
	];

	protected $fillable = [
		'email',
		'password',
		'nama',
		'notelp',
		'gender',
		'remember_token',
		'verified',
		'url_photo',
		'department_id',
		'current_unique'
	];

	public function department()
	{
		return $this->belongsTo(\App\Models\Department::class);
	}

	public function banks()
	{
		return $this->belongsToMany(\App\Models\Bank::class, 'bank_users', 'id_users', 'id_bank')
					->withPivot('id', 'no_rekening', 'nama_pemilik', 'cabang', 'deleted_at');
	}

	public function carts()
	{
		return $this->hasMany(\App\Models\Cart::class, 'id_cust');
	}

	public function h_piutangs()
	{
		return $this->hasMany(\App\Models\HPiutang::class, 'id_operator');
	}

	public function h_transaksis()
	{
		return $this->hasMany(\App\Models\HTransaksi::class, 'id_cust');
	}

	public function users_alamats()
	{
		return $this->hasMany(\App\Models\UsersAlamat::class, 'id_users');
	}

	public function barangs()
	{
		return $this->belongsToMany(\App\Models\Barang::class, 'users_has_barang', 'users_id')
					->withPivot('comment', 'rating', 'tgl');
	}
}
