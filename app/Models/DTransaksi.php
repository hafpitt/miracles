<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 17 Jan 2019 15:35:47 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class DTransaksi
 * 
 * @property int $id
 * @property float $harga
 * @property int $id_cart
 * @property int $id_h_transaksi
 * @property int $barang_id
 * @property int $barang_warna_id
 * @property float $qty
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property string $reff_no
 * @property float $subtotal
 * 
 * @property \App\Models\Barang $barang
 * @property \App\Models\BarangWarna $barang_warna
 * @property \App\Models\Cart $cart
 * @property \App\Models\HTransaksi $h_transaksi
 *
 * @package App\Models
 */
class DTransaksi extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'd_transaksi';

	protected $casts = [
		'harga' => 'float',
		'id_cart' => 'int',
		'id_h_transaksi' => 'int',
		'barang_id' => 'int',
		'barang_warna_id' => 'int',
		'qty' => 'float',
		'subtotal' => 'float'
	];

	protected $fillable = [
		'harga',
		'id_cart',
		'id_h_transaksi',
		'barang_id',
		'barang_warna_id',
		'qty',
		'reff_no',
		'subtotal'
	];

	public function barang()
	{
		return $this->belongsTo(\App\Models\Barang::class);
	}

	public function barang_warna()
	{
		return $this->belongsTo(\App\Models\BarangWarna::class);
	}

	public function cart()
	{
		return $this->belongsTo(\App\Models\Cart::class, 'id_cart');
	}

	public function h_transaksi()
	{
		return $this->belongsTo(\App\Models\HTransaksi::class, 'id_h_transaksi');
	}
}
