<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 11 Sep 2017 15:38:27 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Provinsi
 * 
 * @property int $id
 * @property string $nama
 * @property string $deleted_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $kota
 *
 * @package App\Models
 */
class Provinsi extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'provinsi';
	public $timestamps = false;

	protected $fillable = [
		'nama'
	];

	public function kota()
	{
		return $this->hasMany(\App\Models\Kotum::class, 'id_provinsi');
	}
}
