<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 26 Aug 2017 15:52:59 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class DepartmentHasModul
 * 
 * @property int $department_id
 * @property int $modul_id
 * 
 * @property \App\Models\Department $department
 * @property \App\Models\Modul $modul
 *
 * @package App\Models
 */
class DepartmentHasModul extends Eloquent
{
	protected $table = 'department_has_modul';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'department_id' => 'int',
		'modul_id' => 'int'
	];

	public function department()
	{
		return $this->belongsTo(\App\Models\Department::class);
	}

	public function modul()
	{
		return $this->belongsTo(\App\Models\Modul::class);
	}
}
