<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 17 Nov 2017 02:41:48 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class CmsContact
 * 
 * @property int $id
 * @property string $alamat
 * @property string $notelp
 * @property string $email
 *
 * @package App\Models
 */
class CmsContact extends Eloquent
{
	protected $table = 'cms_contact';
	public $timestamps = false;

	protected $fillable = [
		'alamat',
		'notelp',
		'email'
	];
}
