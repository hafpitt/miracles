<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 25 Feb 2018 12:35:07 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class DPiutang
 * 
 * @property int $id
 * @property float $dibayar
 * @property int $id_h_transaksi
 * @property int $id_h_piutang
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \App\Models\HPiutang $h_piutang
 * @property \App\Models\HTransaksi $h_transaksi
 *
 * @package App\Models
 */
class DPiutang extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'd_piutang';

	protected $casts = [
		'dibayar' => 'float',
		'id_h_transaksi' => 'int',
		'id_h_piutang' => 'int'
	];

	protected $fillable = [
		'dibayar',
		'id_h_transaksi',
		'id_h_piutang'
	];

	public function h_piutang()
	{
		return $this->belongsTo(\App\Models\HPiutang::class, 'id_h_piutang');
	}

	public function h_transaksi()
	{
		return $this->belongsTo(\App\Models\HTransaksi::class, 'id_h_transaksi');
	}
}
