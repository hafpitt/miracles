<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 26 Aug 2017 19:22:14 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class BarangFoto
 * 
 * @property int $id
 * @property string $nama
 * @property string $url_photo
 * @property int $utama
 * @property int $id_barang
 * 
 * @property \App\Models\Barang $barang
 *
 * @package App\Models
 */
class BarangFoto extends Eloquent
{
	protected $table = 'barang_foto';
	public $timestamps = false;

	protected $casts = [
		'utama' => 'int',
		'id_barang' => 'int'
	];

	protected $fillable = [
		'nama',
		'url_photo',
		'utama',
		'id_barang'
	];

	public function barang()
	{
		return $this->belongsTo(\App\Models\Barang::class, 'id_barang');
	}
}
