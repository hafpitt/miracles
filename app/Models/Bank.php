<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 26 Aug 2017 15:52:58 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Bank
 * 
 * @property int $id
 * @property string $nama
 * @property string $url_photo
 * @property string $deleted_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $users
 *
 * @package App\Models
 */
class Bank extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'bank';
	public $timestamps = false;

	protected $fillable = [
		'nama',
		'url_photo'
	];

	public function users()
	{
		return $this->belongsToMany(\App\Models\User::class, 'bank_users', 'id_bank', 'id_users')
					->withPivot('id', 'no_rekening', 'nama_pemilik');
	}
}
