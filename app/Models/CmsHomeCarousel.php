<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 17 Nov 2017 02:18:02 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class CmsHomeCarousel
 * 
 * @property int $id
 * @property string $nama
 * @property string $keterangan
 * @property string $url_photo
 *
 * @package App\Models
 */
class CmsHomeCarousel extends Eloquent
{
	protected $table = 'cms_home_carousel';
	public $timestamps = false;

	protected $fillable = [
		'nama',
		'keterangan',
		'url_photo'
	];
}
