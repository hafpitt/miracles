<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 27 Aug 2017 08:16:50 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UsersHasBarang
 * 
 * @property int $users_id
 * @property int $barang_id
 * @property string $comment
 * @property int $rating
 * @property \Carbon\Carbon $tgl
 * 
 * @property \App\Models\Barang $barang
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class UsersHasBarang extends Eloquent
{
	protected $table = 'users_has_barang';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'users_id' => 'int',
		'barang_id' => 'int',
		'rating' => 'int'
	];

	protected $dates = [
		'tgl'
	];

	protected $fillable = [
		'comment',
		'rating',
		'tgl'
	];

	public function barang()
	{
		return $this->belongsTo(\App\Models\Barang::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'users_id');
	}
}
