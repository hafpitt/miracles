<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 26 Aug 2017 15:52:59 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Department
 * 
 * @property int $id
 * @property string $nama
 * @property string $deleted_at
 * @property int $isdefault
 * @property int $isadmin
 * 
 * @property \Illuminate\Database\Eloquent\Collection $barangs
 * @property \Illuminate\Database\Eloquent\Collection $moduls
 * @property \Illuminate\Database\Eloquent\Collection $users
 *
 * @package App\Models
 */
class Department extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'department';
	public $timestamps = false;

	protected $casts = [
		'isdefault' => 'int',
		'isadmin' => 'int'
	];

	protected $fillable = [
		'nama',
		'isdefault',
		'isadmin'
	];

	public function barangs()
	{
		return $this->belongsToMany(\App\Models\Barang::class, 'barang_has_department')
					->withPivot('harga');
	}

	public function moduls()
	{
		return $this->belongsToMany(\App\Models\Modul::class, 'department_has_modul');
	}

	public function users()
	{
		return $this->hasMany(\App\Models\User::class);
	}
}
