<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 11 Sep 2017 15:38:17 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Kecamatan
 * 
 * @property int $id
 * @property string $nama
 * @property string $deleted_at
 *
 * @package App\Models
 */
class Kecamatan extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'kecamatan';
	public $timestamps = false;

	protected $fillable = [
		'nama'
	];
}
