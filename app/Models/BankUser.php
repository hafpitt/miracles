<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 09 Dec 2017 08:25:21 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class BankUser
 * 
 * @property int $id
 * @property int $id_bank
 * @property int $id_users
 * @property string $no_rekening
 * @property string $nama_pemilik
 * @property string $cabang
 * @property string $deleted_at
 * 
 * @property \App\Models\Bank $bank
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $h_transaksis
 *
 * @package App\Models
 */
class BankUser extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	public $timestamps = false;

	protected $casts = [
		'id_bank' => 'int',
		'id_users' => 'int'
	];

	protected $fillable = [
		'id_bank',
		'id_users',
		'no_rekening',
		'nama_pemilik',
		'cabang'
	];

	public function bank()
	{
		return $this->belongsTo(\App\Models\Bank::class, 'id_bank');
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'id_users');
	}

	public function h_transaksis()
	{
		return $this->hasMany(\App\Models\HTransaksi::class, 'bank_users_id');
	}
}
