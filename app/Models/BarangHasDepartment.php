<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 26 Aug 2017 15:52:59 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class BarangHasDepartment
 * 
 * @property int $barang_id
 * @property int $department_id
 * @property float $harga
 * 
 * @property \App\Models\Barang $barang
 * @property \App\Models\Department $department
 *
 * @package App\Models
 */
class BarangHasDepartment extends Eloquent
{
	protected $table = 'barang_has_department';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'barang_id' => 'int',
		'department_id' => 'int',
		'harga' => 'float'
	];

	protected $fillable = [
		'harga'
	];

	public function barang()
	{
		return $this->belongsTo(\App\Models\Barang::class);
	}

	public function department()
	{
		return $this->belongsTo(\App\Models\Department::class);
	}
}
