<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 10 Apr 2018 02:23:04 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class HTransaksi
 * 
 * @property int $id
 * @property string $no_invoice
 * @property \Carbon\Carbon $tgl
 * @property float $biaya_ongkir
 * @property float $unique_harga
 * @property float $grand_total
 * @property float $terbayar
 * @property float $sisa_bayar
 * @property string $url_bukti
 * @property int $bank_users_id
 * @property int $users_alamat_id
 * @property int $id_cust
 * @property string $kurir
 * @property string $kurir_paket
 * @property int $konfirmasi
 * @property string $keterangan_user
 * @property string $no_resi
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $diterima
 * @property string $url_complain
 * @property string $keterangan_komplain
 * @property int $status_komplain
 * @property \Carbon\Carbon $tgl_komplain
 * @property string $reff_no
 * 
 * @property \App\Models\BankUser $bank_user
 * @property \App\Models\User $user
 * @property \App\Models\UsersAlamat $users_alamat
 * @property \Illuminate\Database\Eloquent\Collection $d_piutangs
 * @property \Illuminate\Database\Eloquent\Collection $d_transaksis
 *
 * @package App\Models
 */
class HTransaksi extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'h_transaksi';

	protected $casts = [
		'biaya_ongkir' => 'float',
		'unique_harga' => 'float',
		'grand_total' => 'float',
		'terbayar' => 'float',
		'sisa_bayar' => 'float',
		'bank_users_id' => 'int',
		'users_alamat_id' => 'int',
		'id_cust' => 'int',
		'konfirmasi' => 'int',
		'diterima' => 'int',
		'status_komplain' => 'int'
	];

	protected $dates = [
		'tgl',
		'tgl_komplain'
	];

	protected $fillable = [
		'no_invoice',
		'tgl',
		'biaya_ongkir',
		'unique_harga',
		'grand_total',
		'terbayar',
		'sisa_bayar',
		'url_bukti',
		'bank_users_id',
		'users_alamat_id',
		'id_cust',
		'kurir',
		'kurir_paket',
		'konfirmasi',
		'keterangan_user',
		'no_resi',
		'diterima',
		'url_complain',
		'keterangan_komplain',
		'status_komplain',
		'tgl_komplain',
		'reff_no'
	];

	public function bank_user()
	{
		return $this->belongsTo(\App\Models\BankUser::class, 'bank_users_id');
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'id_cust');
	}

	public function users_alamat()
	{
		return $this->belongsTo(\App\Models\UsersAlamat::class);
	}

	public function d_piutangs()
	{
		return $this->hasMany(\App\Models\DPiutang::class, 'id_h_transaksi');
	}

	public function d_transaksis()
	{
		return $this->hasMany(\App\Models\DTransaksi::class, 'id_h_transaksi');
	}
}
