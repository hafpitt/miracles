<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 11 Sep 2017 15:38:22 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Kotum
 * 
 * @property int $id
 * @property string $nama
 * @property int $id_provinsi
 * @property string $type
 * @property string $kodepos
 * @property string $deleted_at
 * 
 * @property \App\Models\Provinsi $provinsi
 * @property \Illuminate\Database\Eloquent\Collection $users_alamats
 *
 * @package App\Models
 */
class Kotum extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	public $timestamps = false;

	protected $casts = [
		'id_provinsi' => 'int'
	];

	protected $fillable = [
		'nama',
		'id_provinsi',
		'type',
		'kodepos'
	];

	public function provinsi()
	{
		return $this->belongsTo(\App\Models\Provinsi::class, 'id_provinsi');
	}

	public function users_alamats()
	{
		return $this->hasMany(\App\Models\UsersAlamat::class, 'kota_id');
	}
}
