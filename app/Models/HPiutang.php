<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 25 Feb 2018 12:34:33 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class HPiutang
 * 
 * @property int $id
 * @property string $no_piutang
 * @property \Carbon\Carbon $tgl
 * @property float $grand_total
 * @property bool $status
 * @property int $id_operator
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $d_piutangs
 *
 * @package App\Models
 */
class HPiutang extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'h_piutang';

	protected $casts = [
		'grand_total' => 'float',
		'status' => 'bool',
		'id_operator' => 'int'
	];

	protected $dates = [
		'tgl'
	];

	protected $fillable = [
		'no_piutang',
		'tgl',
		'grand_total',
		'status',
		'id_operator'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'id_operator');
	}

	public function d_piutangs()
	{
		return $this->hasMany(\App\Models\DPiutang::class, 'id_h_piutang');
	}
}
