<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 17 Jan 2019 15:39:42 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class BarangWarna
 * 
 * @property int $id
 * @property string $nama
 * @property string $warna
 * @property bool $tersedia
 * @property int $id_barang
 * 
 * @property \App\Models\Barang $barang
 * @property \Illuminate\Database\Eloquent\Collection $carts
 * @property \Illuminate\Database\Eloquent\Collection $d_transaksis
 *
 * @package App\Models
 */
class BarangWarna extends Eloquent
{
	protected $table = 'barang_warna';
	public $timestamps = false;

	protected $casts = [
		'tersedia' => 'bool',
		'id_barang' => 'int'
	];

	protected $fillable = [
		'nama',
		'warna',
		'tersedia',
		'id_barang'
	];

	public function barang()
	{
		return $this->belongsTo(\App\Models\Barang::class, 'id_barang');
	}

	public function carts()
	{
		return $this->hasMany(\App\Models\Cart::class, 'id_barang_warna');
	}

	public function d_transaksis()
	{
		return $this->hasMany(\App\Models\DTransaksi::class);
	}
}
