<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 17 Jan 2019 15:38:54 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Barang
 * 
 * @property int $id
 * @property string $nama
 * @property float $stok
 * @property string $keterangan
 * @property int $id_barang_kategori
 * @property int $berat
 * @property string $url_page
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property string $keterangandetail
 * 
 * @property \App\Models\BarangKategori $barang_kategori
 * @property \Illuminate\Database\Eloquent\Collection $barang_fotos
 * @property \Illuminate\Database\Eloquent\Collection $departments
 * @property \Illuminate\Database\Eloquent\Collection $barang_warnas
 * @property \Illuminate\Database\Eloquent\Collection $carts
 * @property \Illuminate\Database\Eloquent\Collection $d_transaksis
 * @property \Illuminate\Database\Eloquent\Collection $users
 *
 * @package App\Models
 */
class Barang extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'barang';

	protected $casts = [
		'stok' => 'float',
		'id_barang_kategori' => 'int',
		'berat' => 'int'
	];

	protected $fillable = [
		'nama',
		'stok',
		'keterangan',
		'id_barang_kategori',
		'berat',
		'url_page',
		'keterangandetail'
	];

	public function barang_kategori()
	{
		return $this->belongsTo(\App\Models\BarangKategori::class, 'id_barang_kategori');
	}

	public function barang_fotos()
	{
		return $this->hasMany(\App\Models\BarangFoto::class, 'id_barang');
	}

	public function departments()
	{
		return $this->belongsToMany(\App\Models\Department::class, 'barang_has_department')
					->withPivot('harga');
	}

	public function barang_warnas()
	{
		return $this->hasMany(\App\Models\BarangWarna::class, 'id_barang');
	}

	public function carts()
	{
		return $this->hasMany(\App\Models\Cart::class, 'id_barang');
	}

	public function d_transaksis()
	{
		return $this->hasMany(\App\Models\DTransaksi::class);
	}

	public function users()
	{
		return $this->belongsToMany(\App\Models\User::class, 'users_has_barang', 'barang_id', 'users_id')
					->withPivot('comment', 'rating', 'tgl');
	}
}
