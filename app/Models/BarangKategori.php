<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 26 Aug 2017 15:52:59 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class BarangKategori
 * 
 * @property int $id
 * @property string $nama
 * @property string $url_photo
 * @property string $url_page
 * @property string $deleted_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $barangs
 *
 * @package App\Models
 */
class BarangKategori extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'barang_kategori';
	public $timestamps = false;

	protected $fillable = [
		'nama',
		'url_photo',
		'url_page'
	];

	public function barangs()
	{
		return $this->hasMany(\App\Models\Barang::class, 'id_barang_kategori');
	}
}
