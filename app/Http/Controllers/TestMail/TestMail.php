<?php

namespace App\Http\Controllers\TestMail;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use App\Models\User;
use App\Models\HTransaksi;
use App\Models\BankUser;
use App\Mail\UserVerification;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Crypt;

class TestMail extends Controller
{
  public function IndexVerification()
  {
    $user = User::first();
    return view('front.temp-email.account-verification',compact('user'));
  }
  public function ForgotPassword()
  {
    $user = User::first();
    return view('front.temp-email.forgot-password',compact('user'));
  }
  public function IndexPayInformation()
  {
    // $users = User::with(['banks'=>function($query){
    //   $query->whereNull('bank_users.deleted_at');
    // }])->findOrFail(Auth::id());
    $bankuser = BankUser::with('bank')->where('id_users','=','4')->get();
    $user = User::with(['h_transaksis'=>function($query){
      $query->whereNull('diterima')->orderBy('id','desc')
      ->with('d_transaksis');
    }])->findOrFail(Auth::id());
    // return $user;
    // return $htrans;
    // $user = User::first();
    return view('front.temp-email.payment-information',compact('bankuser','user'));
  }
  public function PaymentConfirmation(Request $request,$id)
  {
    $id=Crypt::decrypt($id);
    $htransaksi = HTransaksi::with('user')->findOrFail($id);
    // return $htransaksi;
    return view('front.temp-email.payment-confirmation',compact('htransaksi'));
  }
  public function PaymentVerification(Request $request,$id)
  {
    $id=Crypt::decrypt($id);
    // return $id;
    // $user = User::with(['h_transaksis'=>function($query){
    //   $query->whereNull('diterima')->orderBy('id','desc')
    //   ->with('d_transaksis');
    // }])->findOrFail(Auth::id());
    $verification = HTransaksi::with('user')->findOrFail($id);
    // return $verification;
    return view('front.temp-email.payment-verification',compact('verification'));
  }

  public function SendMailConfirmation(Request $request, $id)
  {
    $id=Crypt::decrypt($id);
    $htransaksi = HTransaksi::with('user')->findOrFail($id);
    return $htransaksi;
    return view('front.temp-email.send-mail-confirmation');
  }
}
