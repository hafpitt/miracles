<?php

namespace App\Http\Controllers\Back\SendConfirmation;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\DTransaksi;
use App\Models\HTransaksi;
use App\Models\HPiutang;
use App\Models\DPiutang;
use Yajra\Datatables\Facades\Datatables;
use App\Mail\SendMailConfirmation;
use Response;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

class Back_Confirmation extends Controller
{
  public function IndexEntry(Request $request)
  {
    if ($request->has('i'))
    {
      $id = Crypt::decryptString($request->input('i'));
      $idresi = HTransaksi::find($id);
      return view('back.menus.transaction.konfirmasi-kirim.input-resi',compact('idresi'));
    } else {
      return redirect(route('list.resi'))->with('warning', 'Silahkan pilih invoice yang akan diinputkan resi');
    }
  }

  public function IndexList()
  {
    return view('back.menus.transaction.konfirmasi-kirim.list');
  }

  public function StoreNew(Request $request)
  {
    $id = Crypt::decryptString($request->input('id'));
    $resi = $request->input('resi');
    $noresi = HTransaksi::findOrFail($id);
    $noresi->no_resi = $resi;
    $noresi->save();
    if (config('app.debug') <> 1)
    {
      $this->sendMailConfirmation($request,$id);
    }
    // return $noresi;
    return redirect(route('list.resi'))->with('message', 'Resi untuk invoice'.$noresi->no_invoice.'berhasil diinputkan');
  }

  public function sendMailConfirmation(Request $request,$id)
  {
    $htransaksi = HTransaksi::with('user')->findOrFail($id);
    Mail::to($htransaksi->user->email)
    ->send(new SendMailConfirmation($htransaksi));
  }

  public function DataTableSSP()
  {
    $field = ['id','no_invoice','tgl','kurir','no_resi','diterima'];
    $piutang = HTransaksi::where('sisa_bayar','=','0')
    ->get($field);
    $datatable = Datatables::of($piutang)
    ->addColumn('action', function ($item) {
      $ret = '<td class="uk-text-center">';
      if ($item->no_resi == "")
      {
        $ret.='<a href="'.route('entry.resi', ['i' => Crypt::encryptString($item->id)]).'"><i class="md-btn md-btn-primary md-btn-wave-light waves-effect waves-button waves-light">Kirim</i></a>';
      } else if ($item->diterima == "1") {
        $ret.='Selesai';
      } else if ($item->no_resi <> "") {
        $ret.='<a href="'.route('entry.resi', ['i' => Crypt::encryptString($item->id)]).'"><i class="md-btn md-btn-primary md-btn-wave-light waves-effect waves-button waves-light">ubah resi</i></a>';
      }
      $ret.='</td>';
      return $ret;
    })
    ->removeColumn('diterima')
    ->escapeColumns([])
    ->make();
    return $datatable;
  }
}
