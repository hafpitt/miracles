<?php

namespace App\Http\Controllers\Back\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class Back_Login extends Controller
{
  public function Index()
  {
    return view('back.menus.account.login.content');
  }
  public function Authenticate(Request $request)
  {
    $email = $request->input('email');
    $password = $request->input('password');
    if (Auth::guard('web_admin')->attempt(['email' => $email, 'password' => $password])) {
      $isadmin = Auth::guard('web_admin')->user()->department->isadmin;
      if ($isadmin)
      {
        return redirect(route('dashboard.back'));
      }
      else {
        Auth::guard('web_admin')->logout();
        return redirect(route('home.front'))->withInput()->withErrors(['status' => 'Anda tidak memiliki akses!']);
      }
    }
    else {
      return redirect(route('login.back'))->withInput()->withErrors(['status' => 'username atau password salah mohon dicek kembali!']);
    }
  }
  // public function CreateDefault(Request $request)
  // {
  //   $email = $request->input('email');
  //   $nama = $request->input('nama');
  //   $password = $request->input('password');
  //   $user = new User();
  //   $user->nama = $nama;
  //   $user->email = $email;
  //   $user->password=bcrypt($password);
  //   $user->save();
  // }
  public function Logout()
  {
    Auth::guard('web_admin')->logout();
    return redirect(route('login.back'));
  }

}
