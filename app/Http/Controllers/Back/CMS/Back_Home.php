<?php

namespace App\Http\Controllers\Back\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\CmsHomeCarousel;
use Yajra\Datatables\Facades\Datatables;
use Response;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;

class Back_Home extends Controller
{
  public function IndexEntry(Request $request)
  {
    if ($request->has('i'))
    {
      $id = Crypt::decryptString($request->input('i'));
      $cms = CmsHomeCarousel::find($id);
      return view('back.menus.cms.home.entry',compact('cms'));
    }
    else {
      return view('back.menus.cms.home.entry');
    }
  }

  public function IndexList()
  {
    return view('back.menus.cms.home.list');
  }

  public function Post(Request $request)
  {
    $validator = $this->validator($request->all());
    if ($validator->fails())
    {
      return redirect()->back()->withErrors($validator)->withInput();
    }
    else {
      if (!$request->has('id'))
      {
        return $this->StoreNew($request);
      }
      else {
        return $this->StoreEdit($request);
      }
    }
  }

  public function StoreNew(Request $request)
  {
    $nama = $request->input('nama');
    $keterangan = $request->input('keterangan');
    $cms = new CmsHomeCarousel();
    $cms->nama = $nama;
    $cms->keterangan = $keterangan;
    if ($request->hasFile('url_photo')) {
      $cms->url_photo = Storage::disk('public')->put('slider', $request->file('url_photo'));
    }
    $cms->save();
    return redirect($request->url())->with('message', 'Home Slider '.$nama.' berhasil didaftarkan');
  }

  public function StoreEdit(Request $request)
  {
    $id = $request->input('id');
    $nama = $request->input('nama');
    $keterangan = $request->input('keterangan');
    $cms = CmsHomeCarousel::findOrFail($id);
    $cms->nama = $nama;
    $cms->keterangan = $keterangan;
    if ($request->hasFile('url_photo')) {
      Storage::disk('public')->delete($cms->url_photo);
      $cms->url_photo = Storage::disk('public')->put('slider', $request->file('url_photo'));
    }
    $cms->save();
    return redirect(route('list.cms_home'))->with('message', 'Home Slider '.$nama.' berhasil direvisi');
  }

  protected function validator(array $data)
  {
      return Validator::make($data, [
          // 'nama' => 'required',
          // 'keterangan' => 'required',
          'url_photo' => 'image|required_without:id',
      ]);
  }

  public function Destroy(Request $request)
  {
    $id = Crypt::decryptString($request->input('i'));
    $cms = CmsHomeCarousel::find($id);
    $nama = $cms->nama;
    $cms->delete();
    return redirect(route('list.cms_home'))->with('message', 'Home Slider '.$nama.' berhasil dihapus');
  }

  public function DataTableSSP()
  {
    $field = ['id','nama','keterangan','url_photo'];
    $Model = CmsHomeCarousel::get($field);
    $datatable = Datatables::of($Model)
    ->addColumn('action', function ($item) {
      return '<td class="uk-text-center">
          <a href="'.route('entry.cms_home', ['i' => Crypt::encryptString($item->id)]).'"><i class="md-icon material-icons">&#xE254;</i></a>
          <a class="try-delete" href="'.route('destroy.cms_home', ['i' => Crypt::encryptString($item->id)]).'"><i class="md-icon material-icons">&#xE92B;</i></a>
      </td>';
    })
    ->editColumn('url_photo', function ($item) {
      return '<td class="uk-text-center"><img class="md-user-image" src="'.asset(Storage::disk('public')->url($item->url_photo)).'" alt=""/></td>';
    })
    ->escapeColumns([])
    ->make();
    return $datatable;
  }
}
