<?php

namespace App\Http\Controllers\Back\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\Departement as Department;
use App\Models\Modul;
use Yajra\Datatables\Facades\Datatables;
use Response;
use Illuminate\Support\Facades\Crypt;

class Back_Footer extends Controller
{
  public function IndexEntry(Request $request)
  {
    $modul = Modul::all();
    if ($request->has('i'))
    {
      $id = Crypt::decryptString($request->input('i'));
      $department = Department::find($id);
      return view('back.menus.cms.footer.entry',compact('department','modul'));
    }
    else {
      return view('back.menus.cms.footer.entry',compact('modul'));
    }
  }

  public function IndexList()
  {
    return view('back.menus.cms.list');
  }

  public function Post(Request $request)
  {
    $validator = $this->validator($request->all());
    if ($validator->fails())
    {
      return redirect()->back()->withErrors($validator)->withInput();
    }
    else {
      if (!$request->has('id'))
      {
        return $this->StoreNew($request);
      }
      else {
        return $this->StoreEdit($request);
      }
    }
  }

  public function StoreNew(Request $request)
  {
    $nama = $request->input('nama');
    $package_category = new Package_Catagory();
    $package_category->nama = $nama;
    $package_category->url_page = strtolower(str_replace(' ','-',$nama));
    if ($request->hasFile('url_photo')) {
      $package_category->url_photo = Storage::disk('public')->put('package_category', $request->file('url_photo'));
    }
    $package_category->save();
    return redirect($request->url())->with('message', 'Package Catagory '.$nama.' berhasil didaftarkan');
  }

  public function StoreEdit(Request $request)
  {
    $id = $request->input('id');
    $nama = $request->input('nama');
    $package_category = Package_Catagory::findOrFail($id);
    $package_category->nama = $nama;
    $package_category->url_page = strtolower(str_replace(' ','-',$nama));
    if ($request->hasFile('url_photo')) {
      Storage::disk('public')->delete($package_category->url_photo);
      $package_category->url_photo = Storage::disk('public')->put('package_category', $request->file('url_photo'));
    }
    $package_category->save();
    return redirect(route('list.package_catagory'))->with('message', 'Package Catagory '.$nama.' berhasil direvisi');
  }

  protected function validator(array $data)
  {
      return Validator::make($data, [
          'nama' => 'required_without:id|string|min:3|max:255|unique:packet_catagory,nama,'. (isset($data['id']) ? $data['id']: ''),
          'url_photo' => 'image|required_without:id',
      ]);
  }

  public function Destroy(Request $request)
  {
    $id = Crypt::decryptString($request->input('i'));
    $package_catagory = Package_Catagory::find($id);
    $nama = $package_catagory->nama;
    $package_catagory->delete();
    return redirect(route('list.package_catagory'))->with('message', 'Package Catagory '.$nama.' berhasil dihapus');
  }

  public function DataTableSSP()
  {
    $field = ['id','nama','url_photo'];
    $Model = Package_Catagory::get($field);
    $datatable = Datatables::of($Model)
    ->addColumn('action', function ($item) {
      return '<td class="uk-text-center">
          <a href="'.route('entry.package_catagory', ['i' => Crypt::encryptString($item->id)]).'"><i class="md-icon material-icons">&#xE254;</i></a>
          <a class="try-delete" href="'.route('destroy.package_catagory', ['i' => Crypt::encryptString($item->id)]).'"><i class="md-icon material-icons">&#xE92B;</i></a>
      </td>';
    })
    ->editColumn('url_photo', function ($item) {
      return '<td class="uk-text-center"><img class="md-user-image" src="'.asset(Storage::disk('public')->url($item->url_photo)).'" alt=""/></td>';
    })
    ->escapeColumns([])
    ->make();
    return $datatable;
  }
}
