<?php

namespace App\Http\Controllers\Back\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\CmsContact;
use Yajra\Datatables\Facades\Datatables;
use Response;
use Illuminate\Support\Facades\Crypt;

class Back_Contact extends Controller
{
  public function IndexEntry(Request $request)
  {
    $contact = CmsContact::first();
    return view('back.menus.cms.contact.entry',compact('contact'));
  }

  public function Post(Request $request)
  {
    $validator = $this->validator($request->all());
    if ($validator->fails())
    {
      return redirect()->back()->withErrors($validator)->withInput();
    }
    else {
      if (!$request->has('id'))
      {
        return $this->StoreNew($request);
      }
      else {
        return $this->StoreEdit($request);
      }
    }
  }

  public function StoreNew(Request $request)
  {
    $alamat = $request->input('alamat');
    $notelp = $request->input('notelp');
    $email = $request->input('email');
    $contact = new CmsContact();
    $contact->alamat = $alamat;
    $contact->notelp = $notelp;
    $contact->email = $email;
    $contact->save();
    return redirect($request->url())->with('message', 'Contact Info berhasil didaftarkan');
  }

  public function StoreEdit(Request $request)
  {
    $id = $request->input('id');
    $alamat = $request->input('alamat');
    $notelp = $request->input('notelp');
    $email = $request->input('email');
    $contact = CmsContact::findOrFail($id);
    $contact->alamat = $alamat;
    $contact->notelp = $notelp;
    $contact->email = $email;
    $contact->save();
    return redirect($request->url())->with('message', 'Contact Info berhasil edit');
  }

  protected function validator(array $data)
  {
      return Validator::make($data, [
          'alamat' => 'required|string',
          'notelp' => 'required|string',
          'email' => 'required|string|email',
      ]);
  }

}
