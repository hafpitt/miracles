<?php

namespace App\Http\Controllers\Back\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\CmsPageImage;
use Yajra\Datatables\Facades\Datatables;
use Response;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;

class Back_Schedule extends Controller
{
  public function IndexEntry(Request $request)
  {
    $cms = CmsPageImage::where('nama','=','schedule')->first();
    return view('back.menus.cms.page-image.schedule.entry',compact('cms'));
  }

  public function Post(Request $request)
  {
    $validator = $this->validator($request->all());
    if ($validator->fails())
    {
      return redirect()->back()->withErrors($validator)->withInput();
    }
    else {
      if (!$request->has('id'))
      {
        return $this->StoreNew($request);
      }
      else {
        return $this->StoreEdit($request);
      }
    }
  }

  public function StoreNew(Request $request)
  {
    $nama = "schedule";
    $cms = new CmsPageImage();
    $cms->nama = $nama;
    if ($request->hasFile('url_photo')) {
      $cms->url_photo = Storage::disk('public')->put('page-image', $request->file('url_photo'));
    }
    $cms->save();
    return redirect($request->url())->with('message', 'Page Image '.$nama.' berhasil didaftarkan');
  }

  public function StoreEdit(Request $request)
  {
    $id = $request->input('id');
    $nama = "schedule";
    $cms = CmsPageImage::findOrFail($id);
    $cms->nama = $nama;
    if ($request->hasFile('url_photo')) {
      Storage::disk('public')->delete($cms->url_photo);
      $cms->url_photo = Storage::disk('public')->put('page-image', $request->file('url_photo'));
    }
    $cms->save();
    return redirect($request->url())->with('message', 'Page Image '.$nama.' berhasil edit');
  }

  protected function validator(array $data)
  {
      return Validator::make($data, [
        'url_photo' => 'image|required_without:id',
      ]);
  }

}
