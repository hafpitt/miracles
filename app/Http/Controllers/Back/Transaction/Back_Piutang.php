<?php

namespace App\Http\Controllers\Back\Transaction;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\DTransaksi;
use App\Models\HTransaksi;
use App\Models\HPiutang;
use App\Models\DPiutang;
use Yajra\Datatables\Facades\Datatables;
use Response;
use App\Mail\PaymentVerification;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

class Back_Piutang extends Controller
{
  public function IndexEntry(Request $request)
  {
    // return $request->all();
    if ($request->has('i'))
    {

      $id = Crypt::decryptString($request->input('i'));
      $piutang = HPiutang::with('d_piutangs')->findOrFail($id);
      $invoice = HTransaksi::where('konfirmasi','1')->get();
      return view('back.menus.transaction.piutang.entry',compact('piutang','invoice'));
    }
    else {
      $invoice = HTransaksi::where('konfirmasi','1')->where('sisa_bayar','<>','0')->get();
      return view('back.menus.transaction.piutang.entry',compact('invoice'));
    }
  }

  public function IndexList()
  {
    return view('back.menus.transaction.piutang.list');
  }

  public function Post(Request $request)
  {
    $validator = $this->validator($request->all());
    if ($validator->fails())
    {
      return redirect()->back()->withErrors($validator)->withInput();
    }
    else {
      if (!$request->has('id'))
      {
        return $this->StoreNew($request);
      }
      else {
        return $this->StoreEdit($request);
      }
    }
  }

  public function StoreNew(Request $request)
  {
    // return $request->all();
    $invoice = $request->input('inv');
    // return $invoice;

    $grand_total = 0;
    $dpiutang = [];
    foreach ($invoice as $key => $value) {
      if ($value['dibayar'] <= 0){
        continue;
      }
      $grand_total = $grand_total+$value['dibayar'];
      $dpiutang[] = new DPiutang(['dibayar'=>$value['dibayar'], 'id_h_transaksi'=>$key]);
      $trans = HTransaksi::findOrFail($key);
      $trans->terbayar = $trans->terbayar+$value['dibayar'];
      $trans->sisa_bayar = $trans->sisa_bayar-$value['dibayar'];
      // return $trans;
      $trans->save();
    }
    $last_trans = HPiutang::where('tgl','=', Carbon::now()->toDateString())->count()+1;
    $no_piutang = $this->GenerateNota($last_trans,'P');
    $piutang = new HPiutang();
    $piutang->no_piutang =$no_piutang;
    // $piutang->status = $request->input('keterangan');
    $piutang->tgl = Carbon::now()->toDateString();
    $piutang->id_operator = Auth::guard('web_admin')->id();
    $piutang->grand_total = $grand_total;
    // return $piutang;
    $piutang->save();
    $piutang->d_piutangs()->saveMany($dpiutang);
    // return $piutang->d_piutangs;
    if (config('app.debug') <> 1)
    {
      $this->sendPaymentVerification($request,$piutang);
    }
    return redirect($request->url())->with('message', 'Piutang '.$no_piutang. ' berhasil terbuat');
  }

  public function sendPaymentVerification(Request $request,$piutang)
  {
    // $htransaksi = HTransaksi::with('user')->findOrFail($id);gak blas. tak delok sek
     foreach ($piutang->d_piutangs as $key => $value) {
      Mail::to($value->h_transaksi->user->email)
      ->send(new PaymentVerification($value->h_transaksi));
    }
  }

  public function GenerateNota($id,$tipe)
  {
    $date=date_format(date_create(Carbon::now()),"Y/m/d");
    return $tipe.'/'.$date.'/'.sprintf('%04d', $id);
  }

  public function StoreEdit(Request $request)
  {
    // return $request->all();
    $id = $request->input('id');
    $invoice = $request->input('inv');
    $grand_total = 0;
    $dpiutang = [];
    $oldpiutang = DPiutang::where('id_h_piutang',$id);
    foreach ($oldpiutang->get() as $key => $value) {
      $trans = HTransaksi::findOrFail($value->id_h_transaksi);
      $trans->terbayar = $trans->terbayar-$value->dibayar;
      $trans->sisa_bayar = $trans->sisa_bayar+$value->dibayar;
      $trans->save();
    }
    $oldpiutang->delete();
    foreach ($invoice as $key => $value) {
      $grand_total = $grand_total+$value['dibayar'];
      $dpiutang[] = new DPiutang(['dibayar'=>$value['dibayar'], 'id_h_transaksi'=>$key]);
      $trans = HTransaksi::findOrFail($key);
      $trans->terbayar = $trans->terbayar+$value['dibayar'];
      $trans->sisa_bayar = $trans->sisa_bayar-$value['dibayar'];
      $trans->save();
    }
    $last_trans = HPiutang::where('tgl','=', Carbon::now()->toDateString())->count()+1;
    $no_piutang = $this->GenerateNota($last_trans,'P');
    $piutang = HPiutang::findOrFail($id);
    $piutang->no_piutang =$no_piutang;
    // $piutang->status = $request->input('keterangan');
    $piutang->tgl = Carbon::now()->toDateString();
    $piutang->id_operator = Auth::guard('web_admin')->id();
    $piutang->grand_total = $grand_total;
    $piutang->save();
    $piutang->d_piutangs()->saveMany($dpiutang);
    return redirect(route('list.piutang'))->with('message', 'Piutang '.$no_piutang. ' berhasil diedit');
  }

  protected function validator(array $data)
  {
      return Validator::make($data, [
      ]);
  }

  public function Destroy(Request $request)
  {
    $id = Crypt::decryptString($request->input('i'));
    $piutang = HPiutang::findOrFail($id);
    $nama = $piutang->no_piutang;
    $piutang->delete();
    return redirect(route('list.piutang'))->with('message', 'No Piutang '.$nama.' berhasil dihapus');
  }

  public function DataTableSSP()
  {
    $field = ['id','no_piutang','tgl'];
    $piutang = HPiutang::get($field);
    $datatable = Datatables::of($piutang)
    ->addColumn('action', function ($item) {
      return '<td class="uk-text-center">
          <a href="'.route('entry.piutang', ['i' => Crypt::encryptString($item->id)]).'"><i class="md-icon material-icons">&#xE254;</i></a>
          <a class="try-delete" href="'.route('destroy.piutang', ['i' => Crypt::encryptString($item->id)]).'"><i class="md-icon material-icons">&#xE92B;</i></a>
      </td>';
    })
    ->escapeColumns([])
    ->make();
    return $datatable;
  }
}
