<?php

namespace App\Http\Controllers\Back\Transaction;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\Barang;
use App\Models\Cart;
use App\Models\DTransaksi;
use App\Models\HTransaksi;
use App\Models\HPiutang;
use App\Models\DPiutang;
use App\Models\User;
use Yajra\Datatables\Facades\Datatables;
use Response;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class Back_Retur extends Controller
{
  public function IndexPengajuan(Request $request)
  {
    return view('back.menus.transaction.retur.pengajuan-komplain');
  }

  public function IndexPengajuanRetur(Request $request)
  {
    return view('back.menus.transaction.retur.pengajuan-retur');
  }

  public function DataTableSSPPengajuan(Request $request)
  {
    $field = ['id','no_invoice','tgl_komplain','url_complain','status_komplain'];
    $pengajuan = HTransaksi::where('url_complain','<>',"")
    ->where('diterima','=','1')
    ->whereNull('status_komplain')
    ->where('no_invoice', 'like', 'INV%')
    ->get($field);
    $datatable = Datatables::of($pengajuan)
    ->addColumn('action', function ($item) {
      return '<td class="uk-text-center">
          <a href="'.route('acc.komplain', ['j' => Crypt::encryptString($item->id)]).'"><i class="md-icon material-icons">&#xE5CA;</i></a>
          <a class="try-delete" href="'.route('destroy.komplain', ['i' => Crypt::encryptString($item->id)]).'"><i class="md-icon material-icons">&#xE5C9;</i></a>
      </td>';
    })
    ->editColumn('url_complain', function ($item) {
      return '<td class="uk-text-center">
        <a target="blank" href="'.asset(Storage::disk('public')->url($item->url_complain)).'" alt="">Lihat Bukti</a>
      </td>';
    })
    ->removeColumn('status_komplain')
    ->escapeColumns([])
    ->make();
    return $datatable;
  }

  public function DataTableSSPPengajuanRetur(Request $request)
  {
    $field = ['id','no_invoice','tgl'];
    $pengajuan = HTransaksi::where('no_invoice', 'like', 'RET%')
    // where('url_complain','<>',"")
    // ->where('diterima','=','1')
    // ->whereNull('status_komplain')
    // ->where('no_invoice', 'like', 'INV%')
    ->get($field);
    $datatable = Datatables::of($pengajuan)
    ->addColumn('action', function ($item) {
      return '<td class="uk-text-center">
          <a href="'.route('entry.retur', ['j' => Crypt::encryptString($item->id)]).'">Ganti</a>
      </td>';
    })
    // ->editColumn('url_complain', function ($item) {
    //   return '<td class="uk-text-center">
    //     <a target="blank" href="'.asset(Storage::disk('public')->url($item->url_complain)).'" alt="">Lihat Bukti</a>
    //   </td>';
    // })
    // ->removeColumn('status_komplain')
    ->escapeColumns([])
    ->make();
    return $datatable;
  }

  public function AccKomplain(Request $request)
  {
    if ($request->has('j'))
    {
      $id = Crypt::decryptString($request->input('j'));
      $statusKomplain = HTransaksi::findOrFail($id);
      $statusKomplain->status_komplain = 1;
      $statusKomplain->save();
      return redirect()->back()->with('message', 'Berhasil');
      // return view('back.menus.transaction.retur.entry',compact('retur','invoice'));
    }
  }

  public function TolakKomplain(Request $request)
  {
    if ($request->has('j'))
    {
      $id = Crypt::decryptString($request->input('j'));
      $statusKomplain = HTransaksi::findOrFail($id);
      $statusKomplain->status_komplain = 0;
      $statusKomplain->save();
      return redirect()->back()->with('message', 'Berhasil ditolak');
      // return view('back.menus.transaction.retur.entry',compact('retur','invoice'));
    }
  }

  public function IndexEntry(Request $request,$id)
  {
    // return $request->all();
    $id = Crypt::decryptString($id);
    $barangRetur = HTransaksi::with('d_transaksis.cart.barang')->findOrFail($id);
    $loadBarang = Barang::get();
    // $barangRetur->status_komplain = 1;
    // $barangRetur->save();
    // return redirect()->back()->with('message', 'Berhasil');
    // return $barangRetur;
    return view('back.menus.transaction.retur.entry', compact('barangRetur','loadBarang'));
  }

  public function PostEntry(Request $request,$id)
  {
    $jsonstring = json_decode($request->input('jsonstring'),true);
    $request->merge($jsonstring);
    // return $request->all();
    // return response()->json($jsonstring);
    $id = Crypt::decryptString($id);
    $reftransaksi = HTransaksi::findOrFail($id);
    // return $reftransaksi;
    // exit();

    $last_trans = HTransaksi::where('tgl','=', Carbon::now()->toDateString())
    ->where('no_invoice', 'like', 'EXC%')->count()+1;
    $no_invoice = $this->GenerateNota($last_trans,'EXC');
    $htransret = new HTransaksi();
    $htransret->no_invoice = $no_invoice;
    $htransret->tgl = Carbon::now()->toDateString();
    $htransret->biaya_ongkir = 0;
    $htransret->unique_harga = 0;
    $htransret->grand_total = 0;
    $htransret->terbayar = 0;
    $htransret->sisa_bayar = 0;
    $htransret->bank_users_id = $reftransaksi->bank_users_id;
    $htransret->users_alamat_id = $reftransaksi->users_alamat_id;
    $htransret->id_cust = $reftransaksi->id_cust;
    $htransret->reff_no = $id;
    // return $htransret;
    // exit();
    $htransret->save();

    $barang = $request->input('barang');
    // return $barang;
    // exit();
    $user = User::find($reftransaksi->id_cust);
    // return $user;
    foreach ($barang as $key => $value) {
      // return $barang;
      // exit();
        $barang = Barang::with(['barang_warnas','departments' => function($query) use($user){
          $query->where('department_id', '=', $user->department_id);
        }])->find($value['nama']);
        // return $barang;
        // $deta_trans->harga=$item->barang->departments->first()->pivot->harga;

        $cart = new Cart();
        $cart->qty = $value['qty'];
        $cart->id_cust = $reftransaksi->id_cust;
        $cart->id_barang = $value['nama'];
        $cart->id_barang_warna = $barang->barang_warnas->first()->id;
        $cart->check_out = 3;
        // return $cart;
        // exit();
        $cart->save();

        $dtrans = new DTransaksi();
        $dtrans->harga = $barang->departments->first()->pivot->harga;
        $dtrans->subtotal=$value['qty']*$barang->departments->first()->pivot->harga;
        $dtrans->id_cart = $cart->id;
        // $dtrans->cart->barang->stok = $dtrans->cart->barang->stok-$value['qty'];
        // $brg = Barang::find($item->id_barang);
        $barang->stok = $barang->stok-$value['qty'];
        $barang->save();
    		$htransret->d_transaksis()->save($dtrans);


    }
    return redirect($request->url())->with('message', 'Retur '.$no_invoice. ' berhasil terbuat');
  }

  public function GenerateNota($id,$tipe)
  {
    $date=date_format(date_create(Carbon::now()),"Y/m/d");
    return $tipe.'/'.$date.'/'.sprintf('%04d', $id);
  }

  public function IndexList()
  {
    return view('back.menus.transaction.retur.list');
  }

  public function Post(Request $request)
  {
    // return $request->all();
    // sisa insert *htransaksi.reff_no ambil dr ref_no
    // sisa insert *dtransaksi.reff_no ambil dr cart_id
    $validator = $this->validator($request->all());
    if ($validator->fails())
    {
      return redirect()->back()->withErrors($validator)->withInput();
    }
    else {
      if (!$request->has('id'))
      {
        return $this->StoreNew($request);
      }
      else {
        return $this->StoreEdit($request);
      }
    }
  }

  public function StoreNew(Request $request)
  {
    $reftransaksi = HTransaksi::findOrFail($request->input('ref_no'));
    // return $reftransaksi;
    // exit();

    $last_trans = HTransaksi::where('tgl','=', Carbon::now()->toDateString())
    ->where('no_invoice', 'like', 'RET%')->count()+1;
    $no_invoice = $this->GenerateNota($last_trans,'RET');
    $htransret = new HTransaksi();
    $htransret->no_invoice = $no_invoice;
    $htransret->tgl = Carbon::now()->toDateString();
    $htransret->biaya_ongkir = 0;
    $htransret->unique_harga = 0;
    $htransret->grand_total = 0;
    $htransret->terbayar = 0;
    $htransret->sisa_bayar = 0;
    $htransret->bank_users_id = $reftransaksi->bank_users_id;
    $htransret->users_alamat_id = $reftransaksi->users_alamat_id;
    $htransret->id_cust = $reftransaksi->id_cust;
    $htransret->reff_no = $request->input('ref_no');
    $htransret->save();
    $grandtotal=0;
    $cart = $request->input('cart');
    foreach ($cart as $key => $value) {
      if ($value<>0)
      {
        $reftrans = DTransaksi::where('id_cart',$key)->first();
        $cartretur = new Cart();
        $cartretur->id_cust = $reftrans->cart->id_cust;
        $cartretur->id_barang = $reftrans->cart->id_barang;
        $cartretur->id_barang_warna = $reftrans->cart->id_barang_warna;
        $cartretur->check_out = 2;
        $cartretur->qty = $value*-1;
        $cartretur->save();

        $dtransretur = new DTransaksi();
    		$dtransretur->id_cart = $cartretur->id;
    		$dtransretur->harga = $reftrans->harga;
    		$dtransretur->subtotal = $reftrans->harga*($value*-1);
        $dtransretur->reff_no = $reftrans->id;
        // $dtransretur->id_h_transaksi =
        // $dtransretur->cart->barang->stok = $dtransretur->cart->barang->stok+$value;
        $grandtotal=$grandtotal+$reftrans->harga*($value*-1);
        // $brg = Barang::find($reftrans->cart->id_barang);
        // $brg->stok = $brg->stok+$value;
        // $brg->save();
    		$htransret->d_transaksis()->save($dtransretur);
      }
    }
    $htransret->grand_total=$grandtotal;
    $htransret->save();
    // return '';

    // foreach ($cart as $key => $item) {
  	// 	$deta_trans = new DTransaksi();
  	// 	$deta_trans->id_cart=$item->id;
  	// 	$deta_trans->harga=$item->barang->departments->first()->pivot->harga;
  	// 	$deta_trans->subtotal=$item->qty*$item->barang->departments->first()->pivot->harga;
    //   $deta_trans->cart->barang->stok = $deta_trans->cart->barang->stok-$item->qty;
    //   $brg = Barang::find($item->id_barang);
    //   $brg->stok = $brg->stok-$item->qty;
    //   $brg->save();
  	// 	$head_trans->d_transaksis()->save($deta_trans);
    // }
    // $htransret->grand_total = $reftransaksi->
    // return $cartretur;
    // return '';


    // // return $request->all();
    // $invoice = $request->input('inv');
    // // return $invoice;
    //
    // $grand_total = 0;
    // $dpiutang = [];
    // foreach ($invoice as $key => $value) {
    //   if ($value['dibayar'] <= 0){
    //     continue;
    //   }
    //   $grand_total = $grand_total+$value['dibayar'];
    //   $dpiutang[] = new DPiutang(['dibayar'=>$value['dibayar'], 'id_h_transaksi'=>$key]);
    //   $trans = HTransaksi::findOrFail($key);
    //   $trans->terbayar = $trans->terbayar+$value['dibayar'];
    //   $trans->sisa_bayar = $trans->sisa_bayar-$value['dibayar'];
    //   // return $trans;
    //   $trans->save();
    // }
    // $last_trans = HPiutang::where('tgl','=', Carbon::now()->toDateString())->count()+1;
    // $no_piutang = $this->GenerateNota($last_trans,'P');
    // $piutang = new HPiutang();
    // $piutang->no_piutang =$no_piutang;
    // // $piutang->status = $request->input('keterangan');
    // $piutang->tgl = Carbon::now()->toDateString();
    // $piutang->id_operator = Auth::guard('web_admin')->id();
    // $piutang->grand_total = $grand_total;
    // // return $piutang;
    // $piutang->save();
    // $piutang->d_piutangs()->saveMany($dpiutang);
    // // return $piutang->d_piutangs;
    return redirect($request->url())->with('message', 'Retur '.$no_invoice. ' berhasil terbuat');
  }

  public function StoreEdit(Request $request)
  {
    // // return $request->all();
    // $id = $request->input('id');
    // $invoice = $request->input('inv');
    // $grand_total = 0;
    // $dpiutang = [];
    // $oldpiutang = DPiutang::where('id_h_piutang',$id);
    // foreach ($oldpiutang->get() as $key => $value) {
    //   $trans = HTransaksi::findOrFail($value->id_h_transaksi);
    //   $trans->terbayar = $trans->terbayar-$value->dibayar;
    //   $trans->sisa_bayar = $trans->sisa_bayar+$value->dibayar;
    //   $trans->save();
    // }
    // $oldpiutang->delete();
    // foreach ($invoice as $key => $value) {
    //   $grand_total = $grand_total+$value['dibayar'];
    //   $dpiutang[] = new DPiutang(['dibayar'=>$value['dibayar'], 'id_h_transaksi'=>$key]);
    //   $trans = HTransaksi::findOrFail($key);
    //   $trans->terbayar = $trans->terbayar+$value['dibayar'];
    //   $trans->sisa_bayar = $trans->sisa_bayar-$value['dibayar'];
    //   $trans->save();
    // }
    // $last_trans = HPiutang::where('tgl','=', Carbon::now()->toDateString())->count()+1;
    // $no_piutang = $this->GenerateNota($last_trans,'P');
    // $piutang = HPiutang::findOrFail($id);
    // $piutang->no_piutang =$no_piutang;
    // // $piutang->status = $request->input('keterangan');
    // $piutang->tgl = Carbon::now()->toDateString();
    // $piutang->id_operator = Auth::guard('web_admin')->id();
    // $piutang->grand_total = $grand_total;
    // $piutang->save();
    // $piutang->d_piutangs()->saveMany($dpiutang);
    // return redirect(route('list.retur'))->with('message', 'Piutang '.$no_piutang. ' berhasil diedit');
  }

  protected function validator(array $data)
  {
      return Validator::make($data, [
      ]);
  }

  public function Destroy(Request $request)
  {
    $id = Crypt::decryptString($request->input('i'));
    $piutang = HPiutang::findOrFail($id);
    $nama = $piutang->no_piutang;
    $piutang->delete();
    return redirect(route('list.retur'))->with('message', 'No Piutang '.$nama.' berhasil dihapus');
  }

  public function DataTableSSPSearchInvoice()
  {
    $field = ['id','no_invoice','tgl','kurir','no_resi','diterima'];
    $piutang = HTransaksi::where('sisa_bayar','=','0')
    ->where('diterima','=','1')
    ->where('no_invoice', 'like', 'INV%')
    ->get($field);
    $datatable = Datatables::of($piutang)
    ->addColumn('action', function ($item) {
      $ret = '<td class="uk-text-center"><button class="md-btn md-btn-primary ref-invoice" type="button" data-ref="'.$item->id.'" data-invoice="'.$item->no_invoice.'">Pilih</button>';
      $ret.='</td>';
      return $ret;
    })
    ->removeColumn('diterima')
    ->escapeColumns([])
    ->make();
    return $datatable;
  }
  public function DataTableSSPLoadBarang(Request $request)
  {
    // $field = ['cart.id as id_cart', 'cart.qty','barang.id as id_barang', 'barang.nama', 'barang_warna.nama as warna'];
    // $invoice = DB::table('d_transaksi')
    // ->join('cart', 'cart.id', '=', 'd_transaksi.id_cart')
    // ->join('barang', 'barang.id', '=', 'cart.id_barang')
    // ->join('barang_warna', 'barang_warna.id', '=', 'cart.id_barang_warna')
    // ->leftjoin('d_transaksi as d_retur','d_retur.reff_no','d_transaksi.id' )
    // ->where('d_transaksi.id_h_transaksi', '=', $request->input('ref_id'))
    // ->select($field)
    // ->get();
    $field = ['d_cart.id as id_cart', DB::raw('d_cart.qty+ifnull(c_retur.qty,0) as qty'),'barang.id as id_barang', 'barang.nama', 'barang_warna.nama as warna'];
    $invoice = DB::table('d_transaksi')
    ->join('cart as d_cart', 'd_cart.id', '=', 'd_transaksi.id_cart')
    ->join('barang', 'barang.id', '=', 'd_cart.id_barang')
    ->join('barang_warna', 'barang_warna.id', '=', 'd_cart.id_barang_warna')
    ->leftjoin('d_transaksi as d_retur','d_retur.reff_no','d_transaksi.id' )
    ->leftjoin('cart as c_retur','d_retur.id_cart','c_retur.id')
    ->where('d_transaksi.id_h_transaksi', '=', 1)
    ->whereRaw('d_cart.qty+ifnull(c_retur.qty,0)<>0')
    ->select($field)
    ->get();
    // $field = ['id','no_invoice','tgl','kurir','no_resi','diterima'];
    // $invoice = DTransaksi::with(['cart.barang','cart.barang_warna'])
    // ->get($field);
    $datatable = Datatables::of($invoice)
    ->addColumn('action', function ($item) use ($request){
      $ret = '<td class="uk-text-center"><input class="md-input masked_input label-fixed diretur" name="cart['.$item->id_cart.']" type="text" data-inputmask="\'alias\': \'currency\', \'groupSeparator\': \',\', \'autoGroup\': true, \'digits\': 2, \'digitsOptional\': false, \'prefix\': \'RP \', \'placeholder\': \'0\', \'min\':\'0\', \'max\':\''.($item->qty).'\'" data-inputmask-showmaskonhover="false"/>';
      $ret.='</td>';
      return $ret;
    })
    // ->removeColumn('qty_retur')
    ->escapeColumns([])
    ->make();
    return $datatable;
    // $request->input('ref_id')
  }
}
