<?php

namespace App\Http\Controllers\Back\Report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\Barang;
use App\Models\BarangKategori;
use App\Models\HTransaksi;
use App\Models\User;
use Yajra\Datatables\Facades\Datatables;
use Response;
use Illuminate\Support\Facades\Crypt;
use Carbon\Carbon;
use App\DataTables\LapPiutangBelumLunasDataTable;
use App\DataTables\LapSisaStokDataTable;
use Illuminate\Support\Facades\DB;
use Charts;

class Back_Report extends Controller
{
  public function PiutangBelumLunas(Request $request,LapPiutangBelumLunasDataTable $dataTable)
  {
    return $dataTable->render('back.menus.report.piutang-belum-lunas');
  }

  public function SisaStokList()
  {
    $category = BarangKategori::get();
    return view('back.menus.report.sisa-stok',compact('category'));
  }
  public function SisaStokDataTable()
  {
    $field =['id','nama','stok','id_barang_kategori'];
    $barang = Barang::with(array('barang_kategori' => function($query)
    {
     $query->addSelect(array('id','nama'));
    }))->get($field);
    $dataTable = Datatables::of($barang)
    ->editColumn('barang_kategori', function ($item) {
      return '<td class="uk-text-center">'.$item->barang_kategori->nama.'</td>';
    })
    ->removeColumn('id','id_barang_kategori')
    ->escapeColumns([])
    ->make();
    // return $datatable;
    return view('back.menus.report.sisa-stok',compact('dataTable'));
  }

  public function PenjualanPerBulan(Request $request)
  {
    $start = Carbon::parse($request->start_date)->startOfMonth();
    $end = Carbon::parse($request->end_date)->endOfMonth();
    $diff_date = $start->diffInMonths($end)+1;
    $title = 'Tingkat Penjualan Per Bulan ('.$request->start_date.' - '.$request->end_date.')';
    $chart = Charts::database(HTransaksi::whereBetween('created_at', array($start, $end))->get(), 'line', 'highcharts');
    $chart->aggregateColumn('grand_total', 'sum');
    $chart->groupByMonth();
    $chart->elementLabel("grand_total");
    $chart->title($title);
    $chart->lastByMonth($diff_date);;
    // return $chart
    return view('back.menus.report.tingkat-penjualan-perbulan',compact('title','chart'));
  }

  public function PenjualanPerHari(Request $request)
  {
    $start = Carbon::parse($request->start_date);
    $end = Carbon::parse($request->end_date);
    $diff_date = $start->diffInDays($end)+1;
    $title = 'Tingkat Penjualan Per Hari ('.$request->start_date.' - '.$request->end_date.')';
    $chart = Charts::database(HTransaksi::whereBetween('created_at', array($start, $end))->get(), 'line', 'highcharts');
    $chart->aggregateColumn('grand_total', 'sum');
    $chart->elementLabel("grand_total");
    $chart->title($title);
    // $chart->groupBy('tgl');
    $chart->groupByDay();
    $chart->lastByDay($diff_date);
    // return $chart
    return view('back.menus.report.tingkat-penjualan-perhari',compact('title','chart'));
  }

  public function TotalRegisterUserPerTgl(Request $request)
  {
    $start = Carbon::parse($request->start_date);
    $end = Carbon::parse($request->end_date);
    $diff_date = $start->diffInDays($end)+1;
    $title = 'Total Register User Per Tgl ('.$request->start_date.' - '.$request->end_date.')';

    $chart = Charts::database(User::whereBetween('created_at', array($request->start_date, $request->end_date))->get(), 'line', 'highcharts');
    $chart->aggregateColumn('id', 'count');
    $chart->title($title);
    $chart->lastByDay($diff_date);
    return view('back.menus.report.total-register-user-pertgl',compact('title','chart'))->with('input',$request->all());
  }
}
