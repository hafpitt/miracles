<?php

namespace App\Http\Controllers\Back\Broadcast;

use App\Models\Subscriber;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Mail;
use App\Mail\BroadcastSubscriber;
use Validator;

class Back_Broadcast extends Controller
{
  public function Index()
  {
    return view('back.menus.broadcast-subscriber.content');
  }

  public function Post(Request $request)
  {
    $validator = $this->validator($request->all());
    if ($validator->fails())
    {
      return redirect()->back()->withErrors($validator)->withInput();
    }
    else {
      return $this->SendMails($request);
    }

  }
  public function SendMails(Request $request)
  {
    $subscriber = Subscriber::get();
    $subject = $request->input('subject');
    foreach ($subscriber as $key => $value) {
      if (config('app.debug') <> 1)
      {
        Mail::to($value->email)->send(new BroadcastSubscriber($request));
      }
    }
    return redirect($request->url())->with('message', 'Newsletter '.$subject.' berhasil dikirim');
  }

  protected function validator(array $data)
  {
      return Validator::make($data, [
          'subject' => 'required|string|max:255',
          'content' => 'required|string',
      ]);
  }
}
