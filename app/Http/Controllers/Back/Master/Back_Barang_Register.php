<?php

namespace App\Http\Controllers\Back\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\BarangKategori;
use App\Models\BarangFoto;
use App\Models\BarangWarna;
use App\Models\Barang;
use App\Models\Department;
use Yajra\Datatables\Facades\Datatables;
use Response;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;

class Back_Barang_Register extends Controller
{
  public function IndexEntry(Request $request)
  {
    $category = BarangKategori::get();
    $department = Department::where(function ($query) {
    $query->where('id', '=', '2')
          ->orWhere('id', '=', '3')
          ->orWhere('id', '=', '4');
    })->get();
    if ($request->has('i'))
    {
      $id = Crypt::decryptString($request->input('i'));
      $barang_register = Barang::with('departments')->find($id);
      // return $department;
      // return $barang_register->departments;
      // print_r($barang_register->departments[0]->pivot->harga);
      // return;
      return view('back.menus.master.barang.register.entry',compact('barang_register','category','department'));
    }
    else {
      return view('back.menus.master.barang.register.entry',compact('category','department'));
    }
  }

  public function IndexList()
  {
    return view('back.menus.master.barang.register.list');
  }

  public function Post(Request $request)
  {
    if($request->ajax())
    {
      $data = json_decode($request->input('form'),true);
      $request->merge($data);
    }
    $validator = $this->validator($request->all());
    if ($validator->fails())
    {
      return redirect()->back()->withErrors($validator)->withInput();
    }
    else {
      if (!$request->has('id'))
      {
        return $this->StoreNew($request);
      }
      else {
        return $this->StoreEdit($request);
      }
    }
  }

  public function StoreNew(Request $request)
  {
    $nama = $request->input('nama');
    $price = $request->input('price');
    $stok = $request->input('stok');
    $berat = $request->input('berat');
    $keterangan = $request->input('keterangan');
    $keterangandetail = $request->input('keterangandetail');
    $id_barang_kategori = $request->input('id_barang_kategori');
    $warna = $request->input('warna');
    $price = $request->input('price');
    $barang_catagory_url = BarangKategori::find($id_barang_kategori);
    $barang = new Barang();
    $barang->nama = $nama;
    $barang->stok = $stok;
    $barang->keterangan = $keterangan;
    $barang->keterangandetail = $keterangandetail;
    $barang->berat = $berat;
    $barang->id_barang_kategori = $id_barang_kategori;
    $barang->url_page = strtolower(str_replace(' ','-',$barang_catagory_url->url_page).'/'.str_replace(' ','-',$nama));
    $barang->save();
    // return $price;
    $barangprice=[];
    foreach ($price as $key => $value) {
      $barangprice[$key] = ['harga'=>$value];
    }
    // return $barangprice;

    $barang->departments()->sync($barangprice);

		// $warna = Barangwarna::where('id_barang', '=', $barang->id)->delete();
		foreach ($warna as $key => $value) {
      if($value['nama']<>'')
      {
        $warna = new BarangWarna();
        $warna->nama = $value['nama'];
        $warna->warna = '#'.$value['warna'];
        $warna->tersedia = (!isset($value['tersedia'])?"0":"1");
        $barang->barang_warnas()->save($warna);
      }
    }

    $barangfoto = [];
    if ($request->hasFile('url_photo')) {
      foreach ($request->file('url_photo') as $key => $url_photo) {
        $checked=0;
        if ($request->has('photo_utama')) {
          if ($url_photo->getClientOriginalName() == $request->input('photo_utama'))
          {
            $checked=1;
          }
        }
        $barangfoto[] = new BarangFoto(['url_photo' => Storage::disk('public')->put('barang', $url_photo),'utama'=>$checked, 'nama'=>$url_photo->getClientOriginalName()]);
      }
    }
    $barang->barang_fotos()->saveMany($barangfoto);
    // return redirect($request->url())->with('message', 'Barang '.$nama.' berhasil didaftarkan');
  }

  public function StoreEdit(Request $request)
  {
    $id = $request->input('id');
    $nama = $request->input('nama');
    $price = $request->input('price');
    $stok = $request->input('stok');
    $berat = $request->input('berat');
    $keterangan = $request->input('keterangan');
    $keterangandetail = $request->input('keterangandetail');
    $id_barang_kategori = $request->input('id_barang_kategori');
    $new_warna = $request->input('warna');
    $old_warna = $request->input('old_warna');
    $price = $request->input('price');
    $barang_catagory_url = BarangKategori::find($id_barang_kategori);
    $barang = Barang::findOrFail($id);
    $barang->nama = $nama;
    $barang->stok = $stok;
    $barang->keterangan = $keterangan;
    $barang->keterangandetail = $keterangandetail;
    $barang->berat = $berat;
    $barang->id_barang_kategori = $id_barang_kategori;
    $barang->url_page = strtolower(str_replace(' ','-',$barang_catagory_url->url_page).'/'.str_replace(' ','-',$nama));
    $barang->save();
    // return $price;
    $barangprice=[];
    foreach ($price as $key => $value) {
      $barangprice[$key] = ['harga'=>$value];
    }
    // return $barangprice;

    $barang->departments()->sync($barangprice);
    // Barangwarna::where('id_barang', '=', $barang->id)->delete();
    foreach ($old_warna as $key => $value) {
      if($value['nama']<>'')
      {
        $warna = BarangWarna::find($key);
        $warna->nama = $value['nama'];
        $warna->warna = '#'.$value['warna'];
        $warna->tersedia = (!isset($value['tersedia'])?"0":"1");
        $barang->barang_warnas()->save($warna);
      }
    }
		foreach ($new_warna as $key => $value) {
      if($value['nama']<>'')
      {
        $warna2 = new BarangWarna();
        $warna2->nama = $value['nama'];
        $warna2->warna = '#'.$value['warna'];
        $warna2->tersedia = (!isset($value['tersedia'])?"0":"1");
        $barang->barang_warnas()->save($warna2);
      }
    }

    if ($request->has('removed')) {
      foreach ($request->input('removed') as $key => $removed) {
        BarangFoto::where('url_photo','=',$removed)->delete();
        Storage::disk('public')->delete($removed);
      }
    };
    $barangfoto = [];
    if ($request->hasFile('url_photo')) {
      foreach ($request->file('url_photo') as $key => $url_photo) {
        $checked=0;
        if ($request->has('photo_utama')) {
          if ($url_photo->getClientOriginalName() == $request->input('photo_utama'))
          {
            $checked=1;
          }
        }
        $barangfoto[] = new BarangFoto(['url_photo' => Storage::disk('public')->put('barang', $url_photo),'utama'=>$checked, 'nama'=>$url_photo->getClientOriginalName()]);
      }
    }
    $barang->barang_fotos()->saveMany($barangfoto);
    // return redirect(route('list.barang_register'))->with('message', 'Barang '.$nama.' berhasil direvisi');
  }

  protected function validator(array $data)
  {
      return Validator::make($data, [
          'nama' => 'required|string|min:3|max:255|unique:barang,nama,'. (isset($data['id']) ? $data['id']: ''),
          'id_barang_kategori' => 'required',
          'url_photo.*' => 'required|mimes:jpg,jpeg,png,bmp|max:20000',
          'price' => 'required'
      ]);
  }

  public function Destroy(Request $request)
  {
    $id = Crypt::decryptString($request->input('i'));
    $barang = Barang::find($id);
    $nama = $barang->nama;
    $barang->delete();
    return redirect(route('list.barang_register'))->with('message', 'Barang '.$nama.' berhasil dihapus');
  }

  public function DataTableSSP()
  {
    $field =['id','nama','stok','berat','id_barang_kategori'];
    $barang = Barang::with(array('barang_kategori' => function($query)
    {
     $query->addSelect(array('id','nama'));
    }))->get($field);
    // $package = HPacket::get();
    $datatable = Datatables::of($barang)
    ->addColumn('action', function ($item) {
      return '<td class="uk-text-center">
          <a href="'.route('entry.barang_register', ['i' => Crypt::encryptString($item->id)]).'"><i class="md-icon material-icons">&#xE254;</i></a>
      </td>';
    // <a class="try-delete" href="'.route('destroy.barang_register', ['i' => Crypt::encryptString($item->id)]).'"><i class="md-icon material-icons">&#xE92B;</i></a>
    })

    ->editColumn('barang_kategori', function ($item) {
      return '<td class="uk-text-center">'.$item->barang_kategori->nama.'</td>';
    })
    ->removeColumn('id_barang_kategori')
    ->escapeColumns([])
    ->make();
    return $datatable;
  }
}
