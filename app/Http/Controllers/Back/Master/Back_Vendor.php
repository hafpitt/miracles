<?php

namespace App\Http\Controllers\Back\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\Departement as Department;
use App\Models\Vendor;
use Yajra\Datatables\Facades\Datatables;
use Response;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;

class Back_Vendor extends Controller
{
  public function IndexEntry(Request $request)
  {
    $department = Department::all();
    if ($request->has('i'))
    {
      $id = Crypt::decryptString($request->input('i'));
      $vendor = Vendor::find($id);
      return view('back.menus.master.vendor.entry',compact('vendor','department'));
    }
    else {
      return view('back.menus.master.vendor.entry',compact('department'));
    }

  }
  public function IndexList()
  {
    return view('back.menus.master.vendor.list');
  }
  public function Post(Request $request)
  {
    $validator = $this->validator($request->all());
    if ($validator->fails())
    {
      return redirect()->back()->withErrors($validator)->withInput();
    }
    else {
      if (!$request->has('id'))
      {
        return $this->StoreNew($request);
      }
      else {
        return $this->StoreEdit($request);
      }
    }

  }

  public function StoreNew(Request $request)
  {
    $email = $request->input('email');
    $nama = $request->input('nama');
    $password = $request->input('password');
    $no_telp = $request->input('no_telp');
    $department = $request->input('departement');
    $vendor = new Vendor();
    $vendor->nama = $nama;
    $vendor->email = $email;
    $vendor->password=bcrypt($password);
    $vendor->notelp = $no_telp;
    if ($request->hasFile('url_photo')) {
      $vendor->url_photo = Storage::disk('public')->put('avatars', $request->file('url_photo'));
    }
    $vendor->save();
    $vendor->departements()->attach($department);
    return redirect($request->url())->with('message', 'Vendor '.$nama.' berhasil didaftarkan');
  }

  public function StoreEdit(Request $request)
  {
    $id = $request->input('id');
    $email = $request->input('email');
    $nama = $request->input('nama');
    $password = $request->input('password');
    $no_telp = $request->input('no_telp');
    $department = $request->input('departement');
    $vendor = Vendor::findOrFail($id);
    $vendor->nama = $nama;
    $vendor->notelp = $no_telp;
    if ($request->hasFile('url_photo')) {
      Storage::disk('public')->delete($vendor->url_photo);
      $vendor->url_photo = Storage::disk('public')->put('avatars', $request->file('url_photo'));
    }
    $vendor->save();
    $vendor->departements()->sync($department);
    return redirect(route('list.vendor'))->with('message', 'Vendor '.$nama.' berhasil direvisi');
  }

  public function DataTableSSP()
  {
    $field = ['id','nama','email','notelp','url_photo'];
    $Model = Vendor::all($field);
    $datatable = Datatables::of($Model)
    ->addColumn('action', function ($item) {
      return '<td class="uk-text-center">
          <a href="'.route('entry.vendor', ['i' => Crypt::encryptString($item->id)]).'"><i class="md-icon material-icons">&#xE254;</i></a>
          <a class="try-delete" href="'.route('destroy.vendor', ['i' => Crypt::encryptString($item->id)]).'"><i class="md-icon material-icons">&#xE92B;</i></a>
      </td>';
    })
    ->editColumn('url_photo', function ($item) {
      return '<td class="uk-text-center"><img class="md-user-image" src="'.asset(Storage::disk('public')->url($item->url_photo)).'" alt=""/></td>';
    })
    ->escapeColumns([])
    ->make();
    return $datatable;
  }
  protected function validator(array $data)
  {
      return Validator::make($data, [
          'nama' => 'required|string|max:255',
          'email' => 'required_without:id|string|email|max:255|unique:users|unique:vendor',
          'departement' => 'required',
          'no_telp' => 'required',
          'password' => 'required_without:id|string|min:6|confirmed',
          'url_photo' => 'image',
      ]);
  }

  public function Destroy(Request $request)
  {
    $id = Crypt::decryptString($request->input('i'));
    $vendor = Vendor::find($id);
    $nama = $vendor->nama;
    $vendor->delete();
    return redirect(route('list.vendor'))->with('message', 'Vendor '.$nama.' berhasil dihapus');
  }

}
