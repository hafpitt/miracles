<?php

namespace App\Http\Controllers\Back\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\Department as Department;
use App\Models\Modul;
use Yajra\Datatables\Facades\Datatables;
use Response;
use Illuminate\Support\Facades\Crypt;

class Back_Department extends Controller
{
  public function IndexEntry(Request $request)
  {
    $modul = Modul::all();
    if ($request->has('i'))
    {
      $id = Crypt::decryptString($request->input('i'));
      $department = Department::find($id);
      return view('back.menus.master.department.entry',compact('department','modul'));
    }
    else {
      return view('back.menus.master.department.entry',compact('modul'));
    }
  }

  public function IndexList()
  {
    return view('back.menus.master.department.list');
  }

  public function Post(Request $request)
  {
    $validator = $this->validator($request->all());
    if ($validator->fails())
    {
      return redirect()->back()->withErrors($validator)->withInput();
    }
    else {
      if (!$request->has('id'))
      {
        return $this->StoreNew($request);
      }
      else {
        return $this->StoreEdit($request);
      }
    }
  }

  public function StoreNew(Request $request)
  {
    $nama = $request->input('nama');
    $modul = $request->input('modul');
    $department = new Department();
    $department->nama = $nama;
    $department->save();
    $department->moduls()->attach($modul);
    return redirect($request->url())->with('message', 'Department '.$nama.' berhasil didaftarkan');
  }

  public function StoreEdit(Request $request)
  {
    $id = $request->input('id');
    $nama = $request->input('nama');
    $modul = $request->input('modul');
    $department = Department::findOrFail($id);
    $department->nama = $nama;
    $department->save();
    $department->moduls()->sync($modul);
    return redirect(route('list.department'))->with('message', 'Department '.$nama.' berhasil direvisi');
  }

  protected function validator(array $data)
  {
      return Validator::make($data, [
          'nama' => 'required|string|min:3|max:255',
      ]);
  }

  public function Destroy(Request $request)
  {
    $id = Crypt::decryptString($request->input('i'));
    $department = Department::find($id);
    $nama = $department->nama;
    $department->delete();
    return redirect(route('list.department'))->with('message', 'Department '.$nama.' berhasil dihapus');
  }

  public function DataTableSSP()
  {
    $field =['id','nama'];
    $Model = Department::all($field);
    $datatable = Datatables::of($Model)
    ->addColumn('action', function ($item) {
      return '<td class="uk-text-center">
          <a class="try-update" href="'.route('entry.department', ['i' => Crypt::encryptString($item->id)]).'"><i class="md-icon material-icons">&#xE254;</i></a>
          <a class="try-delete" href="'.route('destroy.department', ['i' => Crypt::encryptString($item->id)]).'"><i class="md-icon material-icons">&#xE92B;</i></a>
      </td>';
    })
    ->escapeColumns([])
    ->make();
    return $datatable;
  }
}
