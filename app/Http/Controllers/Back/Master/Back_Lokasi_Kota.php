<?php

namespace App\Http\Controllers\Back\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\Kotum as Kota;
use App\Models\Provinsi;
use Yajra\Datatables\Facades\Datatables;
use Response;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;

class Back_Lokasi_Kota extends Controller
{
  public function IndexEntry(Request $request)
  {
    $provinsi = Provinsi::get();
    if ($request->has('i'))
    {
      $id = Crypt::decryptString($request->input('i'));
      $lokasi_kota = Kota::find($id);
      return view('back.menus.master.lokasi.kota.entry',compact('lokasi_kota','provinsi'));
    }
    else {
      return view('back.menus.master.lokasi.kota.entry',compact('provinsi'));
    }
  }

  public function IndexList()
  {
    return view('back.menus.master.lokasi.kota.list');
  }

  public function Post(Request $request)
  {
    $validator = $this->validator($request->all());
    if ($validator->fails())
    {
      return redirect()->back()->withErrors($validator)->withInput();
    }
    else {
      if (!$request->has('id'))
      {
        return $this->StoreNew($request);
      }
      else {
        return $this->StoreEdit($request);
      }
    }
  }

  public function StoreNew(Request $request)
  {
    $nama = $request->input('nama');
    $provinsi = $request->input('id_provinsi');

    $lokasi_kota = new Kota();
    $lokasi_kota->nama = $nama;
    $lokasi_kota->id_provinsi = $provinsi;
    $lokasi_kota->save();
    return redirect($request->url())->with('message', 'Kota '.$nama.' berhasil didaftarkan');
  }

  public function StoreEdit(Request $request)
  {
    $id = $request->input('id');
    $nama = $request->input('nama');
    $provinsi = $request->input('id_provinsi');

    $lokasi_kota = Kota::findOrFail($id);
    $lokasi_kota->nama = $nama;
    $lokasi_kota->id_provinsi = $provinsi;
    $lokasi_kota->save();
    return redirect(route('list.lokasi_kota'))->with('message', 'Kota '.$nama.' berhasil direvisi');
  }

  protected function validator(array $data)
  {
      return Validator::make($data, [
          'nama' => 'required_without:id|string|min:3|max:255|unique:kota,nama,'. (isset($data['id']) ? $data['id']: ''),
          // 'url_photo' => 'image|required_without:id',
      ]);
  }

  public function Destroy(Request $request)
  {
    $id = Crypt::decryptString($request->input('i'));
    $lokasi_kota = Kota::find($id);
    $nama = $lokasi_kota->nama;
    $lokasi_kota->delete();
    return redirect(route('list.lokasi_kota'))->with('message', 'Kota '.$nama.' berhasil dihapus');
  }

  public function DataTableSSP()
  {
    $field = ['id','nama','id_provinsi'];
    $package = Kota::with(array('provinsi' => function($query)
    {
     $query->addSelect(array('id','nama'));
    }))->get($field);
    $datatable = Datatables::of($package)
    ->addColumn('action', function ($item) {
      return '<td class="uk-text-center">
          <a href="'.route('entry.lokasi_kota', ['i' => Crypt::encryptString($item->id)]).'"><i class="md-icon material-icons">&#xE254;</i></a>
          <a class="try-delete" href="'.route('destroy.lokasi_kota', ['i' => Crypt::encryptString($item->id)]).'"><i class="md-icon material-icons">&#xE92B;</i></a>
      </td>';
    })
    ->editColumn('provinsi', function ($item) {
      return '<td class="uk-text-center">'.$item->provinsi->nama.'</td>';
    })
    ->removeColumn('id_provinsi')
    ->escapeColumns([])
    ->make();
    return $datatable;
  }
}
