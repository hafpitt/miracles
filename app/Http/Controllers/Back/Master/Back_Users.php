<?php

namespace App\Http\Controllers\Back\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\Department as Department;
use App\Models\User;
use Yajra\Datatables\Facades\Datatables;
use Response;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;

class Back_Users extends Controller
{
  public function IndexEntry(Request $request)
  {
    $department = Department::get();
    if ($request->has('i'))
    {
      $id = Crypt::decryptString($request->input('i'));
      $users = User::find($id);
      return view('back.menus.master.users.entry',compact('users','department'));
    }
    else {
      return view('back.menus.master.users.entry',compact('department'));
    }
  }

  public function IndexList()
  {
    return view('back.menus.master.users.list');
  }

  public function Post(Request $request)
  {
    $validator = $this->validator($request->all());
    if ($validator->fails())
    {
      return redirect()->back()->withErrors($validator)->withInput();
    }
    else {
      if (!$request->has('id'))
      {
        return $this->StoreNew($request);
      }
      else {
        return $this->StoreEdit($request);
      }
    }
  }

  public function StoreNew(Request $request)
  {
    $email = $request->input('email');
    $nama = $request->input('nama');
    $password = $request->input('password');
    $no_telp = $request->input('no_telp');
    $department = $request->input('department_id');
    $user = new User();
    $user->nama = strtoupper($nama);
    $user->email = $email;
    $user->password=bcrypt($password);
    $user->notelp = $no_telp;
    $user->verified = 1;
    $user->department_id = $department;
    if ($request->hasFile('url_photo')) {
      $user->url_photo = Storage::disk('public')->put('avatars', $request->file('url_photo'));
    }
    $user->save();

    return redirect($request->url())->with('message', 'User '.$nama.' berhasil didaftarkan');
  }

  public function StoreEdit(Request $request)
  {
    $id = $request->input('id');
    $nama = $request->input('nama');
    $no_telp = $request->input('no_telp');
    $department = $request->input('department_id');
    $user = User::findOrFail($id);
    $user->nama = $nama;
    $user->notelp = $no_telp;
    $user->department_id = $department;
    if ($request->hasFile('url_photo')) {
      Storage::disk('public')->delete($user->url_photo);
      $user->url_photo = Storage::disk('public')->put('avatars', $request->file('url_photo'));
    }
    $user->save();
    return redirect(route('list.users'))->with('message', 'User '.$nama.' berhasil direvisi');
  }

  protected function validator(array $data)
  {
      return Validator::make($data, [
          'nama' => 'required|string|max:255',
          'email' => 'required_without:id|string|email|max:255|unique:users,email',
          'department_id' => 'required',
          'no_telp' => 'required',
          'password' => 'required_without:id|string|min:6|confirmed',
          'url_photo' => 'image',
      ]);
  }

  public function Destroy(Request $request)
  {
    $id = Crypt::decryptString($request->input('i'));
    $users = User::find($id);
    $nama = $users->nama;
    $users->delete();
    return redirect(route('list.users'))->with('message', 'User '.$nama.' berhasil dihapus');
  }

  public function DataTableSSP()
  {
    $field = ['id','nama','email','notelp','url_photo'];
    $Model = User::get($field);
    $datatable = Datatables::of($Model)
    ->addColumn('action', function ($item) {
      return '<td class="uk-text-center">
          <a href="'.route('entry.users', ['i' => Crypt::encryptString($item->id)]).'"><i class="md-icon material-icons">&#xE254;</i></a>
          <a class="try-delete" href="'.route('destroy.users', ['i' => Crypt::encryptString($item->id)]).'"><i class="md-icon material-icons">&#xE92B;</i></a>
      </td>';
    })
    ->editColumn('url_photo', function ($item) {
      return '<td class="uk-text-center"><img class="md-user-image" src="'.asset(Storage::disk('public')->url($item->url_photo)).'" alt=""/></td>';
    })
    ->escapeColumns([])
    ->make();
    return $datatable;
  }
}
