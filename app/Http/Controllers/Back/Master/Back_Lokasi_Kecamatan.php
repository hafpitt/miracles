<?php

namespace App\Http\Controllers\Back\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\Kecamatan;
use App\Models\Kotum as Kota;
use Yajra\Datatables\Facades\Datatables;
use Response;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;

class Back_Lokasi_Kecamatan extends Controller
{
  public function IndexEntry(Request $request)
  {
    $kota = Kota::get();
    if ($request->has('i'))
    {
      $id = Crypt::decryptString($request->input('i'));
      $lokasi_kecamatan = Kecamatan::find($id);
      return view('back.menus.master.lokasi.kecamatan.entry',compact('lokasi_kecamatan','kota'));
    }
    else {
      return view('back.menus.master.lokasi.kecamatan.entry',compact('kota'));
    }
  }

  public function IndexList()
  {
    return view('back.menus.master.lokasi.kecamatan.list');
  }

  public function Post(Request $request)
  {
    $validator = $this->validator($request->all());
    if ($validator->fails())
    {
      return redirect()->back()->withErrors($validator)->withInput();
    }
    else {
      if (!$request->has('id'))
      {
        return $this->StoreNew($request);
      }
      else {
        return $this->StoreEdit($request);
      }
    }
  }

  public function StoreNew(Request $request)
  {
    $nama = $request->input('nama');
    $kota = $request->input('id_kota');

    $lokasi_kecamatan = new Kecamatan();
    $lokasi_kecamatan->nama = $nama;
    $lokasi_kecamatan->id_kota = $kota;
    $lokasi_kecamatan->save();
    return redirect($request->url())->with('message', 'Kecamatan '.$nama.' berhasil didaftarkan');
  }

  public function StoreEdit(Request $request)
  {
    $id = $request->input('id');
    $nama = $request->input('nama');
    $kota = $request->input('id_kota');

    $lokasi_kecamatan = Kecamatan::findOrFail($id);
    $lokasi_kecamatan->nama = $nama;
    $lokasi_kecamatan->id_kota = $kota;
    $lokasi_kecamatan->save();
    return redirect(route('list.lokasi_kecamatan'))->with('message', 'Kecamatan '.$nama.' berhasil direvisi');
  }

  protected function validator(array $data)
  {
      return Validator::make($data, [
          'nama' => 'required_without:id|string|min:3|max:255|unique:kecamatan,nama,'. (isset($data['id']) ? $data['id']: ''),
          // 'url_photo' => 'image|required_without:id',
      ]);
  }

  public function Destroy(Request $request)
  {
    $id = Crypt::decryptString($request->input('i'));
    $lokasi_kecamatan = Kecamatan::find($id);
    $nama = $lokasi_kecamatan->nama;
    $lokasi_kecamatan->delete();
    return redirect(route('list.lokasi_kecamatan'))->with('message', 'Kecamatan '.$nama.' berhasil dihapus');
  }

  public function DataTableSSP()
  {
    $field = ['id','nama','id_kota'];
    $package = Kecamatan::with(array('kotum' => function($query)
    {
     $query->addSelect(array('id','nama'));
    }))->get($field);
    $datatable = Datatables::of($package)
    ->addColumn('action', function ($item) {
      return '<td class="uk-text-center">
          <a href="'.route('entry.lokasi_kecamatan', ['i' => Crypt::encryptString($item->id)]).'"><i class="md-icon material-icons">&#xE254;</i></a>
          <a class="try-delete" href="'.route('destroy.lokasi_kecamatan', ['i' => Crypt::encryptString($item->id)]).'"><i class="md-icon material-icons">&#xE92B;</i></a>
      </td>';
    })
    ->editColumn('kotum', function ($item) {
      return '<td class="uk-text-center">'.$item->kotum->nama.'</td>';
    })
    ->removeColumn('id_kota')
    ->escapeColumns([])
    ->make();
    return $datatable;
  }
}
