<?php

namespace App\Http\Controllers\Back\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\BarangKategori;
use Yajra\Datatables\Facades\Datatables;
use Response;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;

class Back_Barang_Category extends Controller
{
  public function IndexEntry(Request $request)
  {
    if ($request->has('i'))
    {
      $id = Crypt::decryptString($request->input('i'));
      $barang_catagory = BarangKategori::find($id);
      return view('back.menus.master.barang.category.entry',compact('barang_catagory'));
    }
    else {
      return view('back.menus.master.barang.category.entry');
    }
  }

  public function IndexList()
  {
    return view('back.menus.master.barang.category.list');
  }

  public function Post(Request $request)
  {
    $validator = $this->validator($request->all());
    if ($validator->fails())
    {
      return redirect()->back()->withErrors($validator)->withInput();
    }
    else {
      if (!$request->has('id'))
      {
        return $this->StoreNew($request);
      }
      else {
        return $this->StoreEdit($request);
      }
    }
  }

  public function StoreNew(Request $request)
  {
    $nama = $request->input('nama');
    $barang_category = new BarangKategori();
    $barang_category->nama = $nama;
    $barang_category->url_page = strtolower(str_replace(' ','-',$nama));
    if ($request->hasFile('url_photo')) {
      $barang_category->url_photo = Storage::disk('public')->put('barang_category', $request->file('url_photo'));
    }
    $barang_category->save();
    return redirect($request->url())->with('message', 'Barang Catagory '.$nama.' berhasil didaftarkan');
  }

  public function StoreEdit(Request $request)
  {
    $id = $request->input('id');
    $nama = $request->input('nama');
    $barang_category = BarangKategori::findOrFail($id);
    $barang_category->nama = $nama;
    $barang_category->url_page = strtolower(str_replace(' ','-',$nama));
    if ($request->hasFile('url_photo')) {
      Storage::disk('public')->delete($barang_category->url_photo);
      $barang_category->url_photo = Storage::disk('public')->put('barang_category', $request->file('url_photo'));
    }
    $barang_category->save();
    return redirect(route('list.barang_category'))->with('message', 'Barang Catagory '.$nama.' berhasil direvisi');
  }

  protected function validator(array $data)
  {
      return Validator::make($data, [
          'nama' => 'required_without:id|string|min:3|max:255|unique:barang_kategori,nama,'. (isset($data['id']) ? $data['id']: ''),
          'url_photo' => 'image|required_without:id',
      ]);
  }

  public function Destroy(Request $request)
  {
    $id = Crypt::decryptString($request->input('i'));
    $barang_catagory = BarangKategori::find($id);
    $nama = $barang_catagory->nama;
    $barang_catagory->delete();
    return redirect(route('list.barang_category'))->with('message', 'Barang Catagory '.$nama.' berhasil dihapus');
  }

  public function DataTableSSP()
  {
    $field = ['id','nama','url_photo'];
    $Model = BarangKategori::get($field);
    $datatable = Datatables::of($Model)
    ->addColumn('action', function ($item) {
      return '<td class="uk-text-center">
          <a href="'.route('entry.barang_category', ['i' => Crypt::encryptString($item->id)]).'"><i class="md-icon material-icons">&#xE254;</i></a>
          <a class="try-delete" href="'.route('destroy.barang_category', ['i' => Crypt::encryptString($item->id)]).'"><i class="md-icon material-icons">&#xE92B;</i></a>
      </td>';
    })
    ->editColumn('url_photo', function ($item) {
      return '<td class="uk-text-center"><img class="md-user-image" src="'.asset(Storage::disk('public')->url($item->url_photo)).'" alt=""/></td>';
    })
    ->escapeColumns([])
    ->make();
    return $datatable;
  }
}
