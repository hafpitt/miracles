<?php

namespace App\Http\Controllers\Back\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\Bank;
use Yajra\Datatables\Facades\Datatables;
use Response;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;

class Back_Bank extends Controller
{
  public function IndexEntry(Request $request)
  {
    if ($request->has('i'))
    {
      $id = Crypt::decryptString($request->input('i'));
      $bank = Bank::find($id);
      return view('back.menus.master.bank.entry',compact('bank'));
    }
    else {
      return view('back.menus.master.bank.entry');
    }
  }

  public function IndexList()
  {
    return view('back.menus.master.bank.list');
  }

  public function Post(Request $request)
  {
    $validator = $this->validator($request->all());
    if ($validator->fails())
    {
      return redirect()->back()->withErrors($validator)->withInput();
    }
    else {
      if (!$request->has('id'))
      {
        return $this->StoreNew($request);
      }
      else {
        return $this->StoreEdit($request);
      }
    }
  }

  public function StoreNew(Request $request)
  {
    $nama = $request->input('nama');
    $bank = new Bank();
    $bank->nama = $nama;
    if ($request->hasFile('url_photo')) {
      $bank->url_photo = Storage::disk('public')->put('bank', $request->file('url_photo'));
    }
    $bank->save();
    return redirect($request->url())->with('message', 'Bank '.$nama.' berhasil didaftarkan');
  }

  public function StoreEdit(Request $request)
  {
    $id = $request->input('id');
    $nama = $request->input('nama');
    $bank = Bank::findOrFail($id);
    $bank->nama = $nama;
    if ($request->hasFile('url_photo')) {
      Storage::disk('public')->delete($bank->url_photo);
      $bank->url_photo = Storage::disk('public')->put('bank', $request->file('url_photo'));
    }
    $bank->save();
    return redirect(route('list.bank'))->with('message', 'Bank '.$nama.' berhasil direvisi');
  }

  protected function validator(array $data)
  {
      return Validator::make($data, [
          'nama' => 'required_without:id|string|min:3|max:255|unique:bank,nama,'. (isset($data['id']) ? $data['id']: ''),
          'url_photo' => 'image|required_without:id',
      ]);
  }

  public function Destroy(Request $request)
  {
    $id = Crypt::decryptString($request->input('i'));
    $bank = Bank::find($id);
    $nama = $bank->nama;
    $bank->delete();
    return redirect(route('list.bank'))->with('message', 'Bank '.$nama.' berhasil dihapus');
  }

  public function DataTableSSP()
  {
    $field = ['id','nama','url_photo'];
    $Model = Bank::get($field);
    $datatable = Datatables::of($Model)
    ->addColumn('action', function ($item) {
      return '<td class="uk-text-center">
          <a href="'.route('entry.bank', ['i' => Crypt::encryptString($item->id)]).'"><i class="md-icon material-icons">&#xE254;</i></a>
          <a class="try-delete" href="'.route('destroy.bank', ['i' => Crypt::encryptString($item->id)]).'"><i class="md-icon material-icons">&#xE92B;</i></a>
      </td>';
    })
    ->editColumn('url_photo', function ($item) {
      return '<td class="uk-text-center"><img class="md-user-image" src="'.asset(Storage::disk('public')->url($item->url_photo)).'" alt=""/></td>';
    })
    ->escapeColumns([])
    ->make();
    return $datatable;
  }
}
