<?php

namespace App\Http\Controllers\Back\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\HGallery;
use App\Models\DGallery;
use App\Models\PacketCatagory as Package_Catagory;
use App\Models\User;
use Yajra\Datatables\Facades\Datatables;
use Response;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;

class Back_Gallery extends Controller
{
  public function IndexEntry(Request $request)
  {
    $category = Package_Catagory::get();
    $user = User::get();
    if ($request->has('i'))
    {
      $id = Crypt::decryptString($request->input('i'));
      $gallery = HGallery::find($id);
      // foreach ($gallery->d_galleries as $key => $value) {
      //   print_r($value);
      // }
      // return;
      return view('back.menus.master.package.gallery.entry',compact('gallery','category','user'));
    }
    else {
      return view('back.menus.master.package.gallery.entry',compact('category','user'));
    }
  }

  public function IndexList()
  {
    return view('back.menus.master.package.gallery.list');
  }

  public function Post(Request $request)
  {
    if($request->ajax())
    {
      $data = json_decode($request->input('form'),true);
      $request->merge($data);
    }
    // print_r($request->all());
    // return;
    // $image = $request->file('url_photo');
    // $data['url_photo'] = $image;
    $validator = $this->validator($request->all());
    if ($validator->fails())
    {
      return redirect()->back()->withErrors($validator)->withInput();
    }
    else {
      if (!$request->has('id'))
      {
        return $this->StoreNew($request);
      }
      else {
        return $this->StoreEdit($request);
      }
    }
  }

  public function StoreNew(Request $request)
  {
    $nama = $request->input('nama');
    $users_id = $request->input('users_id');
    $quotes = $request->input('quotes');
    $packet_catagory_id = $request->input('packet_catagory_id');

    $gallery = new HGallery();
    $gallery->nama = $nama;
    $gallery->users_id = $users_id;
    $gallery->quotes=$quotes;
    $gallery->packet_catagory_id = $packet_catagory_id;
    $gallery->save();
    $dgallery = [];
    if ($request->hasFile('url_photo')) {
      foreach ($request->file('url_photo') as $key => $url_photo) {
        $checked=0;
        if ($request->has('status_show')) {
          foreach ($request->input('status_show') as $key1 => $status_show) {
            if ($url_photo->getClientOriginalName() == $status_show)
            {
              $checked=1;
            }
          }
        }
        $dgallery[] = new DGallery(['url_photo' => Storage::disk('public')->put('galleries', $url_photo),'status_show'=>$checked, 'file_nama'=>$url_photo->getClientOriginalName()]);
      }
    }
    $gallery->d_galleries()->saveMany($dgallery);
    return redirect($request->url())->with('message', 'Gallery '.$nama.' berhasil didaftarkan');
  }

  public function StoreEdit(Request $request)
  {
    $id = $request->input('id');
    $nama = $request->input('nama');
    $users_id = $request->input('users_id');
    $quotes = $request->input('quotes');
    $packet_catagory_id = $request->input('packet_catagory_id');

    $gallery = HGallery::findOrFail($id);
    $gallery->nama = $nama;
    $gallery->users_id = $users_id;
    $gallery->quotes=$quotes;
    $gallery->packet_catagory_id = $packet_catagory_id;
    $gallery->save();
    $dgallery = [];
    if ($request->has('removed')) {
      foreach ($request->input('removed') as $key => $removed) {
        DGallery::where('url_photo','=',$removed)->delete();
        Storage::disk('public')->delete($removed);
      }
    }
    if ($request->hasFile('url_photo')) {
      foreach ($request->file('url_photo') as $key => $url_photo) {
        $checked=0;
        if ($request->has('status_show')) {
          foreach ($request->input('status_show') as $key1 => $status_show) {
            if ($url_photo->getClientOriginalName() == $status_show)
            {
              $checked=1;
            }
          }
        }
        $dgallery[] = new DGallery(['url_photo' => Storage::disk('public')->put('galleries', $url_photo),'status_show'=>$checked, 'file_nama'=>$url_photo->getClientOriginalName()]);
      }
    }
    $gallery->d_galleries()->saveMany($dgallery);
    // if ($request->hasFile('url_photo')) {
    //   Storage::disk('public')->delete($user->url_photo);
    //   $user->url_photo = Storage::disk('public')->put('avatars', $request->file('url_photo'));
    // }
    // $user->save();
    // $user->departements()->sync($department);
    return redirect(route('list.gallery'))->with('message', 'Gallery '.$nama.' berhasil direvisi');
  }

  protected function validator(array $data)
  {
      return Validator::make($data, [
          'nama' => 'required|string|max:255',
          'packet_catagory_id' => 'required|exists:packet_catagory,id',
          'url_photo.*' => 'required|mimes:jpg,jpeg,png,bmp|max:20000',
          'users_id' => 'required|exists:users,id',
          'quotes' => 'required|string',
      ]);
  }

  public function Destroy(Request $request)
  {
    $id = Crypt::decryptString($request->input('i'));
    $gallery = HGallery::find($id);
    $nama = $gallery->nama;
    $gallery->delete();
    return redirect(route('list.gallery'))->with('message', 'Gallery '.$nama.' berhasil dihapus');
  }

  public function DataTableSSP()
  {
    $field = ['id','nama','users_id','quotes','packet_catagory_id'];
    $gallery = HGallery::with(
      array(
        'packet_catagory' => function($query)
        {
          $query->addSelect(array('id','nama'));
        },
        'user' => function($query)
        {
          $query->addSelect(array('id','nama'));
        }
      )
    )->get($field);
    // $Model = HGallery::get($field);
    $datatable = Datatables::of($gallery)
    ->addColumn('action', function ($item) {
      return '<td class="uk-text-center">
          <a href="'.route('entry.gallery', ['i' => Crypt::encryptString($item->id)]).'"><i class="md-icon material-icons">&#xE254;</i></a>
          <a class="try-delete" href="'.route('destroy.gallery', ['i' => Crypt::encryptString($item->id)]).'"><i class="md-icon material-icons">&#xE92B;</i></a>
      </td>';
    })
    ->editColumn('user', function ($item) {
      return '<td class="uk-text-center">'.$item->user->nama.'</td>';
    })
    ->editColumn('packet_catagory', function ($item) {
      return '<td class="uk-text-center">'.$item->packet_catagory->nama.'</td>';
    })
    ->removeColumn('packet_catagory_id')
    ->removeColumn('users_id')
    ->escapeColumns([])
    ->make();
    return $datatable;
  }
}
