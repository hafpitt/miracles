<?php

namespace App\Http\Controllers\Back\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\Modul;
use Yajra\Datatables\Facades\Datatables;
use Response;
use Illuminate\Support\Facades\Crypt;

class Back_Modul extends Controller
{
  public function IndexEntry(Request $request)
  {
    if ($request->has('i'))
    {
      $id = Crypt::decryptString($request->input('i'));
      $modul = Modul::find($id);
      return view('back.menus.master.modul.entry',compact('modul'));
    }
    else {
      return view('back.menus.master.modul.entry');
    }
  }

  public function IndexList()
  {
    return view('back.menus.master.modul.list');
  }

  public function Post(Request $request)
  {
    // print_r($request->all());
    $validator = $this->validator($request->all());
    if ($validator->fails())
    {
      return redirect()->back()->withErrors($validator)->withInput();
    }
    else {
      if (!$request->has('id'))
      {
        return $this->StoreNew($request);
      }
      else {
        return $this->StoreEdit($request);
      }
    }
  }

  public function StoreNew(Request $request)
  {
    $modul = $request->input('modul');
    $url = $request->input('url');
    $Model = new Modul();
    $Model->modul = $modul;
    $Model->url = $url;
    $Model->save();
    return redirect($request->url())->with('message', 'Modul '.$modul.' berhasil didaftarkan');
  }

  public function StoreEdit(Request $request)
  {
    $id = $request->input('id');
    $modul = $request->input('modul');
    $url = $request->input('url');
    $Model = Modul::findOrFail($id);
    $Model->modul = $modul;
    $Model->url = $url;
    $Model->save();
    return redirect(route('list.modul'))->with('message', 'Modul '.$modul.' berhasil direvisi');
  }

  protected function validator(array $data)
  {
    if (!isset($data['id'])){
      $data['id']="";
    }
      return Validator::make($data, [
          'modul' => 'required|string|min:3|max:255',
          'url' => 'required|string|min:3|max:255|unique:modul,url,'.$data['id'],
      ]);

  }

  public function Destroy(Request $request)
  {
    $id = Crypt::decryptString($request->input('i'));
    $modul = Modul::find($id);
    $nama = $modul->modul;
    $modul->delete();
    return redirect(route('list.modul'))->with('message', 'Modul '.$nama.' berhasil dihapus');
  }

  public function DataTableSSP()
  {
    $field =['id','modul','url'];
    $Model = Modul::all($field);
    $datatable = Datatables::of($Model)
    ->addColumn('action', function ($item) {
      return '<td class="uk-text-center">
          <a class="try-update" href="'.route('entry.modul', ['i' => Crypt::encryptString($item->id)]).'"><i class="md-icon material-icons">&#xE254;</i></a>
          <a class="try-delete" href="'.route('destroy.modul', ['i' => Crypt::encryptString($item->id)]).'"><i class="md-icon material-icons">&#xE92B;</i></a>
      </td>';
    })
    ->editColumn('url', function ($item) {
      return '<td class="uk-text-center"><a class="try-update" href="'.url($item->url).'">'.url($item->url).'</a></td>';
    })
    ->escapeColumns([])
    ->make();
    return $datatable;
  }
}
