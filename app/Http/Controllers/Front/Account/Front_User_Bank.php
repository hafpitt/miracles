<?php

namespace App\Http\Controllers\Front\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use App\Models\CmsPageImage as CmsPageImage;
use App\Models\Bank;
use App\Models\BankUser;
use Validator;
use Veritrans_Config;
use Veritrans_Snap;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Auth;

class Front_User_Bank extends Controller
{
  public function Index(Request $request)
  {
    if ($request->has('i'))
    {
      $id = Crypt::decryptString($request->input('i'));
      $bank = Bank::get();
      $bankuser = BankUser::find($id);
      return view('front.menus.account.add-rekening.content',compact('bank','bankuser'));
    }
    $bank = Bank::get();
    return view('front.menus.account.add-rekening.content',compact('bank'));
  }

  public function Post(Request $request)
  {
    $validator = $this->validator($request->all());
    if ($validator->fails())
    {
      return redirect()->back()->withErrors($validator)->withInput();
    } else {
      if (!$request->has('id'))
      {
        return $this->StoreNew($request);
      } else {
        return $this->StoreEdit($request);
      }
    }
  }

  public function StoreNew(Request $request)
  {
    $id_bank = $request->input('bank');
    $norek = $request->input('norek');
    $pemilik = $request->input('pemilik');
    $multiple_bank = new BankUser();
    $multiple_bank->id_bank = $id_bank;
    $multiple_bank->id_users = Auth::id();
    $multiple_bank->no_rekening = $norek;
    $multiple_bank->nama_pemilik = $pemilik;
    $multiple_bank->save();
    // return redirect()->back()->with('success', 'Akun Bank berhasil ditambahkan');
    return redirect(route('profile.index_profile'))->with('success', 'Bank berhasil ditambahkan');
  }

  public function StoreEdit(Request $request)
  {
    $id = Crypt::decryptString($request->input('id'));
    $id_bank = $request->input('bank');
    $norek = $request->input('norek');
    $pemilik = $request->input('pemilik');
    $multiple_bank = BankUser::findOrFail($id);
    $multiple_bank->id_bank = $id_bank;
    $multiple_bank->id_users = Auth::id();
    $multiple_bank->no_rekening = $norek;
    $multiple_bank->nama_pemilik = $pemilik;
    $multiple_bank->save();
    // return redirect()->back()->with('success', 'Akun Bank berhasil ditambahkan');
    return redirect(route('profile.index_profile'))->with('success', 'Bank berhasil direvisi');
  }

  public function Destroy(Request $request)
  {
    $id = Crypt::decryptString($request->input('i'));
    $delete = BankUser::where('id_users', '=', Auth::id())->findOrFail($id);
    $delete->delete();
    return redirect()->back()->with('success', 'Bank berhasil dihapus');
  }

  protected function validator(array $data)
  {
      return Validator::make($data, [
          'pemilik' => 'required|string',
          'norek' => 'required|string',
          'bank' => 'required|exists:bank,id',
      ]);
  }
}
