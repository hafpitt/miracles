<?php

namespace App\Http\Controllers\Front\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use App\Models\User;
use App\Models\Department;
use App\Models\CmsPageImage;
use App\Mail\UserVerification;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Crypt;

class Front_Register extends Controller
{
  public function Index()
  {
    // $dep = Department::where('isdefault','=','1')->firstOrFail();
    // $depa = Department::where('isdefault','=','1')->findOrFail(2);
    // $depw = Department::where('isdefault','=','1')->get();
    // return $depa;
    $cms = CmsPageImage::where('nama','=','register_user')->first();
    return view('front.menus.account.register.content',compact('cms'));
  }
  public function Register(Request $request)
  {
    $validator = $this->validator($request->all());
    if ($validator->fails())
    {
      return redirect($request->url())->withErrors($validator)->withInput();
    }
    else {
      $email = $request->input('email');
      $nama = $request->input('nama');
      $gender = $request->input('gender');
      $password = $request->input('password');
      $notelp = $request->input('notelp');
      $Model = new User();
      $Model->nama = $nama;
      $Model->email = $email;
      $Model->gender = $gender;
      $Model->password=bcrypt($password);
      $dep = Department::where('isdefault','=','1')->firstOrFail();
      $Model->department_id = $dep->id;
      $Model->notelp = $notelp;
      // return $Model;
      // exit();
      $Model->save();
      if (config('app.debug') <> 1)
      {
        $this->sendVerificationMail($request,$Model);
      }
      return redirect('login')->with('unverified', 'We Sent verification to your email address, please verify to login');
    }
  }

  public function IndexV(Request $request)
  {
    return view('front.temp-email.account-verification');
  }
  public function sendVerificationMail(Request $request,$user)
  {
    Mail::to($request->input('email'))
    ->send(new UserVerification($user));
  }

  protected function validator(array $data)
  {
      return Validator::make($data, [
          'nama' => 'required|string|max:255',
          'gender' => 'required',
          'email' => 'required|string|email|max:255|unique:users',
          'notelp' => 'required',
          'password' => 'required|string|min:6|confirmed',
      ]);
  }
  public function Verification($uid)
  {
    try {
      $id = Crypt::decryptString($uid);
      $user = User::where('id',$id)->whereNull('verified')->firstOrFail();
      $user->verified=1;
      $user->save();
      return redirect('login')->with('verified', 'User berhasil diverifikasi');
    }
    catch (\Illuminate\Contracts\Encryption\DecryptException $e) {
      abort(404);
    }
    catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
      abort(404);
    }
  }
}
