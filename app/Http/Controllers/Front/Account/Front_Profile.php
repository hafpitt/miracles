<?php

namespace App\Http\Controllers\Front\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use App\Models\CmsPageImage as CmsPageImage;
use App\Models\HTran;
use App\Models\DTran;
use App\Models\User;
// use App\Models\Bank;
use Validator;
use Veritrans_Config;
use Veritrans_Snap;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Auth;

class Front_Profile extends Controller
{
  public function Index(Request $request)
  {
    // return $request->all();
    $users = User::with(['users_alamats.kotum.provinsi','banks'=>function($query){
      $query->whereNull('bank_users.deleted_at');
    }])->findOrFail(Auth::id());
    // $banks = User::with(['banks'])->findOrFail(Auth::id());
    // return $users;
    return view('front.menus.account.profile.content',compact('users'));
  }

  public function IndexChange(Request $request)
  {
    $users = User::where('id',Auth::id())->first();
    return view('front.menus.account.profile.change_phone',compact('users'));
  }

  public function PostChange(Request $request,$id)
  {
    // return $request->all();
    $telepon = $request->input('telepon');
    // return $telepon;
    $user = User::findOrFail(Crypt::decryptString($id));
    // $id = Crypt::decryptString($request->input('id'));
    // return $user;
    $user->notelp=$telepon;
    $user->save();
    return redirect()->back();
  }
}
