<?php

namespace App\Http\Controllers\Front\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use App\Models\CmsPageImage as CmsPageImage;
use App\Models\Kotum as Kota;
use App\Models\UsersAlamat;
use Validator;
use Veritrans_Config;
use Veritrans_Snap;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Auth;

class Front_User_Address extends Controller
{
  public function IndexEntry(Request $request)
  {
    if ($request->has('i'))
    {
      $id = Crypt::decryptString($request->input('i'));
      $kota = Kota::get();
      $useralamat = UsersAlamat::with('kotum')->find($id);
      // return $kota;
      return view('front.menus.account.add-address.content',compact('kota','useralamat'));
    } else {
      $kota = Kota::get();
      return view('front.menus.account.add-address.content',compact('kota'));
    }
  }

  public function Post(Request $request)
  {
    // return $request->all();
    $validator = $this->validator($request->all());
    if ($validator->fails())
    {
      return redirect()->back()->withErrors($validator)->withInput();
    } else {
      if (!$request->has('id'))
      {
        return $this->StoreNew($request);
      } else {
        // return "";
        return $this->StoreEdit($request);
      }
    }
  }

  public function StoreNew(Request $request)
  {
    $nama_penerima = $request->input('penerima');
    $telepon = $request->input('telepon');
    $nama_alamat = $request->input('nama');
    $kota_id = $request->input('kota');
    $kodepos = $request->input('kodepos');
    $alamat = $request->input('alamat');
    $multiple_alamat = new UsersAlamat();
    $multiple_alamat->nama = $nama_alamat;
    $multiple_alamat->alamat = $alamat;
    $multiple_alamat->kodepos = $kodepos;
    $multiple_alamat->nama_penerima = $nama_penerima;
    $multiple_alamat->telepon = $telepon;
    $multiple_alamat->id_users = Auth::id();
    $multiple_alamat->utama = 1;
    $multiple_alamat->kota_id = $kota_id;
    $multiple_alamat->save();
    return redirect(route('profile.index_profile'))->with('success', 'Alamat berhasil ditambahkan');
  }

  public function StoreEdit(Request $request)
  {
    // $id = $request->input('id');
    $id = Crypt::decryptString($request->input('id'));
    // return $id;
    $nama_penerima = $request->input('penerima');
    $telepon = $request->input('telepon');
    $nama_alamat = $request->input('nama');
    $kota_id = $request->input('kota');
    $kodepos = $request->input('kodepos');
    $alamat = $request->input('alamat');
    $multiple_alamat = UsersAlamat::findOrFail($id);
    $multiple_alamat->nama = $nama_alamat;
    $multiple_alamat->alamat = $alamat;
    $multiple_alamat->kodepos = $kodepos;
    $multiple_alamat->nama_penerima = $nama_penerima;
    $multiple_alamat->telepon = $telepon;
    $multiple_alamat->id_users = Auth::id();
    // $multiple_alamat->utama = 0;
    $multiple_alamat->kota_id = $kota_id;
    $multiple_alamat->save();
    return redirect(route('profile.index_profile'))->with('success', 'Alamat berhasil direvisi');
  }

  public function Destroy(Request $request)
  {
    $id = Crypt::decryptString($request->input('i'));
    $delete = UsersAlamat::where('id_users', '=', Auth::id())->findOrFail($id);
    $delete->delete();
    return redirect()->back()->with('success', 'Alamat berhasil dihapus');
  }

  protected function validator(array $data)
  {
      return Validator::make($data, [
          'penerima' => 'required|string',
          'telepon' => 'required|string',
          'nama' => 'required|string',
          'kota' => 'required|exists:kota,id',
          'kodepos' => 'required|integer',
          'alamat' => 'required|string',
      ]);
  }
}
