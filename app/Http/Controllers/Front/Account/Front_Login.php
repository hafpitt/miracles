<?php

namespace App\Http\Controllers\Front\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use App\Models\CmsPageImage;
use App\Models\User;
use App\Mail\ResetPassword;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Crypt;

class Front_Login extends Controller
{
  public function Index()
  {
    $cms = CmsPageImage::where('nama','=','login_user')->first();
    return view('front.menus.account.login.content',compact('cms'));
  }

  public function IndexFo(Request $request){
    return view('front.menus.account.forgot-password.content');
  }

  public function IndexF(Request $request)
  {
    $validator = $this->validator($request->all());
    if ($validator->fails())
    {
      return redirect($request->url())->withErrors($validator)->withInput();
    }
    else {
      $email = $request->input('email');
      $Model = User::where('email',$request->input('email'))->first();
      // if (config('app.debug') <> 1)
      // {
        $this->sendForgotPassMail($request,$Model);
      // }
      return redirect('login')->with('unverified', 'We have sent notification to your email address, please check your email and click the link given to reset your password');
    }
  }

  public function sendForgotPassMail(Request $request,$user)
  {
    Mail::to($request->input('email'))
    ->send(new ResetPassword($user));
  }

  public function IndexR()
  {
    return view('front.menus.account.forgot-password.reset');
  }

  public function Authenticate(Request $request)
  {
    $email = $request->input('email');
    $password = $request->input('password');
    if (Auth::attempt(['email' => $email, 'password' => $password, 'verified' => 1],$request->has('rememberme'))) {
      return redirect(route('home.front'));
    }
    else {
      return redirect(route('login.front'))->withInput()->withErrors(['status' => 'username atau password salah mohon dicek kembali!']);
    }
  }

  public function ResetPass(Request $request, $uid)
  {
    try {
      $id = Crypt::decryptString($uid);
      $password = $request->input('newpassword');
      $user = User::where('id',$id)->firstOrFail();
      $user->password=bcrypt($password);
      $user->save();
      return redirect('login')->with('verified', 'Password berhasil diganti');
    }
    catch (\Illuminate\Contracts\Encryption\DecryptException $e) {
      abort(404);
    }
    catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
      abort(404);
    }
  }

  protected function validator(array $data)
  {
      return Validator::make($data, [
          'email' => 'required|string|email|max:255',
          // 'password' => 'required|string|min:6|confirmed',
      ]);
  }

  public function Logout()
  {
    Auth::logout();
    return redirect(route('login.front'))->with('success', 'Anda telah log out dari Miracle');
  }
}
