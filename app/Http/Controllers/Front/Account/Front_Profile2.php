<?php

namespace App\Http\Controllers\Front\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;
use App\Models\CmsPageImage as CmsPageImage;

class Front_Profile extends Controller
{
  public function Index(Request $request)
  {
    $profile = User::findOrFail(Auth::id());
    $cmspageimage = CmsPageImage::where('nama','=','profile_user')->first();
    return view('front.menus.account.profile.content',compact('profile','cmspageimage'));
  }
  public function Post(Request $request)
  {
    $validator = $this->validator($request->all());
    if ($validator->fails())
    {
      return redirect($request->url())->withErrors($validator)->withInput();
    }
    else {
      $id = Auth::id();
      $nama = $request->input('nama');
      $no_telp = $request->input('no_telp');
      $alamat = $request->input('alamat');
      $user = User::findOrFail($id);
      $user->nama = $nama;
      $user->notelp = $no_telp;
      $user->alamat = $alamat;
      if ($request->hasFile('url_photo')) {
        Storage::disk('public')->delete($user->url_photo);
        $user->url_photo = Storage::disk('public')->put('avatars', $request->file('url_photo'));
      }
      $user->save();
      return redirect($request->url())->with('info', 'Data berhasil Update');
    }
  }

  protected function validator(array $data)
  {
      return Validator::make($data, [
          'nama' => 'required|string|max:255',
          'no_telp' => 'required',
          'url_photo' => 'image',
      ]);
  }
  public function IndexChangePassword(Request $request){
    $cmspageimage = CmsPageImage::where('nama','=','profile_user')->first();
    return view('front.menus.account.change-password.content',compact('cmspageimage'));
  }

  public function PostChangePassword(Request $request)
  {
    $validator = $this->validatorpassword($request->all());
    if ($validator->fails())
    {
      return redirect($request->url())->withErrors($validator)->withInput();
    }
    else {
      $old_password = $request->input('old_password');
      $password = $request->input('password');
      $user = Auth::user();
      $user->password=bcrypt($password);
      $user->save();
      return redirect($request->url())->with('info', 'Data berhasil Update');
    }
  }

  protected function validatorpassword(array $data)
  {
      $messages = [
        'old_password.password_hash'    => 'The :attribute and last password must match.',
      ];
      return Validator::make($data, [
          'old_password' => 'required|string|password_hash:' . Auth::user()->password,
          'password' => 'required|string|min:6|confirmed',
      ],$messages);
  }
}
