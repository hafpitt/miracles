<?php

namespace App\Http\Controllers\Front\Transaction;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use App\Models\HTransaksi;
use Auth;
use Validator;
use App\Mail\PaymentConfirmation;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class Front_History extends Controller
{
  public function Index(Request $request)
  {
    $history = HTransaksi::with(['bank_user.bank'])
    ->where('id_cust',Auth::id())
    ->where('no_invoice', 'like', 'inv%')->get();
    // return $history;
    return view('front.menus.transaction.history.content',compact('history'));
  }

  public function IndexConfirmPayment(Request $request,$id)
  {
    $id=Crypt::decrypt($id);
    $transaction = HTransaksi::with(['bank_user.bank',
      'd_transaksis.cart.barang.barang_fotos' => function ($query){
        $query->where('utama', '=', 1);
      },'users_alamat.kotum'])->findOrFail($id);
    // return $transaction;
    return view('front.menus.transaction.confirm-payment.content',compact('transaction'));
  }

  public function PostConfirmPayment(Request $request,$id)
  {
    $id=Crypt::decrypt($id);
    $keteranganUser = $request->input('keterangan');
    $transaction = HTransaksi::findOrFail($id);

    $transaction->konfirmasi = 1;
    $transaction->keterangan_user = $keteranganUser;
    if ($request->hasFile('url_bukti')) {
      Storage::disk('public')->delete($transaction->url_bukti);
      $transaction->url_bukti = Storage::disk('public')->put('payment-confirm', $request->file('url_bukti'));
    }
    $transaction->save();
    if (config('app.debug') <> 1)
    {
      $this->sendPaymentConfirmation($request,$id);
    }
    return redirect('history')->with('success', 'Konfirmasi invoice : '.$transaction->no_invoice.' telah berhasil dilakukan');
  }

  public function sendPaymentConfirmation(Request $request,$id)
  {
    $htransaksi = HTransaksi::with('user')->findOrFail($id);
    // return $htransaksi;
    // return view('front.temp-email.payment-confirmation',compact('htransaksi'));

    Mail::to('admin@miracle.com')
    ->send(new PaymentConfirmation($htransaksi));
  }
  
  public function PostConfirmReceive(Request $request,$id)
  {
      // return $id;
      $id=Crypt::decrypt($id);
      $transaction = HTransaksi::findOrFail($id);
      $transaction->diterima = 1;
      $transaction->save();
      return redirect('history')->with('success', 'Konfirmasi barang diterima telah berhasil dilakukan');
  }
}
