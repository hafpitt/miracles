<?php

namespace App\Http\Controllers\Front\Transaction;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use App\Models\Barang;
use App\Models\Cart;
use Auth;
use Carbon\Carbon;

class Front_Item_Detail extends Controller
{
  public function Index(Request $request,$category,$barang)
  {
    // return Auth::id();
    // return Auth::user()->department->id;
    // return Cart::where('id_cust',Auth::id())->whereNull('check_out')
    // ->with(['barang.barang_fotos' => function ($query) {
    //   $query->where('utama', '=', 1);
    // },'barang_warna','barang.departments' => function ($query) {
    //   $query->where('department_id', '=', Auth::user()->department->id);
    // }])
    // ->get();
    $category = str_replace('-',' ',$category);
    $barang = str_replace('-',' ',$barang);
    try {

      if (Auth::check()){
        $curUser = Auth::user();
        $barang_detail = Barang::with(
          array('barang_fotos' => function($query) {
              $query->orderBy('utama', 'desc');
            },
          )
        )->with(
            array('barang_warnas' => function($query){
              $query->where('barang_warna.tersedia','=','1');
            },
          )
        )->with(['departments' => function($query) use($curUser){
              $query->where('department.id','=', $curUser->department_id);
            },'users']
        )->where('nama','=',$barang)->firstOrFail();
      }
      else
      {
        $barang_detail = Barang::with(
          array('barang_fotos' => function($query) {
              $query->orderBy('utama', 'desc');
            },
          )
        )->with(
            array('barang_warnas' => function($query){
              $query->where('barang_warna.tersedia','=','1'); //bug
            },
          )
        )
        ->with(['departments' => function($query) {
              $query->where('department.isdefault','=', '1');
            },'users'])->where('nama','=',$barang)->firstOrFail();
      };


    } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
      abort(404);
    }
    // return $barang_detail;
    return view('front.menus.transaction.item-detail.content',compact('barang_detail'));
  }

  public function IndexQuick(Request $request,$category,$barang)
  {
    // return $barang_detail;
    $category = str_replace('-',' ',$category);
    $barang = str_replace('-',' ',$barang);
    try {
      if (Auth::check()){
        $curUser = Auth::user();
        $barang_detail = Barang::with(
          array('barang_fotos' => function($query) {
              $query->orderBy('utama', 'desc');
            },
          )
        )->with(
            array('barang_warnas' => function($query){
              $query->where('barang_warna.tersedia','=','1'); //bug
            },
          )
        )->with(['departments' => function($query) use($curUser){
              $query->where('department.id','=', $curUser->department_id);
            },'users'])->where('nama','=',$barang)->firstOrFail();
      }
      else
      {
        $barang_detail = Barang::with(
          array('barang_fotos' => function($query) {
              $query->orderBy('utama', 'desc');
            },
          )
        )->with(
            array('barang_warnas' => function($query){
              $query->where('barang_warna.tersedia','=','1'); //bug
            },
          )
        )
        ->with(['departments' => function($query) {
              $query->where('department.isdefault','=', '1');
            },'users'])->where('nama','=',$barang)->firstOrFail();
      };
    } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
      abort(404);
    }
    return view('front.menus.transaction.item-quick.content',compact('barang_detail'));
  }

  public function PostReview(Request $request,$category,$barang)
  {
    $barang = str_replace('-',' ',$barang);
    $barang_detail = Barang::where('nama','=',$barang)->firstOrFail();
    $barang_detail->users()->detach(Auth::id());
    $barang_detail->users()->attach(Auth::id(), ['tgl'=>date('Y-m-d H:i:s'),'comment' => $request->input('comment'),'rating'=>$request->input('rating')]);
    return redirect()->back()->with('review', 'Comment berhasil input');
  }

  public function AddToCart(Request $request,$catagory,$barang){
    // return $barang;
    // return $request->all();
    $validator = $this->validator($request->all());
    if ($validator->fails())
    {
      return redirect()->back()->withErrors($validator)->withInput();
    }
    else {
      // return $request->all();
      $barang = str_replace('-',' ',$barang);
      $barang_detail = Barang::where('nama','=',$barang)->firstOrFail();
      $cart = new Cart();
      $cart->qty = $request->input('qty');
      $cart->id_barang = $barang_detail->id;
      $cart->id_cust = Auth::id();
      $cart->id_barang_warna = $request->input('id_barang_warna');
      $cart->save();
      return redirect($request->url())->with('success', 'Item berhasil ditambah kedalam keranjang');
    }
  }

  public function AddToCartq(Request $request,$catagory,$barang){
    $validator = $this->validator($request->all());
    if ($validator->fails())
    {
      return redirect()->back()->withErrors($validator)->withInput();
    }
    else {
      $barang = str_replace('-',' ',$barang);
      $barang_detail = Barang::where('nama','=',$barang)->firstOrFail();
      $cart = new Cart();
      $cart->qty = $request->input('qty');
      $cart->id_barang = $barang_detail->id;
      $cart->id_cust = Auth::id();
      $cart->id_barang_warna = $request->input('id_barang_warna');
      $cart->save();
      return redirect()->back()->with('success', 'Item berhasil ditambah kedalam keranjang');
    }
  }

  protected function validator(array $data)
  {
      return Validator::make($data, [
          'id_barang_warna' => 'required',
          'qty' => 'required|integer',
      ]);
  }
}
