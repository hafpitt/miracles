<?php

namespace App\Http\Controllers\Front\Transaction;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\Barang;
use App\Models\Cart;
use App\Models\DTransaksi;
use App\Models\HTransaksi;
use App\Models\HPiutang;
use App\Models\DPiutang;
use Yajra\Datatables\Facades\Datatables;
use Response;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class Front_Retur extends Controller
{
  public function IndexComplain(Request $request,$id)
  {
    $id=Crypt::decrypt($id);
    $transaction = HTransaksi::with(['bank_user.bank',
      'd_transaksis.cart.barang.barang_fotos' => function ($query){
        $query->where('utama', '=', 1);
      },'users_alamat.kotum'])->findOrFail($id);
    return view('front.menus.transaction.retur.pengajuan-komplain',compact('transaction'));
  }

  public function PostComplain(Request $request,$id)
  {
    $id=Crypt::decrypt($id);
    $keteranganKomplain = $request->input('keterangan_komplain');
    $transaction = HTransaksi::findOrFail($id);

    if ($request->hasFile('url_bukti')){
      $transaction->keterangan_komplain = $keteranganKomplain;
      if ($request->hasFile('url_bukti')) {
        Storage::disk('public')->delete($transaction->url_bukti);
        $transaction->url_complain = Storage::disk('public')->put('pengajuan-komplain', $request->file('url_bukti'));
      }
      $transaction->tgl_komplain = Carbon::now()->toDateString();
      $transaction->save();
      return redirect('history')->with('success', 'Pengajuan Komplain untuk no invoice : '.$transaction->no_invoice.' telah berhasil dilakukan');
    }
    else
    {
      return redirect()->back()->with('warning', 'Mohon upload bukti foto barang');
    }
  }


  public function IndexEntry(Request $request,$id)
  {
    $id=Crypt::decrypt($id);
    $transaction = HTransaksi::with('d_transaksis.cart.barang.barang_warnas')->findOrFail($id);
    return view('front.menus.transaction.retur.content',compact('transaction'));
  }

  public function IndexList()
  {
    $returlist = HTransaksi::where('id_cust',Auth::id())
    ->where('url_complain','<>',"")->get();
    // ->where('no_invoice', 'like', '%ret%')->get();
    // return $returlist;
    $returlist2 = HTransaksi::where('id_cust',Auth::id())
    ->where('no_invoice', 'like', '%ret%')->get();
    // return $returlist2;
    return view('front.menus.transaction.retur.list',compact('returlist','returlist2'));
  }

  public function GenerateNota($id,$tipe)
  {
    $date=date_format(date_create(Carbon::now()),"Y/m/d");
    return $tipe.'/'.$date.'/'.sprintf('%04d', $id);
  }

  public function IndexConfirmRetur(Request $request,$id)
  {
    $validator = $this->validator($request->all());
    if ($validator->fails())
    {
      return redirect()->back()->with('warning', 'Silahkan lengkapi data dibawah terlebih dahulu');
    }
    else {
      $id=Crypt::decrypt($id);
      $reftransaksi = HTransaksi::findOrFail($id);
      HTransaksi::where('id',$id)->update(['status_komplain'=>'3']);

      $last_trans = HTransaksi::where('tgl','=', Carbon::now()->toDateString())
      ->where('no_invoice', 'like', 'RET%')->count()+1;
      $no_invoice = $this->GenerateNota($last_trans,'RET');
      $htransret = new HTransaksi();
      $htransret->no_invoice = $no_invoice;
      $htransret->tgl = Carbon::now()->toDateString();
      $htransret->biaya_ongkir = 0;
      $htransret->unique_harga = 0;
      $htransret->grand_total = 0;
      $htransret->terbayar = 0;
      $htransret->sisa_bayar = 0;
      $htransret->bank_users_id = $reftransaksi->bank_users_id;
      $htransret->users_alamat_id = $reftransaksi->users_alamat_id;
      $htransret->id_cust = $reftransaksi->id_cust;
      $htransret->kurir = $request->input('kurir');
      $htransret->no_resi = $request->input('resi');
      $htransret->reff_no = $id;
      $htransret->save();
      $grandtotal=0;
      $cart = $request->input('cart');
      foreach ($cart as $key => $value) {
        if ($value<>0)
        {
          $reftrans = DTransaksi::where('id_cart',$key)->first();
          // perbaruan detail ada qty dan barang dan warna
          // $cartretur = new Cart();
          // $cartretur->id_cust = $reftrans->cart->id_cust;
          // $cartretur->id_barang = $reftrans->cart->id_barang;
          // $cartretur->id_barang_warna = $reftrans->cart->id_barang_warna;
          // $cartretur->check_out = 2;
          // $cartretur->qty = $value*-1;
          // $cartretur->save();

          $dtransretur = new DTransaksi();
      		// $dtransretur->id_cart = $cartretur->id;
          $dtransretur->qty=$value*-1;
          $dtransretur->barang_id =$reftrans->cart->id_barang;
          $dtransretur->barang_warna_id =$reftrans->cart->id_barang_warna;
      		$dtransretur->harga = $reftrans->harga;
      		$dtransretur->subtotal = $reftrans->harga*($value*-1);
          $dtransretur->reff_no = $reftrans->id;
          $grandtotal=$grandtotal+$reftrans->harga*($value*-1);
      		$htransret->d_transaksis()->save($dtransretur);
        }
      }
      $htransret->grand_total=$grandtotal;
      $htransret->save();
      return redirect(route('front.list.retur'))->with('success', 'Pengajuan retur Anda telah berhasil dibuat');
    }
  }

  public function StoreEdit(Request $request)
  {
    // // return $request->all();
    // $id = $request->input('id');
    // $invoice = $request->input('inv');
    // $grand_total = 0;
    // $dpiutang = [];
    // $oldpiutang = DPiutang::where('id_h_piutang',$id);
    // foreach ($oldpiutang->get() as $key => $value) {
    //   $trans = HTransaksi::findOrFail($value->id_h_transaksi);
    //   $trans->terbayar = $trans->terbayar-$value->dibayar;
    //   $trans->sisa_bayar = $trans->sisa_bayar+$value->dibayar;
    //   $trans->save();
    // }
    // $oldpiutang->delete();
    // foreach ($invoice as $key => $value) {
    //   $grand_total = $grand_total+$value['dibayar'];
    //   $dpiutang[] = new DPiutang(['dibayar'=>$value['dibayar'], 'id_h_transaksi'=>$key]);
    //   $trans = HTransaksi::findOrFail($key);
    //   $trans->terbayar = $trans->terbayar+$value['dibayar'];
    //   $trans->sisa_bayar = $trans->sisa_bayar-$value['dibayar'];
    //   $trans->save();
    // }
    // $last_trans = HPiutang::where('tgl','=', Carbon::now()->toDateString())->count()+1;
    // $no_piutang = $this->GenerateNota($last_trans,'P');
    // $piutang = HPiutang::findOrFail($id);
    // $piutang->no_piutang =$no_piutang;
    // // $piutang->status = $request->input('keterangan');
    // $piutang->tgl = Carbon::now()->toDateString();
    // $piutang->id_operator = Auth::guard('web_admin')->id();
    // $piutang->grand_total = $grand_total;
    // $piutang->save();
    // $piutang->d_piutangs()->saveMany($dpiutang);
    // return redirect(route('list.retur'))->with('message', 'Piutang '.$no_piutang. ' berhasil diedit');
  }

  protected function validator(array $data)
  {
      return Validator::make($data, [
          'resi' => 'required',
          'kurir' => 'required',
          // 'cart' => 'array|size:1',
      ]);
  }

  public function Destroy(Request $request)
  {
    $id = Crypt::decryptString($request->input('i'));
    $piutang = HPiutang::findOrFail($id);
    $nama = $piutang->no_piutang;
    $piutang->delete();
    return redirect(route('list.retur'))->with('message', 'No Piutang '.$nama.' berhasil dihapus');
  }
}
