<?php

namespace App\Http\Controllers\Front\Transaction;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use App\Models\BarangKategori;
use App\Models\Barang;
use Auth;
use Config;

class Front_Item_Category extends Controller
{
  public function Index(Request $request,$category)
  {
    $category = str_replace('-',' ',$category);
    $barangkategori = BarangKategori::where('nama','=',$category)->firstOrFail();
    try {
      if (Auth::check()){
        $curUser = Auth::user();
        // $barang = Barang::where('id_barang_kategori',$barangkategori->id);
        $barang_detail = Barang::with([
          'barang_fotos' => function($query) {
            $query->orderBy('utama', 'desc');
          },
          'barang_warnas' => function($query){
            $query->where('barang_warna.tersedia','=','1'); //bug
          },
          'departments' => function($query) use($curUser){
                $query->where('department.id','=', $curUser->department_id);
          },
          'users'
        ])->where('id_barang_kategori',$barangkategori->id);
        // $barangkategori = BarangKategori::with([
        //   'barangs.barang_fotos' => function($query) {
        //       $query->where('barang_foto.utama','=', '1');
        //     },
        //   'barangs.departments' =>function($query) use($curUser) {
        //       $query->where('department.id','=', $curUser->department_id);
        //   }]
        // )->where('nama','=',$category)->firstOrFail();
      }
      else
      {
        $barang_detail = Barang::with([
          'barang_fotos' => function($query) {
            $query->orderBy('utama', 'desc');
          },
          'barang_warnas' => function($query){
            $query->where('barang_warna.tersedia','=','1'); //bug
          },
          'departments' => function($query){
                $query->where('department.isdefault','=', 1);
          },
          'users'
        ])->where('id_barang_kategori',$barangkategori->id);
        // $barangkategori = BarangKategori::with([
        //   'barangs.barang_fotos' => function($query) {
        //       $query->where('barang_foto.utama','=', '1');
        //     },
        //   'barangs.departments' =>function($query){
        //     $query->where('department.isdefault','=', '1');
        //   }]
        // )->where('nama','=',$category)->firstOrFail();
      };
    } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
      abort(404);
    }
    // return $barang_detail;
    // $page_barang = $barangkategori->barangs()->paginate(5);
    $page_barang = $barang_detail->paginate(5);
    // return;
    // return $page_barang->nextPageUrl();
    // return $barangkategori;
    return view('front.menus.transaction.item-category.content',compact('barangkategori','page_barang'));
  }
}
