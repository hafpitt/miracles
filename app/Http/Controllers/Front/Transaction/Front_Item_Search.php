<?php

namespace App\Http\Controllers\Front\Transaction;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use App\Models\BarangKategori;
use App\Models\Barang;
use Auth;
use Config;

class Front_Item_Search extends Controller
{
  public function Index(Request $request)
  {
    $filter = $request->input('filter');
    try {
      if (Auth::check()){
        $curUser = Auth::user();
        $barang = Barang::with([
          'barang_kategori','barang_fotos' => function($query) {
            $query->orderBy('utama', 'desc');
          },
          'departments' => function($query) use($curUser){
            $query->where('department.id','=', $curUser->department_id);
          },
          'users'
        ])
        ->where('nama', 'like', '%' . $filter . '%')
        ->orWhere('keterangan', 'like', '%' . $filter . '%')
        ->orWhere('keterangandetail', 'like', '%' . $filter . '%');
      }
      else
      {
        $barang = Barang::with([
          'barang_kategori','barang_fotos' => function($query) {
            $query->orderBy('utama', 'desc');
          },
          'departments' => function($query){
            $query->where('department.isdefault','=', 1);
          },
          'users'
        ])
        ->where('nama', 'like', '%' . $filter . '%')
        ->orWhere('keterangan', 'like', '%' . $filter . '%')
        ->orWhere('keterangandetail', 'like', '%' . $filter . '%');
      }
    } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
      abort(404);
    }

    $page_barang = $barang->paginate(5);
    return view('front.menus.transaction.item-search.content',compact('page_barang'));

    // $barang = Barang::with([
    //   'barang_kategori','barang_fotos' => function ($query){
    //   $query->orderBy('utama','desc');
    // }])
    // ->where('nama', 'like', '%' . $filter . '%')
    // ->orWhere('keterangan', 'like', '%' . $filter . '%')
    // ->orWhere('keterangandetail', 'like', '%' . $filter . '%');
    // $page_barang = $barang->paginate(5);
    // if ($page_barang->count()<1)
    // {
    //   abort(404);
    // }
    //
    //
    // try {
    //   if (Auth::check()){
    //     $curUser = Auth::user();
    //     $barang_detail = Barang::with([
    //       'barang_fotos' => function($query) {
    //         $query->orderBy('utama', 'desc');
    //       },
    //       'departments' => function($query) use($curUser){
    //             $query->where('department.id','=', $curUser->department_id);
    //       },
    //       'users'
    //     ])->where('id_barang_kategori',$barangkategori->id);
    //   }
    //   else
    //   {
    //     $barang_detail = Barang::with([
    //       'barang_fotos' => function($query) {
    //         $query->orderBy('utama', 'desc');
    //       },
    //       'departments' => function($query) use($curUser){
    //         $query->where('department.isdefault','=', 1);
    //       },
    //       'users'
    //     ])->where('id_barang_kategori',$barangkategori->id);
    //   };
    // } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
    //   abort(404);
    // }
  }
}
