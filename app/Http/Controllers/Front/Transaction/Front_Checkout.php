<?php

namespace App\Http\Controllers\Front\Transaction;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Api\RajaOngkir;
use App\Models\User;
use App\Models\Cart;
use App\Models\UsersAlamat;
use App\Models\BankUser;
use App\Models\HTransaksi;
use App\Models\DTransaksi;
use App\Models\Barang;
use App\Mail\PaymentInformation;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;

class Front_Checkout extends Controller
{
  public function Index(Request $request)
  {
    $user = User::find(Auth::id());
    if ($user->current_unique < 1)
    {
      $user->current_unique = rand(100,999);
      $user->save();
    }
    // return $user;
    // if currentunik==0 then generate
    $bankuser = BankUser::with('bank')->where('id_users','=','4')->get();
    // return $bankuser;
    // return Auth::user()->users_alamats;
    // return $alamat = Auth::user()->users_alamats;

    // $useralamat = UsersAlamat::where('id_users',Auth::id())->where('utama',1)->first();
    // $cart = Cart::with(['barang'])->where('id_cust',Auth::id())->get();
    // $weight_total=0;
    // foreach ($cart as $key => $c) {
    //   $weight_total = $weight_total+($c->qty*$c->barang->berat);
    // }
    // return $weight_total;
    $cartcounter = Cart::where('id_cust',Auth::id())
    ->whereNull('check_out')
    ->count();
    if (!$cartcounter)
    {
      return redirect('cart');
    }
    $bankcounter = BankUser::where('id_users',Auth::id())->first();
    $utamaalamat = UsersAlamat::with(['kotum.provinsi'])->where('id_users',Auth::id())->where('utama',1)->first();
    $bank = BankUser::where('id_users',Auth::id())->get();
    // return $bank;
    if (!isset($utamaalamat) || !isset($bankcounter))
    {
      return redirect('profile')->withErrors(['status' => 'Mohon lengkapi alamat dan akun bank Anda']);
    }
    // return $utamaalamat;s
    // $kurir = $this->HitungOngkir($request);
    return view('front.menus.transaction.checkout.content',compact('utamaalamat','bank','bankuser','user'));
  }

  public function IndexChange(Request $request)
  {
    $alamat = Auth::user()->users_alamats;
    $utamaalamat = UsersAlamat::with(['kotum.provinsi'])->where('id_users',Auth::id())->where('utama',1)->first();
    return view('front.menus.transaction.checkout.change_address',compact('alamat','utamaalamat'));
  }
  public function PostChange(Request $request)
  {
    UsersAlamat::where('id_users',Auth::id())->update(['utama'=>'0']);;
    $useralamat = UsersAlamat::findOrFail($request->input('alamat'));
    $useralamat->utama = 1;
    $useralamat->save();
    return redirect()->back();
  }
  public function GetKurir(Request $request)
  {
    if($request->ajax())
    {
      $data = json_decode($request->input('form'),true);
      $request->merge($data);
    }
    $kurir = $this->HitungOngkir($request->input('kurir'));

    return json_decode(json_encode($kurir,true));
  }
  public function GenerateNota($id,$tipe)
  {
    $date=date_format(date_create(Carbon::now()),"Y/m/d");
    return $tipe.'/'.$date.'/'.sprintf('%04d', $id);
  }
  public function Checkout(Request $request)
  {
    // return $request->all();

    $kurir = $request->input('kurir');
    $kurir_paket = $request->input('paket');
    $biaya_ongkir = $request->input('paket_harga');
    $last_trans = HTransaksi::where('tgl','=', Carbon::now()->toDateString())
    ->where('no_invoice', 'like', 'INV%')->count()+1;
    $no_invoice = $this->GenerateNota($last_trans,'INV');
    $bank_user = $request->input('bank_user');
    $cart = Cart::with(['barang','barang.departments' => function ($query) {
      $query->where('department_id', '=', Auth::user()->department->id);
    }])->where('id_cust',Auth::id())->whereNull('check_out')->get();
    $grandtotal=0;
    $unique_harga = Auth::user()->current_unique;
    // return rand(100,999);
    foreach ($cart as $key => $item) {
      $grandtotal = $grandtotal+($item->qty*$item->barang->departments->first()->pivot->harga);
    }
    // return $grandtotal;\
    $alamat = UsersAlamat::where('id_users',Auth::id())->where('utama',1)->first();
    $head_trans = new HTransaksi();
    $head_trans->no_invoice = $no_invoice;
    $head_trans->tgl = Carbon::now()->toDateString();
    $head_trans->biaya_ongkir = $biaya_ongkir;
    $head_trans->grand_total = $grandtotal+$biaya_ongkir+$unique_harga;
    $head_trans->terbayar = 0;
    $head_trans->sisa_bayar = $grandtotal+$biaya_ongkir+$unique_harga;
    $head_trans->unique_harga = $unique_harga;
    $head_trans->bank_users_id = $bank_user;
    $head_trans->users_alamat_id = $alamat->id;
    $head_trans->id_cust = Auth::id();
    $head_trans->kurir = $kurir;
    $head_trans->kurir_paket = $kurir_paket;
    $head_trans->save();

    foreach ($cart as $key => $item) {
  		$deta_trans = new DTransaksi();
  		$deta_trans->id_cart=$item->id;
      $deta_trans->qty=$item->qty;
      $deta_trans->barang_id =$item->id_barang;
      $deta_trans->barang_warna_id =$item->id_barang_warna;
  		$deta_trans->harga=$item->barang->departments->first()->pivot->harga;
  		$deta_trans->subtotal=$item->qty*$item->barang->departments->first()->pivot->harga;
      $deta_trans->cart->barang->stok = $deta_trans->cart->barang->stok-$item->qty;
      $brg = Barang::find($item->id_barang);
      $brg->stok = $brg->stok-$item->qty;
      $brg->save();
  		$head_trans->d_transaksis()->save($deta_trans);
    }
    Cart::where('id_cust',Auth::id())->update(['check_out'=>'1']);
    User::find(Auth::id())->update(['current_unique'=>'0']);
    if (config('app.debug') <> 1)
    {
      $this->sendPaymentInformation($request);
    }
    return redirect('/');
  }

  public function sendPaymentInformation(Request $request)
  {
    $bankuser = BankUser::with('bank')->where('id_users','=','4')->get();
    $user = User::with(['h_transaksis'=>function($query){
      $query->whereNull('diterima')
      ->with('d_transaksis');
    }])->findOrFail(Auth::id());

    Mail::to($user->email)
    ->send(new PaymentInformation($user,$bankuser));
  }

  public function HitungOngkir($courier)
  {
    $useralamat = UsersAlamat::where('id_users',Auth::id())->where('utama',1)->first();
    $cart = Cart::with(['barang'])->where('id_cust',Auth::id())->get();
    $weight_total=0;
    foreach ($cart as $key => $c) {
      $weight_total = $weight_total+($c->qty*$c->barang->berat);
    }
    $destination = $useralamat->kota_id;
    $weight = $weight_total;
    $courier = $courier;
    $rajaongkir = new RajaOngkir();
    $ongkir = $rajaongkir->getdatabycurl($destination,$weight,$courier);
    return $ongkir;
  }
}
