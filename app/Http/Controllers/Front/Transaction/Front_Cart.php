<?php

namespace App\Http\Controllers\Front\Transaction;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
// use App\Models\CmsPageImage as CmsPageImage;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\Cart;
use App\Models\BarangKategori;
use App\Models\BarangFoto;
use App\Models\BarangWarna;
use App\Models\Barang;
use App\Models\Department;
use Yajra\Datatables\Facades\Datatables;
use Response;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;

class Front_Cart extends Controller
{
  public function Index()
  {
    return view('front.menus.transaction.cart.content');
  }

  public function IndexChange(Request $request,$cart)
  {
    $carts = Cart::findOrFail($cart);
    return view('front.menus.transaction.cart.change_qty',compact('carts'));
  }

  public function PostChange(Request $request,$cart)
  {
    $carts = Cart::findOrFail(Crypt::decryptString($cart));
    $carts->qty= $request->input('qty');
    $carts->save();
    return redirect()->back();
  }
  public function RemoveCart(Request $request,$cart)
  {
    $carts = Cart::findOrFail(Crypt::decryptString($cart));
    $carts->delete();
    return redirect()->back();
  }
  // public function Post(Request $request)
  // {
  //   if($request->ajax())
  //   {
  //     $data = json_decode($request->input('form'),true);
  //     $request->merge($data);
  //   }
  //   $validator = $this->validator($request->all());
  //   if ($validator->fails())
  //   {
  //     return redirect()->back()->withErrors($validator)->withInput();
  //   }
  //   else {
  //     if (!$request->has('id'))
  //     {
  //       return $this->StoreNew($request);
  //     }
  //     else {
  //       return $this->StoreEdit($request);
  //     }
  //   }
  // }

}
