<?php

namespace App\Http\Controllers\Front\Faq;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use Auth;

class Front_Faq extends Controller
{
  public function Index()
  {
    return view('front.menus.faq.content');
  }
}
