<?php

namespace App\Http\Controllers\Front\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use App\Models\Barang;
use App\Models\Cart;
use App\Models\CmsHomeCarousel as Slider;
use Auth;

class Front_Home extends Controller
{
  public function Index()
  {
    // $t = Cart::where('id_cust',Auth::id())->whereNull('check_out')
    // ->with(['barang.barang_fotos' => function ($query) {
    //   $query->where('utama', '=', 1);
    // },'barang_warna','barang.departments' => function ($query) {
    //   $query->where('department_id', '=', Auth::user()->department->id);
    // }])
    // ->get();
    // return $t[0]->barang->departments->first()->pivot->harga;
    // return $t;
    if (Auth::check()){
      $curUser = Auth::user();
      // return Auth::user();
      $barang = Barang::with(
        array('barang_fotos' => function($query) {
            $query->where('barang_foto.utama','=', '1');
          },
        )
      )->with(
          array('departments' => function($query) use($curUser){
            $query->where('department.id','=', $curUser->department_id);
          },
        )
      )->limit(
        '10'
      )->orderBy(
        'id', 'desc'
      )->get();
    } else {
      $barang = Barang::with(
        array('barang_fotos' => function($query) {
            $query->where('barang_foto.utama','=', '1');
          },
        )
      )->with(
          array('departments' => function($query) {
            $query->where('department.isdefault','=', '1');
          },
        )
      )->limit(
        '10'
      )->orderBy(
        'id', 'desc'
      )->get();
    }
    $slider = Slider::limit(
      '5'
    )->get();
    // return $barang[7]->departments[0]->pivot->harga;
    return view('front.menus.home.content',compact('barang','slider'));
  }
}
