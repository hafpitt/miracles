<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\AuthenticationException;
use App\Models\HTransaksi;
// use App\Models\Cart;
use Illuminate\Support\Facades\View;

class TransactionQueue
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    // public function handle($request, Closure $next, $guard = null)
    // {
    //   if (Auth::check()) {
    //     $front_trans_queue = HTransaksi::where('id_cust',Auth::id())->where('konfirmasi','=','0')
    //     ->get();
    //     View::share('front_trans_queue', $front_trans_queue);
    //   }
    //   return $next($request);
    // }

    public function handle($request, Closure $next, $guard = null)
    {
      if (Auth::check()) {
        $front_trans_queue = HTransaksi::where('id_cust',Auth::id())->whereNull('diterima')->where('no_invoice', 'like', 'inv%')->get();
        View::share('front_trans_queue', $front_trans_queue);
      }
      return $next($request);
    }
}
