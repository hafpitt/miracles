<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Support\Facades\View;
use Auth;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
      if (!Auth::guard($guard)->check())
      {
        throw new AuthenticationException($guard, Array());
      }
      else {
        if (config('app.debug') <> 1 && $guard=='web_admin' && basename($request->path()) <> 'destroy')
        {
          $user = Auth::guard($guard)->user()->with(
            array('department.moduls' => function ($query) use($request)  {
              $query->where('modul.url', str_replace($request->segment(1).'/',"",$request->path()));
            })
          )->firstOrFail();
          $exists = 0;
          $exists = $user->department->moduls->count();

          if ($exists<=0)
          {
            abort(404);
          }
        }
        if($guard=='web_admin')
        {
          $user = User::with('department.moduls')->findOrFail(Auth::guard($guard)->id());

          $access=[];
          foreach ($user->department->moduls as $key1 => $value1) {
            $access[]=$value1->url;
          }
          View::share('back_access_menu', $access);
        }
      }
      return $next($request);
    }
}
