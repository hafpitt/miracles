<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\AuthenticationException;
use App\Models\Cart;
use Illuminate\Support\Facades\View;

class UserCart
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
      if (Auth::check()) {
        $front_user_cart = Cart::where('id_cust',Auth::id())->whereNull('check_out')
        ->with(['barang.barang_fotos' => function ($query) {
          $query->where('utama', '=', 1);
        },'barang_warna','barang.departments' => function ($query) {
          $query->where('department_id', '=', Auth::user()->department->id);
        }])
        ->get();
        View::share('front_user_cart', $front_user_cart);
      }
      return $next($request);
    }
}
