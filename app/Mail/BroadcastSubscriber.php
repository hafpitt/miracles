<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Http\Request;


class BroadcastSubscriber extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
     public $sendrequest;

     /**
      * Create a new message instance.
      *
      * @return void
      */
     public function __construct(Request $sendrequest)
     {
         $this->sendrequest = $sendrequest;
     }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      return $this->from('noreply@chandiphotography.com')
      ->view('back.temp-email.broadcast-subscriber');
      // ->with([
      //   'userEmail' => $this->user->email,
      //   'userNama' => $this->user->nama,
      // ]);;
    }
}
