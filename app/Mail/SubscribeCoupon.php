<?php

namespace App\Mail;

use App\Models\Subscriber as Subscribe;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SubscribeCoupon extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
     public $subscribe;

     /**
      * Create a new message instance.
      *
      * @return void
      */
     public function __construct(Subscribe $subscribe)
     {
         $this->subscribe = $subscribe;
     }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      return $this->from('noreply@chandiphotography.com')
      ->view('front.temp-email.subscribe');
      // ->with([
      //   'userEmail' => $this->user->email,
      //   'userNama' => $this->user->nama,
      // ]);;
    }
}
