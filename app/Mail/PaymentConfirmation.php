<?php

namespace App\Mail;

use App\Models\User;
use App\Models\HTransaksi;
use App\Models\BankUser;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Crypt;


class PaymentConfirmation extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
     public $htransaksi;

     /**
      * Create a new message instance.
      *
      * @return void
      */
     public function __construct($htransaksi)
     {
         $this->htransaksi = $htransaksi;
     }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      return $this->from('admin@miracle.com')
      ->view('front.temp-email.payment-confirmation');
    }
}
