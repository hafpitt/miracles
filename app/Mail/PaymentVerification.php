<?php

namespace App\Mail;

use App\Models\User;
use App\Models\HTransaksi;
use App\Models\BankUser;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Crypt;


class PaymentVerification extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
     // public $user;
     public $htransaksi;

     /**
      * Create a new message instance.
      *
      * @return void
      */
     public function __construct($htransaksi)
     {
         // $this->user = $user;
         $this->htransaksi = $htransaksi;
     }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      return $this->from('admin@miracle.com')
      ->view('front.temp-email.payment-verification');
    }
}
