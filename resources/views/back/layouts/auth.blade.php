<!doctype html>
<html lang="en">
<head>
  <title>@yield('title') -Miracle</title>
  @include('back.includes.meta')
  @yield('more-css','')
  <style>

  .content-preloader-overlay {
    background-color: black;
    -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=40)";
    background-color: rgba(0, 0, 0, 0.4);
    position: fixed;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    display: none;
    z-index: 10000;
  }
  </style>
</head>
<body class="login_page app_theme_a">
    <!-- <div class="uk-modal" id="modal_preload"></div> -->
    @yield('content')
    @include('back.includes.jsscript')
    @yield('more-js','')
</body>
</html>
