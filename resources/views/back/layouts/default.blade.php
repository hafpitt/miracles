<!doctype html>
<html lang="en">
<head>
  <title>@yield('title') | Miracle</title>
  @include('back.includes.meta')
  @yield('more-css','')
  <style>
  #top_bar {
    overflow-x: auto;
    overflow-y: hidden;
    height: auto;
  }
  #breadcrumbs {
    overflow: hidden;
    display: inline-block;
    min-width: 100%;
    padding-right: 10%;
  }
  .content-preloader-overlay {
    background-color: black;
    -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=40)";
    background-color: rgba(0, 0, 0, 0.4);
    position: fixed;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    display: none;
    z-index: 10000;
  }
  </style>
</head>
<body class="sidebar_main_open sidebar_main_swipe app_theme_a">
    <!-- <div class="uk-modal" id="modal_preload"></div> -->
    <!-- <div id="overlayer" tabindex="-1" style="opacity: 1.03; display: block;"></div> -->
    @include('back.includes.header')
    @include('back.includes.sidebar')
    <div id="page_content">
        @yield('bread','')
        @yield('content')
    </div>
    @yield('before-more-js','')
    @include('back.includes.jsscript')
    @yield('more-js','')
    @if (false)
      @include('back.includes.jstimeout')
    @endif
    @stack('scripts')
</body>
</html>
