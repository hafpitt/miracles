<!doctype html>
<html lang="en">
<head>
  <title>Miracle - @yield('title')</title>
  @include('back.includes.meta')
  @yield('more-css','')
</head>

<body class="error_page app_theme_a">
  @yield('content')
</body>
</html>
