<aside id="sidebar_main">
  <div class="sidebar_main_header">
    <div class="sidebar_logo">
        <!-- <a href="" class="sSidebar_hide sidebar_logo_large">
            <img class="logo_regular" src="{{asset('/assets/back/img/logo_main.png')}}" alt="" height="15" width="71"/>
            <img class="logo_light" src="{{asset('/assets/back/img/logo_main_white.png')}}" alt="" height="15" width="71"/>
        </a>
        <a href="" class="sSidebar_show sidebar_logo_small">
            <img class="logo_regular" src="{{asset('/assets/back/img/logo_main_small.png')}}" alt="" height="32" width="32"/>
            <img class="logo_light" src="{{asset('/assets/back/img/logo_main_small_light.png')}}" alt="" height="32" width="32"/>
        </a> -->
    </div>
  </div>
  <div class="menu_section">
    <ul>
      @if (isset($back_access_menu) && in_array("dashboard", $back_access_menu))
      <li class="{{ Request::is('back-end/dashboard') ? 'current_section' : '' }}" title="Dashboard">
        <a href="{{ url('back-end/dashboard')}}">
          <span class="menu_icon"><i class="material-icons">&#xE871;</i></span>
          <span class="menu_title">Dashboard</span>
        </a>
      </li>
      @endif
      @if (isset($back_access_menu) && in_array("cms", $back_access_menu))
      <li>
        <a href="#">
          <span class="menu_icon"><i class="material-icons">&#xE060;</i></span>
          <span class="menu_title">CMS</span>
        </a>
        <ul>
          <li class="{{ Request::is('back-end/cms/contact') ? 'act_item' : '' }}"><a href="{{ url('back-end/cms/contact') }}">Contact</a></li>
          <li>
            <a href="#">
              <span class="menu_title">Slider</span>
            </a>
            <ul>
              <li class="{{ Request::is('back-end/cms/home/entry') ? 'act_item' : '' }}"><a href="{{ url('back-end/cms/home/entry') }}">New</a></li>
              <li class="{{ Request::is('back-end/cms/home/list') ? 'act_item' : '' }}"><a href="{{ url('back-end/cms/home/list') }}">Table</a></li>
            </ul>
          </li>
          <li>
            <a href="#">
              <span class="menu_title">Page Image</span>
            </a>
            <ul>
              <li class="{{ Request::is('back-end/cms/page-image/all-top-parallax') ? 'act_item' : '' }}"><a href="{{ url('back-end/cms/page-image/all-top-parallax') }}">All Top Parallax</a></li>
              <li class="{{ Request::is('back-end/cms/page-image/register-user') ? 'act_item' : '' }}"><a href="{{ url('back-end/cms/page-image/register-user') }}">Register User</a></li>
              <li class="{{ Request::is('back-end/cms/page-image/login-user') ? 'act_item' : '' }}"><a href="{{ url('back-end/cms/page-image/login-user') }}">Login User</a></li>
            </ul>
          </li>
        </ul>
      </li>
      @endif
      @if (isset($back_access_menu) && in_array("master", $back_access_menu))
      <li title="Master">
        <a href="#">
          <span class="menu_icon"><i class="material-icons">&#xE02F;</i></span>
          <span class="menu_title">Master</span>
        </a>
        <ul>
          @if (isset($back_access_menu) && in_array("modul", $back_access_menu))
          <li>
            <a href="#">
              <span class="menu_title">Modul</span>
            </a>
            <ul>
              @if (isset($back_access_menu) && in_array("master/modul/entry", $back_access_menu))
              <li class="{{ Request::is('back-end/master/modul/entry') ? 'act_item' : '' }}"><a href="{{ url('back-end/master/modul/entry') }}">New</a></li>
              @endif
              @if (isset($back_access_menu) && in_array("master/modul/list", $back_access_menu))
              <li class="{{ Request::is('back-end/master/modul/list') ? 'act_item' : '' }}"><a href="{{ url('back-end/master/modul/list') }}">Table</a></li>
              @endif
            </ul>
          </li>
          @endif
          @if (isset($back_access_menu) && in_array("bank", $back_access_menu))
          <li>
            <a href="#">
              <span class="menu_title">Bank</span>
            </a>
            <ul>
              @if (isset($back_access_menu) && in_array("master/bank/entry", $back_access_menu))
              <li class="{{ Request::is('back-end/master/bank/entry') ? 'act_item' : '' }}"><a href="{{ url('back-end/master/bank/entry') }}">New</a></li>
              @endif
              @if (isset($back_access_menu) && in_array("master/bank/list", $back_access_menu))
              <li class="{{ Request::is('back-end/master/bank/list') ? 'act_item' : '' }}"><a href="{{ url('back-end/master/bank/list') }}">Table</a></li>
              @endif
            </ul>
          </li>
          @endif
          @if (isset($back_access_menu) && in_array("lokasi", $back_access_menu))
          <li>
            <a href="#">
              <span class="menu_title">Lokasi</span>
            </a>
            <ul>
              <li>
                <a href="#">
                  <span class="menu_title">Provinsi</span>
                </a>
                <ul>
                  @if (isset($back_access_menu) && in_array("master/lokasi/provinsi/entry", $back_access_menu))
                  <li class="{{ Request::is('back-end/master/lokasi/provinsi/entry') ? 'act_item' : '' }}"><a href="{{ url('back-end/master/lokasi/provinsi/entry') }}">New</a></li>
                  @endif
                  @if (isset($back_access_menu) && in_array("master/lokasi/provinsi/list", $back_access_menu))
                  <li class="{{ Request::is('back-end/master/lokasi/provinsi/list') ? 'act_item' : '' }}"><a href="{{ url('back-end/master/lokasi/provinsi/list') }}">Table</a></li>
                  @endif
                </ul>
              </li>
              <li>
                <a href="#">
                  <span class="menu_title">Kota</span>
                </a>
                <ul>
                  @if (isset($back_access_menu) && in_array("master/lokasi/kota/entry", $back_access_menu))
                  <li class="{{ Request::is('back-end/master/lokasi/kota/entry') ? 'act_item' : '' }}"><a href="{{ url('back-end/master/lokasi/kota/entry') }}">New</a></li>
                  @endif
                  @if (isset($back_access_menu) && in_array("master/lokasi/kota/list", $back_access_menu))
                  <li class="{{ Request::is('back-end/master/lokasi/kota/list') ? 'act_item' : '' }}"><a href="{{ url('back-end/master/lokasi/kota/list') }}">Table</a></li>
                  @endif
                </ul>
              </li>
              <li>
                <a href="#">
                  <span class="menu_title">Kecamatan</span>
                </a>
                <ul>
                  @if (isset($back_access_menu) && in_array("master/lokasi/kecamatan/entry", $back_access_menu))
                  <li class="{{ Request::is('back-end/master/lokasi/kecamatan/entry') ? 'act_item' : '' }}"><a href="{{ url('back-end/master/lokasi/kecamatan/entry') }}">New</a></li>
                  @endif
                  @if (isset($back_access_menu) && in_array("master/lokasi/kecamatan/list", $back_access_menu))
                  <li class="{{ Request::is('back-end/master/lokasi/kecamatan/list') ? 'act_item' : '' }}"><a href="{{ url('back-end/master/lokasi/kecamatan/list') }}">Table</a></li>
                  @endif
                </ul>
              </li>
            </ul>
          </li>
          @endif
          @if (isset($back_access_menu) && in_array("department", $back_access_menu))
          <li>
            <a href="#">
              <span class="menu_title">Department</span>
            </a>
            <ul>
              @if (isset($back_access_menu) && in_array("master/department/entry", $back_access_menu))
              <li class="{{ Request::is('back-end/master/department/entry') ? 'act_item' : '' }}"><a href="{{ url('back-end/master/department/entry') }}">New</a></li>
              @endif
              @if (isset($back_access_menu) && in_array("master/department/list", $back_access_menu))
              <li class="{{ Request::is('back-end/master/department/list') ? 'act_item' : '' }}"><a href="{{ url('back-end/master/department/list') }}">Table</a></li>
              @endif
            </ul>
          </li>
          @endif
          @if (isset($back_access_menu) && in_array("users", $back_access_menu))
          <li>
            <a href="#">
              <span class="menu_title">Users</span>
            </a>
            <ul>
              @if (isset($back_access_menu) && in_array("master/users/entry", $back_access_menu))
              <li class="{{ Request::is('back-end/master/users/entry') ? 'act_item' : '' }}"><a href="{{ url('back-end/master/users/entry') }}">New</a></li>
              @endif
              @if (isset($back_access_menu) && in_array("master/users/list", $back_access_menu))
              <li class="{{ Request::is('back-end/master/users/list') ? 'act_item' : '' }}"><a href="{{ url('back-end/master/users/list') }}">Table</a></li>
              @endif
            </ul>
          </li>
          @endif
          @if (isset($back_access_menu) && in_array("barang", $back_access_menu))
          <li>
            <a href="#">
              <span class="menu_title">Barang</span>
            </a>
            <ul>
              <li>
                <a href="#">
                  <span class="menu_title">Category</span>
                </a>
                <ul>
                  @if (isset($back_access_menu) && in_array("master/barang/category/entry", $back_access_menu))
                  <li class="{{ Request::is('back-end/master/barang/category/entry') ? 'act_item' : '' }}"><a href="{{ url('back-end/master/barang/category/entry') }}">New</a></li>
                  @endif
                  @if (isset($back_access_menu) && in_array("master/barang/category/list", $back_access_menu))
                  <li class="{{ Request::is('back-end/master/barang/category/list') ? 'act_item' : '' }}"><a href="{{ url('back-end/master/barang/category/list') }}">Table</a></li>
                  @endif
                </ul>
              </li>
              <li>
                <a href="#">
                  <span class="menu_title">Barang</span>
                </a>
                <ul>
                  @if (isset($back_access_menu) && in_array("master/barang/register/entry", $back_access_menu))
                  <li class="{{ Request::is('back-end/master/barang/register/entry') ? 'act_item' : '' }}"><a href="{{ url('back-end/master/barang/register/entry') }}">New</a></li>
                  @endif
                  @if (isset($back_access_menu) && in_array("master/barang/register/list", $back_access_menu))
                  <li class="{{ Request::is('back-end/master/barang/register/list') ? 'act_item' : '' }}"><a href="{{ url('back-end/master/barang/register/list') }}">Table</a></li>
                  @endif
                </ul>
              </li>
              <!-- <li>
                <a href="#">
                  <span class="menu_title">Discount</span>
                </a>
                <ul>
                  <act_item> untuk block sidebar || is untuk if
                  <li class="{{ Request::is('back-end/master/package/discount/entry') ? 'act_item' : '' }}"><a href="{{ url('back-end/master/package/discount/entry') }}">New</a></li>
                  <li class="{{ Request::is('back-end/master/package/discount/list') ? 'act_item' : '' }}"><a href="{{ url('back-end/master/package/discount/list') }}">Table</a></li>
                </ul>
              </li>
              <li>
                <a href="#">
                  <span class="menu_title">Gallery</span>
                </a>
                <ul>
                  <act_item> untuk block sidebar || is untuk if
                  <li class="{{ Request::is('back-end/master/package/gallery/entry') ? 'act_item' : '' }}"><a href="{{ url('back-end/master/package/gallery/entry') }}">New</a></li>
                  <li class="{{ Request::is('back-end/master/package/gallery/list') ? 'act_item' : '' }}"><a href="{{ url('back-end/master/package/gallery/list') }}">Table</a></li>
                </ul>
              </li> -->
            </ul>
          </li>
          @endif
        </ul>
      </li>
      @endif
      @if (isset($back_access_menu) && in_array("transaction", $back_access_menu))
      <li title="Transaction">
        <a href="#">
          <span class="menu_icon"><i class="material-icons">&#xE227;</i></span>
          <span class="menu_title">Transaction</span>
        </a>
        <ul>
          <li>
            <a href="#">
              <span class="menu_title">Pelunasan Piutang</span>
            </a>
            <ul>
              @if (isset($back_access_menu) && in_array("transaction/piutang/entry", $back_access_menu))
              <li class="{{ Request::is('back-end/transaction/piutang/entry') ? 'act_item' : '' }}"><a href="{{ url('back-end/transaction/piutang/entry') }}">New</a></li>
              @endif
              @if (isset($back_access_menu) && in_array("transaction/piutang/list", $back_access_menu))
              <li class="{{ Request::is('back-end/transaction/piutang/list') ? 'act_item' : '' }}"><a href="{{ url('back-end/transaction/piutang/list') }}">Table</a></li>
              @endif
            </ul>
          </li>
          <li>
            <a href="#">
              <span class="menu_title">Konfirmasi Kirim</span>
            </a>
            <ul>
              @if (isset($back_access_menu) && in_array("transaction/confirmation/list", $back_access_menu))
              <li class="{{ Request::is('back-end/transaction/confirmation/list') ? 'act_item' : '' }}"><a href="{{ url('back-end/transaction/confirmation/list') }}">Table</a></li>
              @endif
            </ul>
          </li>
          <li>
            <a href="#">
              <span class="menu_title">Retur Barang</span>
            </a>
            <ul>
              <li class="{{ Request::is('back-end/transaction/retur/pengajuankomplain') ? 'act_item' : '' }}"><a href="{{ url('back-end/transaction/retur/pengajuankomplain') }}">Pengajuan Komplain</a></li>
              <li class="{{ Request::is('back-end/transaction/retur/pengajuanretur') ? 'act_item' : '' }}"><a href="{{ url('back-end/transaction/retur/pengajuanretur') }}">Retur</a></li>
              <!-- @if (isset($back_access_menu) && in_array("transaction/retur/entry", $back_access_menu))
              <li class="{{ Request::is('back-end/transaction/retur/entry') ? 'act_item' : '' }}"><a href="{{ url('back-end/transaction/retur/entry') }}">New</a></li>
              @endif -->
            </ul>
          </li>
        </ul>
      </li>
      @endif
      <li title="Report">
        <a href="#">
          <span class="menu_icon"><i class="material-icons">&#xE85C;</i></span>
          <span class="menu_title">Report</span>
        </a>
        <ul>
          <li class="{{ Request::is('back-end/report/sisa-stok') ? 'act_item' : '' }}">
            <a href="{{ url('back-end/report/sisa-stok') }}">Sisa Stok</a>
          </li>
          <li class="{{ Request::is('back-end/report/piutang-belum-lunas') ? 'act_item' : '' }}">
            <a href="{{ url('back-end/report/piutang-belum-lunas') }}">Piutang Belum Lunas</a>
          </li>
          <li class="{{ Request::is('back-end/report/tingkat-penjualan-perbulan') ? 'act_item' : '' }}">
            <a href="{{ url('back-end/report/tingkat-penjualan-perbulan') }}">Tingkat Penjualan Per Bulan</a>
          </li>
          <li class="{{ Request::is('back-end/report/tingkat-penjualan-perhari') ? 'act_item' : '' }}">
            <a href="{{ url('back-end/report/tingkat-penjualan-perhari') }}">Tingkat Penjualan Per Hari</a>
          </li>
          <li class="{{ Request::is('back-end/report/total-register-user-pertgl') ? 'act_item' : '' }}">
            <a href="{{ url('back-end/report/total-register-user-pertgl') }}">Tingkat Register User Per Tgl</a>
          </li>
        </ul>
      </li>
      <!-- <li class="{{ Request::is('back-end/broadcast-subscriber') ? 'current_section' : '' }}" title="Broadcast">
        <a href="{{ url('back-end/broadcast-subscriber')}}">
          <span class="menu_icon"><i class="material-icons">&#xE0BE;</i></span>
          <span class="menu_title">Broadcast Email</span>
        </a>
      </li> -->
    </ul>
  </div>
</aside>
