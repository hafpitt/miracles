<meta charset="UTF-8">
<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="msapplication-tap-highlight" content="no"/>
<meta name="csrf-token" content="{{ csrf_token() }}">


<link rel="icon" type="image/jpg" href="{{asset('assets/back/logo.jpg')}}"/>
<link rel="stylesheet" type="text/css" media="all" href="{{asset('assets/back/bower_components/uikit/css/uikit.almost-flat.min.css')}}">
<link rel="stylesheet" type="text/css" media="all" href="{{asset('assets/back/icons/flags/flags.min.css')}}">
<link rel="stylesheet" type="text/css" media="all" href="{{asset('assets/back/css/style_switcher.min.css')}}">
<link rel="stylesheet" type="text/css" media="all" href="{{asset('assets/back/css/main.min.css')}}">
<link rel="stylesheet" type="text/css" media="all" href="{{asset('assets/back/css/themes/themes_combined.min.css')}}">
<style>
.req{
  color:red
}
</style>
