@extends('back.layouts.default')
@section('title', 'Kecamatan')
@section('bread')
<div id="top_bar">
  <ul id="breadcrumbs">
    <li><a href="{{url('back')}}">Home</a></li>
    <li><a href="#">Master</a></li>
    <li><a href="#">Lokasi</a></li>
    <li><a href="#">Kecamatan</a></li>
    <li><a href="{{url()->current()}}" style="color: grey">Entry</a></li>
  </ul>
</div>
@endsection
@section('content')
<div id="page_content_inner">
  @if(session()->has('message'))
      <div class="uk-alert uk-alert-success" data-uk-alert>
        <a href="" class="uk-alert-close uk-close"></a>
          {{ session()->get('message') }}
      </div>
  @endif
  <div class="md-card uk-margin-bottom uk-margin-remove">
      <div class="md-card-toolbar">
          <div class="md-card-toolbar-actions">
              <i class="md-icon material-icons md-card-fullscreen-activate">&#xE5D0;</i>
              <i class="md-icon material-icons md-card-toggle">&#xE316;</i>
          </div>
          <h3 class="md-card-toolbar-heading-text">
              Entry Kecamatan
          </h3>
      </div>
      <div class="md-card-content large-padding">
          <form id="validate" enctype="multipart/form-data" class="uk-form-stacked" method="post" action="{{url()->current()}}">
            {{ csrf_field() }}
            <div class="uk-grid" data-uk-grid-margin>
              <div class="uk-width-medium-1-1">
                  <div class="parsley-row">
                      <label for="nama">Nama<span class="req">*</span></label>
                      <input type="text" name="nama" id="nama" required class="md-input" data-parsley-minlength="3" value="{{ old('nama', isset($lokasi_kecamatan->nama) ? $lokasi_kecamatan->nama : '' ) }}" />
                      @foreach ($errors->get('nama') as $message)
                        <div class="uk-alert uk-alert-danger" data-uk-alert>
                          <a href="" class="uk-alert-close uk-close"></a>
                          <p><strong>Error!</strong> {{$message}}</p>
                        </div>
                      @endforeach
                  </div>
              </div>
            </div>
            <div class="uk-grid" data-uk-grid-margin>
              <div class="uk-width-medium-1-1">
                  <div class="parsley-row">
                      <label for="id_kota">Kota<span class="req">*</span></label>
                      <select data-md-selectize name="id_kota" required>
                        <option value="">Kota</option>
                        @foreach ($kota as $data)
                          <option value="{{$data->id}}" {{(old('id_kota', isset($lokasi_kecamatan->id_kota) ? $lokasi_kecamatan->id_kota : '' ) == $data->id ? 'selected':'') }} >{{$data->nama}}</option>
                        @endforeach
                      </select>
                      @foreach ($errors->get('id_kota') as $message)
                        <div class="uk-alert uk-alert-danger" data-uk-alert>
                          <a href="" class="uk-alert-close uk-close"></a>
                          <p><strong>Error!</strong> {{$message}}</p>
                        </div>
                      @endforeach
                  </div>
              </div>
            </div>
            <div class="uk-grid">
                <div class="uk-width-1-1">
                    <button type="submit" id="submit" class="md-btn md-btn-primary">Submit <i class="material-icons">&#xE163;</i></button>
                </div>
            </div>
            @isset($lokasi_kota)
              <input type="hidden" name="id" value="{{$lokasi_kota->id}}"/>
            @endisset
          </form>
      </div>
  </div>
</div>
@endsection
@section('more-css')
<link rel="stylesheet" type="text/css" media="all" href="{{asset('assets/back/skins/dropify/css/dropify.css')}}">
<link rel="stylesheet" type="text/css" media="all" href="{{asset('assets/back/bower_components/sweetalert/dist/sweetalert.css')}}">
<style>
#parsley-id-5
{
  margin-left: 0;
}
</style>
@endsection
@section('more-js')
<script>
altair_forms.parsley_validation_config();
</script>
<script src="{{asset('assets/back/bower_components/parsleyjs/dist/parsley.min.js')}}"></script>
<script src="{{asset('assets/back/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
<script src="{{asset('assets/back/bower_components/jquery.serializeJSON/jquery.serializejson.min.js')}}"></script>
<script src="{{asset('assets/back/bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js')}}"></script>
<script src="{{asset('assets/back/js/custom/dropify/dist/js/dropify.min.js')}}"></script>
<script>
$(function() {
    thisform.init();
}), thisform = {
  init: function()
  {
    thisform.p_init();
    thisform.s_init();
    thisform.m_init();
    thisform.i_init();
  },
  p_init: function ()
  {
    var i = $("#validate");
    i.parsley().on("form:validated", function() {
        altair_md.update_input(i.find(".md-input-danger"))
    }).on("field:validated", function(i) {
        $(i.$element).hasClass("md-input") && altair_md.update_input($(i.$element))
    }).on("form:submit",function(){
      $(".masked_input").inputmask('remove');
    }), window.Parsley.on("field:validate", function() {
        var i = $(this.$element).closest(".md-input-wrapper").siblings(".error_server_side");
        i && i.hide()
    })
  },
  m_init: function() {
    $maskedInput = $(".masked_input"),
    $maskedInput.length && $maskedInput.inputmask();
  },
  s_init: function()
  {
    $maskedInput = $(".selectize"),
    $maskedInput.length && $maskedInput.selectize();
  },
  i_init: function()
  {
    $(".dropify").dropify(), $(".dropify-fr").dropify({
      messages: {
        "default": "Glissez-déposez un fichier ici ou cliquez",
        replace: "Glissez-déposez un fichier ou cliquez pour remplacer",
        remove: "Supprimer",
        error: "Désolé, le fichier trop volumineux"
      }
    })
  },
};
</script>
@endsection
