@extends('back.layouts.default')
@section('title', 'List Catagory')
@section('bread')
<div id="top_bar">
  <ul id="breadcrumbs">
    <li><a href="{{url('back')}}">Home</a></li>
    <li><a href="#">Master</a></li>
    <li><a href="#">Package</a></li>
    <li><a href="#">Catagory</a></li>
    <li><a href="{{url()->current()}}" style="color: grey">List</a></li>
  </ul>
</div>
@endsection
@section('content')
<div id="page_content_inner">
  @if(session()->has('message'))
      <div class="uk-alert uk-alert-success" data-uk-alert>
        <a href="" class="uk-alert-close uk-close"></a>
          {{ session()->get('message') }}
      </div>
  @endif
  <div class="md-card uk-margin-bottom uk-margin-remove">
      <div class="md-card-toolbar">
          <div class="md-card-toolbar-actions">
              <i class="md-icon material-icons md-card-fullscreen-activate">&#xE5D0;</i>
              <i class="md-icon material-icons md-card-toggle">&#xE316;</i>
          </div>
          <h3 class="md-card-toolbar-heading-text">
              List Catagory
          </h3>
      </div>
      <div class="md-card-content">
        <div class="dt_colVis_buttons"></div>
        <table id="tabledata" class="uk-table" cellspacing="0" width="100%">
          <thead>
          <tr>
             <th>ID</th>
             <th>Nama</th>
             <th>Photo</th>
             <th>Action</th>
          </tr>
         </thead>
        </table>
      </div>
  </div>
</div>
@endsection
@section('more-css')
<link rel="stylesheet" type="text/css" media="all" href="{{asset('assets/back/bower_components/sweetalert/dist/sweetalert.css')}}">
@endsection
@section('more-js')
<script src="{{asset('assets/back/bower_components/parsleyjs/dist/parsley.min.js')}}"></script>
<script src="{{asset('assets/back/bower_components/datatables/media/js/jquery.dataTables.js')}}"></script>
<script src="{{asset('assets/back/bower_components/datatables-buttons/js/dataTables.buttons.js')}}"></script>
<script src="{{asset('assets/back/js/custom/datatables/buttons.uikit.js')}}"></script>
<script src="{{asset('assets/back/bower_components/jszip/dist/jszip.min.js')}}"></script>
<script src="{{asset('assets/back/bower_components/pdfmake/build/pdfmake.min.js')}}"></script>
<script src="{{asset('assets/back/bower_components/pdfmake/build/vfs_fonts.js')}}"></script>
<script src="{{asset('assets/back/bower_components/datatables-buttons/js/buttons.colVis.js')}}"></script>
<script src="{{asset('assets/back/bower_components/datatables-buttons/js/buttons.html5.js')}}"></script>
<script src="{{asset('assets/back/bower_components/datatables-buttons/js/buttons.print.js')}}"></script>
<script src="{{asset('assets/back/js/custom/datatables/datatables.uikit.min.js')}}"></script>
<script src="{{asset('assets/back/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
<script src="{{asset('assets/back/bower_components/jquery.serializeJSON/jquery.serializejson.min.js')}}"></script>
<script src="{{asset('assets/back/bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js')}}"></script>
<script>
$(function() {
    thisform.init();
}), thisform = {
  init: function()
  {
    thisform.d_init();
  },
  d_init: function() {
    var t = $("#tabledata"),
        a = t.prev(".dt_colVis_buttons");
    if (t.length) {
        var e = t.DataTable({
            processing: true,
            serverSide: true,
            ajax: {
              url: '{{url()->current()}}',
              type: 'POST',
              data: {_method: 'options'},
            },dom: "<'dt_colVis_buttons'B><'dt-uikit-header'<'uk-grid'<'uk-width-medium-2-3'l><'uk-width-medium-1-3'f>>><'uk-overflow-container'tr><'dt-uikit-footer'<'uk-grid'<'uk-width-medium-3-10'i><'uk-width-medium-7-10'p>>>",
            buttons: [
            {
              extend: 'print',
              exportOptions: {
                columns: ':visible'
              }
            },
            {
              extend: 'excel',
              exportOptions: {
                columns: ':visible'
              }
            },
            {
              extend: 'pdf',
              exportOptions: {
                columns: ':visible'
              }
            },
            {
              extend: 'copy',
              exportOptions: {
                columns: ':visible'
              }
            },
            {
              extend: 'csv',
              exportOptions: {
                columns: ':visible'
              }
            },
              'colvis'
            ],
            "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
            "drawCallback": function( settings ) {
              thisform.s_init();
            },
        });
    }
  },
  s_init: function() {
    $('.try-delete').on('click',function(event){
      event.preventDefault();
      $link=$(this).attr('href');
      swal({
        title: "Are you sure?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: true,
        closeOnCancel: true
      },
      function(isConfirm){
        if (isConfirm) {
          location.href = $link;
        }
      })
    })
  },
};
</script>
@endsection
