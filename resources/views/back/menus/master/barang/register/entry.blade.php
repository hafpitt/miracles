@extends('back.layouts.default')
@section('title', 'Barang')
@section('bread')
<div id="top_bar">
  <ul id="breadcrumbs">
    <li><a href="{{url('back')}}">Home</a></li>
    <li><a href="#">Master</a></li>
    <li><a href="#">Barang</a></li>
    <li><a href="#">Register</a></li>
    <li><a href="{{url()->current()}}" style="color: grey">Entry</a></li>
  </ul>
</div>
@endsection
@section('content')
<div id="page_content_inner">
  @if(session()->has('message'))
      <div class="uk-alert uk-alert-success" data-uk-alert>
        <a href="" class="uk-alert-close uk-close"></a>
          {{ session()->get('message') }}
      </div>
  @endif

<form id="validate" enctype="multipart/form-data" class="uk-form-stacked" method="post" action="{{url()->current()}}">
  <div class="md-card uk-margin-bottom uk-margin-remove">
      <div class="md-card-toolbar">
          <div class="md-card-toolbar-actions">
              <i class="md-icon material-icons md-card-fullscreen-activate">&#xE5D0;</i>
              <i class="md-icon material-icons md-card-toggle">&#xE316;</i>
          </div>
          <h3 class="md-card-toolbar-heading-text">
              Entry Barang
          </h3>
      </div>
      <div class="md-card-content large-padding">
          {{ csrf_field() }}
          <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium-1-1">
                <div class="parsley-row">
                    <label for="nama">Nama<span class="req">*</span></label>
                    <input type="text" name="nama" id="nama" value="{{ old('nama', isset($barang_register->nama) ? $barang_register->nama : '') }}" required class="md-input"/>
                    @foreach ($errors->get('nama') as $message)
                      <div class="uk-alert uk-alert-danger" data-uk-alert>
                        <a href="" class="uk-alert-close uk-close"></a>
                        <p><strong>Error!</strong> {{$message}}</p>
                      </div>
                    @endforeach
                </div>
            </div>
          </div>
          <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium-1-1">
                <div class="parsley-row">
                    <label for="id_barang_kategori">Barang Category<span class="req">*</span></label>
                    <select data-md-selectize name="id_barang_kategori" required>
                      <option value="">Barang Category</option>
                      @foreach ($category as $data)
                        <option value="{{$data->id}}" {{(old('id_barang_kategori', isset($barang_register->id_barang_kategori) ? $barang_register->id_barang_kategori : '' ) == $data->id ? 'selected':'') }} >{{$data->nama}}</option>
                      @endforeach
                    </select>
                    @foreach ($errors->get('id_barang_kategori') as $message)
                      <div class="uk-alert uk-alert-danger" data-uk-alert>
                        <a href="" class="uk-alert-close uk-close"></a>
                        <p><strong>Error!</strong> {{$message}}</p>
                      </div>
                    @endforeach
                </div>
            </div>
          </div>
          <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium-1-2">
                <div class="parsley-row">
                    <label for="nama">Stok<span class="req">*</span></label>
                    <input type="text" name="stok" id="stok" data-inputmask="'alias': 'currency', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false,'prefix':'','suffix': ' Pcs', 'placeholder': '0'" value="{{ old('stok', isset($barang_register->stok) ? $barang_register->stok : '') }}" required class="md-input masked_input"/>
                    @foreach ($errors->get('stok') as $message)
                      <div class="uk-alert uk-alert-danger" data-uk-alert>
                        <a href="" class="uk-alert-close uk-close"></a>
                        <p><strong>Error!</strong> {{$message}}</p>
                      </div>
                    @endforeach
                </div>
            </div>
            <div class="uk-width-medium-1-2">
                <div class="parsley-row">
                    <label for="nama">Berat<span class="req">*</span></label>
                    <input type="text" name="berat" id="berat" data-inputmask="'alias': 'currency', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false, 'prefix':'','suffix': ' Gram', 'placeholder': '0'" value="{{ old('berat', isset($barang_register->berat) ? $barang_register->berat : '') }}" required class="md-input masked_input"/>
                    @foreach ($errors->get('berat') as $message)
                      <div class="uk-alert uk-alert-danger" data-uk-alert>
                        <a href="" class="uk-alert-close uk-close"></a>
                        <p><strong>Error!</strong> {{$message}}</p>
                      </div>
                    @endforeach
                </div>
            </div>
          </div>
          <div class="uk-grid">
              <div class="uk-width-1-1">
                  <div id="my-dropzone"  class="uk-file-upload dropzone">
                      <div class="fallback">
                        <input name="file" type="file" multiple />
                      </div>
                  </div>
              </div>
          </div>
          <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium-1-2">
                <div class="parsley-row">
                    <label for="keterangan">Keterangan<span class="req">*</span></label>
                    <textarea class="md-input" name="keterangan" cols="10" rows="3" required>{{ old('keterangan', isset($barang_register->keterangan) ? $barang_register->keterangan : '' ) }}</textarea>
                    @foreach ($errors->get('keterangan') as $message)
                      <div class="uk-alert uk-alert-danger" data-uk-alert>
                        <a href="" class="uk-alert-close uk-close"></a>
                        <p><strong>Error!</strong> {{$message}}</p>
                      </div>
                    @endforeach
                </div>
            </div>
              <div class="uk-width-medium-1-2">
                  <div class="parsley-row">
                      <label for="keterangan">Keterangan Detail<span class="req">*</span></label>
                      <textarea class="md-input" name="keterangandetail" cols="10" rows="3" required>{{ old('keterangandetail', isset($barang_register->keterangandetail) ? $barang_register->keterangandetail : '' ) }}</textarea>
                      @foreach ($errors->get('keterangandetail') as $message)
                        <div class="uk-alert uk-alert-danger" data-uk-alert>
                          <a href="" class="uk-alert-close uk-close"></a>
                          <p><strong>Error!</strong> {{$message}}</p>
                        </div>
                      @endforeach
                  </div>
              </div>
          </div>
          <div class="uk-grid" data-uk-grid-margin>
              <div class="uk-width-medium-2-10">
                  <span class="uk-display-block uk-margin-small-top uk-text-large">Colors</span>
              </div>
              <div class="uk-width-medium-8-10">
                  <div class="uk-overflow-container">
                      <table class="uk-table">
                          <thead>
                              <tr>
                                  <th class="uk-width-4-10 uk-text-nowrap">Nama</th>
                                  <th class="uk-width-2-10 uk-text-nowrap">Warna</th>
                                  <th class="uk-width-2-10 uk-text-center uk-text-nowrap">Tersedia</th>
                                  <th class="uk-width-2-10 uk-text-right uk-text-nowrap">Action</th>
                              </tr>
                          </thead>
                          <tbody>
                            @isset($barang_register->barang_warnas)
                              @foreach ($barang_register->barang_warnas as $warna)
                                <tr class="form_section">
                                  <td class="uk-width-4-10">
                                    <input type="text" name="old_warna[{{$warna->id}}][nama]" class="md-input md-input-width-medium" placeholder="Color" value="{{$warna->nama}}"/>
                                  </td>
                                  <td class="uk-width-2-10">
                                    <input type="text" name="old_warna[{{$warna->id}}][warna]" class="md-input md-input-width-medium jscolor" value="{{$warna->warna}}"/>
                                  </td>
                                  <td class="uk-width-2-10 uk-text-middle uk-text-center">
                                    <input type="checkbox" name="old_warna[{{$warna->id}}][tersedia]" data-md-icheck {{($warna->tersedia)?'checked':''}}/>
                                  </td>
                                  <td class="uk-width-2-10 uk-text-right uk-text-middle">
                                    -
                                      <!-- <a href="javascript:void(0);" class="btnSectionClone old_warna"><i class="material-icons md-24">&#xE872;</i></a> -->
                                  </td>
                                </tr>
                              @endforeach
                            @endisset
                            <tr>
                              <td colspan="4" class="uk-padding-remove">
                                  <table class="uk-table" data-dynamic-fields="field_template_a"></table>
                              </td>
                            </tr>
                          </tbody>
                      </table>
                      <script id="field_template_a" type="text/x-handlebars-template">
                          <tr class="form_section">
                            <td class="uk-width-4-10">
                              <input type="text" name="warna[][nama]" class="md-input md-input-width-medium" placeholder="Color" />
                            </td>
                            <td class="uk-width-2-10 style_warna">
                              <input type="text" name="warna[][warna]" class="md-input md-input-width-medium incolor"/>
                            </td>
                            <td class="uk-width-2-10 uk-text-middle uk-text-center">
                              <input type="checkbox" name="warna[][tersedia]" checked data-md-icheck />
                            </td>
                            <td class="uk-width-2-10 uk-text-right uk-text-middle">
                                <a href="javascript:void(0);" class="btnSectionClone new_color"><i class="material-icons md-24">&#xE145;</i></a>
                            </td>
                          </tr>
                      </script>
                  </div>
              </div>
          </div>
          @isset($barang_register)
            <input type="hidden" name="id" value="{{$barang_register->id}}"/>
          @endisset
    </div>
  </div>

  <div class="md-card uk-margin-bottom uk-margin-remove">
    <div class="md-card-toolbar">
      <div class="md-card-toolbar-actions">
          <i class="md-icon material-icons md-card-fullscreen-activate">&#xE5D0;</i>
          <i class="md-icon material-icons md-card-toggle">&#xE316;</i>
      </div>
      <h3 class="md-card-toolbar-heading-text">
          Entry Harga Detail
      </h3>
    </div>
      <div class="md-card-content">
        <table id="tabledata" class="uk-table" cellspacing="0" width="100%">
          <thead>
            <tr>
             <th>Nama Department</th>
             <th>Harga</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($department as $data)
              @php
                $edit=0;
                if (isset($barang_register->departments))
                {
                  foreach ($barang_register->departments as $price)
                  {
                    if($price->id==$data->id){
                      $edit=$price->pivot->harga;
                    }
                  }
                }
              @endphp
            <tr>
              <td>
                <div class="parsley-row">{{$data->nama}}</div>
              </td>
              <td>
                <div class="parsley-row">
                  <input type="text" name="price[{{$data->id}}]" value="{{$edit}}" data-inputmask="'alias': 'currency', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false,'prefix':'IDR ','suffix': '', 'placeholder': '0'" class="md-input masked_input"   required/>
                </div>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>

        <div class="uk-grid">
          <div class="uk-width-1-1">
            <button type="submit" id="submit" class="md-btn md-btn-primary">Submit <i class="material-icons">&#xE163;</i></button>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
@endsection
@section('more-css')
<link rel="stylesheet" type="text/css" media="all" href="{{asset('assets/back/skins/dropify/css/dropify.css')}}">
<link rel="stylesheet" type="text/css" media="all" href="{{asset('assets/back/bower_components/sweetalert/dist/sweetalert.css')}}">
<link rel="stylesheet" type="text/css" media="all" href="{{asset('assets/back/bower_components/dropzone/dropzone.css')}}">
<link rel="stylesheet" type="text/css" media="all" href="{{asset('assets/back/bower_components/HoldOn/src/download/HoldOn.min.css')}}">
<style>
#parsley-id-5
{
  margin-left: 0;
}
</style>
@endsection
@section('more-js')
<script>
altair_forms.parsley_validation_config();
</script>
<script src="{{asset('assets/back/bower_components/parsleyjs/dist/parsley.min.js')}}"></script>
<script src="{{asset('assets/back/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
<script src="{{asset('assets/back/bower_components/jquery.serializeJSON/jquery.serializejson.min.js')}}"></script>
<script src="{{asset('assets/back/bower_components/handlebars/handlebars.min.js')}}"></script>
<script src="{{asset('assets/back/js/custom/handlebars_helpers.min.js')}}"></script>
<script src="{{asset('assets/back/bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js')}}"></script>
<script src="{{asset('assets/back/js/custom/dropify/dist/js/dropify.min.js')}}"></script>
<script src="{{asset('assets/back/bower_components/dropzone/dropzone.js')}}"></script>
<script src="{{asset('assets/back/bower_components/jscolor/jscolor.js')}}"></script>
<script src="{{asset('assets/back/bower_components/HoldOn/src/download/HoldOn.min.js')}}"></script>
<script>
$(function() {
    thisform.init();
}), thisform = {
  init: function()
  {
    thisform.p_init();
    thisform.s_init();
    thisform.m_init();
    thisform.i_init();
    thisform.d_init();
    thisform.r_init();
    thisform.dynamic_fields();
  },
  p_init: function ()
  {
    var i = $("#validate");
    i.parsley().on("form:validated", function() {
        altair_md.update_input(i.find(".md-input-danger"))
    }).on("field:validated", function(i) {
        $(i.$element).hasClass("md-input") && altair_md.update_input($(i.$element))
    }).on("form:submit",function(e){
      HoldOn.open({theme:"sk-cube-grid"});
      if (!$('input[name=photo_utama]:checked').val())
      {
        swal("Failed", "Mohon pilih foto utama", "error");
        HoldOn.close();
        return false;
      };
      $(".masked_input").inputmask('remove');
      if($wrapperThis.files.length>0)
      {
        $wrapperThis.processQueue();
        return false;
      };
      var form_data = JSON.stringify($("#validate").serializeJSON());
      $.ajax({
        url: '{{url()->current()}}',
        type: 'post',
        headers :{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        dataType: 'text',
        data: {'form': form_data},
        success: function (data) {
          setTimeout(function () {
            swal({
              title: "Success",
              text: $('#nama').val()+ " berhasil direvisi",
              type: "success",
              confirmButtonText: "OK"
            },
            function(isConfirm){
              if (isConfirm) {
                @empty($barang_register)
                  location.reload();
                @endempty
                @isset($barang_register)
                  history.go(-1);
                @endisset
              }
            });
          }, 500);
        }
      });
      HoldOn.close();
      return false;
      // $('#validate').append('<input type="hidden" name="form" value="'+form_data+'" /> ');
    }), window.Parsley.on("field:validate", function() {
        var i = $(this.$element).closest(".md-input-wrapper").siblings(".error_server_side");
        i && i.hide()
    })
  },
  dynamic_fields: function() {
      function e(e, i, a) {
          var t = $("#" + i).html(),
              n = Handlebars.compile(t),
              s = n({
                  index: a ? a : 0,
                  counter: a ? "__" + a : "__0"
              });
          e.append(s), altair_md.inputs(e), altair_md.checkbox_radio(e.find("[data-md-icheck]")), altair_forms.switches(e), altair_forms.select_elements(e);
          var input = document.createElement('input');
          input.setAttribute("class", "md-input md-input-width-medium");
          input.setAttribute("name", "warna[][warna]");
          input.setAttribute("type", "text");
          var picker = new jscolor(input);
          e.find('.incolor').replaceWith(input);
      }
      $("[data-dynamic-fields]").each(function() {
          var i = $(this).attr("dynamic-fields-counter", 0),
              a = i.data("dynamicFields");
          e(i, a), i.on("click", ".btnSectionClone", function(t) {
              t.preventDefault();
              i.find(".btnSectionClone").replaceWith('<a href="#" class="btnSectionRemove"><i class="material-icons md-24">&#xE872;</i></a>');
              var n = parseInt(i.attr("dynamic-fields-counter")) + 1;
              i.attr("dynamic-fields-counter", n), e(i, a, n)
          }).on("click", ".btnSectionRemove", function(e) {
              e.preventDefault();
              var i = $(this);
              i.closest(".form_section").next(".form_hr").remove().end().remove()
          })
      })
  },
  m_init: function() {
    $maskedInput = $(".masked_input"),
    $maskedInput.length && $maskedInput.inputmask();
  },
  a_init: function() {
    var input = document.createElement('input');
    var picker = new jscolor(input);
    $(this).append(input);
  },
  r_init: function() {
    $(".old_warna").on('click',function(){
      $(this).parents('tr').remove();
    });
  },
  s_init: function()
  {
    $maskedInput = $(".selectize"),
    $maskedInput.length && $maskedInput.selectize();
  },
  i_init: function()
  {
    $(".dropify").dropify(), $(".dropify-fr").dropify({
      messages: {
        "default": "Glissez-déposez un fichier ici ou cliquez",
        replace: "Glissez-déposez un fichier ou cliquez pour remplacer",
        remove: "Supprimer",
        error: "Désolé, le fichier trop volumineux"
      }
    })
  },
  d_init : function() {
    Dropzone.options.myDropzone = {
      url: '{{url()->current()}}',
      headers :{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      dataType: 'text',
      autoProcessQueue: false,
      uploadMultiple: true,
      parallelUploads: 20,
      maxFiles: 20,
      acceptedFiles : "image/*",
      maxFilesize: 3,
      method: "post",
      paramName: "url_photo",
      addRemoveLinks: true,
      init: function() {
        $wrapperThis = this;
        @isset($barang_register)
          @foreach ($barang_register->barang_fotos as $image)
            var file = {name: '{{$image->nama}}', size: "{{Storage::disk('public')->size($image->url_photo)}}", filename : '{{$image->url_photo}}'};
            $wrapperThis.options.addedfile.call($wrapperThis, file);
            $wrapperThis.options.thumbnail.call($wrapperThis, file, "{{asset(Storage::disk('public')->url($image->url_photo))}}");
            $wrapperThis.emit("complete", file);
            $checkbox = '<div class="default_pic_container"><input type="radio" name="photo_utama" {{!$image->utama ? :"checked"}} value="'+file.name+'"/>Utama</div>';
            $element = Dropzone.createElement($checkbox);
            file.previewElement.appendChild($element);
            file.previewElement.classList.add('dz-success');
            file.previewElement.classList.add('dz-complete');
          @endforeach
        @endisset
        this.on("sendingmultiple", function(file, xhr, formData) {
          $("html, body").animate({ scrollTop: 0 }, "slow");
          var form_data = JSON.stringify($("#validate").serializeJSON());
          formData.append('form',form_data);
        });
        this.on("successmultiple", function(files, response) {
          // return;
          setTimeout(function () {
            swal({
              title: "Success",
              text: $('#nama').val()+ " berhasil didaftarkan",
              type: "success",
              confirmButtonText: "OK"
            },
            function(isConfirm){
              if (isConfirm) {
                @empty($barang_register)
                  location.reload();
                @endempty

                @isset($barang_register)
                  history.go(-1);
                @endisset
              }
            });
          }, 500);
        });
        this.on("addedfile", function(file) {
          if (this.files.length) {
              var _i, _len;
              for (_i = 0, _len = this.files.length; _i < _len - 1; _i++)
              {
                  if(this.files[_i].name === file.name && this.files[_i].size === file.size && this.files[_i].lastModifiedDate.toString() === file.lastModifiedDate.toString())
                  {
                    swal("Failed", "Please input different file name", "error");
                    this.removeFile(file);
                  }
              }
          }
          $checkbox = '<div class="default_pic_container"><input type="radio" name="photo_utama" value="'+file.name+'"/>Utama</div>';
          $element = Dropzone.createElement($checkbox);
          file.previewElement.appendChild($element);
        });
        this.on("removedfile",function(file){
          if ($.inArray('dz-success', file.previewElement.classList) != -1)
          {
            $('<input>').attr({
                type: 'hidden',
                id: 'removed',
                name: 'removed[]',
                value: file.filename,
            }).appendTo('form');
          }
        });
        this.on("error", function(response, xhr) {
          var errorsHtml= '';
          $.each($.parseJSON(response.xhr.responseText), function( key, value ) {
            errorsHtml += value[0] + '<br/>';
          });
          return false;
        });
      }
    };
  },
};
</script>
@endsection
