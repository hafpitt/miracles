@extends('back.layouts.default')
@section('title', 'Gallery')
@section('bread')
<div id="top_bar">
  <ul id="breadcrumbs">
    <li><a href="{{url('back')}}">Home</a></li>
    <li><a href="#">Master</a></li>
    <li><a href="#">Package</a></li>
    <li><a href="#">Gallery</a></li>
    <li><a href="{{url()->current()}}" style="color: grey">Entry</a></li>
  </ul>
</div>
@endsection
@section('content')
<div id="page_content_inner">
  @if(session()->has('message'))
      <div class="uk-alert uk-alert-success" data-uk-alert>
        <a href="" class="uk-alert-close uk-close"></a>
          {{ session()->get('message') }}
      </div>
  @endif
  <div class="md-card uk-margin-bottom uk-margin-remove">
      <div class="md-card-toolbar">
          <div class="md-card-toolbar-actions">
              <i class="md-icon material-icons md-card-fullscreen-activate">&#xE5D0;</i>
              <i class="md-icon material-icons md-card-toggle">&#xE316;</i>
          </div>
          <h3 class="md-card-toolbar-heading-text">
              Entry Gallery
          </h3>
      </div>
      <div class="md-card-content large-padding">
          <form id="validate" enctype="multipart/form-data" class="uk-form-stacked" method="post" action="{{url()->current()}}">
            {{ csrf_field() }}
            <div class="uk-grid" data-uk-grid-margin>
              <div class="uk-width-medium-1-1">
                  <div class="parsley-row">
                      <label for="nama">Nama<span class="req">*</span></label>
                      <input type="text" name="nama" id="nama" value="{{ old('nama', isset($gallery->nama) ? $gallery->nama : '') }}" required class="md-input"/>
                      @foreach ($errors->get('nama') as $message)
                        <div class="uk-alert uk-alert-danger" data-uk-alert>
                          <a href="" class="uk-alert-close uk-close"></a>
                          <p><strong>Error!</strong> {{$message}}</p>
                        </div>
                      @endforeach
                  </div>
              </div>
            </div>
            <div class="uk-grid" data-uk-grid-margin>
              <div class="uk-width-medium-1-1">
                  <div class="parsley-row">
                      <label for="packet_catagory_id">Package Category<span class="req">*</span></label>
                      <select data-md-selectize name="packet_catagory_id" required>
                        <option value="">Package Category</option>
                        @foreach ($category as $data)
                          <option value="{{$data->id}}" {{(old('packet_catagory_id', isset($gallery->packet_catagory_id) ? $gallery->packet_catagory_id : '' ) == $data->id ? 'selected':'') }} >{{$data->nama}}</option>
                        @endforeach
                      </select>
                      @foreach ($errors->get('packet_catagory_id') as $message)
                        <div class="uk-alert uk-alert-danger" data-uk-alert>
                          <a href="" class="uk-alert-close uk-close"></a>
                          <p><strong>Error!</strong> {{$message}}</p>
                        </div>
                      @endforeach
                  </div>
              </div>
            </div>
            <div class="uk-grid" data-uk-grid-margin>
              <div class="uk-width-medium-1-1">
                  <div class="parsley-row">
                      <label for="users_id">User<span class="req">*</span></label>
                      <select data-md-selectize name="users_id" required>
                        <option value="">User</option>
                        @foreach ($user as $data)
                          <option value="{{$data->id}}" {{(old('users_id', isset($gallery->users_id) ? $gallery->users_id : '' ) == $data->id ? 'selected':'') }} >{{$data->nama}}</option>
                        @endforeach
                      </select>
                      @foreach ($errors->get('users_id') as $message)
                        <div class="uk-alert uk-alert-danger" data-uk-alert>
                          <a href="" class="uk-alert-close uk-close"></a>
                          <p><strong>Error!</strong> {{$message}}</p>
                        </div>
                      @endforeach
                  </div>
              </div>
            </div>
            <div class="uk-grid">
                <div class="uk-width-1-1">
                    <div id="my-dropzone"  class="uk-file-upload dropzone">
                        <div class="fallback">
                          <input name="file" type="file" multiple />
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-grid" data-uk-grid-margin>
              <div class="uk-width-medium-1-1">
                  <div class="parsley-row">
                      <label for="quotes">Quotes<span class="req">*</span></label>
                      <textarea class="md-input" name="quotes" cols="10" rows="3" required>{{ old('quotes', isset($gallery->quotes) ? $gallery->quotes : '' ) }}</textarea>
                      @foreach ($errors->get('quotes') as $message)
                        <div class="uk-alert uk-alert-danger" data-uk-alert>
                          <a href="" class="uk-alert-close uk-close"></a>
                          <p><strong>Error!</strong> {{$message}}</p>
                        </div>
                      @endforeach
                  </div>
              </div>
            </div>
            <div class="uk-grid">
                <div class="uk-width-1-1">
                    <button type="submit" id="submit" class="md-btn md-btn-primary">Submit <i class="material-icons">&#xE163;</i></button>
                </div>
            </div>
            @isset($gallery)
              <input type="hidden" name="id" value="{{$gallery->id}}"/>
            @endisset
          </form>
      </div>
  </div>
</div>
@endsection
@section('more-css')
<link rel="stylesheet" type="text/css" media="all" href="{{asset('assets/back/skins/dropify/css/dropify.css')}}">
<link rel="stylesheet" type="text/css" media="all" href="{{asset('assets/back/bower_components/sweetalert/dist/sweetalert.css')}}">
<link rel="stylesheet" type="text/css" media="all" href="{{asset('assets/back/bower_components/dropzone/dropzone.css')}}">
<style>
#parsley-id-5
{
  margin-left: 0;
}
</style>
@endsection
@section('more-js')
<script>
altair_forms.parsley_validation_config();
</script>
<script src="{{asset('assets/back/bower_components/parsleyjs/dist/parsley.min.js')}}"></script>
<script src="{{asset('assets/back/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
<script src="{{asset('assets/back/bower_components/jquery.serializeJSON/jquery.serializejson.min.js')}}"></script>
<script src="{{asset('assets/back/bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js')}}"></script>
<script src="{{asset('assets/back/js/custom/dropify/dist/js/dropify.min.js')}}"></script>
<script src="{{asset('assets/back/bower_components/dropzone/dropzone.js')}}"></script>
<script>
$(function() {
    thisform.init();
}), thisform = {
  init: function()
  {
    thisform.p_init();
    thisform.s_init();
    thisform.m_init();
    thisform.i_init();
    thisform.d_init();
  },
  p_init: function ()
  {
    var i = $("#validate");
    i.parsley().on("form:validated", function() {
        altair_md.update_input(i.find(".md-input-danger"))
    }).on("field:validated", function(i) {
        $(i.$element).hasClass("md-input") && altair_md.update_input($(i.$element))
    }).on("form:submit",function(e){
      $(".masked_input").inputmask('remove');
      if($wrapperThis.files.length>0)
      {
        $wrapperThis.processQueue();
        return false;
      }
    }), window.Parsley.on("field:validate", function() {
        var i = $(this.$element).closest(".md-input-wrapper").siblings(".error_server_side");
        i && i.hide()
    })
  },
  m_init: function() {
    $maskedInput = $(".masked_input"),
    $maskedInput.length && $maskedInput.inputmask();
  },
  s_init: function()
  {
    $maskedInput = $(".selectize"),
    $maskedInput.length && $maskedInput.selectize();
  },
  i_init: function()
  {
    $(".dropify").dropify(), $(".dropify-fr").dropify({
      messages: {
        "default": "Glissez-déposez un fichier ici ou cliquez",
        replace: "Glissez-déposez un fichier ou cliquez pour remplacer",
        remove: "Supprimer",
        error: "Désolé, le fichier trop volumineux"
      }
    })
  },
  d_init : function() {
    Dropzone.options.myDropzone = {
      url: '{{url()->current()}}',
      headers :{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      dataType: 'text',
      autoProcessQueue: false,
      uploadMultiple: true,
      parallelUploads: 20,
      maxFiles: 20,
      acceptedFiles : "image/*",
      maxFilesize: 3,
      method: "post",
      paramName: "url_photo",
      addRemoveLinks: true,
      init: function() {
        $wrapperThis = this;
        @isset($gallery)
          @foreach ($gallery->d_galleries as $image)
            var file = {name: '{{$image->file_nama}}', size: "{{Storage::disk('public')->size($image->url_photo)}}", filename : '{{$image->url_photo}}'};
            $wrapperThis.options.addedfile.call($wrapperThis, file);
            $wrapperThis.options.thumbnail.call($wrapperThis, file, "{{asset(Storage::disk('public')->url($image->url_photo))}}");
            $wrapperThis.emit("complete", file);
            $checkbox = '<div class="default_pic_container"><input type="checkbox" name="status_show[]" {{!$image->status_show ? :"checked"}} value="'+file.name+'"/>Show</div>';
            $element = Dropzone.createElement($checkbox);
            file.previewElement.appendChild($element);
            file.previewElement.classList.add('dz-success');
            file.previewElement.classList.add('dz-complete');
          @endforeach
        @endisset
        this.on("sendingmultiple", function(file, xhr, formData) {
          $("html, body").animate({ scrollTop: 0 }, "slow");
          var form_data = JSON.stringify($("#validate").serializeJSON());
          formData.append('form',form_data);
        });
        this.on("successmultiple", function(files, response) {
          setTimeout(function () {
            swal({
              title: "Success",
              text: $('#nama').val()+ " berhasil didaftarkan",
              type: "success",
              confirmButtonText: "OK"
            },
            function(isConfirm){
              if (isConfirm) {
                @empty($gallery)
                  location.reload();
                @endempty

                @isset($gallery)
                  history.go(-1);
                @endisset
              }
            });
          }, 500);
        });
        this.on("addedfile", function(file) {
          if (this.files.length) {
              var _i, _len;
              for (_i = 0, _len = this.files.length; _i < _len - 1; _i++)
              {
                  if(this.files[_i].name === file.name && this.files[_i].size === file.size && this.files[_i].lastModifiedDate.toString() === file.lastModifiedDate.toString())
                  {
                    swal("Failed", "Please input different file name", "error");
                    this.removeFile(file);
                  }
              }
          }
          $checkbox = '<div class="default_pic_container"><input type="checkbox" name="status_show[]" value="'+file.name+'"/>Show</div>';
          $element = Dropzone.createElement($checkbox);
          file.previewElement.appendChild($element);
        });
        this.on("removedfile",function(file){
          if ($.inArray('dz-success', file.previewElement.classList) != -1)
          {
            $('<input>').attr({
                type: 'hidden',
                id: 'removed',
                name: 'removed[]',
                value: file.filename,
            }).appendTo('form');
          }
        });
        this.on("error", function(response, xhr) {
          var errorsHtml= '';
          $.each($.parseJSON(response.xhr.responseText), function( key, value ) {
            errorsHtml += value[0] + '<br/>';
          });
          return false;
        });
      }
    };
  },
};
</script>
@endsection
