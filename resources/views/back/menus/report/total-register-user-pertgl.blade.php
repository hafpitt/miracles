@extends('back.layouts.default')
@section('title', $title)
@section('bread')
<div id="top_bar">
  <ul id="breadcrumbs">
    <li><a href="#">Home</a></li>
    <li><a href="#">Report</a></li>
    <li><a href="{{url()->current()}}" style="color: grey">{{$title}}</a></li>
  </ul>
</div>
@endsection
@section('content')
<div id="page_content_inner">
  @if(session()->has('message'))
      <div class="uk-alert uk-alert-success" data-uk-alert>
        <a href="" class="uk-alert-close uk-close"></a>
          {{ session()->get('message') }}
      </div>
  @endif
  <div class="md-card uk-margin-bottom uk-margin-remove">
    <div class="md-card-toolbar">
        <div class="md-card-toolbar-actions">
            <i class="md-icon material-icons md-card-fullscreen-activate">&#xE5D0;</i>
            <i class="md-icon material-icons md-card-toggle">&#xE316;</i>
        </div>
        <h3 class="md-card-toolbar-heading-text">
            {{$title}}
        </h3>
    </div>
    <div class="md-card-content">
      <form id="validate" class="uk-form-stacked" method="get" action="{{url()->current()}}">
        <div class="uk-grid" data-uk-grid-margin>
          <div class="uk-width-medium-1-2">
            <div class="parsley-row">
              <div class="uk-input-group">
                  <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
                  <label for="uk_dp_start">Start Date</label>
                  <input class="md-input" name="start_date" type="text" id="uk_dp_start" value="{{ old('start_date', isset($input['start_date']) ?  $input['start_date'] : Carbon\Carbon::now()->toDateString()) }}">
              </div>
            </div>
          </div>
          <div class="uk-width-medium-1-2">
            <div class="parsley-row">
              <div class="uk-input-group">
                  <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
                  <label for="uk_dp_end">End Date</label>
                  <input class="md-input" name="end_date" type="text" id="uk_dp_end" value="{{ old('end_date', isset($input['end_date']) ?  $input['end_date'] : Carbon\Carbon::now()->toDateString()) }}">
              </div>
            </div>
          </div>
        </div>
        <div class="uk-grid">
          <div class="uk-width-1-1">
              <button type="submit" class="md-btn md-btn-primary">Filter <i class="material-icons">&#xE163;</i></button>
          </div>
        </div>
      </form>
    </div>
    @isset($input['start_date'])
    <div class="md-card-content">
      <hr>
      <div class="app">
          <center>
              {!! $chart->html() !!}
          </center>
      </div>
    </div>
    @endisset
  </div>
</div>
@endsection
@section('more-css')
<link rel="stylesheet" type="text/css" media="all" href="{{asset('assets/back/bower_components/sweetalert/dist/sweetalert.css')}}">

@isset($input['start_date'])
{!! Charts::styles() !!}
@endisset

@endsection
@section ('before-more-js')

@isset($input['start_date'])
{!! Charts::scripts() !!}
{!! $chart->script() !!}
@endisset

@endsection
@section('more-js')
<script src="{{asset('assets/back/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
<script src="{{asset('assets/back/bower_components/jquery.serializeJSON/jquery.serializejson.min.js')}}"></script>
<script src="{{asset('assets/back/bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js')}}"></script>
<script type="text/javascript">
$(function() {
    thisform.init();
}), thisform = {
  init: function()
  {
    thisform.d_init();
  },
  d_init : function() {
      var t = $("#uk_dp_start"),
          e = $("#uk_dp_end"),
          i = UIkit.datepicker(t, {
              format: "YYYY-MM-DD",
              maxDate : '{{Carbon\Carbon::now()->addDays(1)->toDateString()}}'
          }),
          n = UIkit.datepicker(e, {
              format: "YYYY-MM-DD",
              maxDate : '{{Carbon\Carbon::now()->addDays(1)->toDateString()}}'
          });
      t.on("change", function() {
          n.options.minDate = t.val(), setTimeout(function() {
              e.focus()
          }, 300)
      }), e.on("change", function() {
          i.options.maxDate = e.val()
      })
  },
}
</script>
@endsection
@push('scripts')
@endpush
