@extends('back.layouts.default')
@section('title', 'Piutang Belum Lunas')
@section('bread')
<div id="top_bar">
  <ul id="breadcrumbs">
    <li><a href="#">Home</a></li>
    <li><a href="#">Report</a></li>
    <li><a href="{{url()->current()}}" style="color: grey">Piutang Belum Lunas</a></li>
  </ul>
</div>
@endsection
@section('content')
<div id="page_content_inner">
  @if(session()->has('message'))
      <div class="uk-alert uk-alert-success" data-uk-alert>
        <a href="" class="uk-alert-close uk-close"></a>
          {{ session()->get('message') }}
      </div>
  @endif
  <div class="md-card uk-margin-bottom uk-margin-remove">
    <div class="md-card-toolbar">
        <div class="md-card-toolbar-actions">
            <i class="md-icon material-icons md-card-fullscreen-activate">&#xE5D0;</i>
            <i class="md-icon material-icons md-card-toggle">&#xE316;</i>
        </div>
        <h3 class="md-card-toolbar-heading-text">
            Piutang Belum Lunas
        </h3>
    </div>
    <div class="md-card-content">
      {!! $dataTable->table(['class' => 'uk-table']) !!}
    </div>
  </div>
</div>
@endsection
@section('more-css')
<link rel="stylesheet" type="text/css" media="all" href="{{asset('assets/back/bower_components/sweetalert/dist/sweetalert.css')}}">
@endsection
@section('more-js')
<script src="{{asset('assets/back/bower_components/datatables/media/js/jquery.dataTables.js')}}"></script>
<script src="{{asset('assets/back/bower_components/datatables-buttons/js/dataTables.buttons.js')}}"></script>
<script src="{{asset('assets/back/js/custom/datatables/buttons.uikit.js')}}"></script>
<script src="{{asset('assets/back/bower_components/jszip/dist/jszip.min.js')}}"></script>
<script src="{{asset('assets/back/bower_components/pdfmake/build/pdfmake.min.js')}}"></script>
<script src="{{asset('assets/back/bower_components/pdfmake/build/vfs_fonts.js')}}"></script>
<script src="{{asset('assets/back/bower_components/datatables-buttons/js/buttons.colVis.js')}}"></script>
<script src="{{asset('assets/back/bower_components/datatables-buttons/js/buttons.html5.js')}}"></script>
<script src="{{asset('assets/back/bower_components/datatables-buttons/js/buttons.print.js')}}"></script>
<script src="{{asset('assets/back/js/custom/datatables/datatables.uikit.min.js')}}"></script>

<script src="{{asset('assets/back/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
<script src="{{asset('assets/back/bower_components/jquery.serializeJSON/jquery.serializejson.min.js')}}"></script>
<script src="{{asset('assets/back/bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js')}}"></script>
@endsection

@push('scripts')
{!! $dataTable->scripts() !!}
@endpush
