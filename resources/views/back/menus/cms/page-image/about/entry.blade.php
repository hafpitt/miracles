@extends('back.layouts.default')
@section('title', 'CMS About')
@section('bread')
<div id="top_bar">
  <ul id="breadcrumbs">
    <li><a href="{{url('back')}}">Home</a></li>
    <li><a href="#">CMS</a></li>
    <li><a href="{{url()->current()}}">About</a></li>
  </ul>
</div>
@endsection
@section('content')
<div id="page_content_inner">
  @if(session()->has('message'))
      <div class="uk-alert uk-alert-success" data-uk-alert>
        <a href="" class="uk-alert-close uk-close"></a>
          {{ session()->get('message') }}
      </div>
  @endif
  <div class="md-card uk-margin-bottom uk-margin-remove">
      <div class="md-card-toolbar">
          <div class="md-card-toolbar-actions">
              <i class="md-icon material-icons md-card-fullscreen-activate">&#xE5D0;</i>
              <i class="md-icon material-icons md-card-toggle">&#xE316;</i>
          </div>
          <h3 class="md-card-toolbar-heading-text">
              About
          </h3>
      </div>
      <div class="md-card-content large-padding">
          <form id="validate" enctype="multipart/form-data" class="uk-form-stacked" method="post" action="{{url()->current()}}">
            {{ csrf_field() }}
            <div class="uk-grid" data-uk-grid-margin>
              <div class="uk-width-medium-1-1">
                  <div class="parsley-row">
                      <label for="input-file-b">Photo<span class="req">*</span></label>
                      <input type="file" id="input-file-b" name="url_photo" class="dropify" data-default-file="{{isset($cms->url_photo) ? Storage::disk('public')->url($cms->url_photo) : ''}}" />
                      @foreach ($errors->get('url_photo') as $message)
                        <div class="uk-alert uk-alert-danger" data-uk-alert>
                          <a href="" class="uk-alert-close uk-close"></a>
                          <p><strong>Error!</strong> {{$message}}</p>
                        </div>
                      @endforeach
                  </div>
              </div>
            </div>
            <div class="uk-grid">
                <div class="uk-width-1-1">
                    <button type="submit" id="submit" class="md-btn md-btn-primary">Submit <i class="material-icons">&#xE163;</i></button>
                </div>
            </div>
            @isset($cms)
              <input type="hidden" name="id" value="{{$cms->id}}"/>
            @endisset
          </form>
      </div>
  </div>
</div>
@endsection
@section('more-css')
<link rel="stylesheet" type="text/css" media="all" href="{{asset('assets/back/skins/dropify/css/dropify.css')}}">
<link rel="stylesheet" type="text/css" media="all" href="{{asset('assets/back/bower_components/sweetalert/dist/sweetalert.css')}}">
<style>
#parsley-id-5
{
  margin-left: 0;
}
</style>
@endsection
@section('more-js')
<script src="{{asset('assets/back/js/custom/dropify/dist/js/dropify.min.js')}}"></script>
<script src="{{asset('assets/back/bower_components/handlebars/handlebars.min.js')}}"></script>
<script>
altair_forms.parsley_validation_config();
</script>
<script src="{{asset('assets/back/bower_components/parsleyjs/dist/parsley.min.js')}}"></script>
<script src="{{asset('assets/back/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
<script src="{{asset('assets/back/bower_components/jquery.serializeJSON/jquery.serializejson.min.js')}}"></script>
<script src="{{asset('assets/back/bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js')}}"></script>
<script>
$(function() {
    thisform.init();
}), thisform = {
  init: function()
  {
    thisform.p_init();
    thisform.m_init();
    thisform.s_init();
    thisform.i_init();
  },
  p_init: function ()
  {
    var i = $("#validate");
    i.parsley().on("form:validated", function() {
        altair_md.update_input(i.find(".md-input-danger"))
    }).on("field:validated", function(i) {
        $(i.$element).hasClass("md-input") && altair_md.update_input($(i.$element))
    }).on("form:submit",function(){
      $(".masked_input").inputmask('remove');
    }), window.Parsley.on("field:validate", function() {
        var i = $(this.$element).closest(".md-input-wrapper").siblings(".error_server_side");
        i && i.hide()
    })
  },
  m_init: function() {
    $maskedInput = $(".masked_input"),
    $maskedInput.length && $maskedInput.inputmask();
  },
  s_init: function()
  {
    $maskedInput = $(".selectize"),
    $maskedInput.length && $maskedInput.selectize();
  },
  i_init: function()
  {
    $maskedInput = $(".dropify"),
    $maskedInput.length && $maskedInput.dropify({
      messages: {
        "default": "Glissez-déposez un fichier ici ou cliquez",
        replace: "Glissez-déposez un fichier ou cliquez pour remplacer",
        remove: "Supprimer",
        error: "Désolé, le fichier trop volumineux"
      }
    });
  },

};
</script>
@endsection
