@extends('back.layouts.default')
@section('title', 'Broadcast')
@section('bread')
<div id="top_bar">
  <ul id="breadcrumbs">
    <li><a href="{{url('back')}}">Home</a></li>
    <li><a href="{{url()->current()}}" style="color: grey">Broadcast</a></li>
  </ul>
</div>
@endsection
@section('content')
<div id="page_content_inner">
  @if(session()->has('message'))
      <div class="uk-alert uk-alert-success" data-uk-alert>
        <a href="" class="uk-alert-close uk-close"></a>
          {{ session()->get('message') }}
      </div>
  @endif
  <div class="md-card uk-margin-bottom uk-margin-remove">
      <div class="md-card-toolbar">
          <div class="md-card-toolbar-actions">
              <i class="md-icon material-icons md-card-fullscreen-activate">&#xE5D0;</i>
              <i class="md-icon material-icons md-card-toggle">&#xE316;</i>
          </div>
          <h3 class="md-card-toolbar-heading-text">
              Broadcast Subscriber
          </h3>
      </div>
      <div class="md-card-content large-padding">
          <form id="validate" class="uk-form-stacked" method="post" action="{{url()->current()}}">
            {{ csrf_field() }}
            <div class="uk-grid" data-uk-grid-margin>
              <div class="uk-width-medium-1-1">
                  <div class="parsley-row">
                      <label for="subject">Subject<span class="req">*</span></label>
                      <input type="text" name="subject" id="nama" required class="md-input" data-parsley-minlength="3" value="{{ old('subject') }}" />
                      @foreach ($errors->get('subject') as $message)
                        <div class="uk-alert uk-alert-danger" data-uk-alert>
                          <a href="" class="uk-alert-close uk-close"></a>
                          <p><strong>Error!</strong> {{$message}}</p>
                        </div>
                      @endforeach
                  </div>
              </div>
            </div>
            <div class="uk-grid" data-uk-grid-margin>
              <div class="uk-width-medium-1-1">
                  <div class="parsley-row">
                      <label for="content">Content<span class="req">*</span></label>
                      <textarea class="md-input" name="content" cols="10" rows="5" required>{{ old('content')}}</textarea>
                      @foreach ($errors->get('content') as $message)
                        <div class="uk-alert uk-alert-danger" data-uk-alert>
                          <a href="" class="uk-alert-close uk-close"></a>
                          <p><strong>Error!</strong> {{$message}}</p>
                        </div>
                      @endforeach
                  </div>
              </div>
            </div>
            <div class="uk-grid">
                <div class="uk-width-1-1">
                    <button type="submit" id="submit" class="md-btn md-btn-primary">Send <i class="material-icons">&#xE163;</i></button>
                </div>
            </div>
          </form>
      </div>
  </div>
</div>
@endsection
@section('more-css')
<link rel="stylesheet" type="text/css" media="all" href="{{asset('assets/back/bower_components/sweetalert/dist/sweetalert.css')}}">
<style>
#parsley-id-5
{
  margin-left: 0;
}
</style>
@endsection
@section('more-js')
<script>
altair_forms.parsley_validation_config();
</script>
<script src="{{asset('assets/back/bower_components/parsleyjs/dist/parsley.min.js')}}"></script>
<script src="{{asset('assets/back/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
<script src="{{asset('assets/back/bower_components/jquery.serializeJSON/jquery.serializejson.min.js')}}"></script>
<script src="{{asset('assets/back/bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js')}}"></script>
<script>
$(function() {
    thisform.init();
}), thisform = {
  init: function()
  {
    thisform.p_init();
    thisform.m_init();
  },
  p_init: function ()
  {
    var i = $("#validate");
    i.parsley().on("form:validated", function() {
        altair_md.update_input(i.find(".md-input-danger"))
    }).on("field:validated", function(i) {
        $(i.$element).hasClass("md-input") && altair_md.update_input($(i.$element))
    }).on("form:submit",function(){
      $(".masked_input").inputmask('remove');
    }), window.Parsley.on("field:validate", function() {
        var i = $(this.$element).closest(".md-input-wrapper").siblings(".error_server_side");
        i && i.hide()
    })
  },
  m_init: function() {
    $maskedInput = $(".masked_input"),
    $maskedInput.length && $maskedInput.inputmask();
  },
};
</script>
@endsection
