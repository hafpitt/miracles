@extends('back.layouts.auth')
@section('title', 'Login')
@section('content')
<div class="login_page_wrapper">
    <div class="md-card" id="login_card">
        <div class="md-card-content large-padding" id="login_form">
            <div class="login_heading">
                <div class="user_avatar"></div>
            </div>
            <form id="form_validation" class="uk-form-stacked" method="post" action="{{url()->current()}}">
              {{ csrf_field() }}
                @foreach ($errors->get('status') as $message)
                  <div class="uk-alert uk-alert-danger" data-uk-alert>
                    <a href="" class="uk-alert-close uk-close"></a>
                    <p><strong>Error!</strong> {{$message}}</p>
                  </div>
                @endforeach
                <div class="uk-form-row">
                    <div class="parsley-row">
                        <label for="email">Email<span class="req">*</span></label>
                        <input type="email" name="email" value="{{ old('email') }}" id="email" data-parsley-trigger="change" required  class="md-input" />
                    </div>
                </div>
                <div class="uk-form-row">
                    <div class="parsley-row">
                        <label for="password">Password<span class="req">*</span></label>
                        <input type="password" name="password" id="password" data-parsley-minlength="6" required class="md-input" />
                    </div>
                </div>
                <div class="uk-margin-medium-top">
                    <button type="submit" class="md-btn md-btn-primary md-btn-block md-btn-large">Sign In</button>
                </div>
                <!-- <div class="uk-margin-top">
                    <span class="icheck-inline">
                        <input type="checkbox" name="remember" id="remember" data-md-icheck />
                        <label for="remember" class="inline-label">Stay signed in</label>
                    </span>
                </div> -->
            </form>
        </div>
    </div>
</div>
@endsection
@section('more-css')
<link rel="stylesheet" type="text/css" media="all" href="{{asset('/assets/back/bower_components/sweetalert/dist/sweetalert.css')}}">
<link rel="stylesheet" type="text/css" media="all" href="{{asset('/assets/back/css/login_page.min.css')}}" />
@endsection
@section('more-js')
<script>
altair_forms.parsley_validation_config();
</script>
<script src="{{asset('/assets/back/bower_components/parsleyjs/dist/parsley.min.js')}}"></script>
<script src="{{asset('/assets/back/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
<script src="{{asset('/assets/back/bower_components/jquery.serializeJSON/jquery.serializejson.min.js')}}"></script>

@endsection
