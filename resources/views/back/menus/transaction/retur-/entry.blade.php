@extends('back.layouts.default')
@section('title', 'Retur Barang')
@section('bread')
<div id="top_bar">
  <ul id="breadcrumbs">
    <li><a href="{{url('back')}}">Home</a></li>
    <li><a href="#">Transaction</a></li>
    <li><a href="#">Retur</a></li>
    <li><a href="{{url()->current()}}" style="color: grey">Entry</a></li>
  </ul>
</div>
@endsection
@section('content')
<div id="page_content_inner">
  @if(session()->has('message'))
      <div class="uk-alert uk-alert-success" data-uk-alert>
        <a href="" class="uk-alert-close uk-close"></a>
          {{ session()->get('message') }}
      </div>
  @endif
  @if(session()->has('error'))
      <div class="uk-alert uk-alert-danger" data-uk-alert>
        <a href="" class="uk-alert-close uk-close"></a>
          {{ session()->get('error') }}
      </div>
  @endif
  <form id="validate" enctype="multipart/form-data" class="uk-form-stacked" method="post" action="{{url()->current()}}">
  <div class="md-card uk-margin-bottom uk-margin-remove">
      <div class="md-card-toolbar">
          <div class="md-card-toolbar-actions">
              <i class="md-icon material-icons md-card-fullscreen-activate">&#xE5D0;</i>
              <i class="md-icon material-icons md-card-toggle">&#xE316;</i>
          </div>
          <h3 class="md-card-toolbar-heading-text">
            Retur Barang
          </h3>
      </div>
      <div class="md-card-content large-padding">
      {{ csrf_field() }}
      <div class="uk-grid" data-uk-grid-margin>
        <div class="uk-width-medium-1-1">
          <div class="parsley-row">
            <label for="keterangan">Tgl<span class="req"></span></label>
            <input class="md-input label-fixed" name="tgl" disabled value="{{ old('tgl', isset($piutang->tgl) ? $piutang->tgl : Carbon\Carbon::now()->toDateString() ) }}"/>
            @foreach ($errors->get('tgl') as $message)
              <div class="uk-alert uk-alert-danger" data-uk-alert>
                <a href="" class="uk-alert-close uk-close"></a>
                <p><strong>Error!</strong> {{$message}}</p>
              </div>
            @endforeach
          </div>
        </div>
      </div>
      <div class="uk-grid" data-uk-grid-margin>
        <div class="uk-width-medium-1-1">
          <div class="parsley-row">
            <div class="uk-input-group">
              <label>No Invoice<span class="req"></span></label>
              <input class="md-input label-fixed" id="no_invoice" readonly/>
              <input class="md-input label-fixed" name="ref_no" id="ref_no" type="hidden"/>
              @foreach ($errors->get('ref_no') as $message)
                <div class="uk-alert uk-alert-danger" data-uk-alert>
                  <a href="" class="uk-alert-close uk-close"></a>
                  <p><strong>Error!</strong> {{$message}}</p>
                </div>
              @endforeach
              <span class="uk-input-group-addon"><button class="md-btn" type="button" data-uk-modal="{target:'#search_invoice'}">Cari Nota</button></span>
            </div>
            <div id="search_invoice" class="uk-modal">
              <div class="uk-modal-dialog">
                <button type="button" class="uk-modal-close uk-close"></button>
                <div class="uk-overflow-container">
                  <table id="list_invoice" class="uk-table" cellspacing="0" width="100%">
                    <thead>
                      <tr>
                        <th>ID</th>
                        <th>No Invoice</th>
                        <th>Tanggal</th>
                        <th>Kurir</th>
                        <th>No Resi</th>
                        <th>Action</th>
                      </tr>
                   </thead>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="uk-grid" data-uk-grid-margin>
        <div class="uk-width-medium-1-1">
          <div class="parsley-row">
            <label for="keterangan">Keterangan<span class="req"></span></label>
            <textarea class="md-input label-fixed" name="keterangan" cols="10" rows="3">{{ old('keterangan', isset($piutang->keterangan) ? $piutang->keterangan : '' ) }}</textarea>
            @foreach ($errors->get('keterangan') as $message)
              <div class="uk-alert uk-alert-danger" data-uk-alert>
                <a href="" class="uk-alert-close uk-close"></a>
                <p><strong>Error!</strong> {{$message}}</p>
              </div>
            @endforeach
          </div>
        </div>
      </div>
      @isset($piutang)
        <input type="hidden" name="id" value="{{$piutang->id}}"/>
      @endisset
      </div>
  </div>

    <div class="md-card uk-margin-bottom uk-margin-remove">
      <div class="md-card-toolbar">
        <div class="md-card-toolbar-actions">
            <i class="md-icon material-icons md-card-fullscreen-activate">&#xE5D0;</i>
            <i class="md-icon material-icons md-card-toggle">&#xE316;</i>
        </div>
        <h3 class="md-card-toolbar-heading-text">
            Invoice
        </h3>
      </div>
        <div class="md-card-content">
          <div class="dt_colVis_buttons"></div>
          <table id="list_barang" class="uk-table" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th>ID Cart</th>
                <th>Qty</th>
                <th>ID Barang</th>
                <th>Nama</th>
                <th>Warna</th>
                <th>Qty Retur</th>
              </tr>
            </thead>
          </table>
          <div class="uk-grid">
            <div class="uk-width-1-1">
                <button type="submit" id="submit" class="md-btn md-btn-primary">Submit <i class="material-icons">&#xE163;</i></button>
            </div>
          </div>
        </div>
      </div>
  </form>
</div>
@endsection
@section('more-css')
<link rel="stylesheet" type="text/css" media="all" href="{{asset('assets/back/skins/dropify/css/dropify.css')}}">
<link rel="stylesheet" type="text/css" media="all" href="{{asset('assets/back/bower_components/sweetalert/dist/sweetalert.css')}}">
<style>
#parsley-id-5
{
  margin-left: 0;
}
</style>
@endsection
@section('more-js')
<script>
altair_forms.parsley_validation_config();
</script>
<script src="{{asset('assets/back/bower_components/parsleyjs/dist/parsley.min.js')}}"></script>
<script src="{{asset('assets/back/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
<script src="{{asset('assets/back/bower_components/jquery.serializeJSON/jquery.serializejson.min.js')}}"></script>
<script src="{{asset('assets/back/bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js')}}"></script>
<script src="{{asset('assets/back/js/custom/dropify/dist/js/dropify.min.js')}}"></script>
<script src="{{asset('assets/back/bower_components/datatables/media/js/jquery.dataTables.js')}}"></script>
<script src="{{asset('assets/back/bower_components/datatables-buttons/js/dataTables.buttons.js')}}"></script>
<script src="{{asset('assets/back/js/custom/datatables/buttons.uikit.js')}}"></script>
<script src="{{asset('assets/back/bower_components/jszip/dist/jszip.min.js')}}"></script>
<script src="{{asset('assets/back/bower_components/pdfmake/build/pdfmake.min.js')}}"></script>
<script src="{{asset('assets/back/bower_components/pdfmake/build/vfs_fonts.js')}}"></script>
<script src="{{asset('assets/back/bower_components/datatables-buttons/js/buttons.colVis.js')}}"></script>
<script src="{{asset('assets/back/bower_components/datatables-buttons/js/buttons.html5.js')}}"></script>
<script src="{{asset('assets/back/bower_components/datatables-buttons/js/buttons.print.js')}}"></script>
<script src="{{asset('assets/back/js/custom/datatables/datatables.uikit.min.js')}}"></script>
<script>
$(function() {
    thisform.init();
}), thisform = {
  init: function()
  {
    thisform.p_init();
    thisform.m_init();
    thisform.s_init();
    thisform.i_init();
    // thisform.barang_init();
    thisform.invoice_init();
  },
  invoice_init: function() {
    var t = $("#list_invoice"),
        a = t.prev(".dt_colVis_buttons");
    if (t.length) {
      var e = t.DataTable({
          processing: true,
          serverSide: true,
          ajax: {
            url: '{{url()->current()."/invoice"}}',
            type: 'POST',
            data: {_method: 'options'},
          },
          "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
          "drawCallback": function( settings ) {
            thisform.brg_choose();
          },
      });
    }
  },
  p_init: function ()
  {
    var i = $("#validate");
    i.parsley().on("form:validated", function() {
        altair_md.update_input(i.find(".md-input-danger"))
    }).on("field:validated", function(i) {
        $(i.$element).hasClass("md-input") && altair_md.update_input($(i.$element));
        $table.search( '' ).columns().search('' ).draw();
    }).on("form:submit",function(){
      $(".masked_input").inputmask('remove');
    }), window.Parsley.on("field:validate", function() {
        var i = $(this.$element).closest(".md-input-wrapper").siblings(".error_server_side");
        i && i.hide();
    })
  },
  brg_choose: function() {
    $(".ref-invoice").on('click',function(){
      // alert($(this).attr('data-invoice'));
      $('#no_invoice').val($(this).attr('data-invoice'));
      $('#ref_no').val($(this).attr('data-ref'));
      thisform.barang_init();
      $('.uk-modal-close').click();
    });
  },
  barang_init: function() {
    var t = $("#list_barang"),
        a = t.prev(".dt_colVis_buttons"),
        ref_id = $('#ref_no').val();
    t.dataTable().fnClearTable();
    t.dataTable().fnDraw();
    t.dataTable().fnDestroy();
    if (t.length) {
      var e = t.DataTable({
          processing: true,
          serverSide: true,
          ajax: {
            url: '{{url()->current()."/barang"}}',
            type: 'POST',
            data: {_method: 'options','ref_id':ref_id},
          },
          "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
          "drawCallback": function( settings ) {
            thisform.s_init();
          },
      });
    }
  },
  m_init: function() {
    $maskedInput = $(".masked_input"),
    $maskedInput.length && $maskedInput.inputmask();
  },
  s_init: function()
  {
    $maskedInput = $(".selectize"),
    $maskedInput.length && $maskedInput.selectize();
  },
  i_init: function()
  {
    $(".dropify").dropify(), $(".dropify-fr").dropify({
      messages: {
        "default": "Glissez-déposez un fichier ici ou cliquez",
        replace: "Glissez-déposez un fichier ou cliquez pour remplacer",
        remove: "Supprimer",
        error: "Désolé, le fichier trop volumineux"
      }
    })
  },
};
</script>
@endsection
