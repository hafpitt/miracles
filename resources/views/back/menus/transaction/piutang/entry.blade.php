@extends('back.layouts.default')
@section('title', 'Pelunasan Piutang')
@section('bread')
<div id="top_bar">
  <ul id="breadcrumbs">
    <li><a href="{{url('back')}}">Home</a></li>
    <li><a href="#">Transaction</a></li>
    <li><a href="#">Piutang</a></li>
    <li><a href="{{url()->current()}}" style="color: grey">Entry</a></li>
  </ul>
</div>
@endsection
@section('content')
<div id="page_content_inner">
  @if(session()->has('message'))
      <div class="uk-alert uk-alert-success" data-uk-alert>
        <a href="" class="uk-alert-close uk-close"></a>
          {{ session()->get('message') }}
      </div>
  @endif
  @if(session()->has('error'))
      <div class="uk-alert uk-alert-danger" data-uk-alert>
        <a href="" class="uk-alert-close uk-close"></a>
          {{ session()->get('error') }}
      </div>
  @endif
  <form id="validate" enctype="multipart/form-data" class="uk-form-stacked" method="post" action="{{url()->current()}}">
  <div class="md-card uk-margin-bottom uk-margin-remove">
      <div class="md-card-toolbar">
          <div class="md-card-toolbar-actions">
              <i class="md-icon material-icons md-card-fullscreen-activate">&#xE5D0;</i>
              <i class="md-icon material-icons md-card-toggle">&#xE316;</i>
          </div>
          <h3 class="md-card-toolbar-heading-text">
            Pelunasan Piutang
          </h3>
      </div>
      <div class="md-card-content large-padding">
      {{ csrf_field() }}
      <div class="uk-grid" data-uk-grid-margin>
        <div class="uk-width-medium-1-1">
          <div class="parsley-row">
            <label for="keterangan">Tgl<span class="req"></span></label>
            <input class="md-input label-fixed" name="tgl" disabled value="{{ old('tgl', isset($piutang->tgl) ? $piutang->tgl : Carbon\Carbon::now()->toDateString() ) }}"/>
            @foreach ($errors->get('tgl') as $message)
              <div class="uk-alert uk-alert-danger" data-uk-alert>
                <a href="" class="uk-alert-close uk-close"></a>
                <p><strong>Error!</strong> {{$message}}</p>
              </div>
            @endforeach
          </div>
        </div>
      </div>
      <div class="uk-grid" data-uk-grid-margin>
        <div class="uk-width-medium-1-1">
          <div class="parsley-row">
            <label for="keterangan">Keterangan<span class="req"></span></label>
            <textarea class="md-input label-fixed" name="keterangan" cols="10" rows="3">{{ old('keterangan', isset($piutang->keterangan) ? $piutang->keterangan : '' ) }}</textarea>
            @foreach ($errors->get('keterangan') as $message)
              <div class="uk-alert uk-alert-danger" data-uk-alert>
                <a href="" class="uk-alert-close uk-close"></a>
                <p><strong>Error!</strong> {{$message}}</p>
              </div>
            @endforeach
          </div>
        </div>
      </div>
      @isset($piutang)
        <input type="hidden" name="id" value="{{$piutang->id}}"/>
      @endisset
      </div>
  </div>

    <div class="md-card uk-margin-bottom uk-margin-remove">
      <div class="md-card-toolbar">
        <div class="md-card-toolbar-actions">
            <i class="md-icon material-icons md-card-fullscreen-activate">&#xE5D0;</i>
            <i class="md-icon material-icons md-card-toggle">&#xE316;</i>
        </div>
        <h3 class="md-card-toolbar-heading-text">
            Invoice
        </h3>
      </div>
        <div class="md-card-content">
          <div class="dt_colVis_buttons"></div>
          <table id="tabledata" class="uk-table" cellspacing="0" width="100%">
            <thead>
              <tr>
               <th>No Invoice</th>
               <th>GrandTotal</th>
               <th>Terbayar</th>
               <th>Sisabayar</th>
               <th>Bukti</th>
               <th>Dibayar</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($invoice as $value)
              @php
                $nominal=0;
                if (isset($piutang))
                {
                  foreach ($piutang->d_piutangs as $edit)
                  {
                    if($value->id==$edit->id_h_transaksi)
                    {
                      $nominal=$edit->dibayar;
                    }
                  }
                }
              @endphp
              <tr>
                <td>{{$value->no_invoice}}</td>
                <td>{{number_format($value->grand_total)}}</td>
                <td>{{number_format($value->terbayar)}}</td>
                <td>{{number_format($value->sisa_bayar)}}</td>
                <td><a target="blank" href="@isset($value->url_bukti){{asset(Storage::disk('public')->url($value->url_bukti))}}@endisset" alt="">Lihat Bukti</a></td>
                <td><input type="text" name="inv[{{$value->id}}][dibayar]" required class="md-input masked_input" data-inputmask="'alias': 'currency', 'groupSeparator': ',', 'autoGroup': true, 'digits': 2, 'digitsOptional': false, 'prefix': 'IDR ', 'placeholder': '0', 'min':'0', 'max':'{{$value->sisa_bayar}}'" value="{{$nominal}}"></td>
              </tr>
              @endforeach
            </tbody>
          </table>

          <div class="uk-grid">
            <div class="uk-width-1-1">
                <button type="submit" id="submit" class="md-btn md-btn-primary">Submit <i class="material-icons">&#xE163;</i></button>
            </div>
          </div>
        </div>
      </div>
  </form>
</div>
@endsection
@section('more-css')
<link rel="stylesheet" type="text/css" media="all" href="{{asset('assets/back/skins/dropify/css/dropify.css')}}">
<link rel="stylesheet" type="text/css" media="all" href="{{asset('assets/back/bower_components/sweetalert/dist/sweetalert.css')}}">
<style>
#parsley-id-5
{
  margin-left: 0;
}
</style>
@endsection
@section('more-js')
<script>
altair_forms.parsley_validation_config();
</script>
<script src="{{asset('assets/back/bower_components/parsleyjs/dist/parsley.min.js')}}"></script>
<script src="{{asset('assets/back/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
<script src="{{asset('assets/back/bower_components/jquery.serializeJSON/jquery.serializejson.min.js')}}"></script>
<script src="{{asset('assets/back/bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js')}}"></script>
<script src="{{asset('assets/back/js/custom/dropify/dist/js/dropify.min.js')}}"></script>
<script src="{{asset('assets/back/bower_components/datatables/media/js/jquery.dataTables.js')}}"></script>
<script src="{{asset('assets/back/bower_components/datatables-buttons/js/dataTables.buttons.js')}}"></script>
<script src="{{asset('assets/back/js/custom/datatables/buttons.uikit.js')}}"></script>
<script src="{{asset('assets/back/bower_components/jszip/dist/jszip.min.js')}}"></script>
<script src="{{asset('assets/back/bower_components/pdfmake/build/pdfmake.min.js')}}"></script>
<script src="{{asset('assets/back/bower_components/pdfmake/build/vfs_fonts.js')}}"></script>
<script src="{{asset('assets/back/bower_components/datatables-buttons/js/buttons.colVis.js')}}"></script>
<script src="{{asset('assets/back/bower_components/datatables-buttons/js/buttons.html5.js')}}"></script>
<script src="{{asset('assets/back/bower_components/datatables-buttons/js/buttons.print.js')}}"></script>
<script src="{{asset('assets/back/js/custom/datatables/datatables.uikit.min.js')}}"></script>
<script>
$(function() {
    thisform.init();
    $table = $("#tabledata").DataTable({paging: false});
}), thisform = {
  init: function()
  {
    thisform.p_init();
    thisform.m_init();
    thisform.s_init();
    thisform.i_init();
  },
  p_init: function ()
  {
    var i = $("#validate");
    i.parsley().on("form:validated", function() {
        altair_md.update_input(i.find(".md-input-danger"))
    }).on("field:validated", function(i) {
        $(i.$element).hasClass("md-input") && altair_md.update_input($(i.$element));
        $table.search( '' ).columns().search('' ).draw();
    }).on("form:submit",function(){
      $(".masked_input").inputmask('remove');
    }), window.Parsley.on("field:validate", function() {
        var i = $(this.$element).closest(".md-input-wrapper").siblings(".error_server_side");
        i && i.hide();
    })
  },
  m_init: function() {
    $maskedInput = $(".masked_input"),
    $maskedInput.length && $maskedInput.inputmask();
  },
  s_init: function()
  {
    $maskedInput = $(".selectize"),
    $maskedInput.length && $maskedInput.selectize();
  },
  i_init: function()
  {
    $(".dropify").dropify(), $(".dropify-fr").dropify({
      messages: {
        "default": "Glissez-déposez un fichier ici ou cliquez",
        replace: "Glissez-déposez un fichier ou cliquez pour remplacer",
        remove: "Supprimer",
        error: "Désolé, le fichier trop volumineux"
      }
    })
  },
};
</script>
@endsection
