@extends('back.layouts.default')
@section('title', 'Pengganti Retur')
@section('bread')
<div id="top_bar">
  <ul id="breadcrumbs">
    <li><a href="{{url('back')}}">Home</a></li>
    <li><a href="#">Transaksi</a></li>
    <li><a href="#">Retur</a></li>
    <li><a href="#">Pengganti Retur</a></li>
    <li><a href="{{url()->current()}}" style="color: grey">Entry</a></li>
  </ul>
</div>
@endsection
@section('content')
<div id="page_content_inner">
  @if(session()->has('message'))
      <div class="uk-alert uk-alert-success" data-uk-alert>
        <a href="" class="uk-alert-close uk-close"></a>
          {{ session()->get('message') }}
      </div>
  @endif

<form id="validate" enctype="multipart/form-data" class="uk-form-stacked" method="post" action="{{url()->current()}}">
  <div class="md-card uk-margin-bottom uk-margin-remove">
    <div class="md-card-toolbar">
      <div class="md-card-toolbar-actions">
          <i class="md-icon material-icons md-card-fullscreen-activate">&#xE5D0;</i>
          <i class="md-icon material-icons md-card-toggle">&#xE316;</i>
      </div>
      <h3 class="md-card-toolbar-heading-text">
          Detail Retur
      </h3>
    </div>
    {{ csrf_field() }}
    <div class="md-card-content">
      <table id="tabledata" class="uk-table" cellspacing="0" width="100%">
        <thead>
          <tr>
           <th>Nama barang</th>
           <th>Qty</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($barangRetur->d_transaksis as $data)
            @php
              $edit=$data->cart->qty*-1;
            @endphp
          <tr>
            <td>
              <div class="parsley-row">{{$data->cart->barang->nama}}</div>
            </td>
            <td>
              <div class="parsley-row">{{$edit}}</div>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>


    </div>
  </div>
  <div class="md-card uk-margin-bottom uk-margin-remove">
    <div class="md-card-content large-padding">
      <div class="uk-grid" data-uk-grid-margin>
        <div class="uk-width-medium-2-10">
          <span class="uk-display-block uk-margin-small-top uk-text-large">Pengganti</span>
        </div>
        <div class="uk-width-medium-8-10">
          <div class="uk-overflow-container">
            <table class="uk-table">
              <thead>
                <tr>
                  <th class="uk-width-4-10 uk-text-nowrap"></th>
                  <th class="uk-width-4-10 uk-text-nowrap"></th>
                </tr>
              </thead>
              <tbody>
                <tr class="form_section">
                  <!-- <td class="uk-width-4-10">
                    <input type="text" name="nama" class="md-input md-input-width-medium" placeholder="Barang" value=""/>
                  </td> -->

                </tr>
                <tr>
                  <td colspan="3" class="uk-padding-remove">
                    <table class="uk-table" data-dynamic-fields="field_template_a"></table>
                  </td>
                </tr>
              </tbody>
            </table>
            <script id="field_template_a" type="text/x-handlebars-template">
              <tr class="form_section">
                <!-- <td class="uk-width-4-10">
                  <input type="text" name="barang" class="md-input md-input-width-medium" placeholder="Inputkan Barang Pengganti" />
                </td>
                <td class="uk-width-4-10">
                  <input type="text" name="qty" class="md-input md-input-width-medium" placeholder="Qty" />
                </td> -->
                <td class="uk-width-3-10">
                  <select data-md-selectize name="barang[][nama]" required>
                    <option value="">Inputkan barang pengganti</option>
                    @foreach ($loadBarang as $data)
                      <option value="{{$data->id}}" >{{$data->nama}}</option>
                    @endforeach
                  </select>
                </td>
                <td class="uk-width-3-10">
                  <input type="text" name="barang[][qty]" class="md-input md-input-width-medium" placeholder="Qty Pengganti" />
                </td>



                <td class="uk-width-2-10 uk-text-right uk-text-middle">
                  <a href="javascript:void(0);" class="btnSectionClone new_color"><i class="material-icons md-24">&#xE145;</i></a>
                </td>
              </tr>
            </script>
          </div>
        </div>
      </div>
      <div class="uk-grid">
        <div class="uk-width-1-1">
          <button type="submit" id="submit" class="md-btn md-btn-primary">Submit <i class="material-icons">&#xE163;</i></button>
        </div>
      </div>
    </div>
  </div>

  </form>
</div>
@endsection
@section('more-css')
<link rel="stylesheet" type="text/css" media="all" href="{{asset('assets/back/skins/dropify/css/dropify.css')}}">
<link rel="stylesheet" type="text/css" media="all" href="{{asset('assets/back/bower_components/sweetalert/dist/sweetalert.css')}}">
<link rel="stylesheet" type="text/css" media="all" href="{{asset('assets/back/bower_components/dropzone/dropzone.css')}}">
<link rel="stylesheet" type="text/css" media="all" href="{{asset('assets/back/bower_components/HoldOn/src/download/HoldOn.min.css')}}">
<style>
#parsley-id-5
{
  margin-left: 0;
}
</style>
@endsection
@section('more-js')
<script>
altair_forms.parsley_validation_config();
</script>
<script src="{{asset('assets/back/bower_components/parsleyjs/dist/parsley.min.js')}}"></script>
<script src="{{asset('assets/back/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
<script src="{{asset('assets/back/bower_components/jquery.serializeJSON/jquery.serializejson.min.js')}}"></script>
<script src="{{asset('assets/back/bower_components/handlebars/handlebars.min.js')}}"></script>
<script src="{{asset('assets/back/js/custom/handlebars_helpers.min.js')}}"></script>
<script src="{{asset('assets/back/bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js')}}"></script>
<script src="{{asset('assets/back/js/custom/dropify/dist/js/dropify.min.js')}}"></script>
<script src="{{asset('assets/back/bower_components/dropzone/dropzone.js')}}"></script>
<script src="{{asset('assets/back/bower_components/jscolor/jscolor.js')}}"></script>
<script src="{{asset('assets/back/bower_components/HoldOn/src/download/HoldOn.min.js')}}"></script>
<script>
$(function() {
    thisform.init();
}), thisform = {
  init: function()
  {
    thisform.p_init();
    thisform.s_init();
    thisform.m_init();
    thisform.i_init();
    thisform.r_init();
    thisform.dynamic_fields();
    // $("#validate").submit( function(eventObj) {
    //   return true;
    // });
  },
  p_init: function ()
  {
    var i = $("#validate");
    i.parsley().on("form:validated", function() {
        altair_md.update_input(i.find(".md-input-danger"))
    }).on("field:validated", function(i) {
        $(i.$element).hasClass("md-input") && altair_md.update_input($(i.$element))
    }).on("form:submit",function(e){
      // HoldOn.open({theme:"sk-cube-grid"});
      $form_data = JSON.stringify($("#validate").serializeJSON());
      $('<input />').attr('type', 'hidden')
          .attr('name', "jsonstring")
          .attr('value', $form_data)
          .appendTo('#validate');
      $(".masked_input").inputmask('remove');
      // HoldOn.close();
    }), window.Parsley.on("field:validate", function() {
        var i = $(this.$element).closest(".md-input-wrapper").siblings(".error_server_side");
        i && i.hide()
    })
  },
  dynamic_fields: function() {
      function e(e, i, a) {
          var t = $("#" + i).html(),
              n = Handlebars.compile(t),
              s = n({
                  index: a ? a : 0,
                  counter: a ? "__" + a : "__0"
              });
          e.append(s), altair_md.inputs(e), altair_md.checkbox_radio(e.find("[data-md-icheck]")), altair_forms.switches(e), altair_forms.select_elements(e);
          var input = document.createElement('input');
          input.setAttribute("class", "md-input md-input-width-medium");
          input.setAttribute("name", "warna[][warna]");
          input.setAttribute("type", "text");
          var picker = new jscolor(input);
          e.find('.incolor').replaceWith(input);
      }
      $("[data-dynamic-fields]").each(function() {
          var i = $(this).attr("dynamic-fields-counter", 0),
              a = i.data("dynamicFields");
          e(i, a), i.on("click", ".btnSectionClone", function(t) {
              t.preventDefault();
              i.find(".btnSectionClone").replaceWith('<a href="#" class="btnSectionRemove"><i class="material-icons md-24">&#xE872;</i></a>');
              var n = parseInt(i.attr("dynamic-fields-counter")) + 1;
              i.attr("dynamic-fields-counter", n), e(i, a, n)
          }).on("click", ".btnSectionRemove", function(e) {
              e.preventDefault();
              var i = $(this);
              i.closest(".form_section").next(".form_hr").remove().end().remove()
          })
      })
  },
  m_init: function() {
    $maskedInput = $(".masked_input"),
    $maskedInput.length && $maskedInput.inputmask();
  },
  r_init: function() {
    $(".old_warna").on('click',function(){
      $(this).parents('tr').remove();
    });
  },
  s_init: function()
  {
    $maskedInput = $(".selectize"),
    $maskedInput.length && $maskedInput.selectize();
  },
  i_init: function()
  {
    $(".dropify").dropify(), $(".dropify-fr").dropify({
      messages: {
        "default": "Glissez-déposez un fichier ici ou cliquez",
        replace: "Glissez-déposez un fichier ou cliquez pour remplacer",
        remove: "Supprimer",
        error: "Désolé, le fichier trop volumineux"
      }
    })
  },
  // d_init : function() {
  // },
};
</script>
@endsection
