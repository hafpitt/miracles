@extends('back.layouts.default')
@section('title', 'Dashboard')
@section('bread')
<div id="top_bar">
    <ul id="breadcrumbs">
        <li><a href="{{url('back')}}">Home</a></li>
        <li><a href="{{url('back')}}" style="color: grey">Dashboard</a></li>
    </ul>
</div>
@endsection
@section('content')
<div id="page_content_inner">
  <div class="uk-grid  uk-text-center uk-sortable sortable-handler uk-margin-bottom tabs-view-container" data-uk-sortable data-uk-grid-margin>
    <div class="uk-width-medium-1-4 hierarchical_show">
        <div class="md-card md-bg-light-blue-A400">
            <div class="md-card-content">
                <div class="uk-float-left uk-margin-top uk-margin-small-left"><i class="uk-icon-users uk-icon-medium md-color-white"></i></div>
                <span class="uk-text-muted uk-text-small md-color-white">Clients</span>
                <h2 class="uk-margin-remove md-color-white"><span class="countUpMe">0<noscript>2</noscript></span></h2>
            </div>
        </div>
    </div>
    <div class="uk-width-medium-1-4 hierarchical_show">
        <div class="md-card md-bg-deep-orange-A700">
            <div class="md-card-content">
                <div class="uk-float-left uk-margin-top uk-margin-small-left"><i class="uk-icon-home uk-icon-medium md-color-white"></i></div>
                <span class="uk-text-muted uk-text-small md-color-white">Available Kavling</span>
                <h2 class="uk-margin-remove md-color-white"><span class="countUpMe">0<noscript>2</noscript></span></h2>
            </div>
        </div>
    </div>
    <div class="uk-width-medium-1-4 hierarchical_show">
        <div class="md-card md-bg-brown-700">
            <div class="md-card-content">
                <div class="uk-float-left uk-margin-top uk-margin-small-left"><i class="uk-icon-check-square-o uk-icon-medium md-color-white"></i></div>
                <span class="uk-text-muted uk-text-small md-color-white">Sold</span>
                <h2 class="uk-margin-remove md-color-white"><span class="countUpMe">0<noscript>2</noscript></span></h2>
            </div>
        </div>
    </div>
    <div class="uk-width-medium-1-4 hierarchical_show">
        <div class="md-card md-bg-yellow-900">
            <div class="md-card-content">
                <div class="uk-float-left uk-margin-top uk-margin-small-left"><i class="uk-icon-dollar uk-icon-medium md-color-white"></i></div>
                <span class="uk-text-muted uk-text-small md-color-white">Income</span>
                <h2 class="uk-margin-remove md-color-white"><span class="countUpMe">0<noscript>2</noscript></span></h2>
            </div>
        </div>
    </div>

    <div class="uk-width-medium-2-4 hierarchical_show">
      <div class="md-card">
          <div class="md-card-content">
              <div class="uk-overflow-container">
                  <table class="uk-table">
                      <thead>
                          <tr>
                              <th class="uk-text-nowrap">Task</th>
                              <th class="uk-text-nowrap">Status</th>
                              <th class="uk-text-nowrap">Progress</th>
                              <th class="uk-text-nowrap uk-text-right">Due Date</th>
                          </tr>
                      </thead>
                      <tbody>
                          <tr class="uk-table-middle">
                              <td class="uk-width-3-10 uk-text-nowrap"><a href="page_scrum_board.html">ALTR-231</a></td>
                              <td class="uk-width-2-10 uk-text-nowrap"><span class="uk-badge">In progress</span></td>
                              <td class="uk-width-3-10">
                                  <div class="uk-progress uk-progress-mini uk-progress-warning uk-margin-remove">
                                      <div class="uk-progress-bar" style="width: 40%;"></div>
                                  </div>
                              </td>
                              <td class="uk-width-2-10 uk-text-right uk-text-muted uk-text-small">24.11.2015</td>
                          </tr>
                          <tr class="uk-table-middle">
                              <td class="uk-width-3-10 uk-text-nowrap"><a href="page_scrum_board.html">ALTR-82</a></td>
                              <td class="uk-width-2-10 uk-text-nowrap"><span class="uk-badge uk-badge-warning">Open</span></td>
                              <td class="uk-width-3-10">
                                  <div class="uk-progress uk-progress-mini uk-progress-success uk-margin-remove">
                                      <div class="uk-progress-bar" style="width: 82%;"></div>
                                  </div>
                              </td>
                              <td class="uk-width-2-10 uk-text-right uk-text-muted uk-text-small">21.11.2015</td>
                          </tr>
                          <tr class="uk-table-middle">
                              <td class="uk-width-3-10 uk-text-nowrap"><a href="page_scrum_board.html">ALTR-123</a></td>
                              <td class="uk-width-2-10 uk-text-nowrap"><span class="uk-badge uk-badge-primary">New</span></td>
                              <td class="uk-width-3-10">
                                  <div class="uk-progress uk-progress-mini uk-margin-remove">
                                      <div class="uk-progress-bar" style="width: 0;"></div>
                                  </div>
                              </td>
                              <td class="uk-width-2-10 uk-text-right uk-text-muted uk-text-small">12.11.2015</td>
                          </tr>
                          <tr class="uk-table-middle">
                              <td class="uk-width-3-10 uk-text-nowrap"><a href="page_scrum_board.html">ALTR-164</a></td>
                              <td class="uk-width-2-10 uk-text-nowrap"><span class="uk-badge uk-badge-success">Resolved</span></td>
                              <td class="uk-width-3-10">
                                  <div class="uk-progress uk-progress-mini uk-progress-primary uk-margin-remove">
                                      <div class="uk-progress-bar" style="width: 61%;"></div>
                                  </div>
                              </td>
                              <td class="uk-width-2-10 uk-text-right uk-text-muted uk-text-small">17.11.2015</td>
                          </tr>
                          <tr class="uk-table-middle">
                              <td class="uk-width-3-10 uk-text-nowrap"><a href="page_scrum_board.html">ALTR-123</a></td>
                              <td class="uk-width-2-10 uk-text-nowrap"><span class="uk-badge uk-badge-danger">Overdue</span></td>
                              <td class="uk-width-3-10">
                                  <div class="uk-progress uk-progress-mini uk-progress-danger uk-margin-remove">
                                      <div class="uk-progress-bar" style="width: 10%;"></div>
                                  </div>
                              </td>
                              <td class="uk-width-2-10 uk-text-right uk-text-muted uk-text-small">12.11.2015</td>
                          </tr>
                          <tr class="uk-table-middle">
                              <td class="uk-width-3-10"><a href="page_scrum_board.html">ALTR-92</a></td>
                              <td class="uk-width-2-10"><span class="uk-badge uk-badge-success">Open</span></td>
                              <td class="uk-width-3-10">
                                  <div class="uk-progress uk-progress-mini uk-margin-remove">
                                      <div class="uk-progress-bar" style="width: 90%;"></div>
                                  </div>
                              </td>
                              <td class="uk-width-2-10 uk-text-right uk-text-muted uk-text-small">08.11.2015</td>
                          </tr>
                      </tbody>
                  </table>
              </div>
          </div>
      </div>
    </div>

    <div class="uk-width-medium-2-4 hierarchical_show">
      <div class="md-card">
        <div class="md-card-content">
            <div class="uk-overflow-container">
                <table class="uk-table uk-table-hover uk-table-nowrap table_check">
                    <thead>
                        <tr>
                            <th class="uk-width-1-10 uk-text-center uk-text-upper">Recent Activity</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-2-5 uk-width-small-1-5 uk-text-center">
                                        <img class="md-user-image-large" src="{{asset('/assets/back/img/avatars/avatar_06.png')}}" alt=""/>
                                    </div>
                                    <div class="uk-width-3-5 uk-width-small-4-5">
                                        <h4 class="heading_a uk-margin-small-bottom">Eugene Johnson</h4>
                                        <p class="uk-margin-remove"><span class="uk-text-muted">Phone:</span> 1-118-951-7924x861</p>
                                        <p class="uk-margin-remove"><span class="uk-text-muted">Email:</span> 1-118-951-7924x861</p>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-2-5 uk-width-small-1-5 uk-text-center">
                                        <img class="md-user-image-large" src="{{asset('/assets/back/img/avatars/avatar_06.png')}}" alt=""/>
                                    </div>
                                    <div class="uk-width-3-5 uk-width-small-4-5">
                                        <h4 class="heading_a uk-margin-small-bottom">Eugene Johnson</h4>
                                        <p class="uk-margin-remove"><span class="uk-text-muted">Phone:</span> 1-118-951-7924x861</p>
                                        <p class="uk-margin-remove"><span class="uk-text-muted">Email:</span> 1-118-951-7924x861</p>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
      </div>
  </div>

    <div class="uk-width-medium-1-4 hierarchical_show">
        <div class="md-card">
            <div class="md-card-content">
                <div class="uk-float-right uk-margin-top uk-margin-small-right"><span class="peity_orders peity_data">2</span></div>
                <span class="uk-text-muted uk-text-small">KPR</span>
                <h2 class="uk-margin-remove"><span class="countUpMe">0<noscript>2</noscript></span></h2>
            </div>
        </div>
        <div class="md-card">
            <div class="md-card-content">
                <div class="uk-float-right uk-margin-top uk-margin-small-right"><span class="peity_orders peity_data">5</span></div>
                <span class="uk-text-muted uk-text-small">Cash</span>
                <h2 class="uk-margin-remove"><span class="countUpMe">0<noscript>5</noscript></span></h2>
            </div>
        </div>
    </div>
    <div class="uk-width-medium-1-4 hierarchical_show">
        <div class="md-card">
            <div class="md-card-content">
                <div class="uk-float-right uk-margin-top uk-margin-small-right"><span class="peity_orders peity_data">12</span></div>
                <span class="uk-text-muted uk-text-small">In House</span>
                <h2 class="uk-margin-remove"><span class="countUpMe">0<noscript>12</noscript></span></h2>
            </div>
        </div>
        <div class="md-card">
            <div class="md-card-content">
                <div class="uk-float-right uk-margin-top uk-margin-small-right"><span class="peity_orders peity_data">3</span></div>
                <span class="uk-text-muted uk-text-small">Bab Dept</span>
                <h2 class="uk-margin-remove"><span class="countUpMe">0<noscript>3</noscript></span></h2>
            </div>
        </div>
    </div>


    <div class="uk-width-medium-1-4 hierarchical_show">
      <div class="md-card">
          <div class="md-card-content"  style="height:250px;">
              <div id="c3_chart_donut" class="c3chart" ></div>
          </div>
      </div>
    </div>
  </div>
</div>

@endsection
@section('more-css')
<link rel="stylesheet" type="text/css" media="all" href="{{asset('/assets/back/bower_components/sweetalert/dist/sweetalert.css')}}">
@endsection

@section('more-js')
<script>
    altair_forms.parsley_validation_config();
</script>
<script src="{{asset('/assets/back/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
<script src="{{asset('/assets/back/bower_components/jquery.serializeJSON/jquery.serializejson.min.js')}}"></script>
<script src="{{asset('/assets/back/bower_components/peity/jquery.peity.min.js')}}"></script>
<script src="{{asset('/assets/back/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js')}}"></script>
<script src="{{asset('/assets/back/bower_components/countUp.js/dist/countUp.min.js')}}"></script>
<script src="{{asset('/assets/back/bower_components/d3/d3.min.js')}}"></script>
<script src="{{asset('/assets/back/bower_components/c3js-chart/c3.min.js')}}"></script>
<script src="{{asset('/assets/back/bower_components/highcharts/highcharts.js')}}"></script>
<script>
$(function() {
  currentform.count_animated(),
  currentform.circular_statistics(),
  currentform.c3js();
}), currentform = {
  count_animated: function() {
    $(".countUpMe").each(function() {
      var e = this,
        t = $(e).text();
      theAnimation = new CountUp(e, 0, t, 0, 2), theAnimation.start()
    })
  },
  circular_statistics: function() {
    $(".epc_chart").easyPieChart({
      scaleColor: !1,
      trackColor: "#f5f5f5",
      lineWidth: 7,
      size: 150,
      easing: bez_easing_swiftOut
    })
  },
  c3js: function() {
    var n = "#c3_chart_donut";
    if ($(n).length) {
      var a = c3.generate({
        bindto: n,
        data: {
          columns: [
            ["data1", 30],
            ["data2", 120]
          ],
          type: "donut",
          onclick: function(e, t) {
            console.log("onclick", e, t)
          },
          onmouseover: function(e, t) {
            console.log("onmouseover", e, t)
          },
          onmouseout: function(e, t) {
            console.log("onmouseout", e, t)
          }
        },
        donut: {
          title: "Iris Petal Width",
          width: 40
        },
        color: {
          pattern: ["#1f77b4", "#ff7f0e", "#2ca02c", "#d62728", "#9467bd", "#8c564b", "#e377c2", "#7f7f7f", "#bcbd22", "#17becf"]
        }
      });
      $(n).waypoint({
        handler: function() {
          setTimeout(function() {
            a.load({
              columns: [
                ["setosa", .2, .2, .2, .2, .2, .4, .3, .2, .2, .1, .2, .2, .1, .1, .2, .4, .4, .3, .3, .3, .2, .4, .2, .5, .2, .2, .4, .2, .2, .2, .2, .4, .1, .2, .2, .2, .2, .1, .2, .2, .3, .3, .2, .6, .4, .3, .2, .2, .2, .2],
                ["versicolor", 1.4, 1.5, 1.5, 1.3, 1.5, 1.3, 1.6, 1, 1.3, 1.4, 1, 1.5, 1, 1.4, 1.3, 1.4, 1.5, 1, 1.5, 1.1, 1.8, 1.3, 1.5, 1.2, 1.3, 1.4, 1.4, 1.7, 1.5, 1, 1.1, 1, 1.2, 1.6, 1.5, 1.6, 1.5, 1.3, 1.3, 1.3, 1.2, 1.4, 1.2, 1, 1.3, 1.2, 1.3, 1.3, 1.1, 1.3],
                ["virginica", 2.5, 1.9, 2.1, 1.8, 2.2, 2.1, 1.7, 1.8, 1.8, 2.5, 2, 1.9, 2.1, 2, 2.4, 2.3, 1.8, 2.2, 2.3, 1.5, 2.3, 2, 2, 1.8, 2.1, 1.8, 1.8, 1.8, 2.1, 1.6, 1.9, 2, 2.2, 1.5, 1.4, 2.3, 2.4, 1.8, 1.8, 2.1, 2.4, 2.3, 1.9, 2.3, 2.5, 2.3, 1.9, 2, 2.3, 1.8]
              ]
            })
          }, 1500), setTimeout(function() {
            a.unload({
              ids: "data1"
            }), a.unload({
              ids: "data2"
            })
          }, 2500), this.destroy()
        },
        offset: "80%"
      }), $window.on("debouncedresize", function() {
        a.resize()
      })
    }

    var l = "#c3_server_load";
    if ($(l).length) {
      var u = c3.generate({
        bindto: l,
        data: {
          columns: [
            ["data", 24]
          ],
          type: "gauge",
          onclick: function(e, t) {
            console.log("onclick", e, t)
          },
          onmouseover: function(e, t) {
            console.log("onmouseover", e, t)
          },
          onmouseout: function(e, t) {
            console.log("onmouseout", e, t)
          }
        },
        gauge: {
          label: {
            format: function(e, t) {
              return e
            },
            show: !1
          },
          min: 0,
          max: 100,
          width: 36
        },
        color: {
          pattern: ["#D32F2F", "#F57C00", "#388E3C"],
          threshold: {
            values: [25, 50, 100]
          }
        },
        size: {
          height: 180
        }
      });
      setInterval(function() {
        var e = Math.floor(100 * Math.random());
        u.load({
          columns: [
            ["data", e]
          ]
        })
      }, 2e3)
    }
  },
}
</script>
@endsection
