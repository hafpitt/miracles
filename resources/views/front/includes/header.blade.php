<header>
  <div class="top_info">
    <div class="container clearfix">
      <div class="logo">
        <a href="{{url('')}}" ><img src="{{asset('assets/front/images/logo_miracle3.png')}}" alt="" /></a>
        <!-- LOGO -->
      </div>
      @if(!Auth::check())
      <ul class="secondary_menu pull-right">
        <li><a href="{{url('login')}}" >Masuk</a></li>
        <li><a href="{{url('register')}}" >Daftar</a></li>
      </ul>
      @endif
    </div>
    <div class="container clearfix">
      <!-- @if(Auth::check())
        <ul class="secondary_menu">
          <li>
            <div class="love_list">
              <a class="love_list_btn" style="padding:5px;border:none;" href="javascript:void(0);" >
                @empty(Auth::user()->url_photo)
                  <i class="fa fa-user"></i>
                @endempty
                @isset(Auth::user()->url_photo)
                  <img style="width: 24px;height: 24px;border-radius: 50%;" class="md-user-image" src="{{asset(Storage::disk('public')->url(Auth::user()->url_photo))}}" alt=""/>
                @endisset
                <p>Account</p>
              </a>
              <div class="cart" style="width:inherit;">
                <ul class="cart-items">
                  <li class="clearfix" style="padding:5px;"><a href="{{url('profile')}}">Profile</a></li>
                </ul>
                <ul class="cart-items">
                  <li class="clearfix" style="padding:5px;"><a href="{{url('logout')}}">Logout</a></li>
                </ul>
              </div>
            </div>
          </li>
        </ul>
      @endif -->
      <!-- <div class="live_chat"><a href="javascript:void(0);" ><i class="fa fa-comment-o"></i> Live chat</a></div>
      <div class="phone_top">have a question? <a href="tel:1 800 888 2828" >1 800 888 2828</a></div> -->
    </div>
  </div>

  <div class="menu_block">
    <div class="container clearfix">

      <div class="kategori pull-left">
        <ul class="navmenu center">
          <li class="sub-menu {{ Request::is('items') ? 'active' : '' }}"><a href="javascript:void(0);" >Kategori &nbsp <i class="fa fa-angle-down"></i></a>
            <ul class="mega_menu megamenu_col1 clearfix">
              <li class="col">
                <ol>
                  @foreach ($front_barang_kategori as $kategori)
                    <li><a href="{{url('items/'.$kategori->url_page)}}" >{{$kategori->nama}}</a></li>
                  @endforeach
                </ol>
              </li>
            </ul>
          </li>
        </ul>
        <!-- <a href="index.html" ><img src="{{asset('assets/front/images/logo.png')}}" alt="" /></a> -->
        <!-- LOGO -->
      </div>
      <div class="top_search_form">
        <a class="top_search_btn" href="javascript:void(0);" ><i class="fa fa-search"></i></a>
        <form method="get" action="{{url('items/s')}}">
          <input type="text" name="filter" value="Cari" onFocus="if (this.value == 'Cari') this.value = '';" onBlur="if (this.value == '') this.value = 'Cari';" />
        </form>
      </div>

      @if(Auth::check())
        <div class="shopping_bag">
          <a class="shopping_bag_btn" href="javascript:void(0);" ><i class="fa fa-shopping-cart"></i><p>Keranjang Belanja</p><span>{{$front_user_cart->count()}}</span></a>
          <div class="cart">
            <ul class="cart-items">
              @php
                $grandtotal = 0;
              @endphp
            @if ($front_user_cart->count() >= 1)
              @foreach ($front_user_cart as $carts)
                <li class="clearfix">
                  <img class="cart_item_product" src="@isset($carts->barang->barang_fotos){{asset(Storage::disk('public')->url($carts->barang->barang_fotos->first()->url_photo))}}@endisset" alt="" />
                  <a href="{{url('items/'.$carts->barang->url_page)}}" class="cart_item_title">{{$carts->barang->nama}}</a>
                  <span class="cart_item_price">{{$carts->qty}} × IDR {{number_format($carts->barang->departments->first()->pivot->harga)}}</span>
                  @php
                    $grandtotal = $grandtotal = $grandtotal+$carts->qty*$carts->barang->departments->first()->pivot->harga;
                  @endphp
                </li>
              @endforeach
            </ul>
            <div class="cart_total">
              <div class="clearfix"><span class="cart_subtotal">subtotal: <b>IDR {{number_format($grandtotal)}}</b></span></div>
                <a class="btn active" href="{{url('cart')}}">Lihat Belanjaanmu</a><br><br>
                <a style="background-color:transparent; border:2px solid #ccc; color:#666;" class="btn active" href="{{url('checkout')}}">Bayar</a>
            </div>
            @else
              <div class="title center" style="margin:28px 0 28px">
                  <h3>Keranjang Belanja Kosong :'( </h3>
                <p>Silahkan pilih barang, kemudian klik "Beli"</p>
              </div>
            @endif
          </div>
        </div>
      @else
      <div class="shopping_bag">
        <a class="shopping_bag_btn" href="javascript:void(0);" ><i class="fa fa-shopping-cart"></i><p>Keranjang Belanja</p></a>
        <div class="cart">
          <div class="cart_total">
            <div class="clearfix"><span class="cart_subtotal">Masuk untuk melanjutkan</span></div>
            <a class="btn active" href="{{url('login')}}">Masuk</a>
          </div>
        </div>
      </div>
      @endif
      <ul class="navmenu center">
        <li class="sub-menu first {{ Request::is('/') ? 'active' : '' }}"><a href="{{url('')}}">Home</a></li>
        <li class="sub-menu first {{ Request::is('faq') ? 'active' : '' }}"><a href="{{url('faq')}}">FAQ</a></li>
        @if(Auth::check())
        <li class="pull-right sub-menu {{ Request::is('items') ? 'active' : '' }}">
          <a href="javascript:void(0);" >
            @empty(Auth::user()->url_photo)
              <i class="fa fa-user"></i>
            @endempty
            @isset(Auth::user()->url_photo)
              <img style="width: 24px;height: 24px;border-radius: 50%;" class="md-user-image" src="{{asset(Storage::disk('public')->url(Auth::user()->url_photo))}}" alt=""/>
            @endisset
            &nbsp {{Auth::user()->nama}} <span class="user-transaction">{{$front_trans_queue->count()}}</span>
          </a>
          <ul class="mega_menu megamenu_col1 clearfix">
            <li class="col">
              <ol>
                  <li class="clearfix" style="padding:5px;"><a href="{{url('profile')}}">Lihat Profil</a></li>
              </ol>
              <ol>
                  <li class="clearfix" style="padding:5px;"><a href="{{url('history')}}">Transaksi Saya <span class="user-transaction">{{$front_trans_queue->count()}}</span></a></li>
              </ol>
              <ol>
                  <li class="clearfix" style="padding:5px;"><a href="{{url('retur-list')}}">Komplain Saya <span class="user-transaction"></span></a></li>
              </ol>
              <ol>
                  <li class="clearfix" style="padding:5px;"><a href="{{url('logout')}}">Keluar</a></li>
              </ol>
            </li>
          </ul>
        </li>
        @endif
      </ul>
    </div>
  </div>
</header>
