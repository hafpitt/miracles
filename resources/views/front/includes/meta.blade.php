<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<meta name="csrf-token" content="{{csrf_token()}}">

<link rel="shortcut icon" href="{{asset('assets/front/images/favicon.ico')}}">

<!-- CSS -->
<link href="{{asset('assets/front/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/front/css/flexslider.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/front/css/fancySelect.css')}}" rel="stylesheet" media="screen, projection" />
<link href="{{asset('assets/front/css/animate.css')}}" rel="stylesheet" type="text/css" media="all" />
<link href="{{asset('assets/front/css/style.css')}}" rel="stylesheet" type="text/css" />

<!-- FONTS -->
<link href="{{asset('assets/front/css/font-awesome.css')}}" rel='stylesheet' type='text/css'>
<link href="{{asset('assets/front/css/roboto.css')}}" rel='stylesheet' type='text/css'>
