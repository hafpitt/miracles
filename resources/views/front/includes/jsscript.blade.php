<script src="{{asset('assets/front/js/jquery.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/front/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/front/js/jquery.sticky.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/front/js/parallax.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/front/js/jquery.flexslider-min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/front/js/jquery.jcarousel.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/front/js/fancySelect.js')}}"></script>
<script src="{{asset('assets/front/js/animate.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/front/js/myscript.js')}}" type="text/javascript"></script>
<script>
  if (top != self) top.location.replace(self.location.href);
</script>
