<footer>
  <div class="container" data-animated='fadeInUp'>
    <div class="row">

      <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 col-ss-12 padbot30">
        <h4>Kontak</h4>
        <div class="foot_address"><span>Miracle</span>@isset($front_cms_contact){{$front_cms_contact->alamat}}@endisset</div>
        <div class="foot_phone"><a href="tel:1234567890" >@isset($front_cms_contact){{$front_cms_contact->notelp}}@endisset</a></div>
        <div class="foot_mail"><a href="@isset($front_cms_contact)mailto:{{$front_cms_contact->email}}@endisset" >@isset($front_cms_contact){{$front_cms_contact->email}}@endisset</a></div>
        <!-- <div class="foot_live_chat"><a href="javascript:void(0);" ><i class="fa fa-comment-o"></i> Live chat</a></div> -->
      </div>

      <div class="col-lg-4 col-md-4 col-sm-6 padbot30">
        <h4>tentang miracle</h4>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi elit ligula, facilisis hendrerit mollis et, molestie sed elit. Curabitur tincidunt semper ornare. Praesent ornare, tellus sed dapibus sollicitudin, est justo ornare sapien, sed fermentum neque libero sed sem.</p>
        <p>Maecenas in urna id lectus tempor congue. Nunc tempor, mauris ac lacinia efficitur, dolor ligula pellentesque orci, sit amet vestibulum magna leo sit amet justo.</p>
      </div>

      <div class="respond_clear_768"></div>

      <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 col-ss-12 padbot30">
        <h4>Informasi</h4>
        <ul class="foot_menu">
          <li><a href="javascript:void(0);" >About Us</a></li>
          <li><a href="javascript:void(0);" >Gallery <span>new</span></a></li>
          <li><a href="javascript:void(0);" >Delivery</a></li>
          <li><a href="javascript:void(0);" >Privacy police</a></li>
          <li><a href="javascript:void(0);" >Blog</a></li>
          <li><a href="javascript:void(0);" >Faqs</a></li>
          <li><a href="javascript:void(0);" >countact us</a></li>
        </ul>
      </div>

      <div class="respond_clear_480"></div>



      <div class="col-lg-4 col-md-4 padbot30">
        <!-- <h4>Newsletter</h4>
        <form class="newsletter_form clearfix" action="javascript:void(0);" method="get">
          <input type="text" name="newsletter" value="Enter E-mail & Get 10% off" onFocus="if (this.value == 'Enter E-mail & Get 10% off') this.value = '';" onBlur="if (this.value == '') this.value = 'Enter E-mail & Get 10% off';" />
          <input class="btn newsletter_btn" type="submit" value="SIGN UP">
        </form> -->

        <h4>Temukan juga kami di</h4>
        <div class="social">
          <!-- <a href="javascript:void(0);" ><i class="fa fa-twitter"></i></a> -->
          <a href="javascript:void(0);" ><i class="fa fa-facebook"></i></a>
          <!-- <a href="javascript:void(0);" ><i class="fa fa-google-plus"></i></a>
          <a href="javascript:void(0);" ><i class="fa fa-pinterest-square"></i></a>
          <a href="javascript:void(0);" ><i class="fa fa-tumblr"></i></a> -->
          <a href="javascript:void(0);" ><i class="fa fa-instagram"></i></a>
        </div>
      </div>
    </div>
  </div>
  <div class="copyright">
    <div class="container clearfix">
      <!-- <div class="foot_logo"><a href="index.html" ><img src="images/foot_logo.png" alt="" /></a></div> -->
      <div class="copyright_inf pull-left" style="color:#fff">Alpha Version</div>

      <div class="copyright_inf">
        <span class="pull-left">Miracle© 2017</span> |
        <span clas="pull-left">All Right Reserved</span> |
        <a class="back_top" href="javascript:void(0);" >Kembali Ke Atas <i class="fa fa-angle-up"></i></a>
      </div>
    </div>
  </div>
</footer>
