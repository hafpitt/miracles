@extends('front.layouts.default')
@section('title', 'History')
@section('content')
<section class="breadcrumb parallax margbot30" style="background-image:url(@isset($front_all_top_parallax){{asset(Storage::disk('public')->url($front_all_top_parallax->url_photo))}}@endisset)"></section>
<section class="page_header">
  <div class="container">
    <h3 class="pull-left"><b>Transaksimu</b></h3>
    <!-- <div class="pull-right">
      <a href="{{url('')}}" >Continue Shopping<i class="fa fa-angle-right"></i></a>
    </div> -->
  </div>
</section>

<section class="love_list_block">
  <div class="container">
    @if(session()->has('success'))
      <div class="alert alert-success alert-dismissable">
        <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{ session()->get('success') }}
      </div>
    @endif
    <div class="row">
      <div class="col-lg-12 col-md-12 padbot40">
        <table id="tabledata" class="table table-striped table-bordered" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th class="text-left">No Invoice</th>
              <th>Tanggal</th>
              <th>Transfer Dari</th>
              <th>Grandtotal</th>
              <th class="text-center">Keterangan</th>
              <th class="text-center">Aksi</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($history as $value)
            <tr>
              <td class="text-left">{{$value->no_invoice}}</td>
              <td>{{$value->tgl}}</td>
              <td style="text-transform:uppercase";>{{$value->bank_user->bank->nama}} : {{$value->bank_user->no_rekening}} a/n {{$value->bank_user->nama_pemilik}}</td>
              <td>{{number_format($value->grand_total)}}</td>
              <td class="text-center">
                @if ($value->sisa_bayar<>0 && $value->konfirmasi==1)
                  SEDANG DILAKUKAN VERIFIKASI
                @elseif ($value->sisa_bayar<>0)
                  SILAHKAN LAKUKAN KONFIRMASI PEMBAYARAN
                @elseif ($value->url_complain<>"" && $value->diterima==1)
                  PANTAU KOMPLAIN ANDA <a href="retur-list">DI SINI</a>
                @elseif ($value->diterima==1)
                  DITERIMA
                @elseif ($value->no_resi<>"")
                  DIKIRIM<br>
                  No Resi : {{$value->no_resi}}
                @else
                  TERVERIFIKASI
                @endif
              </td>
              <td class="text-center">
                @if ($value->sisa_bayar<>0 && $value->konfirmasi==1)

                @elseif ($value->sisa_bayar<>0)
                  <a class="text-info" href="{{url('confirm-payment/'.Crypt::encrypt($value->id))}}">Konfirmasi Pembayaran</a>
                @elseif ($value->no_resi<>"" && $value->diterima=="")
                  <a class="text-info" href="{{url('confirm-receive/'.Crypt::encrypt($value->id))}}">Konfirmasi Terima</a>
                @elseif ($value->url_complain<>"")

                @elseif ($value->diterima<>"" && $value->sisa_bayar==0)
                  <a class="text-info" href="{{url('complain/'.Crypt::encrypt($value->id))}}">Ajukan Komplain</a>
                @endif
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
      <!-- <div id="sidebar" class="col-lg-3 col-md-3 padbot50">
        <div class="sidepanel widget_register_form">
          <h3>Returning Customer</h3>
          <p>Sign in to save and share your Love List.</p>
          <form class="registration_form clearfix" action="javascript:void(0);" method="get">
            <input type="text" name="text" value="Username or email" onFocus="if (this.value == 'Username or email') this.value = '';" onBlur="if (this.value == '') this.value = 'Username or email';" />
            <input type="text" name="pass" value="Password" onFocus="if (this.value == 'Password') this.value = '';" onBlur="if (this.value == '') this.value = 'Password';" />
            <a class="forgot_pass" href="javascript:void(0);" >Forgot password?</a>
            <input type="submit" value="Login">
          </form>
        </div>
        <div class="sidepanel widget_new_account">
          <h3>New customers</h3>
          <p>Don't have an account? Register with Glammy Shop to save and share your Love List.</p>
          <a class="create_acc" href="javascript:void(0);" >Create New Account</a>
        </div>
      </div> -->
    </div>
  </div>
</section>
@endsection
@section('more-css')
<link rel="stylesheet" type="text/css" media="all" href="{{asset('assets/front/bower_components/datatables/datatables.css')}}">

@endsection
@section('more-js')
<script src="{{asset('assets/front/bower_components/datatables/datatables.js')}}"></script>
<script>
$(function() {
    thisform.init();
}), thisform = {
  init: function()
  {
    thisform.d_init();
  },
  d_init: function() {
    var t = $("#tabledata").DataTable(),
        a = t.prev(".dt_colVis_buttons");
    if (t.length) {
        var e = t.DataTable();
    }
  },
};
</script>
@endsection
