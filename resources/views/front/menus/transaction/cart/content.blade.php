@extends('front.layouts.default')
@section('title', 'Cart')
@section('content')
<!-- BREADCRUMBS -->
		<section class="breadcrumb parallax margbot30" style="background-image:url(@isset($front_all_top_parallax){{asset(Storage::disk('public')->url($front_all_top_parallax->url_photo))}}@endisset)"></section>
		<!-- //BREADCRUMBS -->

		<!-- PAGE HEADER -->
		<section class="page_header">
			@if ($front_user_cart->count() >= 1)
			<!-- CONTAINER -->
			<div class="container">
				<h3 class="pull-left"><b>Keranjang Belanja</b></h3>

				<div class="pull-right">
					<a href="{{url('')}}" >Lanjutkan Belanja<i class="fa fa-angle-right"></i></a>
				</div>
			</div><!-- //CONTAINER -->
			@endif
		</section><!-- //PAGE HEADER -->


		<!-- SHOPPING BAG BLOCK -->
		<section class="shopping_bag_block">

			<!-- CONTAINER -->
			<div class="container">

				<!-- ROW -->
				<div class="row">

					<!-- CART TABLE -->
					<div class="col-lg-9 col-md-9 padbot40">
						@php
							$grandtotal = 0;
							$weight_total = 0;
							$item_total = 0;
						@endphp
						@if ($front_user_cart->count() >= 1)
						<table class="shop_table">
							<thead>
								<tr>
									<th class="product-thumbnail"></th>
									<th class="product-name">Item</th>
									<!-- <th class="product-price"></th> -->
									<th class="product-price">Harga</th>
									<th class="product-quantity">Quantity</th>
									<!-- <th class="product-quantity"></th> -->
									<th class="product-subtotal">Total</th>
									<th class="product-remove text-center">Action</th>
									<!-- <th class="product-remove"></th> -->
								</tr>
							</thead>
							<tbody>
								@foreach ($front_user_cart as $carts)
								<tr class="cart_item">
									<td class="product-thumbnail"><a href="{{url('items/'.$carts->barang->url_page)}}" ><img src="@isset($carts->barang->barang_fotos){{asset(Storage::disk('public')->url($carts->barang->barang_fotos->first()->url_photo))}}@endisset" width="100px" alt="" /></a></td>
									<!-- {{asset('assets/front/images/tovar/women/1.jpg')}} -->
									<td class="product-name">
										<a href="{{url('items/'.$carts->barang->url_page)}}">{{$carts->barang->nama}}</a>
										<ul class="variation">
											<li class="variation-Color">Warna: <span>{{$carts->barang_warna->nama}}</span></li>
											<li class="variation-Size">Berat: <span>{{$carts->barang->berat}}</span></li>
										</ul>
									</td>
									<!-- <td class="product-price text-right">IDR</td> -->
									<td class="product-price text-right">IDR {{number_format($carts->barang->departments->first()->pivot->harga)}}</td>
									<td class="product-subtotal text-right">{{$carts->qty}}</td>
									<!-- <td class="product-subtotal text-right">IDR</td> -->
									<td class="product-subtotal text-right">IDR {{number_format($carts->qty*$carts->barang->departments->first()->pivot->harga)}}</td>
									@php
                    $grandtotal = $grandtotal+$carts->qty*$carts->barang->departments->first()->pivot->harga;
										$weight_total = $weight_total+$carts->barang->berat*$carts->qty;
										$item_total = $item_total+$carts->qty;
                  @endphp
									<td class="product-remove text-center">
										<div class="open-project-link-qty">
											<a data-toggle="tooltip" data-placement="top" data-original-title="Edit Qty"
												class="open-project"
												href="javascript:void(0);"
												data-url="{{url('change-qty/'.$carts->id)}}">
												<i style="font-size:25px; border:none;" class="fa fa-pencil"></i>
											</a>
										</div>
										<a data-toggle="tooltip" data-placement="top" data-original-title="Hapus" href="{{url('remove-cart/'.Crypt::encryptString($carts->id))}}" ><i style="font-size:25px; border:none;" class="fa fa-trash-o"></i></a>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
						@else
							<div class="title center" style="margin:90px 0 28px">
								<h1>Keranjang Belanja Kosong :'(</h1>
								<a class="btn btn-sm-change" href="{{url('')}}"> Ayo Belanja</a>
							</div>
						@endif
					</div><!-- //CART TABLE -->

					<!-- SIDEBAR -->
					<div id="sidebar" class="col-lg-3 col-md-3 padbot50">

						<!-- BAG TOTALS -->
						<div class="sidepanel widget_bag_totals">
							<h3>DETAIL BELANJAANMU</h3>
							<table class="bag_total">
								<tr class="cart-subtotal clearfix">
									<th>Total Barang</th>
									<td>{{number_format($item_total)}} pcs</td>
								</tr>
								<tr class="cart-subtotal clearfix">
									<th>Total Berat</th>
									<td>{{number_format($weight_total)}} grams</td>
								</tr>
								<tr class="total clearfix">
									<th>Sub Total</th>
									<td>IDR {{number_format($grandtotal)}}</td>
								</tr>
							</table>
							<!-- <form class="coupon_form" action="javascript:void(0);" method="get">
								<input type="text" name="coupon" value="Have a coupon?" onFocus="if (this.value == 'Have a coupon?') this.value = '';" onBlur="if (this.value == '') this.value = 'Have a coupon?';" />
								<input type="submit" value="Apply">
							</form> -->
							@if ($front_user_cart->count() >= 1)
							<a class="btn active" href="{{url('checkout')}}" >Bayar</a>
							<a class="btn inactive" href="{{url('')}}" >Lanjutkan Belanja</a>
							@endif
						</div><!-- //REGISTRATION FORM -->
					</div><!-- //SIDEBAR -->
				</div><!-- //ROW -->
			</div><!-- //CONTAINER -->
		</section><!-- //SHOPPING BAG BLOCK -->

<!-- TOVAR MODAL CONTENT -->
<div id="modal-body" class="clearfix">
	<div id="tovar_content"></div>
	<div class="close_block"></div>
</div><!-- TOVAR MODAL CONTENT -->


@endsection
