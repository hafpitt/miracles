<div class="tover_view_page element_fade_in">
	<form method="post" action="{{url('change-qty/'.Crypt::encryptString($carts->id))}}">
		{{ csrf_field() }}
		<div class="tover_view_header clearfix">
			<p>Ubah Keranjang Belanja</p>
			<a id="tover_view_page_close" href="javascript:void(0);"> Close<i>X</i></a>
		</div>

		<div class="clearfix">
			<p>Nama Produk <input type="text" nama="barang" value="{{$carts->barang->nama}}" disabled/></p>
			<p>Qty <input type="text" name="qty" value="{{$carts->qty}}"/></p>
		</div>
		<div class="tovar_view_btn">
			<button class="add_bag" type="submit" ><i class="fa fa fa-pencil"></i>Ubah</button>
		</div>
	</form>
</div>
