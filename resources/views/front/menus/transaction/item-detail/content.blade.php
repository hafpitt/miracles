@extends('front.layouts.default')
@section('title', 'Product Detail')
@section('content')

<section class="breadcrumb parallax margbot30" style="background-image:url(@isset($front_all_top_parallax){{asset(Storage::disk('public')->url($front_all_top_parallax->url_photo))}}@endisset)"></section>

<section class="tovar_details padbot70">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 tovar_details_wrapper clearfix">
        <div class="tovar_details_header clearfix margbot35">
          <h3 class="pull-left"><b>{{$barang_detail->barang_kategori->nama}}</b></h3>
          <!-- <div class="tovar_details_pagination pull-right">
            <a class="fa fa-angle-left" href="javascript:void(0);" ></a>
            <span>2 of 34</span>
            <a class="fa fa-angle-right" href="javascript:void(0);" ></a>
          </div> -->
        </div>
        @if(session()->has('success'))
          <div class="alert alert-success alert-dismissable">
            <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{ session()->get('success') }}
          </div>
        @endif
        @if ($errors->any())
          <div class="alert alert-danger alert-dismissable">
            <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <ul>
                  @foreach ($errors->all() as $message)
                      <li>{{$message}}</li>
                  @endforeach
              </ul>
          </div>
        @endif
        <div class="clearfix padbot40">
          <div class="tovar_view_fotos clearfix">
            <div id="slider2" class="flexslider">
              <ul class="slides">
                @foreach ($barang_detail->barang_fotos as $foto)
                  <li><a href="javascript:void(0);" ><img src="@isset($foto){{asset(Storage::disk('public')->url($foto->url_photo))}}@endisset" alt="" /></a></li>
                @endforeach
              </ul>
            </div>
            <div id="carousel2" class="flexslider">
              <ul class="slides">
                @foreach ($barang_detail->barang_fotos as $foto)
                  <li><a href="javascript:void(0);" ><img src="@isset($foto){{asset(Storage::disk('public')->url($foto->url_photo))}}@endisset" alt="" /></a></li>
                @endforeach
              </ul>
            </div>
          </div>

          <div class="col-lg-6 col-md-6">
            <form id="validate" method="post" action="{{url()->current()}}">
            {{ csrf_field() }}
              <div class="row">
                <div class="tovar_view_title">
                  <strong>{{strtoupper($barang_detail->nama)}}</strong>
                  <div class="pull-right tovar_view_price">@isset($barang_detail->departments[0])IDR {{number_format($barang_detail->departments[0]->pivot->harga)}}@endisset</div>
                </div>
              </div>
              <div class="row">
                <div class="clearfix tovar_brend_price">
                  <div class="pull-left tovar_brend">{{strtoupper($barang_detail->barang_kategori->nama)}}
                    <!-- <div class="row"> -->
                      <div class="tovar_view_title">
                        <div class="pull-left tovar_brend">Stock : {{$barang_detail->stok}}</div>
                      </div>
                    <!-- </div> -->
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="tovar_color_select">
        	        <p>Pilih Warna</p>
        					@foreach ($barang_detail->barang_warnas as $warna)
                  <input class="radio-color" type="radio" id="{{$warna->nama.$warna->id}}" name="id_barang_warna" value="{{$warna->id}}" checked="{{$warna[0]}}" {{ old('id_barang_warna') == $warna->id ? 'checked' : ''}}/>
        					<label class="drinkcard-color" style="background-color:{{$warna->warna}};" for="{{$warna->nama.$warna->id}}"></label>
        	        	<!-- <a id="{{$warna->nama.$warna->id}}" value="{{$warna->id}}" style="background-color:{{$warna->warna}}" class="color1 {{ Request::is('/') ? 'active' : '' }}" href="javascript:void(0);" name="warna"></a> -->
        						<!-- <label>{{$warna->nama}}</label> -->
        					@endforeach
        	        <!-- <a class="color2 active" href="javascript:void(0);" ></a>
        	        <a class="color3" href="javascript:void(0);" ></a>
        	        <a class="color4" href="javascript:void(0);" ></a> -->
        	      </div>
              </div>
              <div class="row">
                <div>
                  <div class="form-inline form-group">
                    <label>Qty : </label>
                    <input type="text" name="qty" class="form-control qty md-input masked_input" data-inputmask="'alias': 'numeric', 'placeholder': '0', 'min':'1', 'max':'{{$barang_detail->stok}}'" value="{{ old('qty')}}" >
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="tovar_view_btn">
                  <button class="btn active" type="submit"><i class="fa fa-shopping-cart" ></i>&nbsp&nbsp Beli</button>
                </div>
              </div>
            </form>
          </div>
        </div><!-- //CLEARFIX -->

        <!-- TOVAR INFORMATION -->
        <div class="tovar_information">
          @if(session()->has('review'))
            <div class="alert alert-success alert-dismissable">
              <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
              {{ session()->get('review') }}
            </div>
          @endif
          <ul class="tabs clearfix">
            <li class="current">Details</li>
            <li>Information</li>
            <li>Reviews ({{$barang_detail->users->count()}})</li>
          </ul>
          <div class="box visible">
            <p>{!! nl2br(e($barang_detail->keterangan)) !!}</p>
          </div>
          <div class="box">
            <p class="barang-detail">{!! nl2br(e($barang_detail->keterangandetail)) !!}</p>
          </div>
          <div class="box">
            <ul class="comments">
              @foreach ($barang_detail->users as $comment)
                <li>
                  <div class="clearfix">
                    <p class="pull-left"><strong><a href="javascript:void(0);" >{{$comment->nama}}</a></strong></p>
                    <span class="date">{{date_format(date_create($comment->pivot->tgl),"d-M-Y")}}</span>
                    <div class="pull-right rating-box clearfix">
                      <div class="stars">
                        <input class="star star-5" type="radio" {{($comment->pivot->rating=="5")?"checked":""}} disabled/>
                        <label class="star star-5"></label>
                        <input class="star star-4" type="radio" {{($comment->pivot->rating=="4")?"checked":""}} disabled/>
                        <label class="star star-4"></label>
                        <input class="star star-3" type="radio" {{($comment->pivot->rating=="3")?"checked":""}} disabled/>
                        <label class="star star-3"></label>
                        <input class="star star-2" type="radio" {{($comment->pivot->rating=="2")?"checked":""}} disabled/>
                        <label class="star star-2"></label>
                        <input class="star star-1" type="radio" {{($comment->pivot->rating=="1")?"checked":""}} disabled/>
                        <label class="star star-1"></label>
                      </div>
                    </div>
                  </div>
                  <p>{{$comment->pivot->comment}}</p>
                </li>
              @endforeach
            </ul>
            @if (Auth::check())
            <h3>WRITE A REVIEW</h3>
            <p>Now please write a (short) review....(min. 20, max. 2000 characters)</p>
            <div class="clearfix">
              <form method="post" action="{{url('submit-review/'.$barang_detail->url_page)}}">
                {{ csrf_field() }}
                <textarea name="comment"></textarea>
                <label class="pull-left rating-box-label">Your Rate:</label>
                <div class="pull-left rating-box clearfix">
                  <div class="stars">
                    <input class="star star-5" id="star-5" type="radio" value="5" name="rating"/>
                    <label class="star star-5" for="star-5"></label>
                    <input class="star star-4" id="star-4" type="radio" value="4" name="rating"/>
                    <label class="star star-4" for="star-4"></label>
                    <input class="star star-3" id="star-3" type="radio" value="3" name="rating"/>
                    <label class="star star-3" for="star-3"></label>
                    <input class="star star-2" id="star-2" type="radio" value="2" name="rating"/>
                    <label class="star star-2" for="star-2"></label>
                    <input class="star star-1" id="star-1" type="radio" value="1" name="rating"/>
                    <label class="star star-1" for="star-1"></label>
                  </div>
                </div>
                <input type="submit" class="dark-blue big" value="Submit review">
              </form>
            </div>
            @else
            <h3>PLEASE LOGIN TO WRITE A COMMENT</h3>
            <a class="btn active" href="{{url('login')}}">Login</a>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('more-css')
<style>
div.stars {
  width: 270px;
  display: inline-block;
}
input.star { display: none; }
label.star {
  float: right;
  padding-right: 10px;
  font-size: 20px;
  color: #444;
  transition: all .2s;
}
input.star:checked ~ label.star:before {
  content: '\f005';
  color: #FD4;
  transition: all .25s;
}
input.star-5:checked ~ label.star:before {
  color: #FE7;
  text-shadow: 0 0 20px #952;
}
input.star-1:checked ~ label.star:before { color: #F62; }
label.star:hover { transform: rotate(-15deg) scale(1.3); }
label.star:before {
  content: '\f006';
  font-family: FontAwesome;
}

.cc-selector input{
    margin:0;padding:0;
    -webkit-appearance:none;
       -moz-appearance:none;
            appearance:none;
}
.cc-selector input:active +.drinkcard-cc{opacity: .9;}

.cc-selector input:checked +.drinkcard-cc{
    -webkit-filter: none;
       -moz-filter: none;
            filter: none;
}
.drinkcard-cc{
    cursor:pointer;
    background-size:contain;
    background-repeat:no-repeat;
    display:inline-block;
    width:30px;height:30px;
    -webkit-transition: all 100ms ease-in;
       -moz-transition: all 100ms ease-in;
            transition: all 100ms ease-in;

}
.drinkcard-cc:hover{

}

input[type="radio"]{
  display: none;
}



.form-inline .qty{
  width: 50px;
}

</style>
@endsection

@section('more-js')
<script src="{{asset('assets/front/bower_components/parsley/dist/parsley.js')}}"></script>
<script src="{{asset('assets/front/bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js')}}"></script>
<script>
	$(function(){
    thisform.init();
	}), thisform = {
    init : function()
    {
      $(window).scrollTop(0);
      thisform.p_init();
      thisform.masked_inputs();
    },
		p_init : function()
		{
			var i = $("#validate");
      i.parsley();
			i.parsley().on("form:submit", function() {
        $(".masked_input").inputmask('remove');
			});
		},
    masked_inputs: function() {
      $maskedInput = $(".masked_input"),
      $maskedInput.length && $maskedInput.inputmask();
    },
  }
</script>
@endsection
