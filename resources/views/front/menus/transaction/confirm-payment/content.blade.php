@extends('front.layouts.default')
@section('title', 'Konfirm Payment')
@section('content')
<section class="breadcrumb parallax margbot30" style="background-image:url(@isset($front_all_top_parallax){{asset(Storage::disk('public')->url($front_all_top_parallax->url_photo))}}@endisset)"></section>
<section class="page_header">
  <div class="container">
    <h3 class="pull-left"><b>Konfirmasi Pembayaran</b></h3>
    <div class="pull-right">
      <a href="{{url('')}}" >Continue Shopping<i class="fa fa-angle-right"></i></a>
    </div>
  </div>
</section>

<section class="checkout_page">
	<div class="container">
    <form id="validate" enctype="multipart/form-data" action="{{url()->current()}}" method="post">
    {{ csrf_field() }}
  		<div class="row">
  			<div class="col-lg-9 col-md-9 padbot60">
				  <div class="checkout_confirm_orded clearfix padbot60">
  					<div class="checkout_confirm_orded_bordright clearfix">
  						<div class="billing_information">
                <div class="billing_information">
                  <p class="checkout_title margbot10">Alamat Pengiriman <br><h3><b>{{$transaction->no_invoice}}</b></h3></p>
                </div>
  							<div class="billing_information_content margbot40">
                  Nama penerima : <b>{{ucfirst($transaction->users_alamat->nama_penerima)}}</b><br>
                  Alamat pengiriman :
                  <span>{{ucfirst($transaction->users_alamat->nama)}}</span>
                  <span>{{ucfirst($transaction->users_alamat->alamat)}}</span>
                  <span>{{ucfirst($transaction->users_alamat->kodepos)}}</span>
                  <span>{{ucfirst($transaction->users_alamat->telepon)}}</span>
                  <span>{{ucfirst($transaction->users_alamat->kotum->nama)}}</span>
                  <span>{{ucfirst($transaction->users_alamat->kotum->provinsi->nama)}}</span>
  							</div>
  						</div>
	          </div>
  					<div class="checkout_confirm_orded_products">
  						<p class="checkout_title">Produk</p>
  						<ul class="cart-items" style="overflow-y:scroll;max-height:180px;">
                @php
                  $subtotal = 0;
                @endphp
                @foreach ($transaction->d_transaksis as $trans)
                <li class="clearfix">
                  <img class="cart_item_product" src="@isset($trans->cart->barang->barang_fotos){{asset(Storage::disk('public')->url($trans->cart->barang->barang_fotos->first()->url_photo))}}@endisset" alt="" />
                  <a href="" class="cart_item_title">{{$trans->cart->barang->nama}}</a>
                  <span class="cart_item_price">{{$trans->cart->qty}} × IDR {{number_format($trans->harga)}}</span>
                  @php
                    $subtotal = $subtotal+($trans->cart->qty*$trans->harga);
                  @endphp
                </li>
                @endforeach
  						</ul>
  					</div>
          </div>
          <div class="checkout_confirm_orded clearfix">
            <div class="checkout_confirm_orded_bordright clearfix">
              <div class="billing_information">
                <div class="checkout_confirm_orded_products">
                  <p class="checkout_title margbot10">Upload bukti pembayaran</p>
                </div>
                <div class="image">
                  <input type="file" name="url_bukti" class="dropify" data-default-file="{{isset($transaction->url_bukti) ? Storage::disk('public')->url($transaction->url_bukti) : ''}}"/>
                </div>
              </div>
              <div class="billing_information">
                <div class="checkout_confirm_orded_products">
                  <p class="checkout_title margbot10">Tambahan keterangan (Opsional)</p>
                </div>
                <textarea name="keterangan"></textarea>
              </div>
            </div>
          </div>
        </div>
			<div class="col-lg-3 col-md-3 padbot60">
				<div class="sidepanel widget_bag_totals your_order_block">
					<h3>Detail Pembayaran</h3>
					<table class="bag_total">
						<tr class="cart-subtotal clearfix">
							<th>Sub total</th>
							<td id="v-subtotal" data-subtotal="{{$subtotal}}">{{number_format($subtotal)}}</td>
						</tr>
						<tr class="cart-subtotal clearfix">
							<th>Ongkir</th>
							<td id="v-subtotal" data-shipping="{{$transaction->biaya_ongkir}}">{{number_format($transaction->biaya_ongkir)}}</td>
						</tr>
            <tr class="shipping clearfix">
							<th>Kode Unik</th>
							<td id="v-ship-price" data-shipping="{{$transaction->unique_harga}}">{{number_format($transaction->unique_harga)}}</td>
						</tr>
						<tr class="total clearfix">
							<th>Total</th>
							<td id="v-total" data-tota="{{$transaction->grand_total}}">{{number_format($transaction->grand_total)}}</td>
						</tr>
					</table>
					<button class="btn active" type="submit" >Konfirmasi</button>
				</div>
			</div>
		</div>
  </form>
	</div>
</section>
@endsection
@section('more-css')
<link rel="stylesheet" type="text/css" media="all" href="{{asset('assets/front/bower_components/dropify/dist/css/dropify.css')}}">
@endsection
@section('more-js')
<script src="{{asset('assets/front/bower_components/dropify/dist/js/dropify.min.js')}}"></script>
<script>
	$(function(){
    thisform.init();
	}), thisform = {
    init : function()
    {
      thisform.i_init();
    },
    i_init: function()
    {
      $(".dropify").dropify({
        messages: {
          "default": "Upload Image",
          replace: "Replace",
          remove: "Hapus",
          error: "Error"
        }
      });
    },
  }
</script>
@endsection
