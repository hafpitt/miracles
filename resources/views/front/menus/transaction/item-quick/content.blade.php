<div class="tover_view_page element_fade_in">
	<div class="tover_view_header clearfix">
		<p>Miracle | Quick view</p>
		<a id="tover_view_page_close" href="javascript:void(0);">Close<i>X</i></a>
	</div>

	<div class="clearfix">
    <div class="tovar_view_fotos clearfix">
			<div id="slider1" class="flexslider">
        <ul class="slides">
          @foreach ($barang_detail->barang_fotos as $foto)
            <li><a href="javascript:void(0);" ><img src="@isset($foto){{asset(Storage::disk('public')->url($foto->url_photo))}}@endisset" alt="" /></a></li>
          @endforeach
        </ul>
      </div>
			<div id="carousel1" class="flexslider">
        <ul class="slides">
          @foreach ($barang_detail->barang_fotos as $foto)
            <li><a href="javascript:void(0);" ><img src="@isset($foto){{asset(Storage::disk('public')->url($foto->url_photo))}}@endisset" alt="" /></a></li>
          @endforeach
        </ul>
      </div>
    </div>

		<div class="tovar_view_description">
			<form id="validate" method="post" action="{{url()->current()}}">
			{{ csrf_field() }}
	      <div class="tovar_view_title"><b>{{strtoupper($barang_detail->nama)}}</b></div>
				<div class="tovar_view_title">{{strtoupper($barang_detail->barang_kategori->nama)}}</div>
	      <div class="clearfix tovar_brend_price">
	        <div class="pull-left tovar_brend">Stok : {{strtoupper($barang_detail->stok)}}</div>
	        <div class="pull-right tovar_view_price">@isset($barang_detail->departments[0])IDR {{number_format($barang_detail->departments[0]->pivot->harga)}}@endisset</div>
	      </div>
				<div class="row">
					<div class="tovar_color_select">
						<p>Pilih Warna</p>
						@foreach ($barang_detail->barang_warnas as $warna)
						<input class="radio-color" style="display:none" type="radio" id="{{$warna->nama.$warna->id}}" name="id_barang_warna" value="{{$warna->id}}" {{ old('id_barang_warna') == $warna->id ? 'checked' : ''}}/>
						<label class="drinkcard-color" style="background-color:{{$warna->warna}};" for="{{$warna->nama.$warna->id}}"></label>
							<!-- <a id="{{$warna->nama.$warna->id}}" value="{{$warna->id}}" style="background-color:{{$warna->warna}}" class="color1 {{ Request::is('/') ? 'active' : '' }}" href="javascript:void(0);" name="warna"></a> -->
							<!-- <label>{{$warna->nama}}</label> -->
						@endforeach
						<!-- <a class="color2 active" href="javascript:void(0);" ></a>
						<a class="color3" href="javascript:void(0);" ></a>
						<a class="color4" href="javascript:void(0);" ></a> -->
					</div>
				</div>
	      <!-- <div class="tovar_size_select">
	        <div class="clearfix">
	          <p class="pull-left">Select SIZE</p>
	          <span>Size & Fit</span>
	        </div>
	        <a class="sizeXS" href="javascript:void(0);" >XS</a>
	        <a class="sizeS active" href="javascript:void(0);" >S</a>
	        <a class="sizeM" href="javascript:void(0);" >M</a>
	        <a class="sizeL" href="javascript:void(0);" >L</a>
	        <a class="sizeXL" href="javascript:void(0);" >XL</a>
	        <a class="sizeXXL" href="javascript:void(0);" >XXL</a>
	        <a class="sizeXXXL" href="javascript:void(0);" >XXXL</a>
	      </div> -->

					<div class="row">
						<div class="tovar_color_select">
							<div class="form-inline form-group">
								<label>Qty : </label>
								<input type="text" name="qty" value="{{ old('qty')}}" class="form-control">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="tovar_view_btn">
							<button class="add_bag" type="submit"><i class="fa fa-shopping-cart"></i>Beli</button>
						</div>
					</div>
	        <!-- <a class="add_bag" href="javascript:void(0);" ><i class="fa fa-shopping-cart"></i>Add to cart</a> -->
	        <!-- <a class="add_lovelist" href="javascript:void(0);" ><i class="fa fa-heart"></i></a> -->

      <!-- <div class="tovar_shared clearfix">
        <p>Share item with friends</p>
        <ul>
          <li><a class="facebook" href="javascript:void(0);" ><i class="fa fa-facebook"></i></a></li>
          <li><a class="twitter" href="javascript:void(0);" ><i class="fa fa-twitter"></i></a></li>
          <li><a class="linkedin" href="javascript:void(0);" ><i class="fa fa-linkedin"></i></a></li>
          <li><a class="google-plus" href="javascript:void(0);" ><i class="fa fa-google-plus"></i></a></li>
          <li><a class="tumblr" href="javascript:void(0);" ><i class="fa fa-tumblr"></i></a></li>
        </ul>
      </div> -->
			</form>
    </div>
	</div>
</div>
