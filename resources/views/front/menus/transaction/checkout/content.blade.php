@extends('front.layouts.default')
@section('title', 'Checkout')
@section('content')
<section class="breadcrumb parallax margbot30" style="background-image:url(@isset($front_all_top_parallax){{asset(Storage::disk('public')->url($front_all_top_parallax->url_photo))}}@endisset)"></section>
<section class="page_header">
  <div class="container border0 margbot0">
    <h3 class="pull-left"><b>Pembayaran</b></h3>
    <div class="pull-right">
      <a href="{{url('cart')}}" >Kembali ke Keranjang Belanja<i class="fa fa-angle-right"></i></a>
    </div>
  </div>
</section>
<section class="checkout_page">
	<div class="container">
    <form id="validate" action="{{url()->current()}}" method="post">
    {{ csrf_field() }}
  		<div class="row">
  			<div class="col-lg-9 col-md-9 padbot60">
  				<div class="checkout_confirm_orded clearfix">
  					<div class="checkout_confirm_orded_bordright clearfix">
  						<div class="billing_information">
                <div class="open-project-link">
                  <p class="checkout_title margbot10">Shipping address
                    <a data-toggle="tooltip" data-placement="top" data-original-title="Ganti Alamat" class="open-project tovar_view"
                        style="border:2px solid #ccc; font-size:12px; padding:4px; margin-left:15px;"
                      href="javascript:void(0)"
                      data-url="{{url('change-address')}}">
                    <span style="font-size:18px;" class="fa fa-pencil"></span>
                    </a>
                  </p>
                </div>
  							<div class="billing_information_content margbot40">
                  @isset($utamaalamat)
  								<span>{{ucfirst(strtolower($utamaalamat->nama))}}</span>
  								<span>{{ucfirst(strtolower($utamaalamat->alamat))}}</span>
  								<span>{{$utamaalamat->kodepos}} </span>
                  <span>{{$utamaalamat->telepon}} </span>
  								<span>{{ucfirst(strtolower($utamaalamat->kotum->nama))}}</span>
  								<span>{{ucfirst(strtolower($utamaalamat->kotum->provinsi->nama))}}</span>
  								<span>{{Auth::user()->email}}</span>
                  @endisset
  							</div>
  						</div>
  						<div class="payment_delivery" id="delivery">
                <div>
    							<p class="checkout_title margbot10">Bank</p>
                  <select class="form-control"  name="bank_user" data-parsley-trigger="change" required data-parsley-required-message="Mohon pilih salah satu bank!" data-parsley-error-message="Mohon pilih salah satu bank!">
                    @foreach ($bank as $value)
                    <option value="{{$value->id}}"> {{$value->bank->nama}} : {{$value->nama_pemilik}} : {{$value->no_rekening}} </option>
                    @endforeach
                  </select>
                </div>
                <div>
    							<p class="checkout_title margbot10">Kurir</p>
                  <select class="form-control" id="input-kurir" name="kurir" data-parsley-trigger="change" required data-parsley-required-message="Mohon pilih salah satu kurir!" data-parsley-error-message="Mohon pilih salah satu kurir!">
                    <option value="jne"> JNE </option>
                    <option value="pos"> POS </option>
                    <option value="tiki"> Tiki </option>
                  </select>
                </div>
  						</div>
	          </div>
  					<div class="checkout_confirm_orded_products" >
  						<p class="checkout_title">Produk</p>
  						<ul class="cart-items" style="overflow-y:scroll;max-height:180px;">
                @php
                  $grandtotal = 0;
                  $weight_total = 0;
                @endphp
                @foreach ($front_user_cart as $carts)
                <li class="clearfix">
                  <img class="cart_item_product" src="@isset($carts->barang->barang_fotos){{asset(Storage::disk('public')->url($carts->barang->barang_fotos->first()->url_photo))}}@endisset" alt="" />
                  <a href="{{url('items/'.$carts->barang->url_page)}}" class="cart_item_title">{{$carts->barang->nama}}</a>
                  <span class="cart_item_price">{{$carts->qty}} × IDR {{number_format($carts->barang->departments->first()->pivot->harga)}}</span>
                  @php
                    $grandtotal = $grandtotal = $grandtotal+($carts->qty*$carts->barang->departments->first()->pivot->harga);
                    $weight_total = $weight_total+$carts->barang->berat*$carts->qty;
                  @endphp
                </li>
                @endforeach
  						</ul>
  					</div>
		      </div>
          <div class="checkout_confirm_orded clearfix">
              <h3><b>Cara Pembayaran :</b></h3>
              <div class="billing_information_content margbot40">
                <span>1. Setelah klik <i>Konfirmasi Bayar</i>, segara lakukan pembayaran melalui transfer bank ke salah satu nomor rekening dibawah ini :</span>
                @foreach ($bankuser as $bankadmin)
                  <span><b>{{$bankadmin->bank->nama}} | {{$bankadmin->no_rekening}} a/n {{$bankadmin->nama_pemilik}}</b></span>
                @endforeach
                <span>2. Pastikan Anda melakukan transfer dana sesuai dengan Grand Total yang tertera.</span>
                <span>3. Setelah melakukan transfer dana, silahkan melakukan konfirmasi transfer di halaman <b style="text-transform:uppercase">transaksi saya</b> pada navigasi di atas (sertakan bukti transfer bila ada).</span>
                <span>4. Kami akan segera memproses pesanan Anda.</span>
                <span>5. Pantau transaksi Anda di halaman <b style="text-transform:uppercase">transaksi saya</b>. Kami juga akan mengirim email untuk memudahkan pemantauan.</span>
              </div>


          </div>
        </div>
			<div class="col-lg-3 col-md-3 padbot60">
				<div class="sidepanel widget_bag_totals your_order_block">
					<h3><b>DETAIL BELANJAANMU</b></h3>
					<table class="bag_total">
						<tr class="cart-subtotal clearfix">
							<th>Sub total</th>
							<td id="v-subtotal" data-subtotal="{{$grandtotal}}">{{number_format($grandtotal)}}</td>
						</tr>
            <tr class="cart-subtotal clearfix">
              <th>Total Berat</th>
              <td>{{number_format($weight_total)}} grams</td>
            </tr>
						<tr class="shipping clearfix">
							<th>Ongkir</th>
							<td id="v-ship-price" data-shipping="0">0</td>
						</tr>
            <tr class="cart-subtotal clearfix">
              <th>Kode Unik</th>
              <td id="v-uniq-code" data-uniq="{{$user->current_unique}}">{{number_format($user->current_unique)}}</td>
            </tr>
						<tr class="total clearfix">
							<th>Grand Total</th>
							<td id="v-total" data-tota="0">0</td>
						</tr>
					</table>
					<button class="btn active" type="submit" >Konfirmasi Bayar</button>
				</div>
			</div>
		</div>
  </form>
	</div>
</section>
<div id="modal-body" class="clearfix">
	<div id="tovar_content"></div>
	<div class="close_block"></div>
</div>
@endsection
@section('more-css')
<link rel="stylesheet" type="text/css" media="all" href="{{asset('assets/front/bower_components/HoldOn/src/download/HoldOn.min.css')}}">
@endsection
@section('more-js')
<script src="{{asset('assets/front/bower_components/HoldOn/src/download/HoldOn.min.js')}}"></script>
<script src="{{asset('assets/front/bower_components/jquery.serializeJSON/jquery.serializejson.js')}}"></script>
<script src="{{asset('assets/front/bower_components/parsley/dist/parsley.js')}}"></script>
<script src="{{asset('assets/front/bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js')}}"></script>
<script src="{{asset('assets/front/bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js')}}"></script>
<script>
	$(function(){
    thisform.init();
	}), thisform = {
    init : function()
    {
			$('#daftar-alamat').on('change',function(){
				thisform.select_change();
			});
      // alert('');
      thisform.kurir();
      $('#input-kurir').on('change',function(){
        thisform.kurir();
      })
    },
		select_change : function(){
			// $("#detail-alamat").hide();
			$('#a-nama').html($('#daftar-alamat').find(':selected').data('nama'));
			$('#a-pos').html($('#daftar-alamat').find(':selected').data('dept'));
			$('#a-telp').html($('#daftar-alamat').find(':selected').data('telp'));
			$('#a-alamat').html($('#daftar-alamat').find(':selected').data('alamat'));
			// $("#detail-alamat").delay(200).fadeIn();
		},
    calculate : function (){
      $sub=$('#v-subtotal').data('subtotal');
      $ship=$('#v-ship-price').data('shipping');
      $uniq=$('#v-uniq-code').data('uniq');

      // alert($sub);
      // alert($ship);
      $('#v-total').data('total',Number($sub)+Number($ship)+Number($uniq));
      $('#v-total').html(Inputmask.format(Number($sub)+Number($ship)+Number($uniq), {'alias': 'currency','prefix': '','digits': 0}));
    },
    kurir : function(){
      HoldOn.open({theme:"sk-cube-grid"});
      var form_data = JSON.stringify($("#validate").serializeJSON());
      $.ajax({
        headers :{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        type: 'POST',
        url: '{{url("checkout/kurir")}}',
        data: {'form' : form_data},
        dataType: 'text',
        success: function (data) {
          var jsonResponseText = $.parseJSON(data);
          $(".kurir-list").each(function(){
            $(this).remove();
          });
          $(".paket-list").each(function(){
            $(this).remove();
          });
          $result = '<div class="kurir-list"><p class="checkout_title margbot10">Service</p><select class="form-control" id="pilih-paket" name="paket" data-parsley-trigger="change" required data-parsley-required-message="Mohon pilih salah satu service!" data-parsley-error-message="Mohon pilih salah satu service!">';
          $.each( jsonResponseText['rajaongkir']['results']['0']['costs'] , function(i, item) {
            $result+=
            '<option value="'+item['service']+'" data-price="'+item['cost']['0']['value']+'"> '+item['service']+' | Rp. '+Inputmask.format(item['cost']['0']['value'], {'alias': 'currency','prefix': '','digits': 0})+' | '+item['cost']['0']['etd']+' day/s' +' </option>';
          });
          $result +='</select></div>';
          $('#delivery').append($result);

          $hidden = '<input class="paket-list" type="hidden" name="paket" value="'+$('#pilih-paket').find(':selected').val()+'"/>';
          $hidden += '<input class="paket-list" type="hidden" name="paket_harga" value="'+$('#pilih-paket').find(':selected').data('price')+'"/>';
          $('#delivery').append($hidden);
          $('#v-ship-price').data('shipping',$('#pilih-paket').find(':selected').data('price'));
          $('#v-ship-price').html(Inputmask.format($('#pilih-paket').find(':selected').data('price'), {'alias': 'currency','prefix': '','digits': 0}));
          thisform.calculate();

          $('#pilih-paket').on('change',function()
          {
            $(".paket-list").each(function(){
              $(this).remove();
            });
            $hidden = '<input class="paket-list" type="hidden" name="paket" value="'+$('#pilih-paket').find(':selected').val()+'"/>';
            $hidden += '<input class="paket-list" type="hidden" name="paket_harga" value="'+$('#pilih-paket').find(':selected').data('price')+'"/>';
            $('#delivery').append($hidden);
            $('#v-ship-price').data('shipping',$('#pilih-paket').find(':selected').data('price'));
            $('#v-ship-price').html(Inputmask.format($('#pilih-paket').find(':selected').data('price'), {'alias': 'currency','prefix': '','digits': 0}));
            thisform.calculate();
          });
          HoldOn.close();
        },
        // error : function(xhr, status, error) {
        //   var jsonResponseText = $.parseJSON(xhr.responseText);
        //   var errors = jsonResponseText;
        //   $(".alert").each(function(){
        //     $(this).remove();
        //   });
        //   $errorHtml = '<div class="alert bg-5 alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><ul><strong>There was error when add package</strong>';
        //   $.each( errors , function( key, value ) {
        //       $errorHtml += '<li>' +value[0] + '</li>';
        //   });
        //   $errorHtml +='</ul></div>';
        //   $('#container').prepend($errorHtml);
        //   $('html, body').animate({
        //     scrollTop: $("body").offset().top
        //   }, 1000);
        //   HoldOn.close();
        // }
      });
    }
  }
</script>
@endsection
