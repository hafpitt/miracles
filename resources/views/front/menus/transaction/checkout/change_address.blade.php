<div class="tover_view_page element_fade_in">
	<form method="post" action="{{url('change-address')}}">
		{{ csrf_field() }}
		<div class="tover_view_header clearfix">
			<p class="change-address">Ubah Pengiriman dan Alamat</p>
			<a id="tover_view_page_close" href="javascript:void(0);"> Close<i>X</i></a>
			<a class="btn btn-sm-change" href="{{url('add-address')}}"> Tambah Alamat Baru</a>
		</div>
		<div class="clearfix">
			<div class="tovar_view_price">
				<p>Daftar Alamat</p>
			</div>
			<div class="col-md-12">
				<div class="row">
					<select class="form-control" name="alamat" id="daftar-alamat" onchange="thisform.select_change();">
						@foreach ($alamat as $address)
							<option value="{{$address->id}}"  data-nama="{{$address->nama}}" data-alamat="{{$address->alamat}}" data-pos="{{$utamaalamat->kotum->kodepos}}" data-telp="{{$address->telepon}}">{{$address->nama}} - {{$address->nama}}</option>
						@endforeach
					</select>
				</div>
			</div>
			@isset($utamaalamat)
			<div class="col-md-12" id="detail-alamat">
				<div class="row">
					<div class="col-md-6">
						<p><h3>Nama : </h3><label id="a-nama">{{$utamaalamat->nama}}</label></p>
					</div>
					<div class="col-md-6">
						<p><h3>Alamat : </h3><label id="a-alamat">{{$utamaalamat->alamat}}</label></p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<p><h3>Kode Pos</h3><label id="a-pos">{{$utamaalamat->kotum->kodepos}}</label></p>
					</div>
					<div class="col-md-6">
						<p><h3>Telp</h3><label id="a-telp">{{$utamaalamat->telepon}}</label></p>
					</div>
				</div>
			</div>
			@endisset
			<div class="tovar_view_btn">
        <button class="add_bag" type="submit" ><i class="fa fa fa-pencil"></i>Pilih</button>
      </div>
		</div>
	</form>
</div>
