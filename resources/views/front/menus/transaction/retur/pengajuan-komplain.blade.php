@extends('front.layouts.default')
@section('title', 'Pengajuan Komplain')
@section('content')
<section class="breadcrumb parallax margbot30" style="background-image:url(@isset($front_all_top_parallax){{asset(Storage::disk('public')->url($front_all_top_parallax->url_photo))}}@endisset)"></section>
<section class="page_header">
  <div class="container">
    @if (session()->has('warning'))
      <div class="alert alert-danger alert-dismissable">
        <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <ul>
              {{ session()->get('warning') }}
          </ul>
      </div>
    @endif
    <h3 class="pull-left"><b>Pengajuan Komplain</b><br>{{$transaction->no_invoice}}</h3>
  </div>
</section>

<section class="checkout_page">
	<div class="container">
    <form id="validate" enctype="multipart/form-data" action="{{url()->current()}}" method="post">
    {{ csrf_field() }}
  		<div class="row">
  			<div class="col-lg-12 col-md-12 padbot60">
				  <div class="checkout_confirm_orded clearfix padbot60">

  					<div class="checkout_confirm_orded_products clearfix">
  						<p class="checkout_title">Produk</p>
  						<ul class="cart-items" style="overflow-y:scroll;max-height:180px;">
                @php
                  $subtotal = 0;
                @endphp
                @foreach ($transaction->d_transaksis as $trans)
                <li class="clearfix">
                  <img class="cart_item_product" src="@isset($trans->cart->barang->barang_fotos){{asset(Storage::disk('public')->url($trans->cart->barang->barang_fotos->first()->url_photo))}}@endisset" alt="" />
                  <a href="" class="cart_item_title">{{$trans->cart->barang->nama}}</a>
                  <span class="cart_item_price">{{$trans->cart->qty}} × IDR {{number_format($trans->harga)}}</span>
                  @php
                    $subtotal = $subtotal+($trans->cart->qty*$trans->harga);
                  @endphp
                </li>
                @endforeach
  						</ul>
  					</div>
            <div class="checkout_confirm_orded_bordright clearfix">
              <div class="billing_information">
                <div class="checkout_confirm_orded_products">
                  <p class="checkout_title margbot10">Upload foto barang</p>
                </div>
                <div class="image">
                  <input type="file" name="url_bukti" class="dropify" data-default-file="{{isset($transaction->url_complain) ? Storage::disk('public')->url($transaction->url_complain) : ''}}"/>
                </div>
              </div>
	          </div>
            <div class="checkout_confirm_orded_bordright clearfix">
              <div class="billing_information">
                <div class="checkout_confirm_orded_products">
                  <p class="checkout_title margbot10">Tambahan keterangan (Opsional)</p>
                </div>
                <textarea name="keterangan_komplain"></textarea>
              </div>
            </div>
            <button class="btn active" type="submit" >Ajukan Komplain</button>
          </div>
        </div>
		</div>
  </form>
	</div>
</section>
@endsection
@section('more-css')
<link rel="stylesheet" type="text/css" media="all" href="{{asset('assets/front/bower_components/dropify/dist/css/dropify.css')}}">
@endsection
@section('more-js')
<script src="{{asset('assets/front/bower_components/dropify/dist/js/dropify.min.js')}}"></script>
<script>
	$(function(){
    thisform.init();
	}), thisform = {
    init : function()
    {
      thisform.i_init();
    },
    i_init: function()
    {
      $(".dropify").dropify({
        messages: {
          "default": "Upload Image",
          replace: "Replace",
          remove: "Hapus",
          error: "Error"
        }
      });
    },
  }
</script>
@endsection
