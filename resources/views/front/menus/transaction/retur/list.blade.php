@extends('front.layouts.default')
@section('title', 'Transaksi Retur')
@section('content')
<section class="breadcrumb parallax margbot30" style="background-image:url(@isset($front_all_top_parallax){{asset(Storage::disk('public')->url($front_all_top_parallax->url_photo))}}@endisset)"></section>
<section class="page_header">
  <div class="container">
    <h3 class="pull-left"><b>Transaksi Retur</b></h3>
    <!-- <div class="pull-right">
      <a href="{{url('')}}" >Continue Shopping<i class="fa fa-angle-right"></i></a>
    </div> -->
  </div>
</section>

<section class="love_list_block">
  <div class="container">
    @if(session()->has('success'))
      <div class="alert alert-success alert-dismissable">
        <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{ session()->get('success') }}
      </div>
    @endif
    <div class="row">
      <div class="col-lg-12 col-md-12 padbot40">
        <table id="tabledata" class="table table-striped table-bordered" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th class="text-left">No Invoice</th>
              <th>Tanggal</th>
              <th class="text-center">Keterangan</th>
              <th class="text-center">Aksi</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($returlist2 as $value2)
              @foreach ($returlist as $value)
              <tr>
                <td class="text-left">{{$value->no_invoice}}<br>{{$value2->no_invoice}}</td>
                <td>{{$value->tgl_komplain}}<br>{{$value2->tgl}}</td>
                <td class="text-center">
                  @if ($value2->no_resi<>"")
                  <!-- substr($value2->no_invoice,3) =="ret" &&  -->
                    Anda mengirim dengan kurir {{strtoupper($value2->kurir)}}.
                    No Resi : {{$value2->no_resi}}
                  @elseif ($value->status_komplain=="")
                    Komplain Anda sedang diproses,<br>
                    pantau terus komplain Anda di halaman ini
                  @elseif ($value->status_komplain==1)
                    Komplain Anda telah dietujui
                  @endif
                </td>
                <td class="text-center">
                  @if ($value->status_komplain==1)
                    <a class="text-info" href="{{url('retur-entry/'.Crypt::encrypt($value->id))}}">Ajukan Retur</a>
                  @elseif (substr($value->no_invoice,3)=="ret")
                    pengret
                  @else
                    -
                  @endif
                </td>
              </tr>
              @endforeach
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</section>
@endsection
@section('more-css')
<link rel="stylesheet" type="text/css" media="all" href="{{asset('assets/front/bower_components/datatables/datatables.css')}}">

@endsection
@section('more-js')
<script src="{{asset('assets/front/bower_components/datatables/datatables.js')}}"></script>
<script>
$(function() {
    thisform.init();
}), thisform = {
  init: function()
  {
    thisform.d_init();
  },
  d_init: function() {
    var t = $("#tabledata").DataTable(),
        a = t.prev(".dt_colVis_buttons");
    if (t.length) {
        var e = t.DataTable();
    }
  },
};
</script>
@endsection
