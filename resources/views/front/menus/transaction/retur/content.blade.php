@extends('front.layouts.default')
@section('title', 'Retur Entry')
@section('content')
<!-- BREADCRUMBS -->
		<section class="breadcrumb parallax margbot30" style="background-image:url(@isset($front_all_top_parallax){{asset(Storage::disk('public')->url($front_all_top_parallax->url_photo))}}@endisset)"></section>
		<!-- //BREADCRUMBS -->
    <section class="page_header">
			<div class="container">
				@if(session()->has('success'))
		      <div class="alert alert-success alert-dismissable">
		        <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
		        {{ session()->get('success') }}
		      </div>
		    @endif
				@if (session()->has('warning'))
		      <div class="alert alert-danger alert-dismissable">
		        <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
		          <ul>
		              {{ session()->get('warning') }}
		          </ul>
		      </div>
		    @endif
				<h3 class="pull-left"><b>Retur for Invoice<br><i>{{$transaction->no_invoice}}</i></b></h3>
			</div>
		</section>

		<!-- SHOPPING BAG BLOCK -->
		<section class="shopping_bag_block">

			<!-- CONTAINER -->
			<div class="container">

				<!-- ROW -->
        <form id="validate" method="post" action="{{url()->current()}}">
        {{ csrf_field() }}
  				<div class="row">
						<div class="container">
							<h3 class="pull-left"><b>Detail Retur</b></h3>
						</div>
  					<!-- CART TABLE -->
  					<div class="col-lg-9 col-md-9 padbot40">
  						<table class="shop_table">
  							<thead>
  								<tr>
                    <!-- <th></th> -->
  									<!-- <th class="product-name">ID Cart</th>
  									<th class="product-quantity text-center">ID Barang</th> -->
  									<th class="product-subtotal text-center">Nama</th>
                    <th class="product-price text-center">Qty</th>
  									<th class="product-remove text-center">Qty Retur</th>
  								</tr>
  							</thead>
  							<tbody>
  								@foreach ($transaction->d_transaksis as $value)
  								<tr class="cart_item">
                    <!-- <td></td> -->
  									<!-- <td class="product-price text-left">{{$value->id_cart}}</td>
  									<td class="product-subtotal text-center">{{$value->cart->id_barang}}</td> -->
  									<td class="product-subtotal text-left">{{$value->cart->barang->nama}}</td>
                    <td class="product-price text-center">{{$value->cart->qty}}</td>
                    <td class="product-quantity">
                      <input type="text" name="cart[{{$value->id_cart}}]" class="form-control qty masked_input" class="md-input masked_input" data-inputmask="'alias': 'numeric', 'placeholder': '0', 'min':'0', 'max':'{{$value->cart->qty}}'" value=0>
                    </td>
  								</tr>
  								@endforeach
  							</tbody>
  						</table>
              <h3><b><u>Shipping Information :</u></b></h3>
    					<div class="checkout_confirm_orded_bordright clearfix">
    						<div class="billing_information">
                  <div class="open-project-link">
                    <p class="checkout_title margbot10">No Resi</p>
                  </div>
    							<div class="billing_information_content margbot40">
                    <input type="text" name="resi" class="form-control" class="md-input">
    							</div>
    						</div>
    						<div class="payment_delivery" id="delivery">
                  <div>
      							<p class="checkout_title margbot10">Kurir</p>
                    <select class="form-control" id="input-kurir" name="kurir" data-parsley-trigger="change" required data-parsley-required-message="Mohon pilih salah satu kurir!" data-parsley-error-message="Mohon pilih salah satu kurir!">
                      <option value="jne"> JNE </option>
                      <option value="pos"> POS </option>
                      <option value="tiki"> Tiki </option>
                    </select>
                  </div>
    						</div>
  	          </div>
  					</div><!-- //CART TABLE -->
            <div id="sidebar" class="col-lg-3 col-md-3 padbot50">
  						<div class="sidepanel widget_bag_totals">
  							<h3>Perhatian</h3>
                  <p>- Mohon hanya isi kolom <b>Qty Retur</b> sesuai barang yang akan diretur saja.</p>
                  <p>- Jangan lupa pilih <b>Kurir</b> dan isikan <b>Nomor Resi</b> pada kolom yang tersedia.</p>
                  <p>- Jika data sudah yakin benar, klik tombol <b>Sumbit</b> dibawah.</p>
  							<button class="btn active" type="submit">Submit</button>
  						</div><!-- //REGISTRATION FORM -->
  					</div><!-- //SIDEBAR -->
  				</div><!-- //ROW -->
        </form>
			</div><!-- //CONTAINER -->
		</section><!-- //SHOPPING BAG BLOCK -->

<!-- TOVAR MODAL CONTENT -->
<div id="modal-body" class="clearfix">
	<div id="tovar_content"></div>
	<div class="close_block"></div>
</div><!-- TOVAR MODAL CONTENT -->


@endsection
@section('more-js')
<script src="{{asset('assets/front/bower_components/parsley/dist/parsley.js')}}"></script>
<script src="{{asset('assets/front/bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js')}}"></script>
<script>
	$(function(){
    thisform.init();
	}), thisform = {
    init : function()
    {
      $(window).scrollTop(0);
      thisform.p_init();
      thisform.masked_inputs();
    },
		p_init : function()
		{
			var i = $("#validate");
      i.parsley();
			i.parsley().on("form:submit", function() {
        $(".masked_input").inputmask('remove');
			});
		},
    masked_inputs: function() {
      $maskedInput = $(".masked_input"),
      $maskedInput.length && $maskedInput.inputmask();
    },
  }
</script>
@endsection
