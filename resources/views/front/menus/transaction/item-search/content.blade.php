@extends('front.layouts.default')
@section('title', 'Kategori')
@section('content')
<section class="breadcrumb women parallax margbot30" style="background-image:url(@isset($front_all_top_parallax){{asset(Storage::disk('public')->url($front_all_top_parallax->url_photo))}}@endisset)">
  <div class="container">
    <h2></h2>
  </div>
</section>

<section class="shop">
  <div class="container">
    <div class="row">
      @if(session()->has('success'))
        <div class="alert alert-success alert-dismissable">
          <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
          {{ session()->get('success') }}
        </div>
      @endif
      @if ($errors->any())
        <div class="alert alert-danger alert-dismissable">
          <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <ul>
                @foreach ($errors->all() as $message)
                    <li>{{$message}}</li>
                @endforeach
            </ul>
        </div>
      @endif
      <div id="sidebar" class="col-lg-3 col-md-3 col-sm-3 padbot50">
        <div class="sidepanel widget_categories">
          <h3>Kategori Produk</h3>
          <ul>
            @foreach ($front_barang_kategori as $kategori)
              <li><a href="{{url('items/'.$kategori->url_page)}}" >{{$kategori->nama}}</a></li>
            @endforeach
          </ul>
        </div>
      </div>

      <div class="col-lg-9 col-sm-9 col-sm-9 padbot20">
        <div class="sorting_options clearfix">
          <div class="count_tovar_items">
            <!-- <p></p> -->
            <span> Hasil Pencarian</span>
          </div>

          <div id="toggle-sizes">
            <a class="view_box active" href="javascript:void(0);"><i class="fa fa-th-large"></i></a>
            <a class="view_full" href="javascript:void(0);"><i class="fa fa-th-list"></i></a>
          </div>
        </div>

        <div class="row shop_block">
          @foreach ($page_barang as $barang)
          <div class="tovar_wrapper col-lg-4 col-md-4 col-sm-6 col-xs-6 col-ss-12 padbot40">
            <div class="tovar_item clearfix">
              <div class="tovar_img">
                <a href="{{url('items/'.$barang->url_page)}}">
                  <div class="tovar_img_wrapper">
                    <img class="img" src="@isset($barang->barang_fotos[0]){{asset(Storage::disk('public')->url($barang->barang_fotos[0]->url_photo))}}@endisset" alt="" />
                    <img class="img_h" src="@isset($barang->barang_fotos[1]){{asset(Storage::disk('public')->url($barang->barang_fotos[1]->url_photo))}}@endisset" alt="" />
                  </div>
                </a>
                <div class="tovar_item_btns">
                  <div class="open-project-link">
                    <a class="open-project tovar_view" href="javascript:void(0);" data-url="{{url('items/q/'.$barang->url_page)}}" ><span>quick</span> view</a>
                  </div>
                </div>
              </div>
              <div class="tovar_description clearfix">
                <a class="tovar_title" href="{{url('items/'.$barang->url_page)}}" >{{$barang->nama}}</a>
                <span class="tovar_price">@isset($barang->departments[0])IDR {{number_format($barang->departments[0]->pivot->harga)}}@endisset</span>
              </div>
              <div class="tovar_content">{{$barang->keterangan}}</div>
            </div>
          </div>
          @endforeach
        </div>
        <hr>
        <div class="clearfix">
          <ul class="pagination">
            {!! $page_barang->render() !!}
          </ul>
        </div>
        <hr>
        <div class="padbot60 services_section_description">
          <p>Katalog | {{$barang->barang_kategori->nama}}</p>
          <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vel nisi dictum lorem feugiat euismod. Fusce dapibus sollicitudin ultrices. Nunc dignissim dui non leo cursus, vel consequat metus auctor. Nam non sollicitudin sapien. Vestibulum sit amet est odio. Aliquam sagittis ex at metus elementum volutpat. Pellentesque imperdiet a purus at porttitor. Aliquam tempor suscipit tristique. Praesent massa libero, malesuada vitae odio placerat, ultricies ultrices lacus.</span>
        </div>
      </div>
    </div>
  </div>
</section>

@endsection
