@extends('front.layouts.default')
@section('title', 'Register')
@section('content')
<section class="my_account parallax" style="background-image:url(@isset($cms){{asset(Storage::disk('public')->url($cms->url_photo))}}@endisset)">
  <div class="container">
    <div class="my_account_block clearfix">
      <div class="login">
        <h2>register</h2>
        <form class="login_form" id="validate" action="{{url()->current()}}" method="post">
          @if ($errors->any())
            <div class="alert alert-danger alert-dismissable">
              <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <ul>
                    @foreach ($errors->all() as $message)
                        <li>{{$message}}</li>
                    @endforeach
                </ul>
            </div>
          @endif
          {{ csrf_field() }}
          <input type="text" name="nama" placeholder="Nama"  data-parsley-trigger="change" value="{{ old('nama')}}" required/>
          <input type="email" name="email" placeholder="Email"  data-parsley-trigger="change" value="{{ old('email')}}" required/>

          <div class="checkout_form_input country" required>
            <select class="basic" name="gender">
              <option value="Male" >Male</option>
              <option value="Female" >Female</option>
            </select>
          </div>

          <input type="text" name="notelp" placeholder="No Telepon"  data-parsley-trigger="change" value="{{ old('notelp')}}" data-inputmask="'mask': '9', 'repeat': 14, 'greedy' : false" data-parsley-minlength="10" class="masked_input"  data-inputmask-showmaskonhover="false" required/>
          <input type="password" id="password" name="password" class="" placeholder="Password" data-parsley-trigger="change" required/>
          <input type="password" name="password_confirmation" class="last" placeholder="Password Confirmation" data-parsley-trigger="change" required  data-parsley-equalto="#password"/>
          <div class="center">
            <input type="submit" value="Register" style="color:#fff;"/>
          </div>
        </form>
      </div>
      <div class="new_customers">
        <h2>already have an account?</h2>
        <p>Sign in to Miracle now</p>
        <!-- <ul>
          <li><a href="javascript:void(0);" >—  Online Order Status</a></li>
          <li><a href="javascript:void(0);" >—  Love List</a></li>
          <li><a href="javascript:void(0);" >—  Sign up to receive exclusive news and private sales</a></li>
          <li><a href="javascript:void(0);" >—  Place Test Orders</a></li>
          <li><a href="javascript:void(0);" >—  Quick and easy checkout</a></li>
        </ul> -->
        <div class="center"><a class="btn active" href="{{url('login')}}" >Sign In</a></div>
      </div>
    </div>
    <div class="my_account_note center">
      <!-- HAVE A QUESTION? <b>1 800 888 02828</b> -->
    </div>
  </div>
</section>
@endsection

@section('more-js')
<script src="{{asset('assets/front/bower_components/parsley/dist/parsley.js')}}"></script>
<script src="{{asset('assets/front/bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js')}}"></script>
<script>
	$(function(){
    thisform.init();
	}), thisform = {
    init : function()
    {
      $(window).scrollTop(0);
      thisform.p_init();
      thisform.masked_inputs();
    },
		p_init : function()
		{
			var i = $("#validate");
      i.parsley();
			i.parsley().on("form:submit", function() {
        $(".masked_input").inputmask('remove');
			});
		},
    masked_inputs: function() {
      $maskedInput = $(".masked_input"),
      $maskedInput.length && $maskedInput.inputmask();
    },
  }
</script>
@endsection
@section('more-css')
<style>
.checkout_form_input{
  margin-bottom:14px;
}
div.fancy-select{
  text-transform:none;
  font-weight:400;
  font-size: 12px;
}
div.fancy-select div.trigger{
  border:2px solid #e9e9e9;
  width:530px;
}
</style>
@endsection
