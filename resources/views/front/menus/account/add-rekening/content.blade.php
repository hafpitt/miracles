@extends('front.layouts.default')
@section('title', 'Add Bank Account')
@section('content')
<!-- CHECKOUT PAGE -->
<section class="breadcrumb parallax margbot30" style="background-image:url(@isset($front_all_top_parallax){{asset(Storage::disk('public')->url($front_all_top_parallax->url_photo))}}@endisset)"></section>

<section class="page_header">

  <!-- CONTAINER -->
  <div class="container border0 margbot0">
    <h3 class="pull-left"><b>Add New Bank Account</b></h3>
  </div><!-- //CONTAINER -->
</section><!-- //PAGE HEADER -->

<section class="checkout_page">

  <!-- CONTAINER -->
  <div class="container">

    <!-- CHECKOUT BLOCK -->
    <div class="checkout_block">

      @if(session()->has('success'))
        <div class="alert alert-success alert-dismissable">
          <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
          {{ session()->get('success') }}
        </div>
      @endif
      @if ($errors->any())
        <div class="alert alert-danger alert-dismissable">
          <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <ul>
                @foreach ($errors->all() as $message)
                    <li>{{$message}}</li>
                @endforeach
            </ul>
        </div>
      @endif

      <form class="checkout_form clearfix" action="{{url()->current()}}" method="post">
        {{ csrf_field() }}
        <div class="checkout_form_input last_name">
          <label>Nama Pemilik Rekening <span class="color_red">*</span></label>
          <input type="text" name="pemilik" value="{{ old('nama_pemilik', isset($bankuser->nama_pemilik) ? $bankuser->nama_pemilik : '' ) }}" placeholder="" />
        </div>

        <div class="checkout_form_input phone">
          <label>Nomor Rekening <span class="color_red">*</span></label>
          <input type="text" name="norek" value="{{ old('no_rekening', isset($bankuser->no_rekening) ? $bankuser->no_rekening : '' ) }}" placeholder="" />
        </div>

        <div class="checkout_form_input last country">
          <label>Nama Bank <span class="color_red">*</span></label>
          <select class="basic" name="bank">
            <!-- <option value="">Pilih Kota</option> -->
            @foreach ($bank as $value)
            <option value='{{$value->id}}' {{(old('id_bank', isset($bankuser->id_bank) ? $bankuser->id_bank : '' ) == $value->id ? 'selected':'') }}>{{$value->nama}}</option>
            @endforeach
          </select>
        </div>

        <div class="clear"></div>

        <div class="checkout_form_note">All fields marked with (<span class="color_red">*</span>) are required</div>

        @isset($bankuser)
          <input type="hidden" name="id" value="{{Crypt::encryptString($bankuser->id)}}"/>
        @endisset

        <div class="tovar_view_btn pull-right">
    			<button class="add_bag" type="submit" ><i class="fa fa fa-pencil"></i>Submit</button>
    		</div>
      </form>
    </div><!-- //CHECKOUT BLOCK -->
  </div><!-- //CONTAINER -->
</section><!-- //CHECKOUT PAGE -->
@endsection
