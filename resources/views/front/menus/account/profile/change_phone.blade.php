<div class="tover_view_page element_fade_in">
	<form method="post" action="{{url('change-phone/'.Crypt::encryptString($users->id))}}">
		{{ csrf_field() }}
		<div class="tover_view_header clearfix">
			<p>Ubah Nomor Handphone</p>
			<a id="tover_view_page_close" href="javascript:void(0);"> Close<i>X</i></a>
		</div>

		<div class="clearfix">
			<p>Masukkan Nomor Telepon Baru <input type="text" name="telepon"/></p>
		</div>
		<div class="tovar_view_btn">
			<button class="add_bag" type="submit" ><i class="fa fa fa-pencil"></i>Ubah</button>
		</div>
	</form>
</div>
