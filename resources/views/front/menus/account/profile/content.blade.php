@extends('front.layouts.default')
@section('title', 'Profile')
@section('content')
<!-- BREADCRUMBS -->
<section class="breadcrumb parallax margbot30" style="background-image:url(@isset($front_all_top_parallax){{asset(Storage::disk('public')->url($front_all_top_parallax->url_photo))}}@endisset)"></section>
<!-- //BREADCRUMBS -->


<!-- PAGE HEADER -->
<section class="page_header">

  <!-- CONTAINER -->
  <div class="container border0 margbot0">
    <h3 class="pull-left"><b>{{Auth::user()->nama}}, ini adalah detail Profilmu</b></h3>
  </div><!-- //CONTAINER -->
</section><!-- //PAGE HEADER -->


<!-- CHECKOUT PAGE -->
<section class="checkout_page">

  <!-- CONTAINER -->
  <div class="container">

    <!-- CHECKOUT BLOCK -->
    <div class="checkout_block">

      @if(session()->has('success'))
        <div class="alert alert-success alert-dismissable">
          <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
          {{ session()->get('success') }}
        </div>
      @endif
      @if ($errors->any())
        <div class="alert alert-danger alert-dismissable">
          <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <ul>
              @foreach ($errors->all() as $message)
                <li>{{$message}}</li>
              @endforeach
            </ul>
        </div>
      @endif

      <form class="checkout_form clearfix" action="javascript:void(0);" method="get">

        <h3><b>Biodata</b></h3>

        <!-- <section class="shopping_bag_block"> -->
          <div class="checkout_form_input">
            <label>Nama</label>
            <input type="text" name="name" value="{{$users->nama}}" placeholder="" disabled style="background-color:#ccc;"/>
          </div>

          <div class="checkout_form_input E-mail">
            <label>e-mail</label>
            <input type="text" name="email" value="{{$users->email}}" placeholder="" disabled style="background-color:#ccc;"/>
          </div>

          <!-- <div class="checkout_form_input country">
            <label>Gender</label>
            <select class="basic">
              <option value="">Please Select</option>
              <option>male</option>
              <option>female</option>
            </select>
          </div> -->

          <div class="checkout_form_input E-mail">
            <label>Jenis Kelamin</label>
            <input type="text" name="gender" value="{{$users->gender}}" placeholder="" disabled style="background-color:#ccc;"/>
          </div>

          <!-- <div class="checkout_form_input country">
            <label>Gender</label>
            <select class="basic" name="gender">
              <option value='{{$users->id}}'>{{$users->gender}}</option>
            </select>
          </div> -->

          <div class="checkout_form_input last phone">
            <label>Nomor Telepon</label>
            <b>{{$users->notelp}}</b>
            <div class="open-project-link pull-right">
              <a data-toggle="tooltip" data-placement="top" data-original-title="Edit No HP" class="open-project tovar_view"
                style="text-transform:lowercase; border:2px solid #ccc; font-size:12px; padding:4px; margin-left:15px;"
                href="javascript:void(0)"
                data-url="{{url('change-phone')}}">
              <span style="font-size:18px;" class="fa fa-pencil"></span>
              </a>
            </div>
          </div>
        <!-- </section> -->
        <div class="clear"></div>

        <hr class="clear">

        <h3><b>Daftar Alamat Pengiriman</b>
          @if ($users->users_alamats->count() >= 1)
            <a data-toggle="tooltip" data-placement="top" data-original-title="Tambah Alamat" class="pull-right fa fa-plus btn btn-sm-change" href="{{url('add-address')}}"></a>
          @endif
        </h3>
        <!-- SHOPPING BAG BLOCK -->
    		<section class="shopping_bag_block">

    			<!-- CONTAINER -->
    			<div class="container">

    				<!-- ROW -->
    				<div class="row">

    					<!-- CART TABLE -->
    					<div class="col-lg-11 col-md-11 padbot40">

    						<table class="table type2">
    							<thead>
    								<tr style="text-transform:uppercase;">
                      <th>Nama Penerima</th>
    									<th>Alamat</th>
    									<th>Provinsi</th>
    									<th>Kota</th>
    									<!-- <th class="product-subtotal">Kecamatan</th> -->
                      <th>Kode Pos</th>
    									<th  class="product-remove ">Action</th>
    									<!-- <th class="product-remove"></th> -->
    								</tr>
    							</thead>
    							<tbody>
                    @if ($users->users_alamats->count() >= 1)
                    @foreach ($users->users_alamats as $value)
    								<tr class="cart_item2">
                      <td style="text-transform:uppercase;">{{$value->nama_penerima}}</td>
    									<td>
    										<a style="text-transform:uppercase; font-weight:900" href="javascript:void(0);">{{$value->nama}}</a>
    										<ul class="variation">
    											<li class="variation-Color"><span>{{$value->alamat}}</span></li>
    										</ul>
    									</td>
    									<td>{{$value->kotum->provinsi->nama}}</td>
    									<td>{{$value->kotum->nama}}</td>
    									<!-- <td class="product-subtotal"></td> -->
                      <td>{{$value->kodepos}}</td>
    									<!-- <td  class="product-remove ">
    										<a href=""
    											<i style="border:none;" class="fa fa-pencil"></i>
    										</a>
                        <a href="" >
                          <i style="border:none;" class="fa fa-trash-o"></i>
                        </a>
    									</td> -->
                      <td class="product-remove text-center">
    											<a data-toggle="tooltip" data-placement="top" data-original-title="Edit"  href="{{url('add-address?i='.Crypt::encryptString($value->id))}}">
    												<i style="font-size:25px; border:none;" class="fa fa-pencil"></i>
    											</a>
    										<a data-toggle="tooltip" data-placement="top" data-original-title="Hapus" href="{{url('remove-address?i='.Crypt::encryptString($value->id))}}" ><i style="font-size:25px; border:none;" class="fa fa-trash-o"></i></a>
    									</td>
    								</tr>
                    @endforeach
                    @endif
    							</tbody>
    						</table>
                @if (!$users->users_alamats->count() == 1)
                <div class="title center">
                  <h2>your shipping address is empty</h2>
                  <a class="btn btn-sm-change" href="{{url('add-address')}}"> Tambah Alamat Pengiriman Baru</a>
                </div>
                @endif
    					</div><!-- //CART TABLE -->
    				</div><!-- //ROW -->
    			</div><!-- //CONTAINER -->
    		</section><!-- //SHOPPING BAG BLOCK -->

        <hr class="clear">

        <h3><b>Daftar Rekening Bank</b>
          @if ($users->banks->count() >= 1)
            <a data-toggle="tooltip" data-placement="top" data-original-title="Tambah Akun Bank" class="pull-right fa fa-plus btn btn-sm-change" href="{{url('add-bank')}}"></a>
          @endif
        </h3>
        <!-- SHOPPING BAG BLOCK -->
    		<section class="shopping_bag_block">

    			<!-- CONTAINER -->
    			<div class="container">

    				<!-- ROW -->
    				<div class="row">

    					<!-- CART TABLE -->
    					<div class="col-lg-11 col-md-11 padbot40">

    						<table class="table type2">
    							<thead>
    								<tr style="text-transform:uppercase;">
                      <th>Image</th>
    									<th>Nama Pemilik Rekening</th>
    									<th>Nomor Rekening</th>
    									<th>Nama Bank</th>
                      <!-- <th class="product-subtotal">Kode Pos</th> -->
    									<th class="product-remove ">Action</th>
    									<th></th>
    								</tr>
    							</thead>
    							<tbody>
                    @if ($users->banks->count() >= 1)
                    @foreach ($users->banks as $value)
    								<tr class="cart_item2">
                      <td><a href="javascript:void(0)" ><img src="@isset($value->url_photo){{asset(Storage::disk('public')->url($value->url_photo))}}@endisset" width="100px" alt="" /></a></td>
    									<td>
                        <ul style="text-transform:uppercase; font-weight:900">
                          {{$value->pivot->nama_pemilik}}
                        </ul>
    										<!-- <ul class="variation">
    											<li class="variation-Color"><span>Jalan jalan</span></li>
    										</ul> -->
    									</td>
    									<td>{{$value->pivot->no_rekening}}</td>
    									<td>{{$value->nama}}</td>
                      <!-- <td class="product-subtotal">67312</td> -->
    									<!-- <td class="product-remove ">
    										<a href="{{url('add-bank?i='.Crypt::encryptString($value->pivot->id))}}"
    											<i style="border:none;" class="fa fa-pencil"></i>
    										</a>
                        <a href="{{url('remove-bank?i='.Crypt::encryptString($value->pivot->id))}}" >
                          <i style="border:none;" class="fa fa-trash-o"></i>
                        </a>
    									</td> -->
                      <td class="product-remove text-center">
    											<a data-toggle="tooltip" data-placement="top" data-original-title="Edit" href="{{url('add-bank?i='.Crypt::encryptString($value->pivot->id))}}">
    												<i style="font-size:25px; border:none;" class="fa fa-pencil"></i>
    											</a>
    										<a data-toggle="tooltip" data-placement="top" data-original-title="Hapus" href="{{url('remove-bank?i='.Crypt::encryptString($value->pivot->id))}}" ><i style="font-size:25px; border:none;" class="fa fa-trash-o"></i></a>
    									</td>
    								</tr>
                    @endforeach
                    @endif
    							</tbody>
    						</table>
                @if (!$users->banks->count() == 1)
                <div class="title center">
                  <h2>your bank account is empty</h2>
                  <a class="btn btn-sm-change" href="{{url('add-bank')}}"> Tambah Rekening Baru</a>
                </div>
                @endif
    					</div><!-- //CART TABLE -->
    				</div><!-- //ROW -->
    			</div><!-- //CONTAINER -->
    		</section><!-- //SHOPPING BAG BLOCK -->
        <div class="clear"></div>

        <!-- <div class="checkout_form_note">All fields marked with (<span class="color_red">*</span>) are required</div> -->

        <!-- <a class="btn active pull-right" href="checkout2.html" >Continue</a> -->
      </form>
    </div><!-- //CHECKOUT BLOCK -->
  </div><!-- //CONTAINER -->
</section><!-- //CHECKOUT PAGE -->
@endsection
