@extends('front.layouts.default')
@section('title', 'Login')
@section('content')
<section class="my_account parallax" style="background-image:url(@isset($cms){{asset(Storage::disk('public')->url($cms->url_photo))}}@endisset)">
  <div class="container">
    <div class="my_account_block clearfix">
      <div class="login">
        <h2>LOGIN TO MIRACLE</h2>
        <form class="login_form" id="validate" action="{{url()->current()}}" method="post">

          @if(session()->has('success'))
            <div class="alert alert-success alert-dismissable">
              <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
              {{ session()->get('success') }}
            </div>
          @endif

          @if(session()->has('unverified'))
            <div class="alert alert-info alert-dismissable">
              <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
              {{ session()->get('unverified') }}
            </div>
          @endif
          @if(session()->has('verified'))
            <div class="alert alert-success alert-dismissable">
              <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
              {{ session()->get('verified') }}
            </div>
          @endif
          @if ($errors->any())
            <div class="alert alert-danger alert-dismissable">
              <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <ul>
                    @foreach ($errors->all() as $message)
                        <li>{{$message}}</li>
                    @endforeach
                </ul>
            </div>
          @endif
          {{ csrf_field() }}
          <input type="email" name="email" placeholder="email" value="{{ old('email')}}" required/>
          <input type="password" name="password" class="last" placeholder="Password" value="" required/>
          <div class="clearfix">
            <!-- <div class="pull-left">
              <input type="checkbox" name="rememberme" />
              <label for="rememberme">Keep me signed</label>
            </div> -->
            <div class="pull-right">
              <a class="forgot_pass" href="{{url('forgot')}}" >Forgot password?</a>
            </div>
          </div>
          <div class="center">
            <input type="submit" value="Login" style="color:#fff;"/>
          </div>
        </form>
      </div>
      <div class="new_customers">
        <h2>NEW CUSTOMERS</h2>
        <p>Register to MIRACLE today</p>
        <!-- <ul>
          <li><a href="javascript:void(0);" >—  Online Order Status</a></li>
          <li><a href="javascript:void(0);" >—  Love List</a></li>
          <li><a href="javascript:void(0);" >—  Sign up to receive exclusive news and private sales</a></li>
          <li><a href="javascript:void(0);" >—  Place Test Orders</a></li>
          <li><a href="javascript:void(0);" >—  Quick and easy checkout</a></li>
        </ul> -->
        <div class="center"><a class="btn active" href="{{url('register')}}" >create new account</a></div>
      </div>
    </div>
    <div class="my_account_note center">
      <!-- HAVE A QUESTION? <b>1 800 888 02828</b> -->
    </div>
  </div>
</section>
@endsection
@section('more-js')
<script src="{{asset('assets/front/bower_components/parsley/dist/parsley.js')}}"></script>
<script src="{{asset('assets/front/bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js')}}"></script>
<script>
	$(function(){
    thisform.init();
	}), thisform = {
    init : function()
    {
      $(window).scrollTop(0);
      thisform.p_init();
      thisform.masked_inputs();
    },
		p_init : function()
		{
			var i = $("#validate");
      i.parsley();
			i.parsley().on("form:submit", function() {
        $(".masked_input").inputmask('remove');
			});
		},
    masked_inputs: function() {
      $maskedInput = $(".masked_input"),
      $maskedInput.length && $maskedInput.inputmask();
    },
  }
</script>
@endsection
