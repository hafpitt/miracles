@extends('front.layouts.default')
@section('title', 'Reset Password')
@section('content')
<section class="my_account parallax" style="background-image:url(@isset($cms){{asset(Storage::disk('public')->url($cms->url_photo))}}@endisset)">
  <div class="container">
    <div class="my_account_block clearfix">
        <center><h2>RESET PASSWORD</h2></center>
        <form class="login_form" id="validate" action="{{url()->current()}}" method="post">
          @if(session()->has('success'))
            <div class="alert alert-success alert-dismissable">
              <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
              {{ session()->get('success') }}
            </div>
          @endif
          @if(session()->has('unverified'))
            <div class="alert alert-info alert-dismissable">
              <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
              {{ session()->get('unverified') }}
            </div>
          @endif
          @if(session()->has('verified'))
            <div class="alert alert-success alert-dismissable">
              <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
              {{ session()->get('verified') }}
            </div>
          @endif
          @if ($errors->any())
            <div class="alert alert-danger alert-dismissable">
              <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <ul>
                    @foreach ($errors->all() as $message)
                        <li>{{$message}}</li>
                    @endforeach
                </ul>
            </div>
          @endif
          {{ csrf_field() }}
            <center>
              <input style="width:260px" type="password" id="newpassword" name="newpassword" class="" placeholder="Password" data-parsley-trigger="change" required/><br>
              <input style="width:260px" type="password" name="newpassword_confirmation" class="last" placeholder="Password Confirmation" data-parsley-trigger="change" required  data-parsley-equalto="#newpassword"/>
            </center>
          <div class="clearfix">
          </div>
          <div class="center">
            <input type="submit" value="Submit" style="color:#fff;"/>
          </div>
        </form>
    </div>
  </div>
</section>
@endsection
@section('more-js')
<script src="{{asset('assets/front/bower_components/parsley/dist/parsley.js')}}"></script>
<script src="{{asset('assets/front/bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js')}}"></script>
<script>
	$(function(){
    thisform.init();
	}), thisform = {
    init : function()
    {
      $(window).scrollTop(0);
      thisform.p_init();
      thisform.masked_inputs();
    },
		p_init : function()
		{
			var i = $("#validate");
      i.parsley();
			i.parsley().on("form:submit", function() {
        $(".masked_input").inputmask('remove');
			});
		},
    masked_inputs: function() {
      $maskedInput = $(".masked_input"),
      $maskedInput.length && $maskedInput.inputmask();
    },
  }
</script>
@endsection
