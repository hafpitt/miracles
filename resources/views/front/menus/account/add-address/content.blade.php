@extends('front.layouts.default')
@section('title', 'Add Address')
@section('content')
<!-- CHECKOUT PAGE -->
<section class="breadcrumb parallax margbot30" style="background-image:url(@isset($front_all_top_parallax){{asset(Storage::disk('public')->url($front_all_top_parallax->url_photo))}}@endisset)"></section>

<section class="page_header">

  <!-- CONTAINER -->
  <div class="container border0 margbot0">
    <h3 class="pull-left"><b>Add Shipping Address</b></h3>
  </div><!-- //CONTAINER -->
</section><!-- //PAGE HEADER -->

<section class="checkout_page">

  <!-- CONTAINER -->
  <div class="container">

    <!-- CHECKOUT BLOCK -->
    <div class="checkout_block">

      @if(session()->has('success'))
        <div class="alert alert-success alert-dismissable">
          <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
          {{ session()->get('success') }}
        </div>
      @endif
      @if ($errors->any())
        <div class="alert alert-danger alert-dismissable">
          <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <ul>
              @foreach ($errors->all() as $message)
                <li>{{$message}}</li>
              @endforeach
            </ul>
        </div>
      @endif

      <form class="checkout_form clearfix" action="{{url()->current()}}" method="post">
        {{ csrf_field() }}
        <div class="checkout_form_input last_name">
          <label>Nama Penerima <span class="color_red">*</span></label>
          <input type="text" name="penerima" value="{{ old('nama_penerima', isset($useralamat->nama_penerima) ? $useralamat->nama_penerima : '' ) }}" placeholder="" />
        </div>

        <div class="checkout_form_input last phone">
          <label>Telepon <span class="color_red">*</span></label>
          <input type="text" name="telepon" value="{{ old('telepon', isset($useralamat->telepon) ? $useralamat->telepon : '' ) }}" placeholder="" />
        </div>

        <div class="clear"></div>

        <hr class="clear">

        <div class="checkout_form_input first_name">
          <label>Simpan Alamat Sebagai <span class="color_red">*</span></label>
          <input type="text" name="nama" value="{{ old('nama', isset($useralamat->nama) ? $useralamat->nama : '' ) }}" placeholder="" />
        </div>

        <div class="checkout_form_input country">
          <label>Kota <span class="color_red">*</span></label>

          <select class="selectpicker" data-live-search="true" name="kota">
            @foreach ($kota as $value)
            <option data-tokens="{{$value->nama}}" value="{{$value->id}}" {{(old('kota_id', isset($useralamat->kota_id) ? $useralamat->kota_id : '' ) == $value->id ? 'selected':'') }}>{{$value->nama}}</option>
            @endforeach
          </select>

        </div>

        <div class="checkout_form_input last postcode">
          <label>Kodepos <span class="color_red">*</span></label>
          <input type="text" name="kodepos" value="{{ old('kodepos', isset($useralamat->kodepos) ? $useralamat->kodepos : '' ) }}" placeholder="" />
        </div>

        <div class="checkout_form_input2 adress">
          <label>Street Adress <span class="color_red">*</span></label>
          <input type="text" name="alamat" value="{{ old('alamat', isset($useralamat->alamat) ? $useralamat->alamat : '' ) }}" placeholder="" />
        </div>

        <div class="clear"></div>

        <div class="checkout_form_note">All fields marked with (<span class="color_red">*</span>) are required</div>

        @isset($useralamat)
          <input type="hidden" name="id" value="{{Crypt::encryptString($useralamat->id)}}"/>
        @endisset

        <div class="tovar_view_btn pull-right">
    			<button class="add_bag" type="submit" ><i class="fa fa fa-pencil"></i>Submit</button>
    		</div>
      </form>
    </div><!-- //CHECKOUT BLOCK -->
  </div><!-- //CONTAINER -->
</section><!-- //CHECKOUT PAGE -->
@endsection
