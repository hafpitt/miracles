@extends('front.layouts.default')
@section('title', 'Home')
@section('content')
<section id="home" class="padbot0">
  <div class="flexslider top_slider">
    <ul class="slides">
      @foreach ($slider as $image)
      <li class="slide1" style="background-image:url(@isset($image){{asset(Storage::disk('public')->url($image->url_photo))}}@endisset)">
        <div class="container">
          <div class="flex_caption1">
            <p class="title1 captionDelay2 FromTop">{{$image->nama}}</p>
            <p class="title2 captionDelay3 FromTop">{{$image->keterangan}}</p>
          </div>
          <!-- <a class="flex_caption2" href="javascript:void(0);" ><div class="middle">view<span>product</span>details</div></a> -->
        </div>
      </li>
      @endforeach
      <!-- <li class="slide2">
        <div class="container">
          <div class="flex_caption1">
            <p class="title1 captionDelay2 FromTop">mega sell</p>
            <p class="title2 captionDelay3 FromTop">last week of sales</p>
          </div>
          <a class="flex_caption2" href="javascript:void(0);" ><div class="middle">view<span>product</span>details</div></a>
        </div>
      </li>
      <li class="slide3">
        <div class="container">
          <div class="flex_caption1">
            <p class="title1 captionDelay2 FromTop">mega sell</p>
            <p class="title2 captionDelay3 FromTop">last week of sales</p>
          </div>
          <a class="flex_caption2" href="javascript:void(0);" ><div class="middle">view<span>product</span>details</div></a>
        </div>
      </li> -->
    </ul>
  </div>
</section>

  <section class="tovar_section">
    @if(session()->has('success'))
      <div class="alert alert-success alert-dismissable">
        <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{ session()->get('success') }}
      </div>
    @endif
    @if ($errors->any())
      <div class="alert alert-danger alert-dismissable">
        <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <ul>
              @foreach ($errors->all() as $message)
                  <li>{{$message}}</li>
              @endforeach
          </ul>
      </div>
    @endif
    <div class="container">
      <h2>produk</h2>
      <div class="jcarousel-wrapper">
        <div class="jCarousel_pagination">
          <a href="javascript:void(0);" class="jcarousel-control-prev" ><i class="fa fa-angle-left"></i></a>
          <a href="javascript:void(0);" class="jcarousel-control-next" ><i class="fa fa-angle-right"></i></a>
        </div>
        <div class="jcarousel" data-appear-top-offset='-100' data-animated='fadeInUp'>
          <ul>
            @foreach ($barang as $value)
              <li>
                <div class="tovar_item_new">
                  <a href="{{url('items/'.$value->url_page)}}">
                    <div class="tovar_img">
                      <img src="@isset($value->barang_fotos[0]){{asset(Storage::disk('public')->url($value->barang_fotos[0]->url_photo))}}@endisset" alt="" />
                      <div class="open-project-link">
                        <!-- <a class="open-project tovar_view" href="{{url('items/'.$value->url_page)}}">View Detail</a> -->
                        <div class="open-project-link"><a class="open-project tovar_view" href="javascript:void(0);" data-url="{{url('items/q/'.$value->url_page)}}" >quick view</a></div>
                      </div>
                    </div>
                  </a>
                  <div class="tovar_description clearfix">
                    <a class="tovar_title" href="{{url('items/'.$value->url_page)}}">{{$value->nama}}</a>
                    <span class="tovar_price">@isset($value->departments[0])IDR {{number_format($value->departments[0]->pivot->harga)}}@endisset</span>
                  </div>
                </div>
              </li>
            @endforeach
          </ul>
        </div>
      </div>
    </div>
  </section>

  <hr class="container">
  <!-- <section class="services_section">
    <div class="container">

      <div class="row">
        <div class="col-lg-6 col-md-6 padbot60 services_section_description" data-appear-top-offset='-100' data-animated='fadeInLeft'>
          <p>We empower WordPress developers with design-driven themes and a classy experience their clients will just love</p>
          <span>Gluten-free quinoa selfies carles, kogi gentrify retro marfa viral. Odd future photo booth flannel ethnic pug, occupy keffiyeh synth blue bottle tofu tonx iphone. Blue bottle 90′s vice trust fund gastropub gentrify retro marfa viral</span>
        </div>

        <div class="col-lg-6 col-md-6 padbot30" data-appear-top-offset='-100' data-animated='fadeInRight'>

          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-3 col-xs-6 col-ss-12 padbot30">
              <div class="service_item">
                <div class="clearfix"><i class="fa fa-tablet"></i><p>Responsive Theme</p></div>
                <span>Thundercats squid single-origin coffee YOLO selfies disrupt, master cleanse semiotics letterpress typewriter.</span>
              </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-3 col-xs-6 col-ss-12 padbot30">
              <div class="service_item">
                <div class="clearfix"><i class="fa fa-comments-o"></i><p>Free Support</p></div>
                <span>Thundercats squid single-origin coffee YOLO selfies disrupt, master cleanse semiotics letterpress typewriter.</span>
              </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-3 col-xs-6 col-ss-12 padbot30">
              <div class="service_item">
                <div class="clearfix"><i class="fa fa-eye"></i><p>Retina Ready</p></div>
                <span>Thundercats squid single-origin coffee YOLO selfies disrupt, master cleanse semiotics letterpress typewriter.</span>
              </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-3 col-xs-6 col-ss-12 padbot30">
              <div class="service_item">
                <div class="clearfix"><i class="fa fa-cogs"></i><p>Easy Customize</p></div>
                <span>Thundercats squid single-origin coffee YOLO selfies disrupt, master cleanse semiotics letterpress typewriter.</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section> -->

@endsection
