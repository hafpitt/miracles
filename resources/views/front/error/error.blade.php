@extends('front.layouts.default')
@section('title', $exception->getStatusCode())
@section('content')
<section class="page404 parallax">
  <div class="container">
    <div class="wrapper404">
      <h1>Error {{$exception->getStatusCode()}}</h1>
      <h2>ERROR</h2>
      <p>The page you are looking for does not exist. Return to the home page</p>
      <a class="btn btn-white" href="{{url('/')}}" >Return to home page</a>
    </div>
  </div>
</section>
@endsection
