<!DOCTYPE html>
<html lang="en">
<head>
  <title>@yield('title') | Miracle</title>
  @include('front.includes.meta')
  @yield('more-css','')
  <style>
  
  </style>
</head>
<body>
  <div id="preloader"><img src="{{asset('assets/front/images/preloader.gif')}}" alt="" /></div>
  <div class="preloader_hide">
  	<div id="page">
  		@include('front.includes.header')
      	@yield('content')
      @include('front.includes.footer')
  	</div>
  </div>

  <div id="modal-body" class="clearfix">
  	<div id="tovar_content"></div>
  	<div class="close_block"></div>
  </div>
  @include('front.includes.jsscript')
  @yield('more-js','')
</body>
</html>
