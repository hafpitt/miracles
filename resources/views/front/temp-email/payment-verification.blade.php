<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <title>Payment Verification | Miracle</title>
  <style type="text/css" media="screen">

    /* Force Hotmail to display emails at full width */
    .ExternalClass {
      display: block !important;
      width: 100%;
    }

    /* Force Hotmail to display normal line spacing */
    .ExternalClass,
    .ExternalClass p,
    .ExternalClass span,
    .ExternalClass font,
    .ExternalClass td,
    .ExternalClass div {
      line-height: 100%;
    }

    body,
    p,
    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
      margin: 0;
      padding: 0;
    }

    body,
    p,
    td {
      font-family: Arial, Helvetica, sans-serif;
      font-size: 15px;
      color: #333333;
    }

    h1 {
      font-size: 24px;
      font-weight: normal;
      line-height: 24px;
    }

    body,
    p {
      margin-bottom: 0;
      -webkit-text-size-adjust: none;
      -ms-text-size-adjust: none;
    }

    img {
      line-height: 100%;
      outline: none;
      text-decoration: none;
      -ms-interpolation-mode: bicubic;
    }

    a img {
      border: none;
    }

    .background {
      background-color: #ffffff;
    }

    table.background {
      margin: 0;
      padding: 0;
      width: 100% !important;
    }

    .block-img {
      display: block;
      line-height: 0;
    }

    a,
    a:link {
      color: #2A5DB0;
      text-decoration: underline;
    }

    table td {
      border-collapse: collapse;
    }

    td {
      vertical-align: top;
      text-align: left;
    }

    .wrap {
      max-width: 600px;
      min-width: 240px;
      margin: 0 auto;
    }

    .wrap-cell {
      padding-top: 30px;
      padding-bottom: 30px;
    }

    .header-cell,
    .body-cell,
    .footer-cell {
      padding-left: 20px;
      padding-right: 20px;
    }

    .header-cell {
      background-color: #6495ED;
      font-size: 24px;
      color: #ffffff;
      padding-top: 10px;
      padding-bottom: 10px;
    }

    .body-cell {
      background-color: #ffffff;
      padding-top: 30px;
      padding-bottom: 34px;
    }

    .progress .progress-marker {
      width: 24px;
    }

    .progress .progress-line-pad,
    .progress .progress-line {
      width: 8px;
    }

    .progress.progress-completed .progress-line {
      background-color: #99BEFF;
    }

    .progress.progress-active .progress-line {
      background-color: #cdcdcd;
    }

    .progress .progress-title,
    .progress .progress-text {
      padding-left: 12px;
    }

    .progress.progress-completed .progress-title {
      color: #99BEFF;
    }

    .progress.progress-pending .progress-title {
      color: #b2b2b2;
    }

    .progress .progress-text {
      padding-bottom: 20px;
    }

    .footer-cell {
      background-color: #6495ED;
      text-align: center;
      font-size: 13px;
      padding-top: 30px;
      padding-bottom: 30px;
    }

    .force-full-width {
      width: 100% !important;
    }

  </style>
  <style type="text/css" media="only screen and (max-width: 600px)">
    @media only screen and (max-width: 600px) {
      body[class*="background"],
      table[class*="background"],
      td[class*="background"] {
        background: #eeeeee !important;
      }

      table[class="wrap"] { width: 100% !important; }
      td[class="wrap-cell"] { padding-top: 0 !important; padding-bottom: 0 !important; }
    }
  </style>
</head>

<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0" class="background">
  <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" class="background">
    <tr>
      <td align="center" valign="top" class="background" width="100%">
        <center>
          <table cellpadding="0" cellspacing="0" width="600" class="wrap">
            <tr>
              <td valign="top" class="wrap-cell">
                <table cellpadding="0" cellspacing="0" class="force-full-width">
                  <tr>
                    <td valign="top" class="header-cell">
                      <img width="196" height="60" src="http://www.rottely.com/assets/img/logo_edit2-1.png">
                    </td>
                  </tr>
                  <tr>
                    <td valign="top" class="body-cell">
                      <table cellpadding="0" cellspacing="0" width="100%" >
                        <tr>
                          <td valign="top" style="padding-bottom:20px">
                            Hi {{$htransaksi->user->nama}}, here's your buying progress
                          </td>
                        </tr>
                      </table>
                      <table cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                          <td valign="top">
                            <table cellpadding="0" cellspacing="0" width="100%" class="progress progress-completed">
                              <tr>
                                <td valign="top" class="progress-marker">
                                  <img class="block-img" alt="" src="{{asset('assets/front/images/ico/progress_a.png')}}" width="24" height="41">
                                </td>
                                <td valign="top" class="progress-title">
                                  <h1>Order Confirmation</h1>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top">
                            <table cellpadding="0" cellspacing="0" width="100%" class="progress progress-active">
                              <tr>
                                <td valign="top" colspan="3" class="progress-marker">
                                  <img alt="" class="block-img" src="{{asset('assets/front/images/ico/progress_b.png')}}" width="24" height="35">
                                </td>
                                <td valign="top" class="progress-title">
                                  <h1>Order Processing</h1>
                                </td>
                              </tr>
                              <tr>
                                <td valign="top" class="progress-line-pad">
                                </td>
                                <td valign="top" class="progress-line">
                                </td>
                                <td valign="top" class="progress-line-pad">
                                </td>
                                <td valign="top" class="progress-text">
                                  Thanks! We have received IDR {{$htransaksi->terbayar}} for invoice {{$htransaksi->no_invoice}}. We are processing your order now. Your delivery will be sent to you shortly.
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top">
                            <table cellpadding="0" cellspacing="0" width="100%" class="progress progress-pending">
                              <tr>
                                <td valign="top" class="progress-marker">
                                  <img alt="" class="block-img" src="{{asset('assets/front/images/ico/progress_c.gif')}}" width="24" height="31">
                                </td>
                                <td valign="top" class="progress-title">
                                  <h1>Send Confirmation</h1>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      </table>

                    </td>
                  </tr>
                  <tr>
                    <td valign="top" class="footer-cell">
                      Miracle<br>
                      an Awesome Handphone Acc
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </center>
      </td>
    </tr>
  </table>

</body>
</html>
