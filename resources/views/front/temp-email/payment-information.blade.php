<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <title>Payment Information | Miracle</title>
  <style type="text/css" media="screen">

    /* Force Hotmail to display emails at full width */
    .ExternalClass {
      display: block !important;
      width: 100%;
    }

    /* Force Hotmail to display normal line spacing */
    .ExternalClass,
    .ExternalClass p,
    .ExternalClass span,
    .ExternalClass font,
    .ExternalClass td,
    .ExternalClass div {
      line-height: 100%;
    }

    body,
    p,
    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
      margin: 0;
      padding: 0;
    }

    body,
    p,
    td {
      font-family: Segoe UI,Roboto,Oxygen,Ubuntu,Cantarell,Fira Sans,Droid Sans,Helvetica Neue,sans-serif;
      font-size: 15px;
      color: #333333;
      line-height: 1.5em;
    }

    h1 {
      font-size: 24px;
      font-weight: normal;
      line-height: 24px;
    }

    body,
    p {
      margin-bottom: 0;
      -webkit-text-size-adjust: none;
      -ms-text-size-adjust: none;
    }

    img {
      outline: none;
      text-decoration: none;
      -ms-interpolation-mode: bicubic;
    }

    a img {
      border: none;
    }

    .background {
      background-color: #ffffff;
    }

    table.background {
      margin: 0;
      padding: 0;
      width: 100% !important;
    }

    .block-img {
      display: block;
      line-height: 0;
    }

    a {
      color: white;
      text-decoration: none;
    }

    a,
    a:link {
      color: #2A5DB0;
      text-decoration: underline;
    }

    table td {
      border-collapse: collapse;
    }

    td {
      vertical-align: top;
      text-align: left;
    }

    .wrap {
      width: 600px;
    }

    .wrap-cell {
      padding-top: 5px;
      padding-bottom: 5px;
    }

    .header-cell,
    .body-cell,
    .footer-cell {
      padding-left: 20px;
      padding-right: 20px;
    }

    .header-cell {
      background-color: #6495ED;
      font-size: 18px;
      /* color: #000; */
      padding-top: 5px;
    }

    .body-cell {
      background-color: #eee;
      padding-top: 30px;
      padding-bottom: 34px;
    }

    .footer-cell {
      background-color: #6495ED;
      text-align: center;
      font-size: 13px;
      padding-top: 30px;
      padding-bottom: 30px;
    }

    .card {
      width: 400px;
      margin: 0 auto;
    }

    .data-heading {
      text-align: right;
      padding: 10px;
      background-color: #eee;
      font-weight: bold;
    }

    .data-value {
      text-align: left;
      padding: 10px;
      background-color: #eee;
    }

    .force-full-width {
      width: 100% !important;
    }

  </style>
  <style type="text/css" media="only screen and (max-width: 600px)">
    @media only screen and (max-width: 600px) {
      body[class*="background"],
      table[class*="background"],
      td[class*="background"] {
        background: #eeeeee !important;
      }

      table[class="card"] {
        width: auto !important;
      }

      td[class="data-heading"],
      td[class="data-value"] {
        display: block !important;
      }

      td[class="data-heading"] {
        text-align: left !important;
        padding: 10px 10px 0;
      }

      table[class="wrap"] {
        width: 100% !important;
      }

      td[class="wrap-cell"] {
        padding-top: 0 !important;
        padding-bottom: 0 !important;
      }
    }
  </style>
</head>

<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0" bgcolor="" class="background">
  <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" class="background">
    <tr>
      <td align="center" valign="top" width="100%" class="background">
        <center>
          <table cellpadding="0" cellspacing="0" width="600" class="wrap">
            <tr style="background: #6495ED;">
              <td valign="top" class="wrap-cell">
                <table cellpadding="0" cellspacing="0" class="force-full-width">
                  <tr>
                    <table >
                      <tr >
                        <td>
                          <table>
                            <tr>
                              <td >
                                <img width="196" height="60" src="http://www.rottely.com/assets/img/logo_edit2-1.png" alt="logo">
                              </td>
                            </tr>
                          </table>
                        </td>
                        <td width="100%" style="text-align:right; vertical-align:bottom;">
                          <i>{{$user->h_transaksis[0]->no_invoice}}</i>
                        </td>
                      </tr>
                    </table>
                  </tr>
                  <tr>
                    <td valign="top" class="body-cell">
                      <table cellpadding="0" cellspacing="0" width="100%" bgcolor="#eee">
                        <tr>
                          <td valign="top" style="padding-bottom:15px; background-color:#eee;">
                            <h1>Thank you for ordering from our store!</h1><br>
                            Hi {{$user->nama}},
                            we're getting your order ready to be processed and shipped. We will notify you when it has been sent through email.
                          </td>
                        </tr>
                      </table>
                      Please make your payment to:
                      @foreach ($bankuser as $value)
                      <table cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                          <td>
                            <table cellpadding="0" cellspacing="0" width="82" height="82">
                              <tr>
                                <td style="text-align:left;">
                                  <img width="80" height="50" style="display:block" src="@isset($value->bank->url_photo){{asset(Storage::disk('public')->url($value->bank->url_photo))}}@endisset">
                                </td>
                              </tr>
                            </table>
                          </td>
                          <td  width="450" valign="middle" style="padding-bottom:20px; padding-left:20px; background-color:#eee; vertical-align:middle;">
                            Nomor Rekening : {{$value->no_rekening}}<br>
                            Nama : {{$value->nama_pemilik}}<br>
                            Cabang : {{$value->cabang}}<br>
                          </td>
                        </tr>
                      </table>
                      @endforeach
                      <table cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                          <td>
                            <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#eee">
                              <tr>
                                <td style="width:200px;background:#6495ED;">
                                </td>
                                <td width="360" style="background-color:#eee; font-size:0; line-height:0;">&nbsp;</td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#eee">
                            <tr>
                              <td align="center" style="padding:20px 0;">
                                <center>
                                  <table cellspacing="0" cellpadding="0" class="card">
                                    <tr>
                                      <td style="border:1px solid #6495ED;">
                                        <table cellspacing="0" cellpadding="20" width="100%">
                                          <tr>
                                            <td>
                                              <table cellpadding="0" cellspacing="0" width="100%" bgcolor="#eee">
                                                <tr>
                                                  <td width="150" class="data-heading">
                                                    Total Tagiahan :
                                                  </td>
                                                  <td class="data-value" style="text-align:right;">
                                                    {{number_format($user->h_transaksis[0]->d_transaksis[0]->subtotal)}}
                                                  </td>
                                                </tr>
                                                <tr>
                                                  <td width="150" class="data-heading">
                                                    Ongkos Kirim :
                                                  </td>
                                                  <td class="data-value" style="text-align:right;">
                                                    {{number_format($user->h_transaksis[0]->biaya_ongkir)}}
                                                  </td>
                                                </tr>
                                                <tr>
                                                  <td width="150" class="data-heading">
                                                    Kode Unik :
                                                  </td>
                                                  <td class="data-value" style="text-align:right;">
                                                     {{number_format($user->h_transaksis[0]->unique_harga)}}
                                                  </td>
                                                </tr>
                                                <tr>
                                                  <td width="150" class="data-heading" style="border-top:1px solid #000;border-bottom:1px solid #000;">
                                                    Grand Total :
                                                  </td>
                                                  <td class="data-value" style="text-align:right; border-top:1px solid #000; border-bottom:1px solid #000;">
                                                    <b>Rp. {{number_format($user->h_transaksis[0]->grand_total)}}</b>
                                                  </td>
                                                </tr>
                                              </table>
                                            </td>
                                          </tr>
                                        </table>
                                      </td>
                                    </tr>
                                  </table>
                                </center>
                              </td>
                            </tr>
                          </table>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" style="padding-bottom:15px; background-color:#eee;">
                            Make sure you transfer with the same nominal as shown above.
                            And please confirm your payment by using this link, <a href="http://localhost/miracles/history">here</a>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                  <tr>
                    <td valign="top" class="footer-cell">
                      Miracle<br>
                      an Awesome Handphone Acc
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </center>
      </td>
    </tr>
  </table>

</body>
</html>
