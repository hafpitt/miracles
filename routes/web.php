<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::get('login', 'Back\Account\Back_Login@Index')->middleware('guest:web_admin')->name('login.front');
// BACK END
// Route::get('ve','Front\Account\Front_Register@IndexV');
// Route::post('ta', function(){
//   echo "ASD";
// });
// Route::get('ta',function(){
//   echo url('submit-review/');
// });
Route::group(['prefix' => 'testmail'],function(){
  Route::get('verification', 'TestMail\TestMail@IndexVerification');
  Route::get('forgot', 'TestMail\TestMail@ForgotPassword');
  Route::get('pinformation', 'TestMail\TestMail@IndexPayInformation');
  Route::get('pconfirmation/{id}', 'TestMail\TestMail@PaymentConfirmation');
  Route::get('pverification/{id}', 'TestMail\TestMail@PaymentVerification');
  Route::get('sendmailconfirmation/{id}', 'TestMail\TestMail@SendMailConfirmation');
});
Route::group(['prefix' => '/', 'middleware'=> ['web','revalidate','cart','queue']], function () {
  Route::get('login', 'Front\Account\Front_Login@Index')->middleware('guest')->name('login.front');
  Route::post('login','Front\Account\Front_Login@Authenticate')->middleware('guest');
  Route::get('verify/{uid}', 'Front\Account\Front_Register@Verification');
  Route::get('forgot', 'Front\Account\Front_Login@IndexFo')->middleware('guest');
  Route::post('forgot', 'Front\Account\Front_Login@IndexF')->middleware('guest');
  Route::get('reset-password/{uid}', 'Front\Account\Front_Login@IndexR');
  Route::post('reset-password/{uid}', 'Front\Account\Front_Login@ResetPass');
  // Route::get('forgot-pass/{uid}', 'Front\Account\Front_Login@ResetPass');

  Route::get('complain/{id}', 'Front\Transaction\Front_Retur@IndexComplain')->middleware('auth');
  Route::post('complain/{id}', 'Front\Transaction\Front_Retur@PostComplain')->middleware('auth');
  Route::get('retur-entry/{id}', 'Front\Transaction\Front_Retur@IndexEntry')->middleware('auth');
  Route::post('retur-entry/{id}', 'Front\Transaction\Front_Retur@IndexConfirmRetur')->middleware('auth');
  Route::get('retur-list', 'Front\Transaction\Front_Retur@IndexList')->middleware('auth')->name('front.list.retur');
  // Route::options('list', 'Front\Transaction\Front_Retur@DataTableSSP');

  Route::get('register', 'Front\Account\Front_Register@Index')->middleware('guest');
  Route::post('register','Front\Account\Front_Register@Register')->middleware('guest');
  Route::get('logout','Front\Account\Front_Login@Logout')->middleware('auth');

  Route::get('cart', 'Front\Transaction\Front_Cart@Index')->middleware('auth');
  Route::get('remove-cart/{cart}', 'Front\Transaction\Front_Cart@RemoveCart')->middleware('auth');
  Route::get('change-qty/{cart}', 'Front\Transaction\Front_Cart@IndexChange')->middleware('auth');
  Route::post('change-qty/{cart}', 'Front\Transaction\Front_Cart@PostChange')->middleware('auth');

  Route::get('history', 'Front\Transaction\Front_History@Index')->middleware('auth');

  Route::get('checkout', 'Front\Transaction\Front_Checkout@Index')->middleware('auth');
  Route::post('checkout', 'Front\Transaction\Front_Checkout@Checkout')->middleware('auth');
  Route::post('checkout/kurir', 'Front\Transaction\Front_Checkout@GetKurir')->middleware('auth');

  Route::get('change-address', 'Front\Transaction\Front_Checkout@IndexChange')->middleware('auth');
  Route::post('change-address', 'Front\Transaction\Front_Checkout@PostChange')->middleware('auth');

  Route::get('confirm-payment/{id}', 'Front\Transaction\Front_History@IndexConfirmPayment')->middleware('auth');
  Route::post('confirm-payment/{id}', 'Front\Transaction\Front_History@PostConfirmPayment')->middleware('auth');
  Route::get('confirm-receive/{id}', 'Front\Transaction\Front_History@PostConfirmReceive')->middleware('auth');

  Route::get('add-address', 'Front\Account\Front_User_Address@IndexEntry')->middleware('auth');
  Route::post('add-address', 'Front\Account\Front_User_Address@Post')->middleware('auth');
  Route::get('remove-address', 'Front\Account\Front_User_Address@Destroy')->middleware('auth');

  Route::get('add-bank', 'Front\Account\Front_User_Bank@Index')->middleware('auth');
  Route::post('add-bank', 'Front\Account\Front_User_Bank@Post')->middleware('auth');
  Route::get('remove-bank', 'Front\Account\Front_User_Bank@Destroy')->middleware('auth');

  Route::get('profile', 'Front\Account\Front_Profile@Index')->middleware('auth')->name('profile.index_profile');

  Route::get('change-phone', 'Front\Account\Front_Profile@IndexChange')->middleware('auth');
  Route::post('change-phone/{users}', 'Front\Account\Front_Profile@PostChange')->middleware('auth');

  Route::group(['prefix' => 'items'], function () {
    Route::get('s', 'Front\Transaction\Front_Item_Search@Index');
    Route::get('q/{kategori}/{name}', 'Front\Transaction\Front_Item_Detail@IndexQuick');
    Route::post('q/{kategori}/{name}', 'Front\Transaction\Front_Item_Detail@AddToCartq')->middleware('auth');
    Route::get('{kategori}', 'Front\Transaction\Front_Item_Category@Index');
    Route::get('{kategori}/{name}', 'Front\Transaction\Front_Item_Detail@Index');
    Route::post('{kategori}/{name}', 'Front\Transaction\Front_Item_Detail@AddToCart')->middleware('auth');
  });
  Route::group(['prefix' => 'submit-review','middleware'=> 'auth'], function () {
    Route::post('{kategori}/{name}', 'Front\Transaction\Front_Item_Detail@PostReview');
  });
  Route::get('/', 'Front\Home\Front_Home@Index')->name('home.front');
  Route::get('faq', 'Front\Faq\Front_Faq@Index');

});

Route::group(['prefix' => 'back-end', 'middleware'=> ['web','revalidate']], function () {
  Route::get('login', 'Back\Account\Back_Login@Index')->middleware('guest:web_admin')->name('login.back');
  Route::post('login','Back\Account\Back_Login@Authenticate')->middleware('guest:web_admin');
  Route::get('logout', 'Back\Account\Back_Login@Logout');
  Route::get('dashboard', 'Back\Dashboard\Back_Dashboard@Index')->middleware('auth:web_admin')->name('dashboard.back');

  Route::get('/',  function (){return redirect(route('dashboard.back'));})->middleware('auth:web_admin');

  Route::group(['prefix' => 'broadcast-subscriber', 'middleware' => ['auth:web_admin']], function () {
    Route::get('/', 'Back\Broadcast\Back_Broadcast@Index')->name('broadcast.back');
    Route::post('/', 'Back\Broadcast\Back_Broadcast@Post');
  });

  // Route::group(['prefix' => 'retur'], function () {
  //   Route::get('entry', 'Back\Transaction\Back_Retur@IndexEntry')->name('entry.retur');
  //   Route::post('entry', 'Back\Transaction\Back_Retur@Post');
  //   Route::get('list', 'Back\Transaction\Back_Retur@IndexList')->name('list.retur');
  //   Route::options('list', 'Back\Transaction\Back_Retur@DataTableSSP');
  //   Route::options('entry/invoice', 'Back\Transaction\Back_Retur@DataTableSSPSearchInvoice');
  //   Route::options('entry/barang', 'Back\Transaction\Back_Retur@DataTableSSPLoadBarang');
  //   Route::get('destroy', 'Back\Transaction\Back_Retur@Destroy')->name('destroy.retur');
  // });

  Route::group(['prefix' => 'report'], function () {
    Route::get('sisa-stok', 'Back\Report\Back_Report@SisaStokDataTable');
    // Route::options('sisa-stok', 'Back\Report\Back_Report@SisaStokDataTable');
    Route::get('piutang-belum-lunas', 'Back\Report\Back_Report@PiutangBelumLunas');
    // Route::group(['prefix' => 'piutang-belum-lunas'], function () {
    //   Route::get('/', 'Back\Report\Back_Report@PiutangBelumLunas');
    // });
    Route::group(['prefix' => 'tingkat-penjualan-perbulan'], function () {
      Route::get('/', 'Back\Report\Back_Report@PenjualanPerBulan');
    });
    Route::group(['prefix' => 'tingkat-penjualan-perhari'], function () {
      Route::get('/', 'Back\Report\Back_Report@PenjualanPerHari');
    });
    Route::group(['prefix' => 'total-register-user-pertgl'], function () {
      Route::get('/', 'Back\Report\Back_Report@TotalRegisterUserPerTgl');
    });
  });

  Route::group(['prefix' => 'cms', 'middleware' => ['auth:web_admin']], function () {
    Route::get('contact', 'Back\CMS\Back_Contact@IndexEntry')->name('set.contact');
    Route::post('contact', 'Back\CMS\Back_Contact@Post');

    Route::group(['prefix' => 'page-image'], function () {
      Route::get('all-top-parallax', 'Back\CMS\All_Top_Parallax@IndexEntry')->name('set.home_discount');
      Route::post('all-top-parallax', 'Back\CMS\All_Top_Parallax@Post');

      Route::get('about', 'Back\CMS\Back_About@IndexEntry')->name('set.about');
      Route::post('about', 'Back\CMS\Back_About@Post');

      Route::get('gallery', 'Back\CMS\Back_Gallery@IndexEntry')->name('set.gallery');
      Route::post('gallery', 'Back\CMS\Back_Gallery@Post');

      Route::get('register-user', 'Back\CMS\Back_Register_User@IndexEntry')->name('set.register_user');
      Route::post('register-user', 'Back\CMS\Back_Register_User@Post');

      Route::get('login-user', 'Back\CMS\Back_Login_User@IndexEntry')->name('set.register_user');
      Route::post('login-user', 'Back\CMS\Back_Login_User@Post');

      Route::get('profile-user', 'Back\CMS\Back_Profile_User@IndexEntry')->name('set.profile_user');
      Route::post('profile-user', 'Back\CMS\Back_Profile_User@Post');

      Route::get('cart', 'Back\CMS\Back_Cart@IndexEntry')->name('set.cart');
      Route::post('cart', 'Back\CMS\Back_Cart@Post');

      Route::get('check-out', 'Back\CMS\Back_Check_Out@IndexEntry')->name('set.check_out');
      Route::post('check-out', 'Back\CMS\Back_Check_Out@Post');

      Route::get('schedule', 'Back\CMS\Back_Schedule@IndexEntry')->name('set.schedule');
      Route::post('schedule', 'Back\CMS\Back_Schedule@Post');

    });

    Route::group(['prefix' => 'home'], function () {
      Route::get('entry', 'Back\CMS\Back_Home@IndexEntry')->name('entry.cms_home');
      Route::post('entry', 'Back\CMS\Back_Home@Post');
      Route::get('list', 'Back\CMS\Back_Home@IndexList')->name('list.cms_home');
      Route::options('list', 'Back\CMS\Back_Home@DataTableSSP');
      Route::get('destroy', 'Back\CMS\Back_Home@Destroy')->name('destroy.cms_home');
    });

    Route::group(['prefix' => 'footer'], function () {
      Route::get('entry', 'Back\CMS\Back_Footer@IndexEntry')->name('entry.cms_footer');
      Route::post('entry', 'Back\CMS\Back_Footer@Post');
      Route::get('list', 'Back\CMS\Back_Footer@IndexList')->name('list.cms_footer');
      Route::options('list', 'Back\CMS\Back_Footer@DataTableSSP');
      Route::get('destroy', 'Back\CMS\Back_Footer@Destroy')->name('destroy.cms_footer');
    });
  });

  Route::group(['prefix' => 'transaction', 'middleware' => ['auth:web_admin']], function () {
    Route::group(['prefix' => 'piutang'], function () {
      Route::get('entry', 'Back\Transaction\Back_Piutang@IndexEntry')->name('entry.piutang');
      Route::post('entry', 'Back\Transaction\Back_Piutang@Post');
      Route::get('list', 'Back\Transaction\Back_Piutang@IndexList')->name('list.piutang');
      Route::options('list', 'Back\Transaction\Back_Piutang@DataTableSSP');
      Route::get('destroy', 'Back\Transaction\Back_Piutang@Destroy')->name('destroy.piutang');
    });
    Route::group(['prefix' => 'confirmation'], function () {
      Route::get('entry', 'Back\SendConfirmation\Back_Confirmation@IndexEntry')->name('entry.resi');
      Route::post('entry', 'Back\SendConfirmation\Back_Confirmation@StoreNew');
      Route::get('list', 'Back\SendConfirmation\Back_Confirmation@IndexList')->name('list.resi');
      Route::options('list', 'Back\SendConfirmation\Back_Confirmation@DataTableSSP');
    });
    Route::group(['prefix' => 'retur'], function () {
      Route::get('pengajuankomplain', 'Back\Transaction\Back_Retur@IndexPengajuan');
      Route::options('pengajuankomplain', 'Back\Transaction\Back_Retur@DataTableSSPPengajuan');
      Route::get('acckomplain', 'Back\Transaction\Back_Retur@AccKomplain')->name('acc.komplain');
      Route::get('tolakkomplain', 'Back\Transaction\Back_Retur@TolakKomplain')->name('destroy.komplain');
      Route::get('pengajuanretur', 'Back\Transaction\Back_Retur@IndexPengajuanRetur');
      Route::options('pengajuanretur', 'Back\Transaction\Back_Retur@DataTableSSPPengajuanRetur');

      Route::get('entry/{id}', 'Back\Transaction\Back_Retur@IndexEntry')->name('entry.retur');
      Route::post('entry/{id}', 'Back\Transaction\Back_Retur@PostEntry');
      Route::get('list', 'Back\Transaction\Back_Retur@IndexList')->name('list.retur');
      Route::options('list', 'Back\Transaction\Back_Retur@DataTableSSP');
      Route::options('entry/invoice', 'Back\Transaction\Back_Retur@DataTableSSPSearchInvoice');
      Route::options('entry/barang', 'Back\Transaction\Back_Retur@DataTableSSPLoadBarang');
      Route::get('destroy', 'Back\Transaction\Back_Retur@Destroy')->name('destroy.retur');
    });
  });

  Route::group(['prefix' => 'master', 'middleware' => ['auth:web_admin']], function () {

    Route::group(['prefix' => 'department'], function () {
      Route::get('entry', 'Back\Master\Back_Department@IndexEntry')->name('entry.department');
      Route::post('entry', 'Back\Master\Back_Department@Post');
      Route::get('list', 'Back\Master\Back_Department@IndexList')->name('list.department');
      Route::options('list', 'Back\Master\Back_Department@DataTableSSP');
      Route::get('destroy', 'Back\Master\Back_Department@Destroy')->name('destroy.department');
    });
    Route::group(['prefix' => 'bank'], function () {
      Route::get('entry', 'Back\Master\Back_Bank@IndexEntry')->name('entry.bank');
      Route::post('entry', 'Back\Master\Back_Bank@Post');
      Route::get('list', 'Back\Master\Back_Bank@IndexList')->name('list.bank');
      Route::options('list', 'Back\Master\Back_Bank@DataTableSSP');
      Route::get('destroy', 'Back\Master\Back_Bank@Destroy')->name('destroy.bank');
    });
    Route::group(['prefix' => 'modul'], function () {
      Route::get('entry', 'Back\Master\Back_Modul@IndexEntry')->name('entry.modul');
      Route::post('entry', 'Back\Master\Back_Modul@Post');
      Route::get('list', 'Back\Master\Back_Modul@IndexList')->name('list.modul');
      Route::options('list', 'Back\Master\Back_Modul@DataTableSSP');
      Route::get('destroy', 'Back\Master\Back_Modul@Destroy')->name('destroy.modul');
    });

    Route::group(['prefix' => 'users'], function () {
      Route::get('entry', 'Back\Master\Back_Users@IndexEntry')->name('entry.users');
      Route::post('entry', 'Back\Master\Back_Users@Post');
      Route::get('list', 'Back\Master\Back_Users@IndexList')->name('list.users');
      Route::options('list', 'Back\Master\Back_Users@DataTableSSP');
      Route::get('destroy', 'Back\Master\Back_Users@Destroy')->name('destroy.users');
    });

    Route::group(['prefix' => 'barang'], function () {
      Route::group(['prefix' => 'category'], function () {
        Route::get('entry', 'Back\Master\Back_Barang_Category@IndexEntry')->name('entry.barang_category');
        Route::post('entry', 'Back\Master\Back_Barang_Category@Post');
        Route::get('list', 'Back\Master\Back_Barang_Category@IndexList')->name('list.barang_category');
        Route::options('list', 'Back\Master\Back_Barang_Category@DataTableSSP');
        Route::get('destroy', 'Back\Master\Back_Barang_Category@Destroy')->name('destroy.barang_category');
      });
      Route::group(['prefix' => 'register'], function () {
        Route::get('entry', 'Back\Master\Back_Barang_Register@IndexEntry')->name('entry.barang_register');
        Route::post('entry', 'Back\Master\Back_Barang_Register@Post');
        Route::get('list', 'Back\Master\Back_Barang_Register@IndexList')->name('list.barang_register');
        Route::options('list', 'Back\Master\Back_Barang_Register@DataTableSSP');
        Route::get('destroy', 'Back\Master\Back_Barang_Register@Destroy')->name('destroy.barang_register');
      });
      Route::group(['prefix' => 'discount'], function () {
        Route::get('entry', 'Back\Master\Back_Discount@IndexEntry')->name('entry.discount');
        Route::post('entry', 'Back\Master\Back_Discount@Post');
        Route::get('list', 'Back\Master\Back_Discount@IndexList')->name('list.discount');
        Route::options('list', 'Back\Master\Back_Discount@DataTableSSP');
        Route::get('destroy', 'Back\Master\Back_Discount@Destroy')->name('destroy.discount');
      });
      Route::group(['prefix' => 'gallery'], function () {
        Route::get('entry', 'Back\Master\Back_Gallery@IndexEntry')->name('entry.gallery');
        Route::post('entry', 'Back\Master\Back_Gallery@Post');
        Route::get('list', 'Back\Master\Back_Gallery@IndexList')->name('list.gallery');
        Route::options('list', 'Back\Master\Back_Gallery@DataTableSSP');
        Route::get('destroy', 'Back\Master\Back_Gallery@Destroy')->name('destroy.gallery');
      });
    });

    Route::group(['prefix' => 'lokasi'], function () {
      Route::group(['prefix' => 'provinsi'], function () {
        Route::get('entry', 'Back\Master\Back_Lokasi_Provinsi@IndexEntry')->name('entry.lokasi_provinsi');
        Route::post('entry', 'Back\Master\Back_Lokasi_Provinsi@Post');
        Route::get('list', 'Back\Master\Back_Lokasi_Provinsi@IndexList')->name('list.lokasi_provinsi');
        Route::options('list', 'Back\Master\Back_Lokasi_Provinsi@DataTableSSP');
        Route::get('destroy', 'Back\Master\Back_Lokasi_Provinsi@Destroy')->name('destroy.lokasi_provinsi');
      });
      Route::group(['prefix' => 'kota'], function () {
        Route::get('entry', 'Back\Master\Back_Lokasi_Kota@IndexEntry')->name('entry.lokasi_kota');
        Route::post('entry', 'Back\Master\Back_Lokasi_Kota@Post');
        Route::get('list', 'Back\Master\Back_Lokasi_Kota@IndexList')->name('list.lokasi_kota');
        Route::options('list', 'Back\Master\Back_Lokasi_Kota@DataTableSSP');
        Route::get('destroy', 'Back\Master\Back_Lokasi_Kota@Destroy')->name('destroy.lokasi_kota');
      });
      Route::group(['prefix' => 'kecamatan'], function () {
        Route::get('entry', 'Back\Master\Back_Lokasi_Kecamatan@IndexEntry')->name('entry.lokasi_kecamatan');
        Route::post('entry', 'Back\Master\Back_Lokasi_Kecamatan@Post');
        Route::get('list', 'Back\Master\Back_Lokasi_Kecamatan@IndexList')->name('list.lokasi_kecamatan');
        Route::options('list', 'Back\Master\Back_Lokasi_Kecamatan@DataTableSSP');
        Route::get('destroy', 'Back\Master\Back_Lokasi_Kecamatan@Destroy')->name('destroy.lokasi_kecamatan');
      });
    });
  });

});
